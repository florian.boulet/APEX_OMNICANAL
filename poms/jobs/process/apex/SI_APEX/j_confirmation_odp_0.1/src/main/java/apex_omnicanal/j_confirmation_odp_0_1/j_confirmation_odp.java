
package apex_omnicanal.j_confirmation_odp_0_1;

import routines.Numeric;
import routines.DataOperation;
import routines.TalendDataGenerator;
import routines.TalendStringUtil;
import routines.TalendString;
import routines.StringHandling;
import routines.Relational;
import routines.TalendDate;
import routines.Mathematical;
import routines.SQLike;
import routines.system.*;
import routines.system.api.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.Comparator;

//the import part of tJava_2
//import java.util.List;

//the import part of tJavaRow_1
//import java.util.List;

//the import part of tJava_3
//import java.util.List;

//the import part of j_end_exec_1_tJava_1
//import java.util.List;

//the import part of j_end_exec_1_tJava_2
//import java.util.List;

//the import part of tJava_1
//import java.util.List;

//the import part of j_init_exec_1_tJava_1
import java.util.UUID;

//the import part of j_init_exec_1_tJavaRow_1
//import java.util.List;

@SuppressWarnings("unused")

/**
 * Job: j_confirmation_odp Purpose: <br>
 * Description: <br>
 * 
 * @author user@talend.com
 * @version 8.0.1.20211109_1610
 * @status
 */
public class j_confirmation_odp implements TalendJob {
	static {
		System.setProperty("TalendJob.log", "j_confirmation_odp.log");
	}

	private static org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager
			.getLogger(j_confirmation_odp.class);

	protected static void logIgnoredError(String message, Throwable cause) {
		log.error(message, cause);

	}

	public final Object obj = new Object();

	// for transmiting parameters purpose
	private Object valueObject = null;

	public Object getValueObject() {
		return this.valueObject;
	}

	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}

	private final static String defaultCharset = java.nio.charset.Charset.defaultCharset().name();

	private final static String utf8Charset = "UTF-8";

	// contains type for every context property
	public class PropertiesWithType extends java.util.Properties {
		private static final long serialVersionUID = 1L;
		private java.util.Map<String, String> propertyTypes = new java.util.HashMap<>();

		public PropertiesWithType(java.util.Properties properties) {
			super(properties);
		}

		public PropertiesWithType() {
			super();
		}

		public void setContextType(String key, String type) {
			propertyTypes.put(key, type);
		}

		public String getContextType(String key) {
			return propertyTypes.get(key);
		}
	}

	// create and load default properties
	private java.util.Properties defaultProps = new java.util.Properties();

	// create application properties with default
	public class ContextProperties extends PropertiesWithType {

		private static final long serialVersionUID = 1L;

		public ContextProperties(java.util.Properties properties) {
			super(properties);
		}

		public ContextProperties() {
			super();
		}

		public void synchronizeContext() {

			if (l_FTP_FILE_MASK != null) {

				this.setProperty("l_FTP_FILE_MASK", l_FTP_FILE_MASK.toString());

			}

			if (l_FTP_INPUT_PATH != null) {

				this.setProperty("l_FTP_INPUT_PATH", l_FTP_INPUT_PATH.toString());

			}

			if (l_FTP_OUPUT_PATH != null) {

				this.setProperty("l_FTP_OUPUT_PATH", l_FTP_OUPUT_PATH.toString());

			}

			if (g_ALERTING_BCC_EMAIL != null) {

				this.setProperty("g_ALERTING_BCC_EMAIL", g_ALERTING_BCC_EMAIL.toString());

			}

			if (g_ALERTING_CC_EMAIL != null) {

				this.setProperty("g_ALERTING_CC_EMAIL", g_ALERTING_CC_EMAIL.toString());

			}

			if (g_ALERTING_FROM_EMAIL != null) {

				this.setProperty("g_ALERTING_FROM_EMAIL", g_ALERTING_FROM_EMAIL.toString());

			}

			if (g_ALERTING_OBJECT != null) {

				this.setProperty("g_ALERTING_OBJECT", g_ALERTING_OBJECT.toString());

			}

			if (g_ALERTING_SMTP_HOST != null) {

				this.setProperty("g_ALERTING_SMTP_HOST", g_ALERTING_SMTP_HOST.toString());

			}

			if (g_ALERTING_SMTP_PORT != null) {

				this.setProperty("g_ALERTING_SMTP_PORT", g_ALERTING_SMTP_PORT.toString());

			}

			if (g_ALERTING_SMTP_PWD != null) {

				this.setProperty("g_ALERTING_SMTP_PWD", g_ALERTING_SMTP_PWD.toString());

			}

			if (g_ALERTING_SMTP_USERNAME != null) {

				this.setProperty("g_ALERTING_SMTP_USERNAME", g_ALERTING_SMTP_USERNAME.toString());

			}

			if (g_ALERTING_TARGET_EMAIL != null) {

				this.setProperty("g_ALERTING_TARGET_EMAIL", g_ALERTING_TARGET_EMAIL.toString());

			}

			if (g_BDD_OMNICANAL_DBNAME != null) {

				this.setProperty("g_BDD_OMNICANAL_DBNAME", g_BDD_OMNICANAL_DBNAME.toString());

			}

			if (g_BDD_OMNICANAL_HOST != null) {

				this.setProperty("g_BDD_OMNICANAL_HOST", g_BDD_OMNICANAL_HOST.toString());

			}

			if (g_BDD_OMNICANAL_PORT != null) {

				this.setProperty("g_BDD_OMNICANAL_PORT", g_BDD_OMNICANAL_PORT.toString());

			}

			if (g_BDD_OMNICANAL_PWD != null) {

				this.setProperty("g_BDD_OMNICANAL_PWD", g_BDD_OMNICANAL_PWD.toString());

			}

			if (g_BDD_OMNICANAL_SCHEMA != null) {

				this.setProperty("g_BDD_OMNICANAL_SCHEMA", g_BDD_OMNICANAL_SCHEMA.toString());

			}

			if (g_BDD_OMNICANAL_USERNAME != null) {

				this.setProperty("g_BDD_OMNICANAL_USERNAME", g_BDD_OMNICANAL_USERNAME.toString());

			}

			if (g_FTP_GCE_HOST != null) {

				this.setProperty("g_FTP_GCE_HOST", g_FTP_GCE_HOST.toString());

			}

			if (g_FTP_GCE_PATH != null) {

				this.setProperty("g_FTP_GCE_PATH", g_FTP_GCE_PATH.toString());

			}

			if (g_FTP_GCE_PORT != null) {

				this.setProperty("g_FTP_GCE_PORT", g_FTP_GCE_PORT.toString());

			}

			if (g_FTP_GCE_PWD != null) {

				this.setProperty("g_FTP_GCE_PWD", g_FTP_GCE_PWD.toString());

			}

			if (g_FTP_GCE_USER != null) {

				this.setProperty("g_FTP_GCE_USER", g_FTP_GCE_USER.toString());

			}

			if (g_FTP_ONESTOCK_HOST != null) {

				this.setProperty("g_FTP_ONESTOCK_HOST", g_FTP_ONESTOCK_HOST.toString());

			}

			if (g_FTP_ONESTOCK_PATH != null) {

				this.setProperty("g_FTP_ONESTOCK_PATH", g_FTP_ONESTOCK_PATH.toString());

			}

			if (g_FTP_ONESTOCK_PORT != null) {

				this.setProperty("g_FTP_ONESTOCK_PORT", g_FTP_ONESTOCK_PORT.toString());

			}

			if (g_FTP_ONESTOCK_PWD != null) {

				this.setProperty("g_FTP_ONESTOCK_PWD", g_FTP_ONESTOCK_PWD.toString());

			}

			if (g_FTP_ONESTOCK_USER != null) {

				this.setProperty("g_FTP_ONESTOCK_USER", g_FTP_ONESTOCK_USER.toString());

			}

			if (g_SFTP_MAGENTO_HOST != null) {

				this.setProperty("g_SFTP_MAGENTO_HOST", g_SFTP_MAGENTO_HOST.toString());

			}

			if (g_SFTP_MAGENTO_LOGIN != null) {

				this.setProperty("g_SFTP_MAGENTO_LOGIN", g_SFTP_MAGENTO_LOGIN.toString());

			}

			if (g_SFTP_MAGENTO_PASSWORD != null) {

				this.setProperty("g_SFTP_MAGENTO_PASSWORD", g_SFTP_MAGENTO_PASSWORD.toString());

			}

			if (g_SFTP_MAGENTO_PATH != null) {

				this.setProperty("g_SFTP_MAGENTO_PATH", g_SFTP_MAGENTO_PATH.toString());

			}

			if (g_SFTP_MAGENTO_PORT != null) {

				this.setProperty("g_SFTP_MAGENTO_PORT", g_SFTP_MAGENTO_PORT.toString());

			}

			if (g_SFTP_ONESTOCK_HOST != null) {

				this.setProperty("g_SFTP_ONESTOCK_HOST", g_SFTP_ONESTOCK_HOST.toString());

			}

			if (g_SFTP_ONESTOCK_LOGIN != null) {

				this.setProperty("g_SFTP_ONESTOCK_LOGIN", g_SFTP_ONESTOCK_LOGIN.toString());

			}

			if (g_SFTP_ONESTOCK_PASSWORD != null) {

				this.setProperty("g_SFTP_ONESTOCK_PASSWORD", g_SFTP_ONESTOCK_PASSWORD.toString());

			}

			if (g_SFTP_ONESTOCK_PATH != null) {

				this.setProperty("g_SFTP_ONESTOCK_PATH", g_SFTP_ONESTOCK_PATH.toString());

			}

			if (g_SFTP_ONESTOCK_PORT != null) {

				this.setProperty("g_SFTP_ONESTOCK_PORT", g_SFTP_ONESTOCK_PORT.toString());

			}

			if (g_SFTP_TALEND_HOST != null) {

				this.setProperty("g_SFTP_TALEND_HOST", g_SFTP_TALEND_HOST.toString());

			}

			if (g_SFTP_TALEND_LOGIN != null) {

				this.setProperty("g_SFTP_TALEND_LOGIN", g_SFTP_TALEND_LOGIN.toString());

			}

			if (g_SFTP_TALEND_PASSWORD != null) {

				this.setProperty("g_SFTP_TALEND_PASSWORD", g_SFTP_TALEND_PASSWORD.toString());

			}

			if (g_SFTP_TALEND_PATH != null) {

				this.setProperty("g_SFTP_TALEND_PATH", g_SFTP_TALEND_PATH.toString());

			}

			if (g_SFTP_TALEND_PORT != null) {

				this.setProperty("g_SFTP_TALEND_PORT", g_SFTP_TALEND_PORT.toString());

			}

			if (g_VAR_TALEND_WORKSPACE != null) {

				this.setProperty("g_VAR_TALEND_WORKSPACE", g_VAR_TALEND_WORKSPACE.toString());

			}

		}

		// if the stored or passed value is "<TALEND_NULL>" string, it mean null
		public String getStringValue(String key) {
			String origin_value = this.getProperty(key);
			if (NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY.equals(origin_value)) {
				return null;
			}
			return origin_value;
		}

		public String l_FTP_FILE_MASK;

		public String getL_FTP_FILE_MASK() {
			return this.l_FTP_FILE_MASK;
		}

		public String l_FTP_INPUT_PATH;

		public String getL_FTP_INPUT_PATH() {
			return this.l_FTP_INPUT_PATH;
		}

		public String l_FTP_OUPUT_PATH;

		public String getL_FTP_OUPUT_PATH() {
			return this.l_FTP_OUPUT_PATH;
		}

		public String g_ALERTING_BCC_EMAIL;

		public String getG_ALERTING_BCC_EMAIL() {
			return this.g_ALERTING_BCC_EMAIL;
		}

		public String g_ALERTING_CC_EMAIL;

		public String getG_ALERTING_CC_EMAIL() {
			return this.g_ALERTING_CC_EMAIL;
		}

		public String g_ALERTING_FROM_EMAIL;

		public String getG_ALERTING_FROM_EMAIL() {
			return this.g_ALERTING_FROM_EMAIL;
		}

		public String g_ALERTING_OBJECT;

		public String getG_ALERTING_OBJECT() {
			return this.g_ALERTING_OBJECT;
		}

		public String g_ALERTING_SMTP_HOST;

		public String getG_ALERTING_SMTP_HOST() {
			return this.g_ALERTING_SMTP_HOST;
		}

		public Integer g_ALERTING_SMTP_PORT;

		public Integer getG_ALERTING_SMTP_PORT() {
			return this.g_ALERTING_SMTP_PORT;
		}

		public java.lang.String g_ALERTING_SMTP_PWD;

		public java.lang.String getG_ALERTING_SMTP_PWD() {
			return this.g_ALERTING_SMTP_PWD;
		}

		public String g_ALERTING_SMTP_USERNAME;

		public String getG_ALERTING_SMTP_USERNAME() {
			return this.g_ALERTING_SMTP_USERNAME;
		}

		public String g_ALERTING_TARGET_EMAIL;

		public String getG_ALERTING_TARGET_EMAIL() {
			return this.g_ALERTING_TARGET_EMAIL;
		}

		public String g_BDD_OMNICANAL_DBNAME;

		public String getG_BDD_OMNICANAL_DBNAME() {
			return this.g_BDD_OMNICANAL_DBNAME;
		}

		public String g_BDD_OMNICANAL_HOST;

		public String getG_BDD_OMNICANAL_HOST() {
			return this.g_BDD_OMNICANAL_HOST;
		}

		public String g_BDD_OMNICANAL_PORT;

		public String getG_BDD_OMNICANAL_PORT() {
			return this.g_BDD_OMNICANAL_PORT;
		}

		public java.lang.String g_BDD_OMNICANAL_PWD;

		public java.lang.String getG_BDD_OMNICANAL_PWD() {
			return this.g_BDD_OMNICANAL_PWD;
		}

		public String g_BDD_OMNICANAL_SCHEMA;

		public String getG_BDD_OMNICANAL_SCHEMA() {
			return this.g_BDD_OMNICANAL_SCHEMA;
		}

		public String g_BDD_OMNICANAL_USERNAME;

		public String getG_BDD_OMNICANAL_USERNAME() {
			return this.g_BDD_OMNICANAL_USERNAME;
		}

		public String g_FTP_GCE_HOST;

		public String getG_FTP_GCE_HOST() {
			return this.g_FTP_GCE_HOST;
		}

		public String g_FTP_GCE_PATH;

		public String getG_FTP_GCE_PATH() {
			return this.g_FTP_GCE_PATH;
		}

		public Integer g_FTP_GCE_PORT;

		public Integer getG_FTP_GCE_PORT() {
			return this.g_FTP_GCE_PORT;
		}

		public String g_FTP_GCE_PWD;

		public String getG_FTP_GCE_PWD() {
			return this.g_FTP_GCE_PWD;
		}

		public String g_FTP_GCE_USER;

		public String getG_FTP_GCE_USER() {
			return this.g_FTP_GCE_USER;
		}

		public String g_FTP_ONESTOCK_HOST;

		public String getG_FTP_ONESTOCK_HOST() {
			return this.g_FTP_ONESTOCK_HOST;
		}

		public String g_FTP_ONESTOCK_PATH;

		public String getG_FTP_ONESTOCK_PATH() {
			return this.g_FTP_ONESTOCK_PATH;
		}

		public Integer g_FTP_ONESTOCK_PORT;

		public Integer getG_FTP_ONESTOCK_PORT() {
			return this.g_FTP_ONESTOCK_PORT;
		}

		public String g_FTP_ONESTOCK_PWD;

		public String getG_FTP_ONESTOCK_PWD() {
			return this.g_FTP_ONESTOCK_PWD;
		}

		public String g_FTP_ONESTOCK_USER;

		public String getG_FTP_ONESTOCK_USER() {
			return this.g_FTP_ONESTOCK_USER;
		}

		public String g_SFTP_MAGENTO_HOST;

		public String getG_SFTP_MAGENTO_HOST() {
			return this.g_SFTP_MAGENTO_HOST;
		}

		public String g_SFTP_MAGENTO_LOGIN;

		public String getG_SFTP_MAGENTO_LOGIN() {
			return this.g_SFTP_MAGENTO_LOGIN;
		}

		public String g_SFTP_MAGENTO_PASSWORD;

		public String getG_SFTP_MAGENTO_PASSWORD() {
			return this.g_SFTP_MAGENTO_PASSWORD;
		}

		public String g_SFTP_MAGENTO_PATH;

		public String getG_SFTP_MAGENTO_PATH() {
			return this.g_SFTP_MAGENTO_PATH;
		}

		public String g_SFTP_MAGENTO_PORT;

		public String getG_SFTP_MAGENTO_PORT() {
			return this.g_SFTP_MAGENTO_PORT;
		}

		public String g_SFTP_ONESTOCK_HOST;

		public String getG_SFTP_ONESTOCK_HOST() {
			return this.g_SFTP_ONESTOCK_HOST;
		}

		public String g_SFTP_ONESTOCK_LOGIN;

		public String getG_SFTP_ONESTOCK_LOGIN() {
			return this.g_SFTP_ONESTOCK_LOGIN;
		}

		public String g_SFTP_ONESTOCK_PASSWORD;

		public String getG_SFTP_ONESTOCK_PASSWORD() {
			return this.g_SFTP_ONESTOCK_PASSWORD;
		}

		public String g_SFTP_ONESTOCK_PATH;

		public String getG_SFTP_ONESTOCK_PATH() {
			return this.g_SFTP_ONESTOCK_PATH;
		}

		public String g_SFTP_ONESTOCK_PORT;

		public String getG_SFTP_ONESTOCK_PORT() {
			return this.g_SFTP_ONESTOCK_PORT;
		}

		public String g_SFTP_TALEND_HOST;

		public String getG_SFTP_TALEND_HOST() {
			return this.g_SFTP_TALEND_HOST;
		}

		public String g_SFTP_TALEND_LOGIN;

		public String getG_SFTP_TALEND_LOGIN() {
			return this.g_SFTP_TALEND_LOGIN;
		}

		public String g_SFTP_TALEND_PASSWORD;

		public String getG_SFTP_TALEND_PASSWORD() {
			return this.g_SFTP_TALEND_PASSWORD;
		}

		public String g_SFTP_TALEND_PATH;

		public String getG_SFTP_TALEND_PATH() {
			return this.g_SFTP_TALEND_PATH;
		}

		public String g_SFTP_TALEND_PORT;

		public String getG_SFTP_TALEND_PORT() {
			return this.g_SFTP_TALEND_PORT;
		}

		public String g_VAR_TALEND_WORKSPACE;

		public String getG_VAR_TALEND_WORKSPACE() {
			return this.g_VAR_TALEND_WORKSPACE;
		}
	}

	protected ContextProperties context = new ContextProperties(); // will be instanciated by MS.

	public ContextProperties getContext() {
		return this.context;
	}

	private final String jobVersion = "0.1";
	private final String jobName = "j_confirmation_odp";
	private final String projectName = "APEX_OMNICANAL";
	public Integer errorCode = null;
	private String currentComponent = "";

	private final java.util.Map<String, Object> globalMap = new java.util.HashMap<String, Object>();
	private final static java.util.Map<String, Object> junitGlobalMap = new java.util.HashMap<String, Object>();

	private final java.util.Map<String, Long> start_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Long> end_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Boolean> ok_Hash = new java.util.HashMap<String, Boolean>();
	public final java.util.List<String[]> globalBuffer = new java.util.ArrayList<String[]>();

	private final JobStructureCatcherUtils talendJobLog = new JobStructureCatcherUtils(jobName,
			"_0nJD4P4bEe6rO7eA9VTFYw", "0.1");
	private org.talend.job.audit.JobAuditLogger auditLogger_talendJobLog = null;

	private RunStat runStat = new RunStat(talendJobLog, System.getProperty("audit.interval"));

	// OSGi DataSource
	private final static String KEY_DB_DATASOURCES = "KEY_DB_DATASOURCES";

	private final static String KEY_DB_DATASOURCES_RAW = "KEY_DB_DATASOURCES_RAW";

	public void setDataSources(java.util.Map<String, javax.sql.DataSource> dataSources) {
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		for (java.util.Map.Entry<String, javax.sql.DataSource> dataSourceEntry : dataSources.entrySet()) {
			talendDataSources.put(dataSourceEntry.getKey(),
					new routines.system.TalendDataSource(dataSourceEntry.getValue()));
		}
		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap.put(KEY_DB_DATASOURCES_RAW, new java.util.HashMap<String, javax.sql.DataSource>(dataSources));
	}

	public void setDataSourceReferences(List serviceReferences) throws Exception {

		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		java.util.Map<String, javax.sql.DataSource> dataSources = new java.util.HashMap<String, javax.sql.DataSource>();

		for (java.util.Map.Entry<String, javax.sql.DataSource> entry : BundleUtils
				.getServices(serviceReferences, javax.sql.DataSource.class).entrySet()) {
			dataSources.put(entry.getKey(), entry.getValue());
			talendDataSources.put(entry.getKey(), new routines.system.TalendDataSource(entry.getValue()));
		}

		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap.put(KEY_DB_DATASOURCES_RAW, new java.util.HashMap<String, javax.sql.DataSource>(dataSources));
	}

	LogCatcherUtils tLogCatcher_1 = new LogCatcherUtils();
	LogCatcherUtils j_end_exec_1_tLogCatcher_1 = new LogCatcherUtils();

	private final java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
	private final java.io.PrintStream errorMessagePS = new java.io.PrintStream(new java.io.BufferedOutputStream(baos));

	public String getExceptionStackTrace() {
		if ("failure".equals(this.getStatus())) {
			errorMessagePS.flush();
			return baos.toString();
		}
		return null;
	}

	private Exception exception;

	public Exception getException() {
		if ("failure".equals(this.getStatus())) {
			return this.exception;
		}
		return null;
	}

	private class TalendException extends Exception {

		private static final long serialVersionUID = 1L;

		private java.util.Map<String, Object> globalMap = null;
		private Exception e = null;
		private String currentComponent = null;
		private String virtualComponentName = null;

		public void setVirtualComponentName(String virtualComponentName) {
			this.virtualComponentName = virtualComponentName;
		}

		private TalendException(Exception e, String errorComponent, final java.util.Map<String, Object> globalMap) {
			this.currentComponent = errorComponent;
			this.globalMap = globalMap;
			this.e = e;
		}

		public Exception getException() {
			return this.e;
		}

		public String getCurrentComponent() {
			return this.currentComponent;
		}

		public String getExceptionCauseMessage(Exception e) {
			Throwable cause = e;
			String message = null;
			int i = 10;
			while (null != cause && 0 < i--) {
				message = cause.getMessage();
				if (null == message) {
					cause = cause.getCause();
				} else {
					break;
				}
			}
			if (null == message) {
				message = e.getClass().getName();
			}
			return message;
		}

		@Override
		public void printStackTrace() {
			if (!(e instanceof TalendException || e instanceof TDieException)) {
				if (virtualComponentName != null && currentComponent.indexOf(virtualComponentName + "_") == 0) {
					globalMap.put(virtualComponentName + "_ERROR_MESSAGE", getExceptionCauseMessage(e));
				}
				globalMap.put(currentComponent + "_ERROR_MESSAGE", getExceptionCauseMessage(e));
				System.err.println("Exception in component " + currentComponent + " (" + jobName + ")");
			}
			if (!(e instanceof TDieException)) {
				if (e instanceof TalendException) {
					e.printStackTrace();
				} else {
					e.printStackTrace();
					e.printStackTrace(errorMessagePS);
					j_confirmation_odp.this.exception = e;
				}
			}
			if (!(e instanceof TalendException)) {
				try {
					for (java.lang.reflect.Method m : this.getClass().getEnclosingClass().getMethods()) {
						if (m.getName().compareTo(currentComponent + "_error") == 0) {
							m.invoke(j_confirmation_odp.this, new Object[] { e, currentComponent, globalMap });
							break;
						}
					}

					if (!(e instanceof TDieException)) {
						tLogCatcher_1.addMessage("Java Exception", currentComponent, 6,
								e.getClass().getName() + ":" + e.getMessage(), 1);
						j_end_exec_1_tLogCatcher_1.addMessage("Java Exception", currentComponent, 6,
								e.getClass().getName() + ":" + e.getMessage(), 1);
						tLogCatcher_1Process(globalMap);
						j_end_exec_1_tLogCatcher_1Process(globalMap);
					}
				} catch (TalendException e) {
					// do nothing

				} catch (Exception e) {
					this.e.printStackTrace();
				}
			}
		}
	}

	public void tJava_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tJava_2_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFTPGet_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFTPGet_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFileList_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFileList_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFixedFlowInput_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFileList_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tJavaRow_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFileList_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBOutput_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFileList_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFileInputDelimited_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		try {

			if (this.execStat) {
				runStat.updateStatOnConnection("OnComponentError2", 0, "error");
			}

			errorCode = null;
			tDie_2Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		tFileInputDelimited_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tMap_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFileInputDelimited_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFileOutputDelimited_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		try {

			if (this.execStat) {
				runStat.updateStatOnConnection("OnComponentError3", 0, "error");
			}

			errorCode = null;
			tDie_3Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		tFileInputDelimited_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFTPPut_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		try {

			if (this.execStat) {
				runStat.updateStatOnConnection("OnComponentError1", 0, "error");
			}

			errorCode = null;
			tDie_1Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		tFTPPut_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDie_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDie_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDie_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDie_3_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDie_4_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFileInputDelimited_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDie_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDie_2_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tLogCatcher_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tLogCatcher_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_catch_unexpected_error_1_tMap_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tLogCatcher_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_catch_unexpected_error_1_tHashOutput_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tLogCatcher_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_catch_unexpected_error_1_tDBConnection_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_catch_unexpected_error_1_tDBConnection_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_catch_unexpected_error_1_tHashInput_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_catch_unexpected_error_1_tHashInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_catch_unexpected_error_1_tDBOutput_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_catch_unexpected_error_1_tHashInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_catch_unexpected_error_1_tDBInput_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_catch_unexpected_error_1_tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_catch_unexpected_error_1_tMap_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_catch_unexpected_error_1_tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_catch_unexpected_error_1_tDBOutput_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_catch_unexpected_error_1_tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tPostjob_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tPostjob_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tJava_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tJava_3_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tDBConnection_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tDBConnection_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tDBInput_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tFlowToIterate_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tDBInput_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tFlowToIterate_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tJava_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tFileExist_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tFileExist_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tFTPPut_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tFTPPut_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tDBInput_3_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tDBInput_3_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tMap_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tDBInput_3_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tDBOutput_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tDBInput_3_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tDBClose_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tDBClose_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBClose_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBClose_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFTPClose_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFTPClose_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFTPClose_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFTPClose_2_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tJava_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tJava_2_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tLogCatcher_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tLogCatcher_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tMap_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tLogCatcher_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tDBOutput_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tLogCatcher_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_end_exec_1_tDBInput_4_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tLogCatcher_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tPrejob_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tPrejob_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tJava_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tJava_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_init_exec_1_tJava_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_init_exec_1_tJava_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_init_exec_1_tDBConnection_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_init_exec_1_tDBConnection_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_init_exec_1_tDBInput_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_init_exec_1_tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_init_exec_1_tUniqRow_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_init_exec_1_tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_init_exec_1_tJavaRow_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_init_exec_1_tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_init_exec_1_tFixedFlowInput_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_init_exec_1_tFixedFlowInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_init_exec_1_tDBOutput_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_init_exec_1_tFixedFlowInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void j_init_exec_1_tDBClose_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_init_exec_1_tDBClose_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBConnection_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBConnection_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFTPConnection_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFTPConnection_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFTPConnection_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFTPConnection_2_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_j_end_exec_1_row6_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		j_end_exec_1_tLogCatcher_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void talendJobLog_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		talendJobLog_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tJava_2_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tFTPGet_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tFileList_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tFileInputDelimited_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tFTPPut_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDie_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDie_3_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDie_2_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tLogCatcher_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_catch_unexpected_error_1_tDBConnection_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_catch_unexpected_error_1_tHashInput_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_catch_unexpected_error_1_tDBInput_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tPostjob_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tJava_3_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_end_exec_1_tDBConnection_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_end_exec_1_tDBInput_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_end_exec_1_tDBInput_2_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_end_exec_1_tFileExist_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_end_exec_1_tFTPPut_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_end_exec_1_tDBInput_3_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_end_exec_1_tDBClose_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBClose_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tFTPClose_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tFTPClose_2_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_end_exec_1_tJava_2_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_end_exec_1_tLogCatcher_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tPrejob_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tJava_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_init_exec_1_tJava_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_init_exec_1_tDBConnection_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_init_exec_1_tDBInput_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_init_exec_1_tFixedFlowInput_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void j_init_exec_1_tDBClose_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBConnection_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tFTPConnection_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tFTPConnection_2_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void talendJobLog_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tJava_2Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tJava_2_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tJava_2 begin ] start
				 */

				ok_Hash.put("tJava_2", false);
				start_Hash.put("tJava_2", System.currentTimeMillis());

				currentComponent = "tJava_2";

				int tos_count_tJava_2 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("tJava_2", "__UNIQUE_NAME__<br><b>Init_traitement</b>", "tJava");
					talendJobLogProcess(globalMap);
				}

				System.out.println("Début du traitement : " + jobName);
				System.out.println("-----------------------");

				/**
				 * [tJava_2 begin ] stop
				 */

				/**
				 * [tJava_2 main ] start
				 */

				currentComponent = "tJava_2";

				tos_count_tJava_2++;

				/**
				 * [tJava_2 main ] stop
				 */

				/**
				 * [tJava_2 process_data_begin ] start
				 */

				currentComponent = "tJava_2";

				/**
				 * [tJava_2 process_data_begin ] stop
				 */

				/**
				 * [tJava_2 process_data_end ] start
				 */

				currentComponent = "tJava_2";

				/**
				 * [tJava_2 process_data_end ] stop
				 */

				/**
				 * [tJava_2 end ] start
				 */

				currentComponent = "tJava_2";

				ok_Hash.put("tJava_2", true);
				end_Hash.put("tJava_2", System.currentTimeMillis());

				if (!(Boolean) globalMap.get("isRunning")) {

					if (execStat) {
						runStat.updateStatOnConnection("If1", 0, "true");
					}
					tFTPGet_1Process(globalMap);
				}

				else {
					if (execStat) {
						runStat.updateStatOnConnection("If1", 0, "false");
					}
				}

				/**
				 * [tJava_2 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tJava_2 finally ] start
				 */

				currentComponent = "tJava_2";

				/**
				 * [tJava_2 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tJava_2_SUBPROCESS_STATE", 1);
	}

	public void tFTPGet_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tFTPGet_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tFTPGet_1 begin ] start
				 */

				ok_Hash.put("tFTPGet_1", false);
				start_Hash.put("tFTPGet_1", System.currentTimeMillis());

				currentComponent = "tFTPGet_1";

				int tos_count_tFTPGet_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tFTPGet_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tFTPGet_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tFTPGet_1 = new StringBuilder();
							log4jParamters_tFTPGet_1.append("Parameters:");
							log4jParamters_tFTPGet_1.append("USE_EXISTING_CONNECTION" + " = " + "true");
							log4jParamters_tFTPGet_1.append(" | ");
							log4jParamters_tFTPGet_1.append("CONNECTION" + " = " + "tFTPConnection_2");
							log4jParamters_tFTPGet_1.append(" | ");
							log4jParamters_tFTPGet_1
									.append("LOCALDIR" + " = " + " context.g_VAR_TALEND_WORKSPACE + jobName");
							log4jParamters_tFTPGet_1.append(" | ");
							log4jParamters_tFTPGet_1.append("REMOTEDIR" + " = " + "context.l_FTP_INPUT_PATH");
							log4jParamters_tFTPGet_1.append(" | ");
							log4jParamters_tFTPGet_1.append("MOVE_TO_THE_CURRENT_DIRECTORY" + " = " + "false");
							log4jParamters_tFTPGet_1.append(" | ");
							log4jParamters_tFTPGet_1.append("MODE" + " = " + "ascii");
							log4jParamters_tFTPGet_1.append(" | ");
							log4jParamters_tFTPGet_1.append("OVERWRITE" + " = " + "never");
							log4jParamters_tFTPGet_1.append(" | ");
							log4jParamters_tFTPGet_1.append("APPEND" + " = " + "false");
							log4jParamters_tFTPGet_1.append(" | ");
							log4jParamters_tFTPGet_1.append("PERL5_REGEX" + " = " + "false");
							log4jParamters_tFTPGet_1.append(" | ");
							log4jParamters_tFTPGet_1
									.append("FILES" + " = " + "[{FILEMASK=" + ("context.l_FTP_FILE_MASK") + "}]");
							log4jParamters_tFTPGet_1.append(" | ");
							log4jParamters_tFTPGet_1.append("DIE_ON_ERROR" + " = " + "true");
							log4jParamters_tFTPGet_1.append(" | ");
							log4jParamters_tFTPGet_1.append("IGNORE_FAILURE_AT_QUIT" + " = " + "false");
							log4jParamters_tFTPGet_1.append(" | ");
							log4jParamters_tFTPGet_1.append("PRINT_MESSAGE" + " = " + "false");
							log4jParamters_tFTPGet_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tFTPGet_1 - " + (log4jParamters_tFTPGet_1));
						}
					}
					new BytesLimit65535_tFTPGet_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tFTPGet_1", "__UNIQUE_NAME__<br><b>Récupération fichier</b>", "tFTPGet");
					talendJobLogProcess(globalMap);
				}

				int nb_file_tFTPGet_1 = 0;
				abstract class FTPGetter_tFTPGet_1 {
					protected org.apache.commons.net.ftp.FTPClient ftpClient = null;
					protected int count = 0;

					public void getAllFiles(String remoteDirectory, String localDirectory)
							throws IllegalStateException, IOException, java.io.FileNotFoundException {

						if (!chdir(remoteDirectory)) {
							// change dir fail
							log.error("Change dir fail. Skip Directory: " + remoteDirectory);
							return;
						}
						String path = ftpClient.printWorkingDirectory();
						org.apache.commons.net.ftp.FTPFile[] ftpFiles = null;
						ftpFiles = ftpClient.listFiles();
						String[] nameLists = null;
						try {
							nameLists = ftpClient.listNames();
						} catch (IOException e) {
							e.printStackTrace();
						}
						if (nameLists != null && (ftpFiles == null || ftpFiles.length == 0)) {
							// if the file is folder, catch the FTPException and recur
							for (String ftpFileName : nameLists) {
								try {
									downloadFile(localDirectory + "/" + ftpFileName, ftpFileName);
								} catch (IOException e) {

									log.warn("tFTPGet_1 - " + e.getMessage());

									java.io.File localFile = new java.io.File(localDirectory + "/" + ftpFileName);

									if (!localFile.exists()) {
										localFile.mkdir();
									}
									getAllFiles(path + "/" + ftpFileName, localDirectory + "/" + ftpFileName);
									chdir(remoteDirectory);
								}
							}
						} else {
							for (org.apache.commons.net.ftp.FTPFile ftpFile : ftpFiles) {

								if (ftpFile.isDirectory()) {

									if ((!(".").equals(ftpFile.getName())) && (!("..").equals(ftpFile.getName()))) {
										java.io.File localFile = new java.io.File(
												localDirectory + "/" + ftpFile.getName());

										if (!localFile.exists()) {
											localFile.mkdir();
										}
										getAllFiles(path + "/" + ftpFile.getName(),
												localDirectory + "/" + ftpFile.getName());
										chdir(path);
									}
								} else if (!ftpFile.isSymbolicLink()) {
									downloadFile(localDirectory + "/" + ftpFile.getName(), ftpFile.getName());
								}
							}
						}
					}

					public void getFiles(String remoteDirectory, String localDirectory, String maskStr)
							throws IllegalStateException, IOException, java.io.FileNotFoundException {

						chdir(remoteDirectory);
						String[] nameLists = ftpClient.listNames(".");
						if (nameLists != null) {
							for (String fileName : nameLists) {
								if (fileName.matches(maskStr)) {
									downloadFile(localDirectory + "/" + fileName, fileName);
								}
							}
						}
					}

					public boolean chdir(String path) throws IllegalStateException, IOException {
						if (!".".equals(path)) {
							return ftpClient.changeWorkingDirectory(path);
						}
						return true;
					}

					public String pwd() throws IllegalStateException, IOException {
						return ftpClient.printWorkingDirectory();
					}

					protected abstract void downloadFile(String localFileName, String remoteFileName)
							throws IllegalStateException, java.io.FileNotFoundException, IOException;
				}
				org.apache.commons.net.ftp.FTPClient ftp_tFTPGet_1 = null;
				ftp_tFTPGet_1 = (org.apache.commons.net.ftp.FTPClient) globalMap.get("conn_tFTPConnection_2");

				String rootDir_tFTPGet_1 = ftp_tFTPGet_1.printWorkingDirectory();

				if (ftp_tFTPGet_1 != null) {
					log.info("tFTPGet_1 - Use an existing connection. Connection hostname: "
							+ ftp_tFTPGet_1.getRemoteAddress().toString() + ", Connection port: "
							+ ftp_tFTPGet_1.getRemotePort() + ".");
				}
				ftp_tFTPGet_1.setFileType(org.apache.commons.net.ftp.FTP.ASCII_FILE_TYPE);
				final java.util.List<String> msg_tFTPGet_1 = new java.util.ArrayList<String>();
				FTPGetter_tFTPGet_1 getter_tFTPGet_1 = new FTPGetter_tFTPGet_1() {
					@Override
					protected void downloadFile(String localFileName, String remoteFileName)
							throws IllegalStateException, java.io.FileNotFoundException, IOException {
						java.io.File localFile = new java.io.File(localFileName);
						if (!localFile.exists()) {
							boolean status = downloadFileWithOverwrite(localFileName, remoteFileName);
							if (!status) {
								localFile.delete();
							}
						} else {
							log.info("tFTPGet_1 - file [" + remoteFileName + "] exit transmission.");
							msg_tFTPGet_1.add("file [" + remoteFileName + "] exit transmission.");
							globalMap.put("tFTPGet_1_CURRENT_STATUS", "No file transfered.");
						}
					}

					private boolean downloadFileWithOverwrite(String localFileName, String remoteFileName)
							throws IllegalStateException, java.io.FileNotFoundException, IOException {
						try (java.io.FileOutputStream localFos = new java.io.FileOutputStream(localFileName)) {
							boolean status = ftpClient.retrieveFile(remoteFileName, localFos);
							if (status) {
								log.debug("tFTPGet_1 - Downloaded file " + (count + 1) + " : '" + remoteFileName
										+ "' successfully.");
								msg_tFTPGet_1.add("file [" + remoteFileName + "] downloaded successfully.");
								globalMap.put("tFTPGet_1_CURRENT_STATUS", "File transfer OK.");
								count++;
							}
							return status;
						} catch (IOException e) {
							globalMap.put("tFTPGet_1_ERROR_MESSAGE", e.getMessage());
							msg_tFTPGet_1.add("file [" + remoteFileName + "] downloaded unsuccessfully.");
							globalMap.put("tFTPGet_1_CURRENT_STATUS", "File transfer fail.");
							throw e;
						}
					}
				};
				getter_tFTPGet_1.ftpClient = ftp_tFTPGet_1;
				String remotedir_tFTPGet_1 = context.l_FTP_INPUT_PATH;
				if (!".".equals(remotedir_tFTPGet_1)) {
					boolean cwdSuccess_tFTPGet_1 = ftp_tFTPGet_1.changeWorkingDirectory(remotedir_tFTPGet_1);

					if (!cwdSuccess_tFTPGet_1) {
						throw new RuntimeException(
								"Failed to change remote directory. " + ftp_tFTPGet_1.getReplyString());
					}
				}
				java.util.List<String> maskList_tFTPGet_1 = new java.util.ArrayList<String>();

				maskList_tFTPGet_1.add(context.l_FTP_FILE_MASK);
				String localdir_tFTPGet_1 = context.g_VAR_TALEND_WORKSPACE + jobName;
//create folder if local direcotry (assigned by property) not exists
				java.io.File dirHandle_tFTPGet_1 = new java.io.File(localdir_tFTPGet_1);

				if (!dirHandle_tFTPGet_1.exists()) {
					dirHandle_tFTPGet_1.mkdirs();
				}
				String root_tFTPGet_1 = getter_tFTPGet_1.pwd();
				if ("/".equals(root_tFTPGet_1)) {
					root_tFTPGet_1 = ".";
				}

				log.info("tFTPGet_1 - Downloading files from the server.");
				for (String maskStr_tFTPGet_1 : maskList_tFTPGet_1) {

					/**
					 * [tFTPGet_1 begin ] stop
					 */

					/**
					 * [tFTPGet_1 main ] start
					 */

					currentComponent = "tFTPGet_1";

					try {
						globalMap.put("tFTPGet_1_CURRENT_STATUS", "No file transfered.");
						String dir_tFTPGet_1 = root_tFTPGet_1;

						String mask_tFTPGet_1 = maskStr_tFTPGet_1.replaceAll("\\\\", "/");

						int i_tFTPGet_1 = mask_tFTPGet_1.lastIndexOf('/');

						if (i_tFTPGet_1 != -1) {
							dir_tFTPGet_1 = mask_tFTPGet_1.substring(0, i_tFTPGet_1);
							mask_tFTPGet_1 = mask_tFTPGet_1.substring(i_tFTPGet_1 + 1);
						}

						mask_tFTPGet_1 = org.apache.oro.text.GlobCompiler.globToPerl5(mask_tFTPGet_1.toCharArray(),
								org.apache.oro.text.GlobCompiler.DEFAULT_MASK);

						if (dir_tFTPGet_1 != null && !"".equals(dir_tFTPGet_1)) {
							if ((".*").equals(mask_tFTPGet_1)) {
								getter_tFTPGet_1.getAllFiles(dir_tFTPGet_1, localdir_tFTPGet_1);
							} else {
								getter_tFTPGet_1.getFiles(dir_tFTPGet_1, localdir_tFTPGet_1, mask_tFTPGet_1);
							}
						}
						getter_tFTPGet_1.chdir(root_tFTPGet_1);
					} catch (java.lang.Exception e) {
						globalMap.put("tFTPGet_1_ERROR_MESSAGE", e.getMessage());

						throw (e);

					}

					tos_count_tFTPGet_1++;

					/**
					 * [tFTPGet_1 main ] stop
					 */

					/**
					 * [tFTPGet_1 process_data_begin ] start
					 */

					currentComponent = "tFTPGet_1";

					/**
					 * [tFTPGet_1 process_data_begin ] stop
					 */

					/**
					 * [tFTPGet_1 process_data_end ] start
					 */

					currentComponent = "tFTPGet_1";

					/**
					 * [tFTPGet_1 process_data_end ] stop
					 */

					/**
					 * [tFTPGet_1 end ] start
					 */

					currentComponent = "tFTPGet_1";

				}
				nb_file_tFTPGet_1 = getter_tFTPGet_1.count;

				msg_tFTPGet_1.add(getter_tFTPGet_1.count + " files have been downloaded.");
				String[] msgAll_tFTPGet_1 = msg_tFTPGet_1.toArray(new String[0]);
				StringBuffer sb_tFTPGet_1 = new StringBuffer();

				if (msgAll_tFTPGet_1 != null) {
					for (String item_tFTPGet_1 : msgAll_tFTPGet_1) {
						sb_tFTPGet_1.append(item_tFTPGet_1).append("\n");
					}
				}
				globalMap.put("tFTPGet_1_TRANSFER_MESSAGES", sb_tFTPGet_1.toString());

				ftp_tFTPGet_1.changeWorkingDirectory(rootDir_tFTPGet_1);
				globalMap.put("tFTPGet_1_NB_FILE", nb_file_tFTPGet_1);
				log.info("tFTPGet_1 - Downloaded files count: " + nb_file_tFTPGet_1 + ".");

				if (log.isDebugEnabled())
					log.debug("tFTPGet_1 - " + ("Done."));

				ok_Hash.put("tFTPGet_1", true);
				end_Hash.put("tFTPGet_1", System.currentTimeMillis());

				/**
				 * [tFTPGet_1 end ] stop
				 */
			} // end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:tFTPGet_1:OnSubjobOk", "",
						Thread.currentThread().getId() + "", "", "", "", "", "");
			}

			if (execStat) {
				runStat.updateStatOnConnection("OnSubjobOk5", 0, "ok");
			}

			tFileList_1Process(globalMap);

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tFTPGet_1 finally ] start
				 */

				currentComponent = "tFTPGet_1";

				/**
				 * [tFTPGet_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tFTPGet_1_SUBPROCESS_STATE", 1);
	}

	public static class row5Struct implements routines.system.IPersistableRow<row5Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public String jobname;

		public String getJobname() {
			return this.jobname;
		}

		public String guid;

		public String getGuid() {
			return this.guid;
		}

		public String filename;

		public String getFilename() {
			return this.filename;
		}

		public String filepath;

		public String getFilepath() {
			return this.filepath;
		}

		public Boolean error;

		public Boolean getError() {
			return this.error;
		}

		public String contextname;

		public String getContextname() {
			return this.contextname;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.jobname = readString(dis);

					this.guid = readString(dis);

					this.filename = readString(dis);

					this.filepath = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.error = null;
					} else {
						this.error = dis.readBoolean();
					}

					this.contextname = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.jobname = readString(dis);

					this.guid = readString(dis);

					this.filename = readString(dis);

					this.filepath = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.error = null;
					} else {
						this.error = dis.readBoolean();
					}

					this.contextname = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.jobname, dos);

				// String

				writeString(this.guid, dos);

				// String

				writeString(this.filename, dos);

				// String

				writeString(this.filepath, dos);

				// Boolean

				if (this.error == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.error);
				}

				// String

				writeString(this.contextname, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// String

				writeString(this.jobname, dos);

				// String

				writeString(this.guid, dos);

				// String

				writeString(this.filename, dos);

				// String

				writeString(this.filepath, dos);

				// Boolean

				if (this.error == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.error);
				}

				// String

				writeString(this.contextname, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("jobname=" + jobname);
			sb.append(",guid=" + guid);
			sb.append(",filename=" + filename);
			sb.append(",filepath=" + filepath);
			sb.append(",error=" + String.valueOf(error));
			sb.append(",contextname=" + contextname);
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (jobname == null) {
				sb.append("<null>");
			} else {
				sb.append(jobname);
			}

			sb.append("|");

			if (guid == null) {
				sb.append("<null>");
			} else {
				sb.append(guid);
			}

			sb.append("|");

			if (filename == null) {
				sb.append("<null>");
			} else {
				sb.append(filename);
			}

			sb.append("|");

			if (filepath == null) {
				sb.append("<null>");
			} else {
				sb.append(filepath);
			}

			sb.append("|");

			if (error == null) {
				sb.append("<null>");
			} else {
				sb.append(error);
			}

			sb.append("|");

			if (contextname == null) {
				sb.append("<null>");
			} else {
				sb.append(contextname);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row5Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row2Struct implements routines.system.IPersistableRow<row2Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public String jobname;

		public String getJobname() {
			return this.jobname;
		}

		public String guid;

		public String getGuid() {
			return this.guid;
		}

		public String filename;

		public String getFilename() {
			return this.filename;
		}

		public String filepath;

		public String getFilepath() {
			return this.filepath;
		}

		public Boolean error;

		public Boolean getError() {
			return this.error;
		}

		public String contextname;

		public String getContextname() {
			return this.contextname;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.jobname = readString(dis);

					this.guid = readString(dis);

					this.filename = readString(dis);

					this.filepath = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.error = null;
					} else {
						this.error = dis.readBoolean();
					}

					this.contextname = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.jobname = readString(dis);

					this.guid = readString(dis);

					this.filename = readString(dis);

					this.filepath = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.error = null;
					} else {
						this.error = dis.readBoolean();
					}

					this.contextname = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.jobname, dos);

				// String

				writeString(this.guid, dos);

				// String

				writeString(this.filename, dos);

				// String

				writeString(this.filepath, dos);

				// Boolean

				if (this.error == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.error);
				}

				// String

				writeString(this.contextname, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// String

				writeString(this.jobname, dos);

				// String

				writeString(this.guid, dos);

				// String

				writeString(this.filename, dos);

				// String

				writeString(this.filepath, dos);

				// Boolean

				if (this.error == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.error);
				}

				// String

				writeString(this.contextname, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("jobname=" + jobname);
			sb.append(",guid=" + guid);
			sb.append(",filename=" + filename);
			sb.append(",filepath=" + filepath);
			sb.append(",error=" + String.valueOf(error));
			sb.append(",contextname=" + contextname);
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (jobname == null) {
				sb.append("<null>");
			} else {
				sb.append(jobname);
			}

			sb.append("|");

			if (guid == null) {
				sb.append("<null>");
			} else {
				sb.append(guid);
			}

			sb.append("|");

			if (filename == null) {
				sb.append("<null>");
			} else {
				sb.append(filename);
			}

			sb.append("|");

			if (filepath == null) {
				sb.append("<null>");
			} else {
				sb.append(filepath);
			}

			sb.append("|");

			if (error == null) {
				sb.append("<null>");
			} else {
				sb.append(error);
			}

			sb.append("|");

			if (contextname == null) {
				sb.append("<null>");
			} else {
				sb.append(contextname);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row2Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tFileList_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tFileList_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row2Struct row2 = new row2Struct();
				row5Struct row5 = new row5Struct();

				/**
				 * [tFileList_1 begin ] start
				 */

				int NB_ITERATE_tFixedFlowInput_1 = 0; // for statistics

				ok_Hash.put("tFileList_1", false);
				start_Hash.put("tFileList_1", System.currentTimeMillis());

				currentComponent = "tFileList_1";

				int tos_count_tFileList_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tFileList_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tFileList_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tFileList_1 = new StringBuilder();
							log4jParamters_tFileList_1.append("Parameters:");
							log4jParamters_tFileList_1
									.append("DIRECTORY" + " = " + " context.g_VAR_TALEND_WORKSPACE + jobName");
							log4jParamters_tFileList_1.append(" | ");
							log4jParamters_tFileList_1.append("LIST_MODE" + " = " + "FILES");
							log4jParamters_tFileList_1.append(" | ");
							log4jParamters_tFileList_1.append("INCLUDSUBDIR" + " = " + "false");
							log4jParamters_tFileList_1.append(" | ");
							log4jParamters_tFileList_1.append("CASE_SENSITIVE" + " = " + "YES");
							log4jParamters_tFileList_1.append(" | ");
							log4jParamters_tFileList_1.append("ERROR" + " = " + "false");
							log4jParamters_tFileList_1.append(" | ");
							log4jParamters_tFileList_1.append("GLOBEXPRESSIONS" + " = " + "true");
							log4jParamters_tFileList_1.append(" | ");
							log4jParamters_tFileList_1
									.append("FILES" + " = " + "[{FILEMASK=" + ("context.l_FTP_FILE_MASK") + "}]");
							log4jParamters_tFileList_1.append(" | ");
							log4jParamters_tFileList_1.append("ORDER_BY_NOTHING" + " = " + "true");
							log4jParamters_tFileList_1.append(" | ");
							log4jParamters_tFileList_1.append("ORDER_BY_FILENAME" + " = " + "false");
							log4jParamters_tFileList_1.append(" | ");
							log4jParamters_tFileList_1.append("ORDER_BY_FILESIZE" + " = " + "false");
							log4jParamters_tFileList_1.append(" | ");
							log4jParamters_tFileList_1.append("ORDER_BY_MODIFIEDDATE" + " = " + "false");
							log4jParamters_tFileList_1.append(" | ");
							log4jParamters_tFileList_1.append("ORDER_ACTION_ASC" + " = " + "true");
							log4jParamters_tFileList_1.append(" | ");
							log4jParamters_tFileList_1.append("ORDER_ACTION_DESC" + " = " + "false");
							log4jParamters_tFileList_1.append(" | ");
							log4jParamters_tFileList_1.append("IFEXCLUDE" + " = " + "false");
							log4jParamters_tFileList_1.append(" | ");
							log4jParamters_tFileList_1.append("FORMAT_FILEPATH_TO_SLASH" + " = " + "false");
							log4jParamters_tFileList_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tFileList_1 - " + (log4jParamters_tFileList_1));
						}
					}
					new BytesLimit65535_tFileList_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tFileList_1", "__UNIQUE_NAME__<br><b>Lister Fichiers</b>", "tFileList");
					talendJobLogProcess(globalMap);
				}

				final StringBuffer log4jSb_tFileList_1 = new StringBuffer();

				String directory_tFileList_1 = context.g_VAR_TALEND_WORKSPACE + jobName;
				final java.util.List<String> maskList_tFileList_1 = new java.util.ArrayList<String>();
				final java.util.List<java.util.regex.Pattern> patternList_tFileList_1 = new java.util.ArrayList<java.util.regex.Pattern>();
				maskList_tFileList_1.add(context.l_FTP_FILE_MASK);
				for (final String filemask_tFileList_1 : maskList_tFileList_1) {
					String filemask_compile_tFileList_1 = filemask_tFileList_1;

					filemask_compile_tFileList_1 = org.apache.oro.text.GlobCompiler.globToPerl5(
							filemask_tFileList_1.toCharArray(), org.apache.oro.text.GlobCompiler.DEFAULT_MASK);

					java.util.regex.Pattern fileNamePattern_tFileList_1 = java.util.regex.Pattern
							.compile(filemask_compile_tFileList_1);
					patternList_tFileList_1.add(fileNamePattern_tFileList_1);
				}
				int NB_FILEtFileList_1 = 0;

				final boolean case_sensitive_tFileList_1 = true;

				log.info("tFileList_1 - Starting to search for matching entries.");

				final java.util.List<java.io.File> list_tFileList_1 = new java.util.ArrayList<java.io.File>();
				final java.util.Set<String> filePath_tFileList_1 = new java.util.HashSet<String>();
				java.io.File file_tFileList_1 = new java.io.File(directory_tFileList_1);

				file_tFileList_1.listFiles(new java.io.FilenameFilter() {
					public boolean accept(java.io.File dir, String name) {
						java.io.File file = new java.io.File(dir, name);
						if (!file.isDirectory()) {

							String fileName_tFileList_1 = file.getName();
							for (final java.util.regex.Pattern fileNamePattern_tFileList_1 : patternList_tFileList_1) {
								if (fileNamePattern_tFileList_1.matcher(fileName_tFileList_1).matches()) {
									if (!filePath_tFileList_1.contains(file.getAbsolutePath())) {
										list_tFileList_1.add(file);
										filePath_tFileList_1.add(file.getAbsolutePath());
									}
								}
							}
						}
						return true;
					}
				});
				java.util.Collections.sort(list_tFileList_1);

				log.info("tFileList_1 - Start to list files.");

				for (int i_tFileList_1 = 0; i_tFileList_1 < list_tFileList_1.size(); i_tFileList_1++) {
					java.io.File files_tFileList_1 = list_tFileList_1.get(i_tFileList_1);
					String fileName_tFileList_1 = files_tFileList_1.getName();

					String currentFileName_tFileList_1 = files_tFileList_1.getName();
					String currentFilePath_tFileList_1 = files_tFileList_1.getAbsolutePath();
					String currentFileDirectory_tFileList_1 = files_tFileList_1.getParent();
					String currentFileExtension_tFileList_1 = null;

					if (files_tFileList_1.getName().contains(".") && files_tFileList_1.isFile()) {
						currentFileExtension_tFileList_1 = files_tFileList_1.getName()
								.substring(files_tFileList_1.getName().lastIndexOf(".") + 1);
					} else {
						currentFileExtension_tFileList_1 = "";
					}

					NB_FILEtFileList_1++;
					globalMap.put("tFileList_1_CURRENT_FILE", currentFileName_tFileList_1);
					globalMap.put("tFileList_1_CURRENT_FILEPATH", currentFilePath_tFileList_1);
					globalMap.put("tFileList_1_CURRENT_FILEDIRECTORY", currentFileDirectory_tFileList_1);
					globalMap.put("tFileList_1_CURRENT_FILEEXTENSION", currentFileExtension_tFileList_1);
					globalMap.put("tFileList_1_NB_FILE", NB_FILEtFileList_1);

					log.info("tFileList_1 - Current file or directory path : " + currentFilePath_tFileList_1);

					/**
					 * [tFileList_1 begin ] stop
					 */

					/**
					 * [tFileList_1 main ] start
					 */

					currentComponent = "tFileList_1";

					tos_count_tFileList_1++;

					/**
					 * [tFileList_1 main ] stop
					 */

					/**
					 * [tFileList_1 process_data_begin ] start
					 */

					currentComponent = "tFileList_1";

					/**
					 * [tFileList_1 process_data_begin ] stop
					 */
					NB_ITERATE_tFixedFlowInput_1++;

					if (execStat) {
						runStat.updateStatOnConnection("row1", 3, 0);
					}

					if (execStat) {
						runStat.updateStatOnConnection("OnComponentError3", 3, 0);
					}

					if (execStat) {
						runStat.updateStatOnConnection("to_error", 3, 0);
					}

					if (execStat) {
						runStat.updateStatOnConnection("out1", 3, 0);
					}

					if (execStat) {
						runStat.updateStatOnConnection("OnComponentError1", 3, 0);
					}

					if (execStat) {
						runStat.updateStatOnConnection("OnComponentOk7", 3, 0);
					}

					if (execStat) {
						runStat.updateStatOnConnection("row2", 3, 0);
					}

					if (execStat) {
						runStat.updateStatOnConnection("row5", 3, 0);
					}

					if (execStat) {
						runStat.updateStatOnConnection("OnComponentOk10", 3, 0);
					}

					if (execStat) {
						runStat.updateStatOnConnection("OnComponentError2", 3, 0);
					}

					if (execStat) {
						runStat.updateStatOnConnection("iterate2", 1, "exec" + NB_ITERATE_tFixedFlowInput_1);
						// Thread.sleep(1000);
					}

					/**
					 * [tDBOutput_1 begin ] start
					 */

					ok_Hash.put("tDBOutput_1", false);
					start_Hash.put("tDBOutput_1", System.currentTimeMillis());

					currentComponent = "tDBOutput_1";

					runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "row5");

					int tos_count_tDBOutput_1 = 0;

					if (log.isDebugEnabled())
						log.debug("tDBOutput_1 - " + ("Start to work."));
					if (log.isDebugEnabled()) {
						class BytesLimit65535_tDBOutput_1 {
							public void limitLog4jByte() throws Exception {
								StringBuilder log4jParamters_tDBOutput_1 = new StringBuilder();
								log4jParamters_tDBOutput_1.append("Parameters:");
								log4jParamters_tDBOutput_1.append("USE_EXISTING_CONNECTION" + " = " + "true");
								log4jParamters_tDBOutput_1.append(" | ");
								log4jParamters_tDBOutput_1.append("CONNECTION" + " = " + "tDBConnection_1");
								log4jParamters_tDBOutput_1.append(" | ");
								log4jParamters_tDBOutput_1.append("TABLE" + " = " + "\"files\"");
								log4jParamters_tDBOutput_1.append(" | ");
								log4jParamters_tDBOutput_1.append("TABLE_ACTION" + " = " + "NONE");
								log4jParamters_tDBOutput_1.append(" | ");
								log4jParamters_tDBOutput_1.append("DATA_ACTION" + " = " + "INSERT");
								log4jParamters_tDBOutput_1.append(" | ");
								log4jParamters_tDBOutput_1.append("DIE_ON_ERROR" + " = " + "true");
								log4jParamters_tDBOutput_1.append(" | ");
								log4jParamters_tDBOutput_1.append("USE_ALTERNATE_SCHEMA" + " = " + "false");
								log4jParamters_tDBOutput_1.append(" | ");
								log4jParamters_tDBOutput_1.append("ADD_COLS" + " = " + "[]");
								log4jParamters_tDBOutput_1.append(" | ");
								log4jParamters_tDBOutput_1.append("USE_FIELD_OPTIONS" + " = " + "false");
								log4jParamters_tDBOutput_1.append(" | ");
								log4jParamters_tDBOutput_1.append("ENABLE_DEBUG_MODE" + " = " + "false");
								log4jParamters_tDBOutput_1.append(" | ");
								log4jParamters_tDBOutput_1.append("SUPPORT_NULL_WHERE" + " = " + "false");
								log4jParamters_tDBOutput_1.append(" | ");
								log4jParamters_tDBOutput_1
										.append("CONVERT_COLUMN_TABLE_TO_LOWERCASE" + " = " + "false");
								log4jParamters_tDBOutput_1.append(" | ");
								log4jParamters_tDBOutput_1.append("USE_BATCH_SIZE" + " = " + "true");
								log4jParamters_tDBOutput_1.append(" | ");
								log4jParamters_tDBOutput_1.append("BATCH_SIZE" + " = " + "10000");
								log4jParamters_tDBOutput_1.append(" | ");
								log4jParamters_tDBOutput_1.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlOutput");
								log4jParamters_tDBOutput_1.append(" | ");
								if (log.isDebugEnabled())
									log.debug("tDBOutput_1 - " + (log4jParamters_tDBOutput_1));
							}
						}
						new BytesLimit65535_tDBOutput_1().limitLog4jByte();
					}
					if (enableLogStash) {
						talendJobLog.addCM("tDBOutput_1",
								"__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>", "tPostgresqlOutput");
						talendJobLogProcess(globalMap);
					}

					String dbschema_tDBOutput_1 = null;
					dbschema_tDBOutput_1 = (String) globalMap.get("schema_" + "tDBConnection_1");

					String tableName_tDBOutput_1 = null;
					if (dbschema_tDBOutput_1 == null || dbschema_tDBOutput_1.trim().length() == 0) {
						tableName_tDBOutput_1 = ("files");
					} else {
						tableName_tDBOutput_1 = dbschema_tDBOutput_1 + "\".\"" + ("files");
					}

					int nb_line_tDBOutput_1 = 0;
					int nb_line_update_tDBOutput_1 = 0;
					int nb_line_inserted_tDBOutput_1 = 0;
					int nb_line_deleted_tDBOutput_1 = 0;
					int nb_line_rejected_tDBOutput_1 = 0;

					int deletedCount_tDBOutput_1 = 0;
					int updatedCount_tDBOutput_1 = 0;
					int insertedCount_tDBOutput_1 = 0;
					int rowsToCommitCount_tDBOutput_1 = 0;
					int rejectedCount_tDBOutput_1 = 0;

					boolean whetherReject_tDBOutput_1 = false;

					java.sql.Connection conn_tDBOutput_1 = null;
					String dbUser_tDBOutput_1 = null;

					conn_tDBOutput_1 = (java.sql.Connection) globalMap.get("conn_tDBConnection_1");

					if (log.isDebugEnabled())
						log.debug("tDBOutput_1 - " + ("Uses an existing connection with username '")
								+ (conn_tDBOutput_1.getMetaData().getUserName()) + ("'. Connection URL: ")
								+ (conn_tDBOutput_1.getMetaData().getURL()) + ("."));

					if (log.isDebugEnabled())
						log.debug("tDBOutput_1 - " + ("Connection is set auto commit to '")
								+ (conn_tDBOutput_1.getAutoCommit()) + ("'."));

					int batchSize_tDBOutput_1 = 10000;
					int batchSizeCounter_tDBOutput_1 = 0;

					int count_tDBOutput_1 = 0;
					String insert_tDBOutput_1 = "INSERT INTO \"" + tableName_tDBOutput_1
							+ "\" (\"jobname\",\"guid\",\"filename\",\"filepath\",\"error\",\"contextname\") VALUES (?,?,?,?,?,?)";

					java.sql.PreparedStatement pstmt_tDBOutput_1 = conn_tDBOutput_1
							.prepareStatement(insert_tDBOutput_1);
					resourceMap.put("pstmt_tDBOutput_1", pstmt_tDBOutput_1);

					/**
					 * [tDBOutput_1 begin ] stop
					 */

					/**
					 * [tJavaRow_1 begin ] start
					 */

					ok_Hash.put("tJavaRow_1", false);
					start_Hash.put("tJavaRow_1", System.currentTimeMillis());

					currentComponent = "tJavaRow_1";

					runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "row2");

					int tos_count_tJavaRow_1 = 0;

					if (enableLogStash) {
						talendJobLog.addCM("tJavaRow_1",
								"__UNIQUE_NAME__<br><b>currentFile_Name<br>currentFile_Path</b>", "tJavaRow");
						talendJobLogProcess(globalMap);
					}

					int nb_line_tJavaRow_1 = 0;

					/**
					 * [tJavaRow_1 begin ] stop
					 */

					/**
					 * [tFixedFlowInput_1 begin ] start
					 */

					ok_Hash.put("tFixedFlowInput_1", false);
					start_Hash.put("tFixedFlowInput_1", System.currentTimeMillis());

					currentComponent = "tFixedFlowInput_1";

					int tos_count_tFixedFlowInput_1 = 0;

					if (enableLogStash) {
						talendJobLog.addCM("tFixedFlowInput_1", "tFixedFlowInput_1", "tFixedFlowInput");
						talendJobLogProcess(globalMap);
					}

					for (int i_tFixedFlowInput_1 = 0; i_tFixedFlowInput_1 < 1; i_tFixedFlowInput_1++) {

						row2.jobname = jobName;

						row2.guid = (String) globalMap.get("guid");

						row2.filename = (String) globalMap.get("tFileList_1_CURRENT_FILE");

						row2.filepath = context.l_FTP_INPUT_PATH;

						row2.error = false;

						row2.contextname = "GCE";

						/**
						 * [tFixedFlowInput_1 begin ] stop
						 */

						/**
						 * [tFixedFlowInput_1 main ] start
						 */

						currentComponent = "tFixedFlowInput_1";

						tos_count_tFixedFlowInput_1++;

						/**
						 * [tFixedFlowInput_1 main ] stop
						 */

						/**
						 * [tFixedFlowInput_1 process_data_begin ] start
						 */

						currentComponent = "tFixedFlowInput_1";

						/**
						 * [tFixedFlowInput_1 process_data_begin ] stop
						 */

						/**
						 * [tJavaRow_1 main ] start
						 */

						currentComponent = "tJavaRow_1";

						if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

								, "row2", "tFixedFlowInput_1", "tFixedFlowInput_1", "tFixedFlowInput", "tJavaRow_1",
								"__UNIQUE_NAME__<br><b>currentFile_Name<br>currentFile_Path</b>", "tJavaRow"

						)) {
							talendJobLogProcess(globalMap);
						}

						if (log.isTraceEnabled()) {
							log.trace("row2 - " + (row2 == null ? "" : row2.toLogString()));
						}

						// Propagate data
						row5.jobname = row2.jobname;
						row5.guid = row2.guid;
						row5.filename = row2.filename;
						row5.filepath = row2.filepath;
						row5.error = row2.error;
						row5.contextname = row2.contextname;

// Save data in globalMap (For j_catch_unexpected_error)
						globalMap.put("currentFile_Name", row2.filename);
						globalMap.put("currentFile_Path", row2.filepath);

						nb_line_tJavaRow_1++;

						tos_count_tJavaRow_1++;

						/**
						 * [tJavaRow_1 main ] stop
						 */

						/**
						 * [tJavaRow_1 process_data_begin ] start
						 */

						currentComponent = "tJavaRow_1";

						/**
						 * [tJavaRow_1 process_data_begin ] stop
						 */

						/**
						 * [tDBOutput_1 main ] start
						 */

						currentComponent = "tDBOutput_1";

						if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

								, "row5", "tJavaRow_1",
								"__UNIQUE_NAME__<br><b>currentFile_Name<br>currentFile_Path</b>", "tJavaRow",
								"tDBOutput_1", "__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>",
								"tPostgresqlOutput"

						)) {
							talendJobLogProcess(globalMap);
						}

						if (log.isTraceEnabled()) {
							log.trace("row5 - " + (row5 == null ? "" : row5.toLogString()));
						}

						whetherReject_tDBOutput_1 = false;
						if (row5.jobname == null) {
							pstmt_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
						} else {
							pstmt_tDBOutput_1.setString(1, row5.jobname);
						}

						if (row5.guid == null) {
							pstmt_tDBOutput_1.setNull(2, java.sql.Types.VARCHAR);
						} else {
							pstmt_tDBOutput_1.setString(2, row5.guid);
						}

						if (row5.filename == null) {
							pstmt_tDBOutput_1.setNull(3, java.sql.Types.VARCHAR);
						} else {
							pstmt_tDBOutput_1.setString(3, row5.filename);
						}

						if (row5.filepath == null) {
							pstmt_tDBOutput_1.setNull(4, java.sql.Types.VARCHAR);
						} else {
							pstmt_tDBOutput_1.setString(4, row5.filepath);
						}

						if (row5.error == null) {
							pstmt_tDBOutput_1.setNull(5, java.sql.Types.BOOLEAN);
						} else {
							pstmt_tDBOutput_1.setBoolean(5, row5.error);
						}

						if (row5.contextname == null) {
							pstmt_tDBOutput_1.setNull(6, java.sql.Types.VARCHAR);
						} else {
							pstmt_tDBOutput_1.setString(6, row5.contextname);
						}

						pstmt_tDBOutput_1.addBatch();
						nb_line_tDBOutput_1++;

						if (log.isDebugEnabled())
							log.debug("tDBOutput_1 - " + ("Adding the record ") + (nb_line_tDBOutput_1) + (" to the ")
									+ ("INSERT") + (" batch."));
						batchSizeCounter_tDBOutput_1++;

						if (!whetherReject_tDBOutput_1) {
						}
						if ((batchSize_tDBOutput_1 > 0) && (batchSize_tDBOutput_1 <= batchSizeCounter_tDBOutput_1)) {
							try {
								int countSum_tDBOutput_1 = 0;

								if (log.isDebugEnabled())
									log.debug("tDBOutput_1 - " + ("Executing the ") + ("INSERT") + (" batch."));
								for (int countEach_tDBOutput_1 : pstmt_tDBOutput_1.executeBatch()) {
									countSum_tDBOutput_1 += (countEach_tDBOutput_1 < 0 ? 0 : countEach_tDBOutput_1);
								}
								if (log.isDebugEnabled())
									log.debug("tDBOutput_1 - " + ("The ") + ("INSERT")
											+ (" batch execution has succeeded."));
								rowsToCommitCount_tDBOutput_1 += countSum_tDBOutput_1;

								insertedCount_tDBOutput_1 += countSum_tDBOutput_1;

								batchSizeCounter_tDBOutput_1 = 0;
							} catch (java.sql.BatchUpdateException e_tDBOutput_1) {
								globalMap.put("tDBOutput_1_ERROR_MESSAGE", e_tDBOutput_1.getMessage());
								java.sql.SQLException ne_tDBOutput_1 = e_tDBOutput_1.getNextException(),
										sqle_tDBOutput_1 = null;
								String errormessage_tDBOutput_1;
								if (ne_tDBOutput_1 != null) {
									// build new exception to provide the original cause
									sqle_tDBOutput_1 = new java.sql.SQLException(
											e_tDBOutput_1.getMessage() + "\ncaused by: " + ne_tDBOutput_1.getMessage(),
											ne_tDBOutput_1.getSQLState(), ne_tDBOutput_1.getErrorCode(),
											ne_tDBOutput_1);
									errormessage_tDBOutput_1 = sqle_tDBOutput_1.getMessage();
								} else {
									errormessage_tDBOutput_1 = e_tDBOutput_1.getMessage();
								}

								if (ne_tDBOutput_1 != null) {
									throw (sqle_tDBOutput_1);
								} else {
									throw (e_tDBOutput_1);
								}

							}
						}

						tos_count_tDBOutput_1++;

						/**
						 * [tDBOutput_1 main ] stop
						 */

						/**
						 * [tDBOutput_1 process_data_begin ] start
						 */

						currentComponent = "tDBOutput_1";

						/**
						 * [tDBOutput_1 process_data_begin ] stop
						 */

						/**
						 * [tDBOutput_1 process_data_end ] start
						 */

						currentComponent = "tDBOutput_1";

						/**
						 * [tDBOutput_1 process_data_end ] stop
						 */

						/**
						 * [tJavaRow_1 process_data_end ] start
						 */

						currentComponent = "tJavaRow_1";

						/**
						 * [tJavaRow_1 process_data_end ] stop
						 */

						/**
						 * [tFixedFlowInput_1 process_data_end ] start
						 */

						currentComponent = "tFixedFlowInput_1";

						/**
						 * [tFixedFlowInput_1 process_data_end ] stop
						 */

						/**
						 * [tFixedFlowInput_1 end ] start
						 */

						currentComponent = "tFixedFlowInput_1";

					}
					globalMap.put("tFixedFlowInput_1_NB_LINE", 1);

					ok_Hash.put("tFixedFlowInput_1", true);
					end_Hash.put("tFixedFlowInput_1", System.currentTimeMillis());

					/**
					 * [tFixedFlowInput_1 end ] stop
					 */

					/**
					 * [tJavaRow_1 end ] start
					 */

					currentComponent = "tJavaRow_1";

					globalMap.put("tJavaRow_1_NB_LINE", nb_line_tJavaRow_1);
					if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "row2", 2, 0,
							"tFixedFlowInput_1", "tFixedFlowInput_1", "tFixedFlowInput", "tJavaRow_1",
							"__UNIQUE_NAME__<br><b>currentFile_Name<br>currentFile_Path</b>", "tJavaRow", "output")) {
						talendJobLogProcess(globalMap);
					}

					ok_Hash.put("tJavaRow_1", true);
					end_Hash.put("tJavaRow_1", System.currentTimeMillis());

					/**
					 * [tJavaRow_1 end ] stop
					 */

					/**
					 * [tDBOutput_1 end ] start
					 */

					currentComponent = "tDBOutput_1";

					try {
						int countSum_tDBOutput_1 = 0;
						if (pstmt_tDBOutput_1 != null && batchSizeCounter_tDBOutput_1 > 0) {

							if (log.isDebugEnabled())
								log.debug("tDBOutput_1 - " + ("Executing the ") + ("INSERT") + (" batch."));
							for (int countEach_tDBOutput_1 : pstmt_tDBOutput_1.executeBatch()) {
								countSum_tDBOutput_1 += (countEach_tDBOutput_1 < 0 ? 0 : countEach_tDBOutput_1);
							}
							rowsToCommitCount_tDBOutput_1 += countSum_tDBOutput_1;

							if (log.isDebugEnabled())
								log.debug(
										"tDBOutput_1 - " + ("The ") + ("INSERT") + (" batch execution has succeeded."));
						}

						insertedCount_tDBOutput_1 += countSum_tDBOutput_1;

					} catch (java.sql.BatchUpdateException e_tDBOutput_1) {
						globalMap.put("tDBOutput_1_ERROR_MESSAGE", e_tDBOutput_1.getMessage());
						java.sql.SQLException ne_tDBOutput_1 = e_tDBOutput_1.getNextException(),
								sqle_tDBOutput_1 = null;
						String errormessage_tDBOutput_1;
						if (ne_tDBOutput_1 != null) {
							// build new exception to provide the original cause
							sqle_tDBOutput_1 = new java.sql.SQLException(
									e_tDBOutput_1.getMessage() + "\ncaused by: " + ne_tDBOutput_1.getMessage(),
									ne_tDBOutput_1.getSQLState(), ne_tDBOutput_1.getErrorCode(), ne_tDBOutput_1);
							errormessage_tDBOutput_1 = sqle_tDBOutput_1.getMessage();
						} else {
							errormessage_tDBOutput_1 = e_tDBOutput_1.getMessage();
						}

						if (ne_tDBOutput_1 != null) {
							throw (sqle_tDBOutput_1);
						} else {
							throw (e_tDBOutput_1);
						}

					}

					if (pstmt_tDBOutput_1 != null) {

						pstmt_tDBOutput_1.close();
						resourceMap.remove("pstmt_tDBOutput_1");
					}
					resourceMap.put("statementClosed_tDBOutput_1", true);

					nb_line_deleted_tDBOutput_1 = nb_line_deleted_tDBOutput_1 + deletedCount_tDBOutput_1;
					nb_line_update_tDBOutput_1 = nb_line_update_tDBOutput_1 + updatedCount_tDBOutput_1;
					nb_line_inserted_tDBOutput_1 = nb_line_inserted_tDBOutput_1 + insertedCount_tDBOutput_1;
					nb_line_rejected_tDBOutput_1 = nb_line_rejected_tDBOutput_1 + rejectedCount_tDBOutput_1;

					globalMap.put("tDBOutput_1_NB_LINE", nb_line_tDBOutput_1);
					globalMap.put("tDBOutput_1_NB_LINE_UPDATED", nb_line_update_tDBOutput_1);
					globalMap.put("tDBOutput_1_NB_LINE_INSERTED", nb_line_inserted_tDBOutput_1);
					globalMap.put("tDBOutput_1_NB_LINE_DELETED", nb_line_deleted_tDBOutput_1);
					globalMap.put("tDBOutput_1_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_1);

					if (log.isDebugEnabled())
						log.debug("tDBOutput_1 - " + ("Has ") + ("inserted") + (" ") + (nb_line_inserted_tDBOutput_1)
								+ (" record(s)."));

					if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "row5", 2, 0,
							"tJavaRow_1", "__UNIQUE_NAME__<br><b>currentFile_Name<br>currentFile_Path</b>", "tJavaRow",
							"tDBOutput_1", "__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>",
							"tPostgresqlOutput", "output")) {
						talendJobLogProcess(globalMap);
					}

					if (log.isDebugEnabled())
						log.debug("tDBOutput_1 - " + ("Done."));

					ok_Hash.put("tDBOutput_1", true);
					end_Hash.put("tDBOutput_1", System.currentTimeMillis());

					if (execStat) {
						runStat.updateStatOnConnection("OnComponentOk10", 0, "ok");
					}
					tFileInputDelimited_1Process(globalMap);

					/**
					 * [tDBOutput_1 end ] stop
					 */

					if (execStat) {
						runStat.updateStatOnConnection("iterate2", 2, "exec" + NB_ITERATE_tFixedFlowInput_1);
					}

					/**
					 * [tFileList_1 process_data_end ] start
					 */

					currentComponent = "tFileList_1";

					/**
					 * [tFileList_1 process_data_end ] stop
					 */

					/**
					 * [tFileList_1 end ] start
					 */

					currentComponent = "tFileList_1";

				}
				globalMap.put("tFileList_1_NB_FILE", NB_FILEtFileList_1);

				log.info("tFileList_1 - File or directory count : " + NB_FILEtFileList_1);

				if (log.isDebugEnabled())
					log.debug("tFileList_1 - " + ("Done."));

				ok_Hash.put("tFileList_1", true);
				end_Hash.put("tFileList_1", System.currentTimeMillis());

				/**
				 * [tFileList_1 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tFileList_1 finally ] start
				 */

				currentComponent = "tFileList_1";

				/**
				 * [tFileList_1 finally ] stop
				 */

				/**
				 * [tFixedFlowInput_1 finally ] start
				 */

				currentComponent = "tFixedFlowInput_1";

				/**
				 * [tFixedFlowInput_1 finally ] stop
				 */

				/**
				 * [tJavaRow_1 finally ] start
				 */

				currentComponent = "tJavaRow_1";

				/**
				 * [tJavaRow_1 finally ] stop
				 */

				/**
				 * [tDBOutput_1 finally ] start
				 */

				currentComponent = "tDBOutput_1";

				if (resourceMap.get("statementClosed_tDBOutput_1") == null) {
					java.sql.PreparedStatement pstmtToClose_tDBOutput_1 = null;
					if ((pstmtToClose_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap
							.remove("pstmt_tDBOutput_1")) != null) {
						pstmtToClose_tDBOutput_1.close();
					}
				}

				/**
				 * [tDBOutput_1 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tFileList_1_SUBPROCESS_STATE", 1);
	}

	public static class out1Struct implements routines.system.IPersistableRow<out1Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public String item_id;

		public String getItem_id() {
			return this.item_id;
		}

		public String order_id;

		public String getOrder_id() {
			return this.order_id;
		}

		public String line_state;

		public String getLine_state() {
			return this.line_state;
		}

		public Integer quantity;

		public Integer getQuantity() {
			return this.quantity;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.item_id = readString(dis);

					this.order_id = readString(dis);

					this.line_state = readString(dis);

					this.quantity = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.item_id = readString(dis);

					this.order_id = readString(dis);

					this.line_state = readString(dis);

					this.quantity = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.item_id, dos);

				// String

				writeString(this.order_id, dos);

				// String

				writeString(this.line_state, dos);

				// Integer

				writeInteger(this.quantity, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// String

				writeString(this.item_id, dos);

				// String

				writeString(this.order_id, dos);

				// String

				writeString(this.line_state, dos);

				// Integer

				writeInteger(this.quantity, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("item_id=" + item_id);
			sb.append(",order_id=" + order_id);
			sb.append(",line_state=" + line_state);
			sb.append(",quantity=" + String.valueOf(quantity));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (item_id == null) {
				sb.append("<null>");
			} else {
				sb.append(item_id);
			}

			sb.append("|");

			if (order_id == null) {
				sb.append("<null>");
			} else {
				sb.append(order_id);
			}

			sb.append("|");

			if (line_state == null) {
				sb.append("<null>");
			} else {
				sb.append(line_state);
			}

			sb.append("|");

			if (quantity == null) {
				sb.append("<null>");
			} else {
				sb.append(quantity);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(out1Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class to_errorStruct implements routines.system.IPersistableRow<to_errorStruct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public String item_id;

		public String getItem_id() {
			return this.item_id;
		}

		public String order_id;

		public String getOrder_id() {
			return this.order_id;
		}

		public String line_state;

		public String getLine_state() {
			return this.line_state;
		}

		public Integer quantity;

		public Integer getQuantity() {
			return this.quantity;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.item_id = readString(dis);

					this.order_id = readString(dis);

					this.line_state = readString(dis);

					this.quantity = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.item_id = readString(dis);

					this.order_id = readString(dis);

					this.line_state = readString(dis);

					this.quantity = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.item_id, dos);

				// String

				writeString(this.order_id, dos);

				// String

				writeString(this.line_state, dos);

				// Integer

				writeInteger(this.quantity, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// String

				writeString(this.item_id, dos);

				// String

				writeString(this.order_id, dos);

				// String

				writeString(this.line_state, dos);

				// Integer

				writeInteger(this.quantity, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("item_id=" + item_id);
			sb.append(",order_id=" + order_id);
			sb.append(",line_state=" + line_state);
			sb.append(",quantity=" + String.valueOf(quantity));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (item_id == null) {
				sb.append("<null>");
			} else {
				sb.append(item_id);
			}

			sb.append("|");

			if (order_id == null) {
				sb.append("<null>");
			} else {
				sb.append(order_id);
			}

			sb.append("|");

			if (line_state == null) {
				sb.append("<null>");
			} else {
				sb.append(line_state);
			}

			sb.append("|");

			if (quantity == null) {
				sb.append("<null>");
			} else {
				sb.append(quantity);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(to_errorStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row1Struct implements routines.system.IPersistableRow<row1Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public String NUMCDEWEB;

		public String getNUMCDEWEB() {
			return this.NUMCDEWEB;
		}

		public String DATLIV;

		public String getDATLIV() {
			return this.DATLIV;
		}

		public String CODPRO;

		public String getCODPRO() {
			return this.CODPRO;
		}

		public Integer QTECDE;

		public Integer getQTECDE() {
			return this.QTECDE;
		}

		public String STATUT;

		public String getSTATUT() {
			return this.STATUT;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.NUMCDEWEB = readString(dis);

					this.DATLIV = readString(dis);

					this.CODPRO = readString(dis);

					this.QTECDE = readInteger(dis);

					this.STATUT = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.NUMCDEWEB = readString(dis);

					this.DATLIV = readString(dis);

					this.CODPRO = readString(dis);

					this.QTECDE = readInteger(dis);

					this.STATUT = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.NUMCDEWEB, dos);

				// String

				writeString(this.DATLIV, dos);

				// String

				writeString(this.CODPRO, dos);

				// Integer

				writeInteger(this.QTECDE, dos);

				// String

				writeString(this.STATUT, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// String

				writeString(this.NUMCDEWEB, dos);

				// String

				writeString(this.DATLIV, dos);

				// String

				writeString(this.CODPRO, dos);

				// Integer

				writeInteger(this.QTECDE, dos);

				// String

				writeString(this.STATUT, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("NUMCDEWEB=" + NUMCDEWEB);
			sb.append(",DATLIV=" + DATLIV);
			sb.append(",CODPRO=" + CODPRO);
			sb.append(",QTECDE=" + String.valueOf(QTECDE));
			sb.append(",STATUT=" + STATUT);
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (NUMCDEWEB == null) {
				sb.append("<null>");
			} else {
				sb.append(NUMCDEWEB);
			}

			sb.append("|");

			if (DATLIV == null) {
				sb.append("<null>");
			} else {
				sb.append(DATLIV);
			}

			sb.append("|");

			if (CODPRO == null) {
				sb.append("<null>");
			} else {
				sb.append(CODPRO);
			}

			sb.append("|");

			if (QTECDE == null) {
				sb.append("<null>");
			} else {
				sb.append(QTECDE);
			}

			sb.append("|");

			if (STATUT == null) {
				sb.append("<null>");
			} else {
				sb.append(STATUT);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row1Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tFileInputDelimited_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tFileInputDelimited_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row1Struct row1 = new row1Struct();
				out1Struct out1 = new out1Struct();
				to_errorStruct to_error = new to_errorStruct();

				/**
				 * [tFileOutputDelimited_1 begin ] start
				 */

				ok_Hash.put("tFileOutputDelimited_1", false);
				start_Hash.put("tFileOutputDelimited_1", System.currentTimeMillis());

				currentComponent = "tFileOutputDelimited_1";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "out1");

				int tos_count_tFileOutputDelimited_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tFileOutputDelimited_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tFileOutputDelimited_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tFileOutputDelimited_1 = new StringBuilder();
							log4jParamters_tFileOutputDelimited_1.append("Parameters:");
							log4jParamters_tFileOutputDelimited_1.append("USESTREAM" + " = " + "false");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							log4jParamters_tFileOutputDelimited_1.append("FILENAME" + " = "
									+ " context.g_VAR_TALEND_WORKSPACE + jobName +\"/\"+ (String)globalMap.get(\"currentFile_Name\")");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							log4jParamters_tFileOutputDelimited_1.append("ROWSEPARATOR" + " = " + "\"\\n\"");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							log4jParamters_tFileOutputDelimited_1.append("FIELDSEPARATOR" + " = " + "\";\"");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							log4jParamters_tFileOutputDelimited_1.append("APPEND" + " = " + "false");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							log4jParamters_tFileOutputDelimited_1.append("INCLUDEHEADER" + " = " + "false");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							log4jParamters_tFileOutputDelimited_1.append("COMPRESS" + " = " + "false");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							log4jParamters_tFileOutputDelimited_1.append("ADVANCED_SEPARATOR" + " = " + "false");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							log4jParamters_tFileOutputDelimited_1.append("CSV_OPTION" + " = " + "false");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							log4jParamters_tFileOutputDelimited_1.append("CREATE" + " = " + "true");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							log4jParamters_tFileOutputDelimited_1.append("SPLIT" + " = " + "false");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							log4jParamters_tFileOutputDelimited_1.append("FLUSHONROW" + " = " + "false");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							log4jParamters_tFileOutputDelimited_1.append("ROW_MODE" + " = " + "false");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							log4jParamters_tFileOutputDelimited_1.append("ENCODING" + " = " + "\"ISO-8859-15\"");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							log4jParamters_tFileOutputDelimited_1.append("DELETE_EMPTYFILE" + " = " + "false");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							log4jParamters_tFileOutputDelimited_1.append("FILE_EXIST_EXCEPTION" + " = " + "true");
							log4jParamters_tFileOutputDelimited_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tFileOutputDelimited_1 - " + (log4jParamters_tFileOutputDelimited_1));
						}
					}
					new BytesLimit65535_tFileOutputDelimited_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tFileOutputDelimited_1", "__UNIQUE_NAME__<br><b>Stockage du Fichier</b>",
							"tFileOutputDelimited");
					talendJobLogProcess(globalMap);
				}

				String fileName_tFileOutputDelimited_1 = "";
				fileName_tFileOutputDelimited_1 = (new java.io.File(
						context.g_VAR_TALEND_WORKSPACE + jobName + "/" + (String) globalMap.get("currentFile_Name")))
								.getAbsolutePath().replace("\\", "/");
				String fullName_tFileOutputDelimited_1 = null;
				String extension_tFileOutputDelimited_1 = null;
				String directory_tFileOutputDelimited_1 = null;
				if ((fileName_tFileOutputDelimited_1.indexOf("/") != -1)) {
					if (fileName_tFileOutputDelimited_1.lastIndexOf(".") < fileName_tFileOutputDelimited_1
							.lastIndexOf("/")) {
						fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1;
						extension_tFileOutputDelimited_1 = "";
					} else {
						fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(0,
								fileName_tFileOutputDelimited_1.lastIndexOf("."));
						extension_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1
								.substring(fileName_tFileOutputDelimited_1.lastIndexOf("."));
					}
					directory_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(0,
							fileName_tFileOutputDelimited_1.lastIndexOf("/"));
				} else {
					if (fileName_tFileOutputDelimited_1.lastIndexOf(".") != -1) {
						fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(0,
								fileName_tFileOutputDelimited_1.lastIndexOf("."));
						extension_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1
								.substring(fileName_tFileOutputDelimited_1.lastIndexOf("."));
					} else {
						fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1;
						extension_tFileOutputDelimited_1 = "";
					}
					directory_tFileOutputDelimited_1 = "";
				}
				boolean isFileGenerated_tFileOutputDelimited_1 = true;
				java.io.File filetFileOutputDelimited_1 = new java.io.File(fileName_tFileOutputDelimited_1);
				globalMap.put("tFileOutputDelimited_1_FILE_NAME", fileName_tFileOutputDelimited_1);
				if (filetFileOutputDelimited_1.exists()) {
					throw new RuntimeException("The particular file \"" + filetFileOutputDelimited_1.getAbsoluteFile()
							+ "\" already exist. If you want to overwrite the file, please uncheck the"
							+ " \"Throw an error if the file already exist\" option in Advanced settings.");
				}
				int nb_line_tFileOutputDelimited_1 = 0;
				int splitedFileNo_tFileOutputDelimited_1 = 0;
				int currentRow_tFileOutputDelimited_1 = 0;

				final String OUT_DELIM_tFileOutputDelimited_1 = /** Start field tFileOutputDelimited_1:FIELDSEPARATOR */
						";"/** End field tFileOutputDelimited_1:FIELDSEPARATOR */
				;

				final String OUT_DELIM_ROWSEP_tFileOutputDelimited_1 = /**
																		 * Start field
																		 * tFileOutputDelimited_1:ROWSEPARATOR
																		 */
						"\n"/** End field tFileOutputDelimited_1:ROWSEPARATOR */
				;

				// create directory only if not exists
				if (directory_tFileOutputDelimited_1 != null && directory_tFileOutputDelimited_1.trim().length() != 0) {
					java.io.File dir_tFileOutputDelimited_1 = new java.io.File(directory_tFileOutputDelimited_1);
					if (!dir_tFileOutputDelimited_1.exists()) {
						log.info("tFileOutputDelimited_1 - Creating directory '"
								+ dir_tFileOutputDelimited_1.getCanonicalPath() + "'.");
						dir_tFileOutputDelimited_1.mkdirs();
						log.info("tFileOutputDelimited_1 - The directory '"
								+ dir_tFileOutputDelimited_1.getCanonicalPath() + "' has been created successfully.");
					}
				}

				// routines.system.Row
				java.io.Writer outtFileOutputDelimited_1 = null;

				java.io.File fileToDelete_tFileOutputDelimited_1 = new java.io.File(fileName_tFileOutputDelimited_1);
				if (fileToDelete_tFileOutputDelimited_1.exists()) {
					fileToDelete_tFileOutputDelimited_1.delete();
				}
				outtFileOutputDelimited_1 = new java.io.BufferedWriter(new java.io.OutputStreamWriter(
						new java.io.FileOutputStream(fileName_tFileOutputDelimited_1, false), "ISO-8859-15"));

				resourceMap.put("out_tFileOutputDelimited_1", outtFileOutputDelimited_1);
				resourceMap.put("nb_line_tFileOutputDelimited_1", nb_line_tFileOutputDelimited_1);

				/**
				 * [tFileOutputDelimited_1 begin ] stop
				 */

				/**
				 * [tDie_4 begin ] start
				 */

				ok_Hash.put("tDie_4", false);
				start_Hash.put("tDie_4", System.currentTimeMillis());

				currentComponent = "tDie_4";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "to_error");

				int tos_count_tDie_4 = 0;

				if (log.isDebugEnabled())
					log.debug("tDie_4 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tDie_4 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tDie_4 = new StringBuilder();
							log4jParamters_tDie_4.append("Parameters:");
							log4jParamters_tDie_4
									.append("MESSAGE" + " = " + "\"STATUT différent de : Expédié/Manquant\"");
							log4jParamters_tDie_4.append(" | ");
							log4jParamters_tDie_4.append("CODE" + " = " + "4");
							log4jParamters_tDie_4.append(" | ");
							log4jParamters_tDie_4.append("PRIORITY" + " = " + "5");
							log4jParamters_tDie_4.append(" | ");
							log4jParamters_tDie_4.append("EXIT_JVM" + " = " + "false");
							log4jParamters_tDie_4.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tDie_4 - " + (log4jParamters_tDie_4));
						}
					}
					new BytesLimit65535_tDie_4().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tDie_4", "__UNIQUE_NAME__<br><b>error on STATUT</b>", "tDie");
					talendJobLogProcess(globalMap);
				}

				/**
				 * [tDie_4 begin ] stop
				 */

				/**
				 * [tMap_1 begin ] start
				 */

				ok_Hash.put("tMap_1", false);
				start_Hash.put("tMap_1", System.currentTimeMillis());

				currentComponent = "tMap_1";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "row1");

				int tos_count_tMap_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tMap_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tMap_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tMap_1 = new StringBuilder();
							log4jParamters_tMap_1.append("Parameters:");
							log4jParamters_tMap_1.append("LINK_STYLE" + " = " + "AUTO");
							log4jParamters_tMap_1.append(" | ");
							log4jParamters_tMap_1.append("TEMPORARY_DATA_DIRECTORY" + " = " + "");
							log4jParamters_tMap_1.append(" | ");
							log4jParamters_tMap_1.append("ROWS_BUFFER_SIZE" + " = " + "2000000");
							log4jParamters_tMap_1.append(" | ");
							log4jParamters_tMap_1.append("CHANGE_HASH_AND_EQUALS_FOR_BIGDECIMAL" + " = " + "true");
							log4jParamters_tMap_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tMap_1 - " + (log4jParamters_tMap_1));
						}
					}
					new BytesLimit65535_tMap_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tMap_1", "__UNIQUE_NAME__<br><b>Mapping</b>", "tMap");
					talendJobLogProcess(globalMap);
				}

// ###############################
// # Lookup's keys initialization
				int count_row1_tMap_1 = 0;

// ###############################        

// ###############################
// # Vars initialization
				class Var__tMap_1__Struct {
				}
				Var__tMap_1__Struct Var__tMap_1 = new Var__tMap_1__Struct();
// ###############################

// ###############################
// # Outputs initialization
				int count_out1_tMap_1 = 0;

				out1Struct out1_tmp = new out1Struct();
				int count_to_error_tMap_1 = 0;

				to_errorStruct to_error_tmp = new to_errorStruct();
// ###############################

				/**
				 * [tMap_1 begin ] stop
				 */

				/**
				 * [tFileInputDelimited_1 begin ] start
				 */

				ok_Hash.put("tFileInputDelimited_1", false);
				start_Hash.put("tFileInputDelimited_1", System.currentTimeMillis());

				currentComponent = "tFileInputDelimited_1";

				int tos_count_tFileInputDelimited_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tFileInputDelimited_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tFileInputDelimited_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tFileInputDelimited_1 = new StringBuilder();
							log4jParamters_tFileInputDelimited_1.append("Parameters:");
							log4jParamters_tFileInputDelimited_1.append("FILENAME" + " = "
									+ " context.g_VAR_TALEND_WORKSPACE + jobName +\"/\"+ (String)globalMap.get(\"currentFile_Name\")");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("CSV_OPTION" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("ROWSEPARATOR" + " = " + "\"\\n\"");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("FIELDSEPARATOR" + " = " + "\";\"");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("HEADER" + " = " + "0");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("FOOTER" + " = " + "0");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("LIMIT" + " = " + "");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("REMOVE_EMPTY_ROW" + " = " + "true");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("UNCOMPRESS" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("DIE_ON_ERROR" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("ADVANCED_SEPARATOR" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("RANDOM" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("TRIMALL" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("TRIMSELECT" + " = " + "[{TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("NUMCDEWEB") + "}, {TRIM=" + ("false") + ", SCHEMA_COLUMN="
									+ ("DATLIV") + "}, {TRIM=" + ("false") + ", SCHEMA_COLUMN=" + ("CODPRO")
									+ "}, {TRIM=" + ("false") + ", SCHEMA_COLUMN=" + ("QTECDE") + "}, {TRIM="
									+ ("false") + ", SCHEMA_COLUMN=" + ("STATUT") + "}]");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("CHECK_FIELDS_NUM" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("CHECK_DATE" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("ENCODING" + " = " + "\"ISO-8859-15\"");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("SPLITRECORD" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("ENABLE_DECODE" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tFileInputDelimited_1 - " + (log4jParamters_tFileInputDelimited_1));
						}
					}
					new BytesLimit65535_tFileInputDelimited_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tFileInputDelimited_1", "__UNIQUE_NAME__<br><b>Lecture  Fichier</b>",
							"tFileInputDelimited");
					talendJobLogProcess(globalMap);
				}

				final routines.system.RowState rowstate_tFileInputDelimited_1 = new routines.system.RowState();

				int nb_line_tFileInputDelimited_1 = 0;
				org.talend.fileprocess.FileInputDelimited fid_tFileInputDelimited_1 = null;
				int limit_tFileInputDelimited_1 = -1;
				try {

					Object filename_tFileInputDelimited_1 = context.g_VAR_TALEND_WORKSPACE + jobName + "/"
							+ (String) globalMap.get("currentFile_Name");
					if (filename_tFileInputDelimited_1 instanceof java.io.InputStream) {

						int footer_value_tFileInputDelimited_1 = 0, random_value_tFileInputDelimited_1 = -1;
						if (footer_value_tFileInputDelimited_1 > 0 || random_value_tFileInputDelimited_1 > 0) {
							throw new java.lang.Exception(
									"When the input source is a stream,footer and random shouldn't be bigger than 0.");
						}

					}
					try {
						fid_tFileInputDelimited_1 = new org.talend.fileprocess.FileInputDelimited(
								context.g_VAR_TALEND_WORKSPACE + jobName + "/"
										+ (String) globalMap.get("currentFile_Name"),
								"ISO-8859-15", ";", "\n", true, 0, 0, limit_tFileInputDelimited_1, -1, false);
					} catch (java.lang.Exception e) {
						globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE", e.getMessage());

						log.error("tFileInputDelimited_1 - " + e.getMessage());

						System.err.println(e.getMessage());

					}

					log.info("tFileInputDelimited_1 - Retrieving records from the datasource.");

					while (fid_tFileInputDelimited_1 != null && fid_tFileInputDelimited_1.nextRecord()) {
						rowstate_tFileInputDelimited_1.reset();

						row1 = null;

						boolean whetherReject_tFileInputDelimited_1 = false;
						row1 = new row1Struct();
						try {

							int columnIndexWithD_tFileInputDelimited_1 = 0;

							String temp = "";

							columnIndexWithD_tFileInputDelimited_1 = 0;

							row1.NUMCDEWEB = fid_tFileInputDelimited_1.get(columnIndexWithD_tFileInputDelimited_1);

							columnIndexWithD_tFileInputDelimited_1 = 1;

							row1.DATLIV = fid_tFileInputDelimited_1.get(columnIndexWithD_tFileInputDelimited_1);

							columnIndexWithD_tFileInputDelimited_1 = 2;

							row1.CODPRO = fid_tFileInputDelimited_1.get(columnIndexWithD_tFileInputDelimited_1);

							columnIndexWithD_tFileInputDelimited_1 = 3;

							temp = fid_tFileInputDelimited_1.get(columnIndexWithD_tFileInputDelimited_1);
							if (temp.length() > 0) {

								try {

									row1.QTECDE = ParserUtils.parseTo_Integer(temp);

								} catch (java.lang.Exception ex_tFileInputDelimited_1) {
									globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",
											ex_tFileInputDelimited_1.getMessage());
									rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format(
											"Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
											"QTECDE", "row1", temp, ex_tFileInputDelimited_1),
											ex_tFileInputDelimited_1));
								}

							} else {

								row1.QTECDE = null;

							}

							columnIndexWithD_tFileInputDelimited_1 = 4;

							row1.STATUT = fid_tFileInputDelimited_1.get(columnIndexWithD_tFileInputDelimited_1);

							if (rowstate_tFileInputDelimited_1.getException() != null) {
								throw rowstate_tFileInputDelimited_1.getException();
							}

						} catch (java.lang.Exception e) {
							globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE", e.getMessage());
							whetherReject_tFileInputDelimited_1 = true;

							log.error("tFileInputDelimited_1 - " + e.getMessage());

							System.err.println(e.getMessage());
							row1 = null;

						}

						log.debug("tFileInputDelimited_1 - Retrieving the record "
								+ fid_tFileInputDelimited_1.getRowNumber() + ".");

						/**
						 * [tFileInputDelimited_1 begin ] stop
						 */

						/**
						 * [tFileInputDelimited_1 main ] start
						 */

						currentComponent = "tFileInputDelimited_1";

						tos_count_tFileInputDelimited_1++;

						/**
						 * [tFileInputDelimited_1 main ] stop
						 */

						/**
						 * [tFileInputDelimited_1 process_data_begin ] start
						 */

						currentComponent = "tFileInputDelimited_1";

						/**
						 * [tFileInputDelimited_1 process_data_begin ] stop
						 */
// Start of branch "row1"
						if (row1 != null) {

							/**
							 * [tMap_1 main ] start
							 */

							currentComponent = "tMap_1";

							if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

									, "row1", "tFileInputDelimited_1", "__UNIQUE_NAME__<br><b>Lecture  Fichier</b>",
									"tFileInputDelimited", "tMap_1", "__UNIQUE_NAME__<br><b>Mapping</b>", "tMap"

							)) {
								talendJobLogProcess(globalMap);
							}

							if (log.isTraceEnabled()) {
								log.trace("row1 - " + (row1 == null ? "" : row1.toLogString()));
							}

							boolean hasCasePrimitiveKeyWithNull_tMap_1 = false;

							// ###############################
							// # Input tables (lookups)
							boolean rejectedInnerJoin_tMap_1 = false;
							boolean mainRowRejected_tMap_1 = false;

							// ###############################
							{ // start of Var scope

								// ###############################
								// # Vars tables

								Var__tMap_1__Struct Var = Var__tMap_1;// ###############################
								// ###############################
								// # Output tables

								out1 = null;
								to_error = null;

// # Output table : 'out1'
// # Filter conditions 
								if (

								row1.STATUT.equals("Expédié") || row1.STATUT.equals("Manquant")

								) {
									count_out1_tMap_1++;

									out1_tmp.item_id = row1.CODPRO;
									out1_tmp.order_id = row1.NUMCDEWEB;
									out1_tmp.line_state = row1.STATUT;
									out1_tmp.quantity = row1.QTECDE;
									out1 = out1_tmp;
									log.debug("tMap_1 - Outputting the record " + count_out1_tMap_1
											+ " of the output table 'out1'.");

								} // closing filter/reject

// # Output table : 'to_error'
// # Filter conditions 
								if (

								!row1.STATUT.equals("Expédié") && !row1.STATUT.equals("Manquant")

								) {
									count_to_error_tMap_1++;

									to_error_tmp.item_id = row1.CODPRO;
									to_error_tmp.order_id = row1.NUMCDEWEB;
									to_error_tmp.line_state = row1.STATUT;
									to_error_tmp.quantity = row1.QTECDE;
									to_error = to_error_tmp;
									log.debug("tMap_1 - Outputting the record " + count_to_error_tMap_1
											+ " of the output table 'to_error'.");

								} // closing filter/reject
// ###############################

							} // end of Var scope

							rejectedInnerJoin_tMap_1 = false;

							tos_count_tMap_1++;

							/**
							 * [tMap_1 main ] stop
							 */

							/**
							 * [tMap_1 process_data_begin ] start
							 */

							currentComponent = "tMap_1";

							/**
							 * [tMap_1 process_data_begin ] stop
							 */
// Start of branch "out1"
							if (out1 != null) {

								/**
								 * [tFileOutputDelimited_1 main ] start
								 */

								currentComponent = "tFileOutputDelimited_1";

								if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

										, "out1", "tMap_1", "__UNIQUE_NAME__<br><b>Mapping</b>", "tMap",
										"tFileOutputDelimited_1", "__UNIQUE_NAME__<br><b>Stockage du Fichier</b>",
										"tFileOutputDelimited"

								)) {
									talendJobLogProcess(globalMap);
								}

								if (log.isTraceEnabled()) {
									log.trace("out1 - " + (out1 == null ? "" : out1.toLogString()));
								}

								StringBuilder sb_tFileOutputDelimited_1 = new StringBuilder();
								if (out1.item_id != null) {
									sb_tFileOutputDelimited_1.append(out1.item_id);
								}
								sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
								if (out1.order_id != null) {
									sb_tFileOutputDelimited_1.append(out1.order_id);
								}
								sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
								if (out1.line_state != null) {
									sb_tFileOutputDelimited_1.append(out1.line_state);
								}
								sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
								if (out1.quantity != null) {
									sb_tFileOutputDelimited_1.append(out1.quantity);
								}
								sb_tFileOutputDelimited_1.append(OUT_DELIM_ROWSEP_tFileOutputDelimited_1);

								nb_line_tFileOutputDelimited_1++;
								resourceMap.put("nb_line_tFileOutputDelimited_1", nb_line_tFileOutputDelimited_1);

								outtFileOutputDelimited_1.write(sb_tFileOutputDelimited_1.toString());
								log.debug("tFileOutputDelimited_1 - Writing the record "
										+ nb_line_tFileOutputDelimited_1 + ".");

								tos_count_tFileOutputDelimited_1++;

								/**
								 * [tFileOutputDelimited_1 main ] stop
								 */

								/**
								 * [tFileOutputDelimited_1 process_data_begin ] start
								 */

								currentComponent = "tFileOutputDelimited_1";

								/**
								 * [tFileOutputDelimited_1 process_data_begin ] stop
								 */

								/**
								 * [tFileOutputDelimited_1 process_data_end ] start
								 */

								currentComponent = "tFileOutputDelimited_1";

								/**
								 * [tFileOutputDelimited_1 process_data_end ] stop
								 */

							} // End of branch "out1"

// Start of branch "to_error"
							if (to_error != null) {

								/**
								 * [tDie_4 main ] start
								 */

								currentComponent = "tDie_4";

								if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

										, "to_error", "tMap_1", "__UNIQUE_NAME__<br><b>Mapping</b>", "tMap", "tDie_4",
										"__UNIQUE_NAME__<br><b>error on STATUT</b>", "tDie"

								)) {
									talendJobLogProcess(globalMap);
								}

								if (log.isTraceEnabled()) {
									log.trace("to_error - " + (to_error == null ? "" : to_error.toLogString()));
								}

								try {
									tLogCatcher_1.addMessage("tDie", "tDie_4", 5,
											"STATUT différent de : Expédié/Manquant", 4);
									tLogCatcher_1Process(globalMap);

									j_end_exec_1_tLogCatcher_1.addMessage("tDie", "tDie_4", 5,
											"STATUT différent de : Expédié/Manquant", 4);
									j_end_exec_1_tLogCatcher_1Process(globalMap);

									globalMap.put("tDie_4_DIE_PRIORITY", 5);
									System.err.println("STATUT différent de : Expédié/Manquant");

									log.error("tDie_4 - The die message: " + "STATUT différent de : Expédié/Manquant");

									globalMap.put("tDie_4_DIE_MESSAGE", "STATUT différent de : Expédié/Manquant");
									globalMap.put("tDie_4_DIE_MESSAGES", "STATUT différent de : Expédié/Manquant");

								} catch (Exception | Error e_tDie_4) {
									globalMap.put("tDie_4_ERROR_MESSAGE", e_tDie_4.getMessage());
									logIgnoredError(String.format(
											"tDie_4 - tDie failed to log message due to internal error: %s", e_tDie_4),
											e_tDie_4);
								}

								currentComponent = "tDie_4";
								status = "failure";
								errorCode = new Integer(4);
								globalMap.put("tDie_4_DIE_CODE", errorCode);

								if (true) {
									throw new TDieException();
								}

								tos_count_tDie_4++;

								/**
								 * [tDie_4 main ] stop
								 */

								/**
								 * [tDie_4 process_data_begin ] start
								 */

								currentComponent = "tDie_4";

								/**
								 * [tDie_4 process_data_begin ] stop
								 */

								/**
								 * [tDie_4 process_data_end ] start
								 */

								currentComponent = "tDie_4";

								/**
								 * [tDie_4 process_data_end ] stop
								 */

							} // End of branch "to_error"

							/**
							 * [tMap_1 process_data_end ] start
							 */

							currentComponent = "tMap_1";

							/**
							 * [tMap_1 process_data_end ] stop
							 */

						} // End of branch "row1"

						/**
						 * [tFileInputDelimited_1 process_data_end ] start
						 */

						currentComponent = "tFileInputDelimited_1";

						/**
						 * [tFileInputDelimited_1 process_data_end ] stop
						 */

						/**
						 * [tFileInputDelimited_1 end ] start
						 */

						currentComponent = "tFileInputDelimited_1";

					}
				} finally {
					if (!((Object) (context.g_VAR_TALEND_WORKSPACE + jobName + "/"
							+ (String) globalMap.get("currentFile_Name")) instanceof java.io.InputStream)) {
						if (fid_tFileInputDelimited_1 != null) {
							fid_tFileInputDelimited_1.close();
						}
					}
					if (fid_tFileInputDelimited_1 != null) {
						globalMap.put("tFileInputDelimited_1_NB_LINE", fid_tFileInputDelimited_1.getRowNumber());

						log.info("tFileInputDelimited_1 - Retrieved records count: "
								+ fid_tFileInputDelimited_1.getRowNumber() + ".");

					}
				}

				if (log.isDebugEnabled())
					log.debug("tFileInputDelimited_1 - " + ("Done."));

				ok_Hash.put("tFileInputDelimited_1", true);
				end_Hash.put("tFileInputDelimited_1", System.currentTimeMillis());

				/**
				 * [tFileInputDelimited_1 end ] stop
				 */

				/**
				 * [tMap_1 end ] start
				 */

				currentComponent = "tMap_1";

// ###############################
// # Lookup hashes releasing
// ###############################      
				log.debug("tMap_1 - Written records count in the table 'out1': " + count_out1_tMap_1 + ".");
				log.debug("tMap_1 - Written records count in the table 'to_error': " + count_to_error_tMap_1 + ".");

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "row1", 2, 0,
						"tFileInputDelimited_1", "__UNIQUE_NAME__<br><b>Lecture  Fichier</b>", "tFileInputDelimited",
						"tMap_1", "__UNIQUE_NAME__<br><b>Mapping</b>", "tMap", "output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("tMap_1 - " + ("Done."));

				ok_Hash.put("tMap_1", true);
				end_Hash.put("tMap_1", System.currentTimeMillis());

				/**
				 * [tMap_1 end ] stop
				 */

				/**
				 * [tFileOutputDelimited_1 end ] start
				 */

				currentComponent = "tFileOutputDelimited_1";

				if (outtFileOutputDelimited_1 != null) {
					outtFileOutputDelimited_1.flush();
					outtFileOutputDelimited_1.close();
				}

				globalMap.put("tFileOutputDelimited_1_NB_LINE", nb_line_tFileOutputDelimited_1);
				globalMap.put("tFileOutputDelimited_1_FILE_NAME", fileName_tFileOutputDelimited_1);

				resourceMap.put("finish_tFileOutputDelimited_1", true);

				log.debug("tFileOutputDelimited_1 - Written records count: " + nb_line_tFileOutputDelimited_1 + " .");

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "out1", 2, 0, "tMap_1",
						"__UNIQUE_NAME__<br><b>Mapping</b>", "tMap", "tFileOutputDelimited_1",
						"__UNIQUE_NAME__<br><b>Stockage du Fichier</b>", "tFileOutputDelimited", "output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("tFileOutputDelimited_1 - " + ("Done."));

				ok_Hash.put("tFileOutputDelimited_1", true);
				end_Hash.put("tFileOutputDelimited_1", System.currentTimeMillis());

				if (execStat) {
					runStat.updateStatOnConnection("OnComponentOk7", 0, "ok");
				}
				tFTPPut_1Process(globalMap);

				/**
				 * [tFileOutputDelimited_1 end ] stop
				 */

				/**
				 * [tDie_4 end ] start
				 */

				currentComponent = "tDie_4";

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "to_error", 2, 0,
						"tMap_1", "__UNIQUE_NAME__<br><b>Mapping</b>", "tMap", "tDie_4",
						"__UNIQUE_NAME__<br><b>error on STATUT</b>", "tDie", "output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("tDie_4 - " + ("Done."));

				ok_Hash.put("tDie_4", true);
				end_Hash.put("tDie_4", System.currentTimeMillis());

				/**
				 * [tDie_4 end ] stop
				 */

			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tFileInputDelimited_1 finally ] start
				 */

				currentComponent = "tFileInputDelimited_1";

				/**
				 * [tFileInputDelimited_1 finally ] stop
				 */

				/**
				 * [tMap_1 finally ] start
				 */

				currentComponent = "tMap_1";

				/**
				 * [tMap_1 finally ] stop
				 */

				/**
				 * [tFileOutputDelimited_1 finally ] start
				 */

				currentComponent = "tFileOutputDelimited_1";

				if (resourceMap.get("finish_tFileOutputDelimited_1") == null) {

					java.io.Writer outtFileOutputDelimited_1 = (java.io.Writer) resourceMap
							.get("out_tFileOutputDelimited_1");
					if (outtFileOutputDelimited_1 != null) {
						outtFileOutputDelimited_1.flush();
						outtFileOutputDelimited_1.close();
					}

				}

				/**
				 * [tFileOutputDelimited_1 finally ] stop
				 */

				/**
				 * [tDie_4 finally ] start
				 */

				currentComponent = "tDie_4";

				/**
				 * [tDie_4 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tFileInputDelimited_1_SUBPROCESS_STATE", 1);
	}

	public void tFTPPut_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tFTPPut_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tFTPPut_1 begin ] start
				 */

				ok_Hash.put("tFTPPut_1", false);
				start_Hash.put("tFTPPut_1", System.currentTimeMillis());

				currentComponent = "tFTPPut_1";

				int tos_count_tFTPPut_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tFTPPut_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tFTPPut_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tFTPPut_1 = new StringBuilder();
							log4jParamters_tFTPPut_1.append("Parameters:");
							log4jParamters_tFTPPut_1.append("USE_EXISTING_CONNECTION" + " = " + "true");
							log4jParamters_tFTPPut_1.append(" | ");
							log4jParamters_tFTPPut_1.append("CONNECTION" + " = " + "tFTPConnection_1");
							log4jParamters_tFTPPut_1.append(" | ");
							log4jParamters_tFTPPut_1
									.append("LOCALDIR" + " = " + " context.g_VAR_TALEND_WORKSPACE + jobName");
							log4jParamters_tFTPPut_1.append(" | ");
							log4jParamters_tFTPPut_1.append("REMOTEDIR" + " = " + "context.l_FTP_OUPUT_PATH");
							log4jParamters_tFTPPut_1.append(" | ");
							log4jParamters_tFTPPut_1.append("CREATE_DIR_IF_NOT_EXIST" + " = " + "false");
							log4jParamters_tFTPPut_1.append(" | ");
							log4jParamters_tFTPPut_1.append("MOVE_TO_THE_CURRENT_DIRECTORY" + " = " + "true");
							log4jParamters_tFTPPut_1.append(" | ");
							log4jParamters_tFTPPut_1.append("MODE" + " = " + "ascii");
							log4jParamters_tFTPPut_1.append(" | ");
							log4jParamters_tFTPPut_1.append("OVERWRITE" + " = " + "never");
							log4jParamters_tFTPPut_1.append(" | ");
							log4jParamters_tFTPPut_1.append("APPEND" + " = " + "false");
							log4jParamters_tFTPPut_1.append(" | ");
							log4jParamters_tFTPPut_1.append("PERL5_REGEX" + " = " + "false");
							log4jParamters_tFTPPut_1.append(" | ");
							log4jParamters_tFTPPut_1.append("FILES" + " = " + "[{FILEMASK="
									+ ("(String)globalMap.get(\"currentFile_Name\")") + ", NEWNAME="
									+ ("StringHandling.CHANGE((String)globalMap.get(\"currentFile_Name\"),\".csv\",\"\")+TalendDate.formatDate(\"YYYYMMDDhhmmss\",TalendDate.getCurrentDate())+\".csv\"")
									+ "}]");
							log4jParamters_tFTPPut_1.append(" | ");
							log4jParamters_tFTPPut_1.append("DIE_ON_ERROR" + " = " + "false");
							log4jParamters_tFTPPut_1.append(" | ");
							log4jParamters_tFTPPut_1.append("IGNORE_FAILURE_AT_QUIT" + " = " + "false");
							log4jParamters_tFTPPut_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tFTPPut_1 - " + (log4jParamters_tFTPPut_1));
						}
					}
					new BytesLimit65535_tFTPPut_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tFTPPut_1", "__UNIQUE_NAME__<br><b>dépot du fichier</b>", "tFTPPut");
					talendJobLogProcess(globalMap);
				}

				int nb_file_tFTPPut_1 = 0;
				// *** ftp *** //
				org.apache.commons.net.ftp.FTPClient ftp_tFTPPut_1 = null;
				ftp_tFTPPut_1 = (org.apache.commons.net.ftp.FTPClient) globalMap.get("conn_tFTPConnection_1");
				if (ftp_tFTPPut_1 != null) {
					log.info("tFTPPut_1 - Use an existing connection. Connection hostname: "
							+ ftp_tFTPPut_1.getRemoteAddress().toString() + ", Connection port: "
							+ ftp_tFTPPut_1.getRemotePort() + ".");
				}
				// msg_tFTPPut_1 likes a String[] to save the message from transfer.
				java.util.List<String> msg_tFTPPut_1 = new java.util.ArrayList<>();

				ftp_tFTPPut_1.setFileType(org.apache.commons.net.ftp.FTP.ASCII_FILE_TYPE);

				String rootDir_tFTPPut_1 = ftp_tFTPPut_1.printWorkingDirectory();
				String remotedir_tFTPPut_1 = (context.l_FTP_OUPUT_PATH).replaceAll("\\\\", "/");
				boolean cwdSuccess_tFTPPut_1 = ftp_tFTPPut_1.changeWorkingDirectory(remotedir_tFTPPut_1);

				if (!cwdSuccess_tFTPPut_1) {
					throw new RuntimeException("Failed to change remote directory. " + ftp_tFTPPut_1.getReplyString());
				}

				java.util.List<java.util.Map<String, String>> listtFTPPut_1 = new java.util.ArrayList<java.util.Map<String, String>>();

				java.util.Map<String, String> maptFTPPut_10 = new java.util.HashMap<String, String>();
				maptFTPPut_10.put((String) globalMap.get("currentFile_Name"),
						StringHandling.CHANGE((String) globalMap.get("currentFile_Name"), ".csv", "")
								+ TalendDate.formatDate("YYYYMMDDhhmmss", TalendDate.getCurrentDate()) + ".csv");
				listtFTPPut_1.add(maptFTPPut_10);
				String localdirtFTPPut_1 = context.g_VAR_TALEND_WORKSPACE + jobName;
				log.info("tFTPPut_1 - Uploading files to the server.");
				for (java.util.Map<String, String> maptFTPPut_1 : listtFTPPut_1) {

					/**
					 * [tFTPPut_1 begin ] stop
					 */

					/**
					 * [tFTPPut_1 main ] start
					 */

					currentComponent = "tFTPPut_1";

					try {
						String currentStatus_tFTPPut_1 = "No file transfered.";
						globalMap.put("tFTPPut_1_CURRENT_STATUS", currentStatus_tFTPPut_1);
						java.util.Set<String> keySettFTPPut_1 = maptFTPPut_1.keySet();

						for (String keytFTPPut_1 : keySettFTPPut_1) {
							if (keytFTPPut_1 == null || "".equals(keytFTPPut_1)) {
								log.error("tFTPPut_1 - file name invalid!");
								System.err.println("file name invalid!");
								continue;
							}
							String tempdirtFTPPut_1 = localdirtFTPPut_1;
							String filemasktFTPPut_1 = keytFTPPut_1;
							String dirtFTPPut_1 = null;
							String masktFTPPut_1 = filemasktFTPPut_1.replaceAll("\\\\", "/");
							int itFTPPut_1 = masktFTPPut_1.lastIndexOf('/');
							if (itFTPPut_1 != -1) {
								dirtFTPPut_1 = masktFTPPut_1.substring(0, itFTPPut_1);
								masktFTPPut_1 = masktFTPPut_1.substring(itFTPPut_1 + 1);
							}
							if (dirtFTPPut_1 != null && !"".equals(dirtFTPPut_1))
								tempdirtFTPPut_1 = tempdirtFTPPut_1 + "/" + dirtFTPPut_1;
							masktFTPPut_1 = masktFTPPut_1.replaceAll("\\.", "\\\\.").replaceAll("\\*", ".*");
							final String finalMasktFTPPut_1 = masktFTPPut_1;
							java.io.File[] listingstFTPPut_1 = null;
							java.io.File filetFTPPut_1 = new java.io.File(tempdirtFTPPut_1);
							if (filetFTPPut_1.isDirectory()) {
								listingstFTPPut_1 = filetFTPPut_1.listFiles(new java.io.FileFilter() {
									public boolean accept(java.io.File pathname) {
										boolean result = false;
										if (pathname != null && pathname.isFile()) {
											result = java.util.regex.Pattern.compile(finalMasktFTPPut_1)
													.matcher(pathname.getName()).find();
										}
										return result;
									}
								});
							}

							java.util.Set<String> remoteExistsFiles_tFTPPut_1 = new java.util.HashSet<>();

							String[] ftpFileNames_tFTPPut_1 = ftp_tFTPPut_1.listNames();
							for (String ftpFileName : ftpFileNames_tFTPPut_1) {
								if ("".equals(maptFTPPut_1.get(keytFTPPut_1))) {
									if (ftpFileName.matches(masktFTPPut_1)) {
										remoteExistsFiles_tFTPPut_1.add(ftpFileName);
									}
								} else {
									if (ftpFileName.matches(maptFTPPut_1.get(keytFTPPut_1))) {
										remoteExistsFiles_tFTPPut_1.add(ftpFileName);
									}
								}
							}

							if (listingstFTPPut_1 != null && listingstFTPPut_1.length > 0) {
								for (int mtFTPPut_1 = 0; mtFTPPut_1 < listingstFTPPut_1.length; mtFTPPut_1++) {
									if (listingstFTPPut_1[mtFTPPut_1].getName().matches(masktFTPPut_1)) {
										java.io.File file_in_localDir_tFTPPut_1 = listingstFTPPut_1[mtFTPPut_1];
										java.io.FileInputStream file_stream_tFTPPut_1 = new java.io.FileInputStream(
												file_in_localDir_tFTPPut_1);

										final String destRename_tFTPPut_1 = maptFTPPut_1.get(keytFTPPut_1);
										final String dest_tFTPPut_1;
										if (destRename_tFTPPut_1 == null || destRename_tFTPPut_1.isEmpty()) {
											dest_tFTPPut_1 = listingstFTPPut_1[mtFTPPut_1].getName();
										} else {
											dest_tFTPPut_1 = destRename_tFTPPut_1;
										}
										globalMap.put("tFTPPut_1_CURRENT_FILE_EXISTS",
												remoteExistsFiles_tFTPPut_1.contains(dest_tFTPPut_1));

										if (!(remoteExistsFiles_tFTPPut_1.contains(dest_tFTPPut_1))) {
											ftp_tFTPPut_1.storeFile(dest_tFTPPut_1, file_stream_tFTPPut_1);
											log.debug(
													"tFTPPut_1 - Uploaded file '" + dest_tFTPPut_1 + "' successfully.");
											globalMap.put("tFTPPut_1_CURRENT_FILE_EXISTS",
													remoteExistsFiles_tFTPPut_1.contains(dest_tFTPPut_1));
											remoteExistsFiles_tFTPPut_1.add(dest_tFTPPut_1);
										}
										file_stream_tFTPPut_1.close();
										msg_tFTPPut_1.add("file: " + file_in_localDir_tFTPPut_1.getAbsolutePath()
												+ ", size: " + file_in_localDir_tFTPPut_1.length()
												+ " bytes upload successfully");
										nb_file_tFTPPut_1++;
									}
								}
							} else {
								log.warn("tFTPPut_1 - No matches found for mask '" + keytFTPPut_1 + "'!");
								System.err.println("No matches found for mask '" + keytFTPPut_1 + "'!");
							}
						}
					} catch (java.lang.Exception e_tFTPPut_1) {
						globalMap.put("tFTPPut_1_ERROR_MESSAGE", e_tFTPPut_1.getMessage());
						msg_tFTPPut_1.add("file not found?: " + e_tFTPPut_1.getMessage());
						log.error("tFTPPut_1 - " + e_tFTPPut_1.getMessage());
						System.err.print(e_tFTPPut_1.getMessage());
					}

					tos_count_tFTPPut_1++;

					/**
					 * [tFTPPut_1 main ] stop
					 */

					/**
					 * [tFTPPut_1 process_data_begin ] start
					 */

					currentComponent = "tFTPPut_1";

					/**
					 * [tFTPPut_1 process_data_begin ] stop
					 */

					/**
					 * [tFTPPut_1 process_data_end ] start
					 */

					currentComponent = "tFTPPut_1";

					/**
					 * [tFTPPut_1 process_data_end ] stop
					 */

					/**
					 * [tFTPPut_1 end ] start
					 */

					currentComponent = "tFTPPut_1";

					// *** ftp *** //
				}

				msg_tFTPPut_1.add(nb_file_tFTPPut_1 + " files have been uploaded.");

				String[] msgAll_tFTPPut_1 = msg_tFTPPut_1.toArray(new String[0]);
				StringBuffer sb_tFTPPut_1 = new StringBuffer();
				if (msgAll_tFTPPut_1 != null) {
					for (String item_tFTPPut_1 : msgAll_tFTPPut_1) {
						sb_tFTPPut_1.append(item_tFTPPut_1).append("\n");
					}
				}
				globalMap.put("tFTPPut_1_TRANSFER_MESSAGES", sb_tFTPPut_1.toString());

				globalMap.put("tFTPPut_1_NB_FILE", nb_file_tFTPPut_1);
				log.info("tFTPPut_1 - Uploaded files count: " + nb_file_tFTPPut_1 + ".");

				if (log.isDebugEnabled())
					log.debug("tFTPPut_1 - " + ("Done."));

				ok_Hash.put("tFTPPut_1", true);
				end_Hash.put("tFTPPut_1", System.currentTimeMillis());

				/**
				 * [tFTPPut_1 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tFTPPut_1 finally ] start
				 */

				currentComponent = "tFTPPut_1";

				/**
				 * [tFTPPut_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tFTPPut_1_SUBPROCESS_STATE", 1);
	}

	public void tDie_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tDie_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tDie_1 begin ] start
				 */

				ok_Hash.put("tDie_1", false);
				start_Hash.put("tDie_1", System.currentTimeMillis());

				currentComponent = "tDie_1";

				int tos_count_tDie_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tDie_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tDie_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tDie_1 = new StringBuilder();
							log4jParamters_tDie_1.append("Parameters:");
							log4jParamters_tDie_1
									.append("MESSAGE" + " = " + "((String)globalMap.get(\"tFTPPut_1_ERROR_MESSAGE\"))");
							log4jParamters_tDie_1.append(" | ");
							log4jParamters_tDie_1.append("CODE" + " = " + "4");
							log4jParamters_tDie_1.append(" | ");
							log4jParamters_tDie_1.append("PRIORITY" + " = " + "5");
							log4jParamters_tDie_1.append(" | ");
							log4jParamters_tDie_1.append("EXIT_JVM" + " = " + "false");
							log4jParamters_tDie_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tDie_1 - " + (log4jParamters_tDie_1));
						}
					}
					new BytesLimit65535_tDie_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tDie_1", "tDie_1", "tDie");
					talendJobLogProcess(globalMap);
				}

				/**
				 * [tDie_1 begin ] stop
				 */

				/**
				 * [tDie_1 main ] start
				 */

				currentComponent = "tDie_1";

				try {
					tLogCatcher_1.addMessage("tDie", "tDie_1", 5, ((String) globalMap.get("tFTPPut_1_ERROR_MESSAGE")),
							4);
					tLogCatcher_1Process(globalMap);

					j_end_exec_1_tLogCatcher_1.addMessage("tDie", "tDie_1", 5,
							((String) globalMap.get("tFTPPut_1_ERROR_MESSAGE")), 4);
					j_end_exec_1_tLogCatcher_1Process(globalMap);

					globalMap.put("tDie_1_DIE_PRIORITY", 5);
					System.err.println(((String) globalMap.get("tFTPPut_1_ERROR_MESSAGE")));

					log.error("tDie_1 - The die message: " + ((String) globalMap.get("tFTPPut_1_ERROR_MESSAGE")));

					globalMap.put("tDie_1_DIE_MESSAGE", ((String) globalMap.get("tFTPPut_1_ERROR_MESSAGE")));
					globalMap.put("tDie_1_DIE_MESSAGES", ((String) globalMap.get("tFTPPut_1_ERROR_MESSAGE")));

				} catch (Exception | Error e_tDie_1) {
					globalMap.put("tDie_1_ERROR_MESSAGE", e_tDie_1.getMessage());
					logIgnoredError(
							String.format("tDie_1 - tDie failed to log message due to internal error: %s", e_tDie_1),
							e_tDie_1);
				}

				currentComponent = "tDie_1";
				status = "failure";
				errorCode = new Integer(4);
				globalMap.put("tDie_1_DIE_CODE", errorCode);

				if (true) {
					throw new TDieException();
				}

				tos_count_tDie_1++;

				/**
				 * [tDie_1 main ] stop
				 */

				/**
				 * [tDie_1 process_data_begin ] start
				 */

				currentComponent = "tDie_1";

				/**
				 * [tDie_1 process_data_begin ] stop
				 */

				/**
				 * [tDie_1 process_data_end ] start
				 */

				currentComponent = "tDie_1";

				/**
				 * [tDie_1 process_data_end ] stop
				 */

				/**
				 * [tDie_1 end ] start
				 */

				currentComponent = "tDie_1";

				if (log.isDebugEnabled())
					log.debug("tDie_1 - " + ("Done."));

				ok_Hash.put("tDie_1", true);
				end_Hash.put("tDie_1", System.currentTimeMillis());

				/**
				 * [tDie_1 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDie_1 finally ] start
				 */

				currentComponent = "tDie_1";

				/**
				 * [tDie_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDie_1_SUBPROCESS_STATE", 1);
	}

	public void tDie_3Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tDie_3_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tDie_3 begin ] start
				 */

				ok_Hash.put("tDie_3", false);
				start_Hash.put("tDie_3", System.currentTimeMillis());

				currentComponent = "tDie_3";

				int tos_count_tDie_3 = 0;

				if (log.isDebugEnabled())
					log.debug("tDie_3 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tDie_3 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tDie_3 = new StringBuilder();
							log4jParamters_tDie_3.append("Parameters:");
							log4jParamters_tDie_3.append("MESSAGE" + " = "
									+ "((String)globalMap.get(\"tFileOutputDelimited_1_ERROR_MESSAGE\"))");
							log4jParamters_tDie_3.append(" | ");
							log4jParamters_tDie_3.append("CODE" + " = " + "4");
							log4jParamters_tDie_3.append(" | ");
							log4jParamters_tDie_3.append("PRIORITY" + " = " + "5");
							log4jParamters_tDie_3.append(" | ");
							log4jParamters_tDie_3.append("EXIT_JVM" + " = " + "false");
							log4jParamters_tDie_3.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tDie_3 - " + (log4jParamters_tDie_3));
						}
					}
					new BytesLimit65535_tDie_3().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tDie_3", "tDie_3", "tDie");
					talendJobLogProcess(globalMap);
				}

				/**
				 * [tDie_3 begin ] stop
				 */

				/**
				 * [tDie_3 main ] start
				 */

				currentComponent = "tDie_3";

				try {
					tLogCatcher_1.addMessage("tDie", "tDie_3", 5,
							((String) globalMap.get("tFileOutputDelimited_1_ERROR_MESSAGE")), 4);
					tLogCatcher_1Process(globalMap);

					j_end_exec_1_tLogCatcher_1.addMessage("tDie", "tDie_3", 5,
							((String) globalMap.get("tFileOutputDelimited_1_ERROR_MESSAGE")), 4);
					j_end_exec_1_tLogCatcher_1Process(globalMap);

					globalMap.put("tDie_3_DIE_PRIORITY", 5);
					System.err.println(((String) globalMap.get("tFileOutputDelimited_1_ERROR_MESSAGE")));

					log.error("tDie_3 - The die message: "
							+ ((String) globalMap.get("tFileOutputDelimited_1_ERROR_MESSAGE")));

					globalMap.put("tDie_3_DIE_MESSAGE",
							((String) globalMap.get("tFileOutputDelimited_1_ERROR_MESSAGE")));
					globalMap.put("tDie_3_DIE_MESSAGES",
							((String) globalMap.get("tFileOutputDelimited_1_ERROR_MESSAGE")));

				} catch (Exception | Error e_tDie_3) {
					globalMap.put("tDie_3_ERROR_MESSAGE", e_tDie_3.getMessage());
					logIgnoredError(
							String.format("tDie_3 - tDie failed to log message due to internal error: %s", e_tDie_3),
							e_tDie_3);
				}

				currentComponent = "tDie_3";
				status = "failure";
				errorCode = new Integer(4);
				globalMap.put("tDie_3_DIE_CODE", errorCode);

				if (true) {
					throw new TDieException();
				}

				tos_count_tDie_3++;

				/**
				 * [tDie_3 main ] stop
				 */

				/**
				 * [tDie_3 process_data_begin ] start
				 */

				currentComponent = "tDie_3";

				/**
				 * [tDie_3 process_data_begin ] stop
				 */

				/**
				 * [tDie_3 process_data_end ] start
				 */

				currentComponent = "tDie_3";

				/**
				 * [tDie_3 process_data_end ] stop
				 */

				/**
				 * [tDie_3 end ] start
				 */

				currentComponent = "tDie_3";

				if (log.isDebugEnabled())
					log.debug("tDie_3 - " + ("Done."));

				ok_Hash.put("tDie_3", true);
				end_Hash.put("tDie_3", System.currentTimeMillis());

				/**
				 * [tDie_3 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDie_3 finally ] start
				 */

				currentComponent = "tDie_3";

				/**
				 * [tDie_3 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDie_3_SUBPROCESS_STATE", 1);
	}

	public void tDie_2Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tDie_2_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tDie_2 begin ] start
				 */

				ok_Hash.put("tDie_2", false);
				start_Hash.put("tDie_2", System.currentTimeMillis());

				currentComponent = "tDie_2";

				int tos_count_tDie_2 = 0;

				if (log.isDebugEnabled())
					log.debug("tDie_2 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tDie_2 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tDie_2 = new StringBuilder();
							log4jParamters_tDie_2.append("Parameters:");
							log4jParamters_tDie_2.append("MESSAGE" + " = "
									+ "((String)globalMap.get(\"tFileInputDelimited_1_ERROR_MESSAGE\"))");
							log4jParamters_tDie_2.append(" | ");
							log4jParamters_tDie_2.append("CODE" + " = " + "4");
							log4jParamters_tDie_2.append(" | ");
							log4jParamters_tDie_2.append("PRIORITY" + " = " + "5");
							log4jParamters_tDie_2.append(" | ");
							log4jParamters_tDie_2.append("EXIT_JVM" + " = " + "false");
							log4jParamters_tDie_2.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tDie_2 - " + (log4jParamters_tDie_2));
						}
					}
					new BytesLimit65535_tDie_2().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tDie_2", "tDie_2", "tDie");
					talendJobLogProcess(globalMap);
				}

				/**
				 * [tDie_2 begin ] stop
				 */

				/**
				 * [tDie_2 main ] start
				 */

				currentComponent = "tDie_2";

				try {
					tLogCatcher_1.addMessage("tDie", "tDie_2", 5,
							((String) globalMap.get("tFileInputDelimited_1_ERROR_MESSAGE")), 4);
					tLogCatcher_1Process(globalMap);

					j_end_exec_1_tLogCatcher_1.addMessage("tDie", "tDie_2", 5,
							((String) globalMap.get("tFileInputDelimited_1_ERROR_MESSAGE")), 4);
					j_end_exec_1_tLogCatcher_1Process(globalMap);

					globalMap.put("tDie_2_DIE_PRIORITY", 5);
					System.err.println(((String) globalMap.get("tFileInputDelimited_1_ERROR_MESSAGE")));

					log.error("tDie_2 - The die message: "
							+ ((String) globalMap.get("tFileInputDelimited_1_ERROR_MESSAGE")));

					globalMap.put("tDie_2_DIE_MESSAGE",
							((String) globalMap.get("tFileInputDelimited_1_ERROR_MESSAGE")));
					globalMap.put("tDie_2_DIE_MESSAGES",
							((String) globalMap.get("tFileInputDelimited_1_ERROR_MESSAGE")));

				} catch (Exception | Error e_tDie_2) {
					globalMap.put("tDie_2_ERROR_MESSAGE", e_tDie_2.getMessage());
					logIgnoredError(
							String.format("tDie_2 - tDie failed to log message due to internal error: %s", e_tDie_2),
							e_tDie_2);
				}

				currentComponent = "tDie_2";
				status = "failure";
				errorCode = new Integer(4);
				globalMap.put("tDie_2_DIE_CODE", errorCode);

				if (true) {
					throw new TDieException();
				}

				tos_count_tDie_2++;

				/**
				 * [tDie_2 main ] stop
				 */

				/**
				 * [tDie_2 process_data_begin ] start
				 */

				currentComponent = "tDie_2";

				/**
				 * [tDie_2 process_data_begin ] stop
				 */

				/**
				 * [tDie_2 process_data_end ] start
				 */

				currentComponent = "tDie_2";

				/**
				 * [tDie_2 process_data_end ] stop
				 */

				/**
				 * [tDie_2 end ] start
				 */

				currentComponent = "tDie_2";

				if (log.isDebugEnabled())
					log.debug("tDie_2 - " + ("Done."));

				ok_Hash.put("tDie_2", true);
				end_Hash.put("tDie_2", System.currentTimeMillis());

				/**
				 * [tDie_2 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDie_2 finally ] start
				 */

				currentComponent = "tDie_2";

				/**
				 * [tDie_2 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDie_2_SUBPROCESS_STATE", 1);
	}

	public static class j_catch_unexpected_error_1_toErrorStruct
			implements routines.system.IPersistableRow<j_catch_unexpected_error_1_toErrorStruct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public String jobname;

		public String getJobname() {
			return this.jobname;
		}

		public String guid;

		public String getGuid() {
			return this.guid;
		}

		public String message;

		public String getMessage() {
			return this.message;
		}

		public String filename;

		public String getFilename() {
			return this.filename;
		}

		public String filepath;

		public String getFilepath() {
			return this.filepath;
		}

		public Boolean send_alert;

		public Boolean getSend_alert() {
			return this.send_alert;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.jobname = readString(dis);

					this.guid = readString(dis);

					this.message = readString(dis);

					this.filename = readString(dis);

					this.filepath = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.send_alert = null;
					} else {
						this.send_alert = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.jobname = readString(dis);

					this.guid = readString(dis);

					this.message = readString(dis);

					this.filename = readString(dis);

					this.filepath = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.send_alert = null;
					} else {
						this.send_alert = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.jobname, dos);

				// String

				writeString(this.guid, dos);

				// String

				writeString(this.message, dos);

				// String

				writeString(this.filename, dos);

				// String

				writeString(this.filepath, dos);

				// Boolean

				if (this.send_alert == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.send_alert);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// String

				writeString(this.jobname, dos);

				// String

				writeString(this.guid, dos);

				// String

				writeString(this.message, dos);

				// String

				writeString(this.filename, dos);

				// String

				writeString(this.filepath, dos);

				// Boolean

				if (this.send_alert == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.send_alert);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("jobname=" + jobname);
			sb.append(",guid=" + guid);
			sb.append(",message=" + message);
			sb.append(",filename=" + filename);
			sb.append(",filepath=" + filepath);
			sb.append(",send_alert=" + String.valueOf(send_alert));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (jobname == null) {
				sb.append("<null>");
			} else {
				sb.append(jobname);
			}

			sb.append("|");

			if (guid == null) {
				sb.append("<null>");
			} else {
				sb.append(guid);
			}

			sb.append("|");

			if (message == null) {
				sb.append("<null>");
			} else {
				sb.append(message);
			}

			sb.append("|");

			if (filename == null) {
				sb.append("<null>");
			} else {
				sb.append(filename);
			}

			sb.append("|");

			if (filepath == null) {
				sb.append("<null>");
			} else {
				sb.append(filepath);
			}

			sb.append("|");

			if (send_alert == null) {
				sb.append("<null>");
			} else {
				sb.append(send_alert);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(j_catch_unexpected_error_1_toErrorStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row3Struct implements routines.system.IPersistableRow<row3Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public java.util.Date moment;

		public java.util.Date getMoment() {
			return this.moment;
		}

		public String pid;

		public String getPid() {
			return this.pid;
		}

		public String root_pid;

		public String getRoot_pid() {
			return this.root_pid;
		}

		public String father_pid;

		public String getFather_pid() {
			return this.father_pid;
		}

		public String project;

		public String getProject() {
			return this.project;
		}

		public String job;

		public String getJob() {
			return this.job;
		}

		public String context;

		public String getContext() {
			return this.context;
		}

		public Integer priority;

		public Integer getPriority() {
			return this.priority;
		}

		public String type;

		public String getType() {
			return this.type;
		}

		public String origin;

		public String getOrigin() {
			return this.origin;
		}

		public String message;

		public String getMessage() {
			return this.message;
		}

		public Integer code;

		public Integer getCode() {
			return this.code;
		}

		private java.util.Date readDate(ObjectInputStream dis) throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = unmarshaller.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(unmarshaller.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (date1 == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeLong(date1.getTime());
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.moment = readDate(dis);

					this.pid = readString(dis);

					this.root_pid = readString(dis);

					this.father_pid = readString(dis);

					this.project = readString(dis);

					this.job = readString(dis);

					this.context = readString(dis);

					this.priority = readInteger(dis);

					this.type = readString(dis);

					this.origin = readString(dis);

					this.message = readString(dis);

					this.code = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.moment = readDate(dis);

					this.pid = readString(dis);

					this.root_pid = readString(dis);

					this.father_pid = readString(dis);

					this.project = readString(dis);

					this.job = readString(dis);

					this.context = readString(dis);

					this.priority = readInteger(dis);

					this.type = readString(dis);

					this.origin = readString(dis);

					this.message = readString(dis);

					this.code = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// java.util.Date

				writeDate(this.moment, dos);

				// String

				writeString(this.pid, dos);

				// String

				writeString(this.root_pid, dos);

				// String

				writeString(this.father_pid, dos);

				// String

				writeString(this.project, dos);

				// String

				writeString(this.job, dos);

				// String

				writeString(this.context, dos);

				// Integer

				writeInteger(this.priority, dos);

				// String

				writeString(this.type, dos);

				// String

				writeString(this.origin, dos);

				// String

				writeString(this.message, dos);

				// Integer

				writeInteger(this.code, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// java.util.Date

				writeDate(this.moment, dos);

				// String

				writeString(this.pid, dos);

				// String

				writeString(this.root_pid, dos);

				// String

				writeString(this.father_pid, dos);

				// String

				writeString(this.project, dos);

				// String

				writeString(this.job, dos);

				// String

				writeString(this.context, dos);

				// Integer

				writeInteger(this.priority, dos);

				// String

				writeString(this.type, dos);

				// String

				writeString(this.origin, dos);

				// String

				writeString(this.message, dos);

				// Integer

				writeInteger(this.code, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("moment=" + String.valueOf(moment));
			sb.append(",pid=" + pid);
			sb.append(",root_pid=" + root_pid);
			sb.append(",father_pid=" + father_pid);
			sb.append(",project=" + project);
			sb.append(",job=" + job);
			sb.append(",context=" + context);
			sb.append(",priority=" + String.valueOf(priority));
			sb.append(",type=" + type);
			sb.append(",origin=" + origin);
			sb.append(",message=" + message);
			sb.append(",code=" + String.valueOf(code));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (moment == null) {
				sb.append("<null>");
			} else {
				sb.append(moment);
			}

			sb.append("|");

			if (pid == null) {
				sb.append("<null>");
			} else {
				sb.append(pid);
			}

			sb.append("|");

			if (root_pid == null) {
				sb.append("<null>");
			} else {
				sb.append(root_pid);
			}

			sb.append("|");

			if (father_pid == null) {
				sb.append("<null>");
			} else {
				sb.append(father_pid);
			}

			sb.append("|");

			if (project == null) {
				sb.append("<null>");
			} else {
				sb.append(project);
			}

			sb.append("|");

			if (job == null) {
				sb.append("<null>");
			} else {
				sb.append(job);
			}

			sb.append("|");

			if (context == null) {
				sb.append("<null>");
			} else {
				sb.append(context);
			}

			sb.append("|");

			if (priority == null) {
				sb.append("<null>");
			} else {
				sb.append(priority);
			}

			sb.append("|");

			if (type == null) {
				sb.append("<null>");
			} else {
				sb.append(type);
			}

			sb.append("|");

			if (origin == null) {
				sb.append("<null>");
			} else {
				sb.append(origin);
			}

			sb.append("|");

			if (message == null) {
				sb.append("<null>");
			} else {
				sb.append(message);
			}

			sb.append("|");

			if (code == null) {
				sb.append("<null>");
			} else {
				sb.append(code);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row3Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tLogCatcher_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tLogCatcher_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row3Struct row3 = new row3Struct();
				j_catch_unexpected_error_1_toErrorStruct j_catch_unexpected_error_1_toError = new j_catch_unexpected_error_1_toErrorStruct();

				/**
				 * [j_catch_unexpected_error_1_tHashOutput_1 begin ] start
				 */

				ok_Hash.put("j_catch_unexpected_error_1_tHashOutput_1", false);
				start_Hash.put("j_catch_unexpected_error_1_tHashOutput_1", System.currentTimeMillis());

				currentComponent = "j_catch_unexpected_error_1_tHashOutput_1";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0,
						"j_catch_unexpected_error_1_toError");

				int tos_count_j_catch_unexpected_error_1_tHashOutput_1 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("j_catch_unexpected_error_1_tHashOutput_1", "__UNIQUE_NAME__<br><b>errors</b>",
							"tHashOutput");
					talendJobLogProcess(globalMap);
				}

				org.talend.designer.components.hashfile.common.MapHashFile mf_j_catch_unexpected_error_1_tHashOutput_1 = org.talend.designer.components.hashfile.common.MapHashFile
						.getMapHashFile();
				org.talend.designer.components.hashfile.memory.AdvancedMemoryHashFile<j_catch_unexpected_error_1_toErrorStruct> tHashFile_j_catch_unexpected_error_1_tHashOutput_1 = null;
				String hashKey_j_catch_unexpected_error_1_tHashOutput_1 = "tHashFile_j_confirmation_odp_" + pid
						+ "_j_catch_unexpected_error_1_tHashOutput_1";
				synchronized (org.talend.designer.components.hashfile.common.MapHashFile.resourceLockMap
						.get(hashKey_j_catch_unexpected_error_1_tHashOutput_1)) {
					if (mf_j_catch_unexpected_error_1_tHashOutput_1.getResourceMap()
							.get(hashKey_j_catch_unexpected_error_1_tHashOutput_1) == null) {
						mf_j_catch_unexpected_error_1_tHashOutput_1.getResourceMap().put(
								hashKey_j_catch_unexpected_error_1_tHashOutput_1,
								new org.talend.designer.components.hashfile.memory.AdvancedMemoryHashFile<j_catch_unexpected_error_1_toErrorStruct>(
										org.talend.designer.components.hashfile.common.MATCHING_MODE.KEEP_ALL));
						tHashFile_j_catch_unexpected_error_1_tHashOutput_1 = mf_j_catch_unexpected_error_1_tHashOutput_1
								.getResourceMap().get(hashKey_j_catch_unexpected_error_1_tHashOutput_1);
					} else {
						tHashFile_j_catch_unexpected_error_1_tHashOutput_1 = mf_j_catch_unexpected_error_1_tHashOutput_1
								.getResourceMap().get(hashKey_j_catch_unexpected_error_1_tHashOutput_1);
					}
				}
				int nb_line_j_catch_unexpected_error_1_tHashOutput_1 = 0;

				/**
				 * [j_catch_unexpected_error_1_tHashOutput_1 begin ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tMap_2 begin ] start
				 */

				ok_Hash.put("j_catch_unexpected_error_1_tMap_2", false);
				start_Hash.put("j_catch_unexpected_error_1_tMap_2", System.currentTimeMillis());

				currentComponent = "j_catch_unexpected_error_1_tMap_2";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "row3");

				int tos_count_j_catch_unexpected_error_1_tMap_2 = 0;

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tMap_2 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_catch_unexpected_error_1_tMap_2 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_catch_unexpected_error_1_tMap_2 = new StringBuilder();
							log4jParamters_j_catch_unexpected_error_1_tMap_2.append("Parameters:");
							log4jParamters_j_catch_unexpected_error_1_tMap_2.append("LINK_STYLE" + " = " + "AUTO");
							log4jParamters_j_catch_unexpected_error_1_tMap_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tMap_2
									.append("TEMPORARY_DATA_DIRECTORY" + " = " + "");
							log4jParamters_j_catch_unexpected_error_1_tMap_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tMap_2
									.append("ROWS_BUFFER_SIZE" + " = " + "2000000");
							log4jParamters_j_catch_unexpected_error_1_tMap_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tMap_2
									.append("CHANGE_HASH_AND_EQUALS_FOR_BIGDECIMAL" + " = " + "true");
							log4jParamters_j_catch_unexpected_error_1_tMap_2.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_catch_unexpected_error_1_tMap_2 - "
										+ (log4jParamters_j_catch_unexpected_error_1_tMap_2));
						}
					}
					new BytesLimit65535_j_catch_unexpected_error_1_tMap_2().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_catch_unexpected_error_1_tMap_2", "j_catch_unexpected_error_1_tMap_2",
							"tMap");
					talendJobLogProcess(globalMap);
				}

// ###############################
// # Lookup's keys initialization
				int count_row3_j_catch_unexpected_error_1_tMap_2 = 0;

// ###############################        

// ###############################
// # Vars initialization
				class Var__j_catch_unexpected_error_1_tMap_2__Struct {
					String fileName;
					String filePath;
				}
				Var__j_catch_unexpected_error_1_tMap_2__Struct Var__j_catch_unexpected_error_1_tMap_2 = new Var__j_catch_unexpected_error_1_tMap_2__Struct();
// ###############################

// ###############################
// # Outputs initialization
				int count_j_catch_unexpected_error_1_toError_j_catch_unexpected_error_1_tMap_2 = 0;

				j_catch_unexpected_error_1_toErrorStruct j_catch_unexpected_error_1_toError_tmp = new j_catch_unexpected_error_1_toErrorStruct();
// ###############################

				/**
				 * [j_catch_unexpected_error_1_tMap_2 begin ] stop
				 */

				/**
				 * [tLogCatcher_1 begin ] start
				 */

				ok_Hash.put("tLogCatcher_1", false);
				start_Hash.put("tLogCatcher_1", System.currentTimeMillis());

				currentComponent = "tLogCatcher_1";

				int tos_count_tLogCatcher_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tLogCatcher_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tLogCatcher_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tLogCatcher_1 = new StringBuilder();
							log4jParamters_tLogCatcher_1.append("Parameters:");
							log4jParamters_tLogCatcher_1.append("CATCH_JAVA_EXCEPTION" + " = " + "true");
							log4jParamters_tLogCatcher_1.append(" | ");
							log4jParamters_tLogCatcher_1.append("CATCH_TDIE" + " = " + "true");
							log4jParamters_tLogCatcher_1.append(" | ");
							log4jParamters_tLogCatcher_1.append("CATCH_TWARN" + " = " + "true");
							log4jParamters_tLogCatcher_1.append(" | ");
							log4jParamters_tLogCatcher_1.append("CATCH_TACTIONFAILURE" + " = " + "true");
							log4jParamters_tLogCatcher_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tLogCatcher_1 - " + (log4jParamters_tLogCatcher_1));
						}
					}
					new BytesLimit65535_tLogCatcher_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tLogCatcher_1", "tLogCatcher_1", "tLogCatcher");
					talendJobLogProcess(globalMap);
				}

				try {
					for (LogCatcherUtils.LogCatcherMessage lcm : tLogCatcher_1.getMessages()) {
						row3.type = lcm.getType();
						row3.origin = (lcm.getOrigin() == null || lcm.getOrigin().length() < 1 ? null
								: lcm.getOrigin());
						row3.priority = lcm.getPriority();
						row3.message = lcm.getMessage();
						row3.code = lcm.getCode();

						row3.moment = java.util.Calendar.getInstance().getTime();

						row3.pid = pid;
						row3.root_pid = rootPid;
						row3.father_pid = fatherPid;

						row3.project = projectName;
						row3.job = jobName;
						row3.context = contextStr;

						/**
						 * [tLogCatcher_1 begin ] stop
						 */

						/**
						 * [tLogCatcher_1 main ] start
						 */

						currentComponent = "tLogCatcher_1";

						tos_count_tLogCatcher_1++;

						/**
						 * [tLogCatcher_1 main ] stop
						 */

						/**
						 * [tLogCatcher_1 process_data_begin ] start
						 */

						currentComponent = "tLogCatcher_1";

						/**
						 * [tLogCatcher_1 process_data_begin ] stop
						 */

						/**
						 * [j_catch_unexpected_error_1_tMap_2 main ] start
						 */

						currentComponent = "j_catch_unexpected_error_1_tMap_2";

						if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

								, "row3", "tLogCatcher_1", "tLogCatcher_1", "tLogCatcher",
								"j_catch_unexpected_error_1_tMap_2", "j_catch_unexpected_error_1_tMap_2", "tMap"

						)) {
							talendJobLogProcess(globalMap);
						}

						if (log.isTraceEnabled()) {
							log.trace("row3 - " + (row3 == null ? "" : row3.toLogString()));
						}

						boolean hasCasePrimitiveKeyWithNull_j_catch_unexpected_error_1_tMap_2 = false;

						// ###############################
						// # Input tables (lookups)
						boolean rejectedInnerJoin_j_catch_unexpected_error_1_tMap_2 = false;
						boolean mainRowRejected_j_catch_unexpected_error_1_tMap_2 = false;

						// ###############################
						{ // start of Var scope

							// ###############################
							// # Vars tables

							Var__j_catch_unexpected_error_1_tMap_2__Struct Var = Var__j_catch_unexpected_error_1_tMap_2;
							Var.fileName = ("".equals((String) globalMap.get("currentFile_Name"))
									|| (String) globalMap.get("currentFile_Name") == null ? ""
											: (String) globalMap.get("currentFile_Name"));
							Var.filePath = ("".equals((String) globalMap.get("currentFile_Path"))
									|| (String) globalMap.get("currentFile_Path") == null ? ""
											: (String) globalMap.get("currentFile_Path"));// ###############################
																							// ###############################
																							// # Output tables

							j_catch_unexpected_error_1_toError = null;

// # Output table : 'j_catch_unexpected_error_1_toError'
							count_j_catch_unexpected_error_1_toError_j_catch_unexpected_error_1_tMap_2++;

							j_catch_unexpected_error_1_toError_tmp.jobname = jobName;
							j_catch_unexpected_error_1_toError_tmp.guid = (String) globalMap.get("guid");
							j_catch_unexpected_error_1_toError_tmp.message = row3.message;
							j_catch_unexpected_error_1_toError_tmp.filename = Var.fileName;
							j_catch_unexpected_error_1_toError_tmp.filepath = Var.filePath;
							j_catch_unexpected_error_1_toError_tmp.send_alert = true;
							j_catch_unexpected_error_1_toError = j_catch_unexpected_error_1_toError_tmp;
							log.debug("j_catch_unexpected_error_1_tMap_2 - Outputting the record "
									+ count_j_catch_unexpected_error_1_toError_j_catch_unexpected_error_1_tMap_2
									+ " of the output table 'j_catch_unexpected_error_1_toError'.");

// ###############################

						} // end of Var scope

						rejectedInnerJoin_j_catch_unexpected_error_1_tMap_2 = false;

						tos_count_j_catch_unexpected_error_1_tMap_2++;

						/**
						 * [j_catch_unexpected_error_1_tMap_2 main ] stop
						 */

						/**
						 * [j_catch_unexpected_error_1_tMap_2 process_data_begin ] start
						 */

						currentComponent = "j_catch_unexpected_error_1_tMap_2";

						/**
						 * [j_catch_unexpected_error_1_tMap_2 process_data_begin ] stop
						 */
// Start of branch "j_catch_unexpected_error_1_toError"
						if (j_catch_unexpected_error_1_toError != null) {

							/**
							 * [j_catch_unexpected_error_1_tHashOutput_1 main ] start
							 */

							currentComponent = "j_catch_unexpected_error_1_tHashOutput_1";

							if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

									, "j_catch_unexpected_error_1_toError", "j_catch_unexpected_error_1_tMap_2",
									"j_catch_unexpected_error_1_tMap_2", "tMap",
									"j_catch_unexpected_error_1_tHashOutput_1", "__UNIQUE_NAME__<br><b>errors</b>",
									"tHashOutput"

							)) {
								talendJobLogProcess(globalMap);
							}

							if (log.isTraceEnabled()) {
								log.trace("j_catch_unexpected_error_1_toError - "
										+ (j_catch_unexpected_error_1_toError == null ? ""
												: j_catch_unexpected_error_1_toError.toLogString()));
							}

							j_catch_unexpected_error_1_toErrorStruct oneRow_j_catch_unexpected_error_1_tHashOutput_1 = new j_catch_unexpected_error_1_toErrorStruct();

							oneRow_j_catch_unexpected_error_1_tHashOutput_1.jobname = j_catch_unexpected_error_1_toError.jobname;
							oneRow_j_catch_unexpected_error_1_tHashOutput_1.guid = j_catch_unexpected_error_1_toError.guid;
							oneRow_j_catch_unexpected_error_1_tHashOutput_1.message = j_catch_unexpected_error_1_toError.message;
							oneRow_j_catch_unexpected_error_1_tHashOutput_1.filename = j_catch_unexpected_error_1_toError.filename;
							oneRow_j_catch_unexpected_error_1_tHashOutput_1.filepath = j_catch_unexpected_error_1_toError.filepath;
							oneRow_j_catch_unexpected_error_1_tHashOutput_1.send_alert = j_catch_unexpected_error_1_toError.send_alert;

							tHashFile_j_catch_unexpected_error_1_tHashOutput_1
									.put(oneRow_j_catch_unexpected_error_1_tHashOutput_1);
							nb_line_j_catch_unexpected_error_1_tHashOutput_1++;

							tos_count_j_catch_unexpected_error_1_tHashOutput_1++;

							/**
							 * [j_catch_unexpected_error_1_tHashOutput_1 main ] stop
							 */

							/**
							 * [j_catch_unexpected_error_1_tHashOutput_1 process_data_begin ] start
							 */

							currentComponent = "j_catch_unexpected_error_1_tHashOutput_1";

							/**
							 * [j_catch_unexpected_error_1_tHashOutput_1 process_data_begin ] stop
							 */

							/**
							 * [j_catch_unexpected_error_1_tHashOutput_1 process_data_end ] start
							 */

							currentComponent = "j_catch_unexpected_error_1_tHashOutput_1";

							/**
							 * [j_catch_unexpected_error_1_tHashOutput_1 process_data_end ] stop
							 */

						} // End of branch "j_catch_unexpected_error_1_toError"

						/**
						 * [j_catch_unexpected_error_1_tMap_2 process_data_end ] start
						 */

						currentComponent = "j_catch_unexpected_error_1_tMap_2";

						/**
						 * [j_catch_unexpected_error_1_tMap_2 process_data_end ] stop
						 */

						/**
						 * [tLogCatcher_1 process_data_end ] start
						 */

						currentComponent = "tLogCatcher_1";

						/**
						 * [tLogCatcher_1 process_data_end ] stop
						 */

						/**
						 * [tLogCatcher_1 end ] start
						 */

						currentComponent = "tLogCatcher_1";

					}
				} catch (Exception e_tLogCatcher_1) {
					globalMap.put("tLogCatcher_1_ERROR_MESSAGE", e_tLogCatcher_1.getMessage());
					logIgnoredError(String.format(
							"tLogCatcher_1 - tLogCatcher failed to process log message(s) due to internal error: %s",
							e_tLogCatcher_1), e_tLogCatcher_1);
				}

				if (log.isDebugEnabled())
					log.debug("tLogCatcher_1 - " + ("Done."));

				ok_Hash.put("tLogCatcher_1", true);
				end_Hash.put("tLogCatcher_1", System.currentTimeMillis());

				/**
				 * [tLogCatcher_1 end ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tMap_2 end ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tMap_2";

// ###############################
// # Lookup hashes releasing
// ###############################      
				log.debug(
						"j_catch_unexpected_error_1_tMap_2 - Written records count in the table 'j_catch_unexpected_error_1_toError': "
								+ count_j_catch_unexpected_error_1_toError_j_catch_unexpected_error_1_tMap_2 + ".");

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "row3", 2, 0,
						"tLogCatcher_1", "tLogCatcher_1", "tLogCatcher", "j_catch_unexpected_error_1_tMap_2",
						"j_catch_unexpected_error_1_tMap_2", "tMap", "output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tMap_2 - " + ("Done."));

				ok_Hash.put("j_catch_unexpected_error_1_tMap_2", true);
				end_Hash.put("j_catch_unexpected_error_1_tMap_2", System.currentTimeMillis());

				/**
				 * [j_catch_unexpected_error_1_tMap_2 end ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tHashOutput_1 end ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tHashOutput_1";

				globalMap.put("j_catch_unexpected_error_1_tHashOutput_1_NB_LINE",
						nb_line_j_catch_unexpected_error_1_tHashOutput_1);
				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId,
						"j_catch_unexpected_error_1_toError", 2, 0, "j_catch_unexpected_error_1_tMap_2",
						"j_catch_unexpected_error_1_tMap_2", "tMap", "j_catch_unexpected_error_1_tHashOutput_1",
						"__UNIQUE_NAME__<br><b>errors</b>", "tHashOutput", "output")) {
					talendJobLogProcess(globalMap);
				}

				ok_Hash.put("j_catch_unexpected_error_1_tHashOutput_1", true);
				end_Hash.put("j_catch_unexpected_error_1_tHashOutput_1", System.currentTimeMillis());

				if (execStat) {
					runStat.updateStatOnConnection("j_catch_unexpected_error_1_OnComponentOk1", 0, "ok");
				}
				j_catch_unexpected_error_1_tDBConnection_1Process(globalMap);

				/**
				 * [j_catch_unexpected_error_1_tHashOutput_1 end ] stop
				 */

			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tLogCatcher_1 finally ] start
				 */

				currentComponent = "tLogCatcher_1";

				/**
				 * [tLogCatcher_1 finally ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tMap_2 finally ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tMap_2";

				/**
				 * [j_catch_unexpected_error_1_tMap_2 finally ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tHashOutput_1 finally ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tHashOutput_1";

				/**
				 * [j_catch_unexpected_error_1_tHashOutput_1 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tLogCatcher_1_SUBPROCESS_STATE", 1);
	}

	public void j_catch_unexpected_error_1_tDBConnection_1Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("j_catch_unexpected_error_1_tDBConnection_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [j_catch_unexpected_error_1_tDBConnection_1 begin ] start
				 */

				ok_Hash.put("j_catch_unexpected_error_1_tDBConnection_1", false);
				start_Hash.put("j_catch_unexpected_error_1_tDBConnection_1", System.currentTimeMillis());

				currentComponent = "j_catch_unexpected_error_1_tDBConnection_1";

				int tos_count_j_catch_unexpected_error_1_tDBConnection_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBConnection_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_catch_unexpected_error_1_tDBConnection_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_catch_unexpected_error_1_tDBConnection_1 = new StringBuilder();
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1.append("Parameters:");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1
									.append("DB_VERSION" + " = " + "V9_X");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1
									.append("HOST" + " = " + "context.g_BDD_OMNICANAL_HOST");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1
									.append("PORT" + " = " + "context.g_BDD_OMNICANAL_PORT");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1
									.append("DBNAME" + " = " + "context.g_BDD_OMNICANAL_DBNAME");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1
									.append("SCHEMA_DB" + " = " + "context.g_BDD_OMNICANAL_SCHEMA");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1
									.append("USER" + " = " + "context.g_BDD_OMNICANAL_USERNAME");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1
									.append("PASS" + " = "
											+ String.valueOf(routines.system.PasswordEncryptUtil
													.encryptPassword(context.g_BDD_OMNICANAL_PWD)).substring(0, 4)
											+ "...");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1
									.append("USE_SHARED_CONNECTION" + " = " + "true");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1
									.append("SHARED_CONNECTION_NAME" + " = " + "\"BDD_OMNICANAL\"");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1
									.append("PROPERTIES" + " = " + "\"noDatetimeStringSync=true\"");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1
									.append("AUTO_COMMIT" + " = " + "true");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlConnection");
							log4jParamters_j_catch_unexpected_error_1_tDBConnection_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_catch_unexpected_error_1_tDBConnection_1 - "
										+ (log4jParamters_j_catch_unexpected_error_1_tDBConnection_1));
						}
					}
					new BytesLimit65535_j_catch_unexpected_error_1_tDBConnection_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_catch_unexpected_error_1_tDBConnection_1",
							"__UNIQUE_NAME__<br><b>BDD_OMNICANAL</b><br><i>CommitAuto</i>", "tPostgresqlConnection");
					talendJobLogProcess(globalMap);
				}

				String dbProperties_j_catch_unexpected_error_1_tDBConnection_1 = "noDatetimeStringSync=true";
				String url_j_catch_unexpected_error_1_tDBConnection_1 = "jdbc:postgresql://"
						+ context.g_BDD_OMNICANAL_HOST + ":" + context.g_BDD_OMNICANAL_PORT + "/"
						+ context.g_BDD_OMNICANAL_DBNAME;

				if (dbProperties_j_catch_unexpected_error_1_tDBConnection_1 != null
						&& !"".equals(dbProperties_j_catch_unexpected_error_1_tDBConnection_1.trim())) {
					url_j_catch_unexpected_error_1_tDBConnection_1 = url_j_catch_unexpected_error_1_tDBConnection_1
							+ "?" + dbProperties_j_catch_unexpected_error_1_tDBConnection_1;
				}
				String dbUser_j_catch_unexpected_error_1_tDBConnection_1 = context.g_BDD_OMNICANAL_USERNAME;

				final String decryptedPassword_j_catch_unexpected_error_1_tDBConnection_1 = context.g_BDD_OMNICANAL_PWD;
				String dbPwd_j_catch_unexpected_error_1_tDBConnection_1 = decryptedPassword_j_catch_unexpected_error_1_tDBConnection_1;

				java.sql.Connection conn_j_catch_unexpected_error_1_tDBConnection_1 = null;

				java.util.Enumeration<java.sql.Driver> drivers_j_catch_unexpected_error_1_tDBConnection_1 = java.sql.DriverManager
						.getDrivers();
				java.util.Set<String> redShiftDriverNames_j_catch_unexpected_error_1_tDBConnection_1 = new java.util.HashSet<String>(
						java.util.Arrays.asList("com.amazon.redshift.jdbc.Driver", "com.amazon.redshift.jdbc41.Driver",
								"com.amazon.redshift.jdbc42.Driver"));
				while (drivers_j_catch_unexpected_error_1_tDBConnection_1.hasMoreElements()) {
					java.sql.Driver d_j_catch_unexpected_error_1_tDBConnection_1 = drivers_j_catch_unexpected_error_1_tDBConnection_1
							.nextElement();
					if (redShiftDriverNames_j_catch_unexpected_error_1_tDBConnection_1
							.contains(d_j_catch_unexpected_error_1_tDBConnection_1.getClass().getName())) {
						try {
							java.sql.DriverManager.deregisterDriver(d_j_catch_unexpected_error_1_tDBConnection_1);
							java.sql.DriverManager.registerDriver(d_j_catch_unexpected_error_1_tDBConnection_1);
						} catch (java.lang.Exception e_j_catch_unexpected_error_1_tDBConnection_1) {
							globalMap.put("j_catch_unexpected_error_1_tDBConnection_1_ERROR_MESSAGE",
									e_j_catch_unexpected_error_1_tDBConnection_1.getMessage());
							// do nothing
						}
					}
				}

				SharedDBConnectionLog4j.initLogger(log.getName(), "j_catch_unexpected_error_1_tDBConnection_1");
				String sharedConnectionName_j_catch_unexpected_error_1_tDBConnection_1 = "BDD_OMNICANAL";
				conn_j_catch_unexpected_error_1_tDBConnection_1 = SharedDBConnectionLog4j.getDBConnection(
						"org.postgresql.Driver", url_j_catch_unexpected_error_1_tDBConnection_1,
						dbUser_j_catch_unexpected_error_1_tDBConnection_1,
						dbPwd_j_catch_unexpected_error_1_tDBConnection_1,
						sharedConnectionName_j_catch_unexpected_error_1_tDBConnection_1);
				globalMap.put("conn_j_catch_unexpected_error_1_tDBConnection_1",
						conn_j_catch_unexpected_error_1_tDBConnection_1);
				if (null != conn_j_catch_unexpected_error_1_tDBConnection_1) {

					log.debug("j_catch_unexpected_error_1_tDBConnection_1 - Connection is set auto commit to 'true'.");
					conn_j_catch_unexpected_error_1_tDBConnection_1.setAutoCommit(true);
				}

				globalMap.put("schema_" + "j_catch_unexpected_error_1_tDBConnection_1", context.g_BDD_OMNICANAL_SCHEMA);

				/**
				 * [j_catch_unexpected_error_1_tDBConnection_1 begin ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tDBConnection_1 main ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tDBConnection_1";

				tos_count_j_catch_unexpected_error_1_tDBConnection_1++;

				/**
				 * [j_catch_unexpected_error_1_tDBConnection_1 main ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tDBConnection_1 process_data_begin ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tDBConnection_1";

				/**
				 * [j_catch_unexpected_error_1_tDBConnection_1 process_data_begin ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tDBConnection_1 process_data_end ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tDBConnection_1";

				/**
				 * [j_catch_unexpected_error_1_tDBConnection_1 process_data_end ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tDBConnection_1 end ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tDBConnection_1";

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBConnection_1 - " + ("Done."));

				ok_Hash.put("j_catch_unexpected_error_1_tDBConnection_1", true);
				end_Hash.put("j_catch_unexpected_error_1_tDBConnection_1", System.currentTimeMillis());

				/**
				 * [j_catch_unexpected_error_1_tDBConnection_1 end ] stop
				 */
			} // end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil.addLog("CHECKPOINT",
						"CONNECTION:SUBJOB_OK:j_catch_unexpected_error_1_tDBConnection_1:OnSubjobOk", "",
						Thread.currentThread().getId() + "", "", "", "", "", "");
			}

			if (execStat) {
				runStat.updateStatOnConnection("j_catch_unexpected_error_1_OnSubjobOk1", 0, "ok");
			}

			j_catch_unexpected_error_1_tHashInput_1Process(globalMap);

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_catch_unexpected_error_1_tDBConnection_1 finally ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tDBConnection_1";

				/**
				 * [j_catch_unexpected_error_1_tDBConnection_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_catch_unexpected_error_1_tDBConnection_1_SUBPROCESS_STATE", 1);
	}

	public static class j_catch_unexpected_error_1_row3Struct
			implements routines.system.IPersistableRow<j_catch_unexpected_error_1_row3Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public String jobname;

		public String getJobname() {
			return this.jobname;
		}

		public String guid;

		public String getGuid() {
			return this.guid;
		}

		public String message;

		public String getMessage() {
			return this.message;
		}

		public String filename;

		public String getFilename() {
			return this.filename;
		}

		public String filepath;

		public String getFilepath() {
			return this.filepath;
		}

		public Boolean send_alert;

		public Boolean getSend_alert() {
			return this.send_alert;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.jobname = readString(dis);

					this.guid = readString(dis);

					this.message = readString(dis);

					this.filename = readString(dis);

					this.filepath = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.send_alert = null;
					} else {
						this.send_alert = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.jobname = readString(dis);

					this.guid = readString(dis);

					this.message = readString(dis);

					this.filename = readString(dis);

					this.filepath = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.send_alert = null;
					} else {
						this.send_alert = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.jobname, dos);

				// String

				writeString(this.guid, dos);

				// String

				writeString(this.message, dos);

				// String

				writeString(this.filename, dos);

				// String

				writeString(this.filepath, dos);

				// Boolean

				if (this.send_alert == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.send_alert);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// String

				writeString(this.jobname, dos);

				// String

				writeString(this.guid, dos);

				// String

				writeString(this.message, dos);

				// String

				writeString(this.filename, dos);

				// String

				writeString(this.filepath, dos);

				// Boolean

				if (this.send_alert == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.send_alert);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("jobname=" + jobname);
			sb.append(",guid=" + guid);
			sb.append(",message=" + message);
			sb.append(",filename=" + filename);
			sb.append(",filepath=" + filepath);
			sb.append(",send_alert=" + String.valueOf(send_alert));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (jobname == null) {
				sb.append("<null>");
			} else {
				sb.append(jobname);
			}

			sb.append("|");

			if (guid == null) {
				sb.append("<null>");
			} else {
				sb.append(guid);
			}

			sb.append("|");

			if (message == null) {
				sb.append("<null>");
			} else {
				sb.append(message);
			}

			sb.append("|");

			if (filename == null) {
				sb.append("<null>");
			} else {
				sb.append(filename);
			}

			sb.append("|");

			if (filepath == null) {
				sb.append("<null>");
			} else {
				sb.append(filepath);
			}

			sb.append("|");

			if (send_alert == null) {
				sb.append("<null>");
			} else {
				sb.append(send_alert);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(j_catch_unexpected_error_1_row3Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void j_catch_unexpected_error_1_tHashInput_1Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("j_catch_unexpected_error_1_tHashInput_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				j_catch_unexpected_error_1_row3Struct j_catch_unexpected_error_1_row3 = new j_catch_unexpected_error_1_row3Struct();

				/**
				 * [j_catch_unexpected_error_1_tDBOutput_2 begin ] start
				 */

				ok_Hash.put("j_catch_unexpected_error_1_tDBOutput_2", false);
				start_Hash.put("j_catch_unexpected_error_1_tDBOutput_2", System.currentTimeMillis());

				currentComponent = "j_catch_unexpected_error_1_tDBOutput_2";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0,
						"j_catch_unexpected_error_1_row3");

				int tos_count_j_catch_unexpected_error_1_tDBOutput_2 = 0;

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_catch_unexpected_error_1_tDBOutput_2 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_catch_unexpected_error_1_tDBOutput_2 = new StringBuilder();
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append("Parameters:");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("USE_EXISTING_CONNECTION" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append("DB_VERSION" + " = " + "V9_X");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("HOST" + " = " + "context.g_BDD_OMNICANAL_HOST");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("PORT" + " = " + "context.g_BDD_OMNICANAL_PORT");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("DBNAME" + " = " + "context.g_BDD_OMNICANAL_DBNAME");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("SCHEMA_DB" + " = " + "context.g_BDD_OMNICANAL_SCHEMA");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("USER" + " = " + "context.g_BDD_OMNICANAL_USERNAME");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("PASS" + " = "
											+ String.valueOf(routines.system.PasswordEncryptUtil
													.encryptPassword(context.g_BDD_OMNICANAL_PWD)).substring(0, 4)
											+ "...");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("TABLE" + " = " + "\"errors\"");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("TABLE_ACTION" + " = " + "NONE");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("DATA_ACTION" + " = " + "INSERT");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("SPECIFY_DATASOURCE_ALIAS" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("DIE_ON_ERROR" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append("PROPERTIES" + " = " + "\"\"");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("COMMIT_EVERY" + " = " + "10000");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append("ADD_COLS" + " = " + "[]");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("USE_FIELD_OPTIONS" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("ENABLE_DEBUG_MODE" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("SUPPORT_NULL_WHERE" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("CONVERT_COLUMN_TABLE_TO_LOWERCASE" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("USE_BATCH_SIZE" + " = " + "true");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("BATCH_SIZE" + " = " + "10000");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlOutput");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_2.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_catch_unexpected_error_1_tDBOutput_2 - "
										+ (log4jParamters_j_catch_unexpected_error_1_tDBOutput_2));
						}
					}
					new BytesLimit65535_j_catch_unexpected_error_1_tDBOutput_2().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_catch_unexpected_error_1_tDBOutput_2",
							"__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>", "tPostgresqlOutput");
					talendJobLogProcess(globalMap);
				}

				String dbschema_j_catch_unexpected_error_1_tDBOutput_2 = null;
				dbschema_j_catch_unexpected_error_1_tDBOutput_2 = context.g_BDD_OMNICANAL_SCHEMA;

				String tableName_j_catch_unexpected_error_1_tDBOutput_2 = null;
				if (dbschema_j_catch_unexpected_error_1_tDBOutput_2 == null
						|| dbschema_j_catch_unexpected_error_1_tDBOutput_2.trim().length() == 0) {
					tableName_j_catch_unexpected_error_1_tDBOutput_2 = ("errors");
				} else {
					tableName_j_catch_unexpected_error_1_tDBOutput_2 = dbschema_j_catch_unexpected_error_1_tDBOutput_2
							+ "\".\"" + ("errors");
				}

				int nb_line_j_catch_unexpected_error_1_tDBOutput_2 = 0;
				int nb_line_update_j_catch_unexpected_error_1_tDBOutput_2 = 0;
				int nb_line_inserted_j_catch_unexpected_error_1_tDBOutput_2 = 0;
				int nb_line_deleted_j_catch_unexpected_error_1_tDBOutput_2 = 0;
				int nb_line_rejected_j_catch_unexpected_error_1_tDBOutput_2 = 0;

				int deletedCount_j_catch_unexpected_error_1_tDBOutput_2 = 0;
				int updatedCount_j_catch_unexpected_error_1_tDBOutput_2 = 0;
				int insertedCount_j_catch_unexpected_error_1_tDBOutput_2 = 0;
				int rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_2 = 0;
				int rejectedCount_j_catch_unexpected_error_1_tDBOutput_2 = 0;

				boolean whetherReject_j_catch_unexpected_error_1_tDBOutput_2 = false;

				java.sql.Connection conn_j_catch_unexpected_error_1_tDBOutput_2 = null;
				String dbUser_j_catch_unexpected_error_1_tDBOutput_2 = null;

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("Driver ClassName: ")
							+ ("org.postgresql.Driver") + ("."));
				java.lang.Class.forName("org.postgresql.Driver");

				String url_j_catch_unexpected_error_1_tDBOutput_2 = "jdbc:postgresql://" + context.g_BDD_OMNICANAL_HOST
						+ ":" + context.g_BDD_OMNICANAL_PORT + "/" + context.g_BDD_OMNICANAL_DBNAME;
				dbUser_j_catch_unexpected_error_1_tDBOutput_2 = context.g_BDD_OMNICANAL_USERNAME;

				final String decryptedPassword_j_catch_unexpected_error_1_tDBOutput_2 = context.g_BDD_OMNICANAL_PWD;

				String dbPwd_j_catch_unexpected_error_1_tDBOutput_2 = decryptedPassword_j_catch_unexpected_error_1_tDBOutput_2;

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("Connection attempts to '")
							+ (url_j_catch_unexpected_error_1_tDBOutput_2) + ("' with the username '")
							+ (dbUser_j_catch_unexpected_error_1_tDBOutput_2) + ("'."));
				conn_j_catch_unexpected_error_1_tDBOutput_2 = java.sql.DriverManager.getConnection(
						url_j_catch_unexpected_error_1_tDBOutput_2, dbUser_j_catch_unexpected_error_1_tDBOutput_2,
						dbPwd_j_catch_unexpected_error_1_tDBOutput_2);
				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("Connection to '")
							+ (url_j_catch_unexpected_error_1_tDBOutput_2) + ("' has succeeded."));

				resourceMap.put("conn_j_catch_unexpected_error_1_tDBOutput_2",
						conn_j_catch_unexpected_error_1_tDBOutput_2);
				conn_j_catch_unexpected_error_1_tDBOutput_2.setAutoCommit(false);
				int commitEvery_j_catch_unexpected_error_1_tDBOutput_2 = 10000;
				int commitCounter_j_catch_unexpected_error_1_tDBOutput_2 = 0;
				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("Connection is set auto commit to '")
							+ (conn_j_catch_unexpected_error_1_tDBOutput_2.getAutoCommit()) + ("'."));

				int batchSize_j_catch_unexpected_error_1_tDBOutput_2 = 10000;
				int batchSizeCounter_j_catch_unexpected_error_1_tDBOutput_2 = 0;

				int count_j_catch_unexpected_error_1_tDBOutput_2 = 0;
				String insert_j_catch_unexpected_error_1_tDBOutput_2 = "INSERT INTO \""
						+ tableName_j_catch_unexpected_error_1_tDBOutput_2
						+ "\" (\"jobname\",\"guid\",\"message\",\"filename\",\"filepath\",\"send_alert\") VALUES (?,?,?,?,?,?)";

				java.sql.PreparedStatement pstmt_j_catch_unexpected_error_1_tDBOutput_2 = conn_j_catch_unexpected_error_1_tDBOutput_2
						.prepareStatement(insert_j_catch_unexpected_error_1_tDBOutput_2);
				resourceMap.put("pstmt_j_catch_unexpected_error_1_tDBOutput_2",
						pstmt_j_catch_unexpected_error_1_tDBOutput_2);

				/**
				 * [j_catch_unexpected_error_1_tDBOutput_2 begin ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tHashInput_1 begin ] start
				 */

				ok_Hash.put("j_catch_unexpected_error_1_tHashInput_1", false);
				start_Hash.put("j_catch_unexpected_error_1_tHashInput_1", System.currentTimeMillis());

				currentComponent = "j_catch_unexpected_error_1_tHashInput_1";

				int tos_count_j_catch_unexpected_error_1_tHashInput_1 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("j_catch_unexpected_error_1_tHashInput_1", "__UNIQUE_NAME__<br><b>errors</b>",
							"tHashInput");
					talendJobLogProcess(globalMap);
				}

				int nb_line_j_catch_unexpected_error_1_tHashInput_1 = 0;

				org.talend.designer.components.hashfile.common.MapHashFile mf_j_catch_unexpected_error_1_tHashInput_1 = org.talend.designer.components.hashfile.common.MapHashFile
						.getMapHashFile();
				org.talend.designer.components.hashfile.memory.AdvancedMemoryHashFile<j_catch_unexpected_error_1_toErrorStruct> tHashFile_j_catch_unexpected_error_1_tHashInput_1 = mf_j_catch_unexpected_error_1_tHashInput_1
						.getAdvancedMemoryHashFile(
								"tHashFile_j_confirmation_odp_" + pid + "_j_catch_unexpected_error_1_tHashOutput_1");
				if (tHashFile_j_catch_unexpected_error_1_tHashInput_1 == null) {
					throw new RuntimeException(
							"The hash is not initialized : The hash must exist before you read from it");
				}
				java.util.Iterator<j_catch_unexpected_error_1_toErrorStruct> iterator_j_catch_unexpected_error_1_tHashInput_1 = tHashFile_j_catch_unexpected_error_1_tHashInput_1
						.iterator();
				while (iterator_j_catch_unexpected_error_1_tHashInput_1.hasNext()) {
					j_catch_unexpected_error_1_toErrorStruct next_j_catch_unexpected_error_1_tHashInput_1 = iterator_j_catch_unexpected_error_1_tHashInput_1
							.next();

					j_catch_unexpected_error_1_row3.jobname = next_j_catch_unexpected_error_1_tHashInput_1.jobname;
					j_catch_unexpected_error_1_row3.guid = next_j_catch_unexpected_error_1_tHashInput_1.guid;
					j_catch_unexpected_error_1_row3.message = next_j_catch_unexpected_error_1_tHashInput_1.message;
					j_catch_unexpected_error_1_row3.filename = next_j_catch_unexpected_error_1_tHashInput_1.filename;
					j_catch_unexpected_error_1_row3.filepath = next_j_catch_unexpected_error_1_tHashInput_1.filepath;
					j_catch_unexpected_error_1_row3.send_alert = next_j_catch_unexpected_error_1_tHashInput_1.send_alert;

					/**
					 * [j_catch_unexpected_error_1_tHashInput_1 begin ] stop
					 */

					/**
					 * [j_catch_unexpected_error_1_tHashInput_1 main ] start
					 */

					currentComponent = "j_catch_unexpected_error_1_tHashInput_1";

					tos_count_j_catch_unexpected_error_1_tHashInput_1++;

					/**
					 * [j_catch_unexpected_error_1_tHashInput_1 main ] stop
					 */

					/**
					 * [j_catch_unexpected_error_1_tHashInput_1 process_data_begin ] start
					 */

					currentComponent = "j_catch_unexpected_error_1_tHashInput_1";

					/**
					 * [j_catch_unexpected_error_1_tHashInput_1 process_data_begin ] stop
					 */

					/**
					 * [j_catch_unexpected_error_1_tDBOutput_2 main ] start
					 */

					currentComponent = "j_catch_unexpected_error_1_tDBOutput_2";

					if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

							, "j_catch_unexpected_error_1_row3", "j_catch_unexpected_error_1_tHashInput_1",
							"__UNIQUE_NAME__<br><b>errors</b>", "tHashInput", "j_catch_unexpected_error_1_tDBOutput_2",
							"__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>", "tPostgresqlOutput"

					)) {
						talendJobLogProcess(globalMap);
					}

					if (log.isTraceEnabled()) {
						log.trace("j_catch_unexpected_error_1_row3 - " + (j_catch_unexpected_error_1_row3 == null ? ""
								: j_catch_unexpected_error_1_row3.toLogString()));
					}

					whetherReject_j_catch_unexpected_error_1_tDBOutput_2 = false;
					if (j_catch_unexpected_error_1_row3.jobname == null) {
						pstmt_j_catch_unexpected_error_1_tDBOutput_2.setNull(1, java.sql.Types.VARCHAR);
					} else {
						pstmt_j_catch_unexpected_error_1_tDBOutput_2.setString(1,
								j_catch_unexpected_error_1_row3.jobname);
					}

					if (j_catch_unexpected_error_1_row3.guid == null) {
						pstmt_j_catch_unexpected_error_1_tDBOutput_2.setNull(2, java.sql.Types.VARCHAR);
					} else {
						pstmt_j_catch_unexpected_error_1_tDBOutput_2.setString(2, j_catch_unexpected_error_1_row3.guid);
					}

					if (j_catch_unexpected_error_1_row3.message == null) {
						pstmt_j_catch_unexpected_error_1_tDBOutput_2.setNull(3, java.sql.Types.VARCHAR);
					} else {
						pstmt_j_catch_unexpected_error_1_tDBOutput_2.setString(3,
								j_catch_unexpected_error_1_row3.message);
					}

					if (j_catch_unexpected_error_1_row3.filename == null) {
						pstmt_j_catch_unexpected_error_1_tDBOutput_2.setNull(4, java.sql.Types.VARCHAR);
					} else {
						pstmt_j_catch_unexpected_error_1_tDBOutput_2.setString(4,
								j_catch_unexpected_error_1_row3.filename);
					}

					if (j_catch_unexpected_error_1_row3.filepath == null) {
						pstmt_j_catch_unexpected_error_1_tDBOutput_2.setNull(5, java.sql.Types.VARCHAR);
					} else {
						pstmt_j_catch_unexpected_error_1_tDBOutput_2.setString(5,
								j_catch_unexpected_error_1_row3.filepath);
					}

					if (j_catch_unexpected_error_1_row3.send_alert == null) {
						pstmt_j_catch_unexpected_error_1_tDBOutput_2.setNull(6, java.sql.Types.BOOLEAN);
					} else {
						pstmt_j_catch_unexpected_error_1_tDBOutput_2.setBoolean(6,
								j_catch_unexpected_error_1_row3.send_alert);
					}

					pstmt_j_catch_unexpected_error_1_tDBOutput_2.addBatch();
					nb_line_j_catch_unexpected_error_1_tDBOutput_2++;

					if (log.isDebugEnabled())
						log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("Adding the record ")
								+ (nb_line_j_catch_unexpected_error_1_tDBOutput_2) + (" to the ") + ("INSERT")
								+ (" batch."));
					batchSizeCounter_j_catch_unexpected_error_1_tDBOutput_2++;

					if (!whetherReject_j_catch_unexpected_error_1_tDBOutput_2) {
					}
					if ((batchSize_j_catch_unexpected_error_1_tDBOutput_2 > 0)
							&& (batchSize_j_catch_unexpected_error_1_tDBOutput_2 <= batchSizeCounter_j_catch_unexpected_error_1_tDBOutput_2)) {
						try {
							int countSum_j_catch_unexpected_error_1_tDBOutput_2 = 0;

							if (log.isDebugEnabled())
								log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("Executing the ") + ("INSERT")
										+ (" batch."));
							for (int countEach_j_catch_unexpected_error_1_tDBOutput_2 : pstmt_j_catch_unexpected_error_1_tDBOutput_2
									.executeBatch()) {
								countSum_j_catch_unexpected_error_1_tDBOutput_2 += (countEach_j_catch_unexpected_error_1_tDBOutput_2 < 0
										? 0
										: countEach_j_catch_unexpected_error_1_tDBOutput_2);
							}
							if (log.isDebugEnabled())
								log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("The ") + ("INSERT")
										+ (" batch execution has succeeded."));
							rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_2 += countSum_j_catch_unexpected_error_1_tDBOutput_2;

							insertedCount_j_catch_unexpected_error_1_tDBOutput_2 += countSum_j_catch_unexpected_error_1_tDBOutput_2;

							batchSizeCounter_j_catch_unexpected_error_1_tDBOutput_2 = 0;
						} catch (java.sql.BatchUpdateException e_j_catch_unexpected_error_1_tDBOutput_2) {
							globalMap.put("j_catch_unexpected_error_1_tDBOutput_2_ERROR_MESSAGE",
									e_j_catch_unexpected_error_1_tDBOutput_2.getMessage());
							java.sql.SQLException ne_j_catch_unexpected_error_1_tDBOutput_2 = e_j_catch_unexpected_error_1_tDBOutput_2
									.getNextException(), sqle_j_catch_unexpected_error_1_tDBOutput_2 = null;
							String errormessage_j_catch_unexpected_error_1_tDBOutput_2;
							if (ne_j_catch_unexpected_error_1_tDBOutput_2 != null) {
								// build new exception to provide the original cause
								sqle_j_catch_unexpected_error_1_tDBOutput_2 = new java.sql.SQLException(
										e_j_catch_unexpected_error_1_tDBOutput_2.getMessage() + "\ncaused by: "
												+ ne_j_catch_unexpected_error_1_tDBOutput_2.getMessage(),
										ne_j_catch_unexpected_error_1_tDBOutput_2.getSQLState(),
										ne_j_catch_unexpected_error_1_tDBOutput_2.getErrorCode(),
										ne_j_catch_unexpected_error_1_tDBOutput_2);
								errormessage_j_catch_unexpected_error_1_tDBOutput_2 = sqle_j_catch_unexpected_error_1_tDBOutput_2
										.getMessage();
							} else {
								errormessage_j_catch_unexpected_error_1_tDBOutput_2 = e_j_catch_unexpected_error_1_tDBOutput_2
										.getMessage();
							}

							int countSum_j_catch_unexpected_error_1_tDBOutput_2 = 0;
							for (int countEach_j_catch_unexpected_error_1_tDBOutput_2 : e_j_catch_unexpected_error_1_tDBOutput_2
									.getUpdateCounts()) {
								countSum_j_catch_unexpected_error_1_tDBOutput_2 += (countEach_j_catch_unexpected_error_1_tDBOutput_2 < 0
										? 0
										: countEach_j_catch_unexpected_error_1_tDBOutput_2);
							}
							rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_2 += countSum_j_catch_unexpected_error_1_tDBOutput_2;

							insertedCount_j_catch_unexpected_error_1_tDBOutput_2 += countSum_j_catch_unexpected_error_1_tDBOutput_2;

							log.error("j_catch_unexpected_error_1_tDBOutput_2 - "
									+ (errormessage_j_catch_unexpected_error_1_tDBOutput_2));
							System.err.println(errormessage_j_catch_unexpected_error_1_tDBOutput_2);

						}
					}

					commitCounter_j_catch_unexpected_error_1_tDBOutput_2++;
					if (commitEvery_j_catch_unexpected_error_1_tDBOutput_2 <= commitCounter_j_catch_unexpected_error_1_tDBOutput_2) {
						if ((batchSize_j_catch_unexpected_error_1_tDBOutput_2 > 0)
								&& (batchSizeCounter_j_catch_unexpected_error_1_tDBOutput_2 > 0)) {
							try {
								int countSum_j_catch_unexpected_error_1_tDBOutput_2 = 0;

								if (log.isDebugEnabled())
									log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("Executing the ")
											+ ("INSERT") + (" batch."));
								for (int countEach_j_catch_unexpected_error_1_tDBOutput_2 : pstmt_j_catch_unexpected_error_1_tDBOutput_2
										.executeBatch()) {
									countSum_j_catch_unexpected_error_1_tDBOutput_2 += (countEach_j_catch_unexpected_error_1_tDBOutput_2 < 0
											? 0
											: countEach_j_catch_unexpected_error_1_tDBOutput_2);
								}
								if (log.isDebugEnabled())
									log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("The ") + ("INSERT")
											+ (" batch execution has succeeded."));
								rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_2 += countSum_j_catch_unexpected_error_1_tDBOutput_2;

								insertedCount_j_catch_unexpected_error_1_tDBOutput_2 += countSum_j_catch_unexpected_error_1_tDBOutput_2;

								batchSizeCounter_j_catch_unexpected_error_1_tDBOutput_2 = 0;
							} catch (java.sql.BatchUpdateException e_j_catch_unexpected_error_1_tDBOutput_2) {
								globalMap.put("j_catch_unexpected_error_1_tDBOutput_2_ERROR_MESSAGE",
										e_j_catch_unexpected_error_1_tDBOutput_2.getMessage());
								java.sql.SQLException ne_j_catch_unexpected_error_1_tDBOutput_2 = e_j_catch_unexpected_error_1_tDBOutput_2
										.getNextException(), sqle_j_catch_unexpected_error_1_tDBOutput_2 = null;
								String errormessage_j_catch_unexpected_error_1_tDBOutput_2;
								if (ne_j_catch_unexpected_error_1_tDBOutput_2 != null) {
									// build new exception to provide the original cause
									sqle_j_catch_unexpected_error_1_tDBOutput_2 = new java.sql.SQLException(
											e_j_catch_unexpected_error_1_tDBOutput_2.getMessage() + "\ncaused by: "
													+ ne_j_catch_unexpected_error_1_tDBOutput_2.getMessage(),
											ne_j_catch_unexpected_error_1_tDBOutput_2.getSQLState(),
											ne_j_catch_unexpected_error_1_tDBOutput_2.getErrorCode(),
											ne_j_catch_unexpected_error_1_tDBOutput_2);
									errormessage_j_catch_unexpected_error_1_tDBOutput_2 = sqle_j_catch_unexpected_error_1_tDBOutput_2
											.getMessage();
								} else {
									errormessage_j_catch_unexpected_error_1_tDBOutput_2 = e_j_catch_unexpected_error_1_tDBOutput_2
											.getMessage();
								}

								int countSum_j_catch_unexpected_error_1_tDBOutput_2 = 0;
								for (int countEach_j_catch_unexpected_error_1_tDBOutput_2 : e_j_catch_unexpected_error_1_tDBOutput_2
										.getUpdateCounts()) {
									countSum_j_catch_unexpected_error_1_tDBOutput_2 += (countEach_j_catch_unexpected_error_1_tDBOutput_2 < 0
											? 0
											: countEach_j_catch_unexpected_error_1_tDBOutput_2);
								}
								rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_2 += countSum_j_catch_unexpected_error_1_tDBOutput_2;

								insertedCount_j_catch_unexpected_error_1_tDBOutput_2 += countSum_j_catch_unexpected_error_1_tDBOutput_2;

								log.error("j_catch_unexpected_error_1_tDBOutput_2 - "
										+ (errormessage_j_catch_unexpected_error_1_tDBOutput_2));
								System.err.println(errormessage_j_catch_unexpected_error_1_tDBOutput_2);

							}
						}
						if (rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_2 != 0) {

							if (log.isDebugEnabled())
								log.debug("j_catch_unexpected_error_1_tDBOutput_2 - "
										+ ("Connection starting to commit ")
										+ (rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_2) + (" record(s)."));
						}
						conn_j_catch_unexpected_error_1_tDBOutput_2.commit();
						if (rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_2 != 0) {

							if (log.isDebugEnabled())
								log.debug("j_catch_unexpected_error_1_tDBOutput_2 - "
										+ ("Connection commit has succeeded."));
							rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_2 = 0;
						}
						commitCounter_j_catch_unexpected_error_1_tDBOutput_2 = 0;
					}

					tos_count_j_catch_unexpected_error_1_tDBOutput_2++;

					/**
					 * [j_catch_unexpected_error_1_tDBOutput_2 main ] stop
					 */

					/**
					 * [j_catch_unexpected_error_1_tDBOutput_2 process_data_begin ] start
					 */

					currentComponent = "j_catch_unexpected_error_1_tDBOutput_2";

					/**
					 * [j_catch_unexpected_error_1_tDBOutput_2 process_data_begin ] stop
					 */

					/**
					 * [j_catch_unexpected_error_1_tDBOutput_2 process_data_end ] start
					 */

					currentComponent = "j_catch_unexpected_error_1_tDBOutput_2";

					/**
					 * [j_catch_unexpected_error_1_tDBOutput_2 process_data_end ] stop
					 */

					/**
					 * [j_catch_unexpected_error_1_tHashInput_1 process_data_end ] start
					 */

					currentComponent = "j_catch_unexpected_error_1_tHashInput_1";

					/**
					 * [j_catch_unexpected_error_1_tHashInput_1 process_data_end ] stop
					 */

					/**
					 * [j_catch_unexpected_error_1_tHashInput_1 end ] start
					 */

					currentComponent = "j_catch_unexpected_error_1_tHashInput_1";

					nb_line_j_catch_unexpected_error_1_tHashInput_1++;
				}

				org.talend.designer.components.hashfile.common.MapHashFile.resourceLockMap
						.remove("tHashFile_j_confirmation_odp_" + pid + "_j_catch_unexpected_error_1_tHashOutput_1");

				globalMap.put("j_catch_unexpected_error_1_tHashInput_1_NB_LINE",
						nb_line_j_catch_unexpected_error_1_tHashInput_1);

				ok_Hash.put("j_catch_unexpected_error_1_tHashInput_1", true);
				end_Hash.put("j_catch_unexpected_error_1_tHashInput_1", System.currentTimeMillis());

				/**
				 * [j_catch_unexpected_error_1_tHashInput_1 end ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tDBOutput_2 end ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tDBOutput_2";

				try {
					int countSum_j_catch_unexpected_error_1_tDBOutput_2 = 0;
					if (pstmt_j_catch_unexpected_error_1_tDBOutput_2 != null
							&& batchSizeCounter_j_catch_unexpected_error_1_tDBOutput_2 > 0) {

						if (log.isDebugEnabled())
							log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("Executing the ") + ("INSERT")
									+ (" batch."));
						for (int countEach_j_catch_unexpected_error_1_tDBOutput_2 : pstmt_j_catch_unexpected_error_1_tDBOutput_2
								.executeBatch()) {
							countSum_j_catch_unexpected_error_1_tDBOutput_2 += (countEach_j_catch_unexpected_error_1_tDBOutput_2 < 0
									? 0
									: countEach_j_catch_unexpected_error_1_tDBOutput_2);
						}
						rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_2 += countSum_j_catch_unexpected_error_1_tDBOutput_2;

						if (log.isDebugEnabled())
							log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("The ") + ("INSERT")
									+ (" batch execution has succeeded."));
					}

					insertedCount_j_catch_unexpected_error_1_tDBOutput_2 += countSum_j_catch_unexpected_error_1_tDBOutput_2;

				} catch (java.sql.BatchUpdateException e_j_catch_unexpected_error_1_tDBOutput_2) {
					globalMap.put("j_catch_unexpected_error_1_tDBOutput_2_ERROR_MESSAGE",
							e_j_catch_unexpected_error_1_tDBOutput_2.getMessage());
					java.sql.SQLException ne_j_catch_unexpected_error_1_tDBOutput_2 = e_j_catch_unexpected_error_1_tDBOutput_2
							.getNextException(), sqle_j_catch_unexpected_error_1_tDBOutput_2 = null;
					String errormessage_j_catch_unexpected_error_1_tDBOutput_2;
					if (ne_j_catch_unexpected_error_1_tDBOutput_2 != null) {
						// build new exception to provide the original cause
						sqle_j_catch_unexpected_error_1_tDBOutput_2 = new java.sql.SQLException(
								e_j_catch_unexpected_error_1_tDBOutput_2.getMessage() + "\ncaused by: "
										+ ne_j_catch_unexpected_error_1_tDBOutput_2.getMessage(),
								ne_j_catch_unexpected_error_1_tDBOutput_2.getSQLState(),
								ne_j_catch_unexpected_error_1_tDBOutput_2.getErrorCode(),
								ne_j_catch_unexpected_error_1_tDBOutput_2);
						errormessage_j_catch_unexpected_error_1_tDBOutput_2 = sqle_j_catch_unexpected_error_1_tDBOutput_2
								.getMessage();
					} else {
						errormessage_j_catch_unexpected_error_1_tDBOutput_2 = e_j_catch_unexpected_error_1_tDBOutput_2
								.getMessage();
					}

					int countSum_j_catch_unexpected_error_1_tDBOutput_2 = 0;
					for (int countEach_j_catch_unexpected_error_1_tDBOutput_2 : e_j_catch_unexpected_error_1_tDBOutput_2
							.getUpdateCounts()) {
						countSum_j_catch_unexpected_error_1_tDBOutput_2 += (countEach_j_catch_unexpected_error_1_tDBOutput_2 < 0
								? 0
								: countEach_j_catch_unexpected_error_1_tDBOutput_2);
					}
					rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_2 += countSum_j_catch_unexpected_error_1_tDBOutput_2;

					insertedCount_j_catch_unexpected_error_1_tDBOutput_2 += countSum_j_catch_unexpected_error_1_tDBOutput_2;

					log.error("j_catch_unexpected_error_1_tDBOutput_2 - "
							+ (errormessage_j_catch_unexpected_error_1_tDBOutput_2));
					System.err.println(errormessage_j_catch_unexpected_error_1_tDBOutput_2);

				}

				if (pstmt_j_catch_unexpected_error_1_tDBOutput_2 != null) {

					pstmt_j_catch_unexpected_error_1_tDBOutput_2.close();
					resourceMap.remove("pstmt_j_catch_unexpected_error_1_tDBOutput_2");
				}
				resourceMap.put("statementClosed_j_catch_unexpected_error_1_tDBOutput_2", true);
				if (rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_2 != 0) {

					if (log.isDebugEnabled())
						log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("Connection starting to commit ")
								+ (rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_2) + (" record(s)."));
				}
				conn_j_catch_unexpected_error_1_tDBOutput_2.commit();
				if (rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_2 != 0) {

					if (log.isDebugEnabled())
						log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("Connection commit has succeeded."));
					rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_2 = 0;
				}
				commitCounter_j_catch_unexpected_error_1_tDBOutput_2 = 0;

				if (log.isDebugEnabled())
					log.debug(
							"j_catch_unexpected_error_1_tDBOutput_2 - " + ("Closing the connection to the database."));
				conn_j_catch_unexpected_error_1_tDBOutput_2.close();

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("Connection to the database has closed."));
				resourceMap.put("finish_j_catch_unexpected_error_1_tDBOutput_2", true);

				nb_line_deleted_j_catch_unexpected_error_1_tDBOutput_2 = nb_line_deleted_j_catch_unexpected_error_1_tDBOutput_2
						+ deletedCount_j_catch_unexpected_error_1_tDBOutput_2;
				nb_line_update_j_catch_unexpected_error_1_tDBOutput_2 = nb_line_update_j_catch_unexpected_error_1_tDBOutput_2
						+ updatedCount_j_catch_unexpected_error_1_tDBOutput_2;
				nb_line_inserted_j_catch_unexpected_error_1_tDBOutput_2 = nb_line_inserted_j_catch_unexpected_error_1_tDBOutput_2
						+ insertedCount_j_catch_unexpected_error_1_tDBOutput_2;
				nb_line_rejected_j_catch_unexpected_error_1_tDBOutput_2 = nb_line_rejected_j_catch_unexpected_error_1_tDBOutput_2
						+ rejectedCount_j_catch_unexpected_error_1_tDBOutput_2;

				globalMap.put("j_catch_unexpected_error_1_tDBOutput_2_NB_LINE",
						nb_line_j_catch_unexpected_error_1_tDBOutput_2);
				globalMap.put("j_catch_unexpected_error_1_tDBOutput_2_NB_LINE_UPDATED",
						nb_line_update_j_catch_unexpected_error_1_tDBOutput_2);
				globalMap.put("j_catch_unexpected_error_1_tDBOutput_2_NB_LINE_INSERTED",
						nb_line_inserted_j_catch_unexpected_error_1_tDBOutput_2);
				globalMap.put("j_catch_unexpected_error_1_tDBOutput_2_NB_LINE_DELETED",
						nb_line_deleted_j_catch_unexpected_error_1_tDBOutput_2);
				globalMap.put("j_catch_unexpected_error_1_tDBOutput_2_NB_LINE_REJECTED",
						nb_line_rejected_j_catch_unexpected_error_1_tDBOutput_2);

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("Has ") + ("inserted") + (" ")
							+ (nb_line_inserted_j_catch_unexpected_error_1_tDBOutput_2) + (" record(s)."));

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId,
						"j_catch_unexpected_error_1_row3", 2, 0, "j_catch_unexpected_error_1_tHashInput_1",
						"__UNIQUE_NAME__<br><b>errors</b>", "tHashInput", "j_catch_unexpected_error_1_tDBOutput_2",
						"__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>", "tPostgresqlOutput",
						"output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_2 - " + ("Done."));

				ok_Hash.put("j_catch_unexpected_error_1_tDBOutput_2", true);
				end_Hash.put("j_catch_unexpected_error_1_tDBOutput_2", System.currentTimeMillis());

				if (!"".equals((String) globalMap.get("currentFile_Name"))
						&& (String) globalMap.get("currentFile_Name") != null) {

					if (execStat) {
						runStat.updateStatOnConnection("j_catch_unexpected_error_1_If1", 0, "true");
					}
					j_catch_unexpected_error_1_tDBInput_1Process(globalMap);
				}

				else {
					if (execStat) {
						runStat.updateStatOnConnection("j_catch_unexpected_error_1_If1", 0, "false");
					}
				}

				/**
				 * [j_catch_unexpected_error_1_tDBOutput_2 end ] stop
				 */

			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_catch_unexpected_error_1_tHashInput_1 finally ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tHashInput_1";

				/**
				 * [j_catch_unexpected_error_1_tHashInput_1 finally ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tDBOutput_2 finally ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tDBOutput_2";

				try {
					if (resourceMap.get("statementClosed_j_catch_unexpected_error_1_tDBOutput_2") == null) {
						java.sql.PreparedStatement pstmtToClose_j_catch_unexpected_error_1_tDBOutput_2 = null;
						if ((pstmtToClose_j_catch_unexpected_error_1_tDBOutput_2 = (java.sql.PreparedStatement) resourceMap
								.remove("pstmt_j_catch_unexpected_error_1_tDBOutput_2")) != null) {
							pstmtToClose_j_catch_unexpected_error_1_tDBOutput_2.close();
						}
					}
				} finally {
					if (resourceMap.get("finish_j_catch_unexpected_error_1_tDBOutput_2") == null) {
						java.sql.Connection ctn_j_catch_unexpected_error_1_tDBOutput_2 = null;
						if ((ctn_j_catch_unexpected_error_1_tDBOutput_2 = (java.sql.Connection) resourceMap
								.get("conn_j_catch_unexpected_error_1_tDBOutput_2")) != null) {
							try {
								if (log.isDebugEnabled())
									log.debug("j_catch_unexpected_error_1_tDBOutput_2 - "
											+ ("Closing the connection to the database."));
								ctn_j_catch_unexpected_error_1_tDBOutput_2.close();
								if (log.isDebugEnabled())
									log.debug("j_catch_unexpected_error_1_tDBOutput_2 - "
											+ ("Connection to the database has closed."));
							} catch (java.sql.SQLException sqlEx_j_catch_unexpected_error_1_tDBOutput_2) {
								String errorMessage_j_catch_unexpected_error_1_tDBOutput_2 = "failed to close the connection in j_catch_unexpected_error_1_tDBOutput_2 :"
										+ sqlEx_j_catch_unexpected_error_1_tDBOutput_2.getMessage();
								log.error("j_catch_unexpected_error_1_tDBOutput_2 - "
										+ (errorMessage_j_catch_unexpected_error_1_tDBOutput_2));
								System.err.println(errorMessage_j_catch_unexpected_error_1_tDBOutput_2);
							}
						}
					}
				}

				/**
				 * [j_catch_unexpected_error_1_tDBOutput_2 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_catch_unexpected_error_1_tHashInput_1_SUBPROCESS_STATE", 1);
	}

	public static class j_catch_unexpected_error_1_toFileStruct
			implements routines.system.IPersistableRow<j_catch_unexpected_error_1_toFileStruct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public Integer id;

		public Integer getId() {
			return this.id;
		}

		public Boolean error;

		public Boolean getError() {
			return this.error;
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

					length = dis.readByte();
					if (length == -1) {
						this.error = null;
					} else {
						this.error = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

					length = dis.readByte();
					if (length == -1) {
						this.error = null;
					} else {
						this.error = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

				// Boolean

				if (this.error == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.error);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

				// Boolean

				if (this.error == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.error);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("id=" + String.valueOf(id));
			sb.append(",error=" + String.valueOf(error));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (id == null) {
				sb.append("<null>");
			} else {
				sb.append(id);
			}

			sb.append("|");

			if (error == null) {
				sb.append("<null>");
			} else {
				sb.append(error);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(j_catch_unexpected_error_1_toFileStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class j_catch_unexpected_error_1_row2Struct
			implements routines.system.IPersistableRow<j_catch_unexpected_error_1_row2Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public Integer id;

		public Integer getId() {
			return this.id;
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("id=" + String.valueOf(id));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (id == null) {
				sb.append("<null>");
			} else {
				sb.append(id);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(j_catch_unexpected_error_1_row2Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void j_catch_unexpected_error_1_tDBInput_1Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("j_catch_unexpected_error_1_tDBInput_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				j_catch_unexpected_error_1_row2Struct j_catch_unexpected_error_1_row2 = new j_catch_unexpected_error_1_row2Struct();
				j_catch_unexpected_error_1_toFileStruct j_catch_unexpected_error_1_toFile = new j_catch_unexpected_error_1_toFileStruct();

				/**
				 * [j_catch_unexpected_error_1_tDBOutput_1 begin ] start
				 */

				ok_Hash.put("j_catch_unexpected_error_1_tDBOutput_1", false);
				start_Hash.put("j_catch_unexpected_error_1_tDBOutput_1", System.currentTimeMillis());

				currentComponent = "j_catch_unexpected_error_1_tDBOutput_1";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0,
						"j_catch_unexpected_error_1_toFile");

				int tos_count_j_catch_unexpected_error_1_tDBOutput_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_catch_unexpected_error_1_tDBOutput_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_catch_unexpected_error_1_tDBOutput_1 = new StringBuilder();
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append("Parameters:");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("USE_EXISTING_CONNECTION" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append("DB_VERSION" + " = " + "V9_X");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("HOST" + " = " + "context.g_BDD_OMNICANAL_HOST");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("PORT" + " = " + "context.g_BDD_OMNICANAL_PORT");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("DBNAME" + " = " + "context.g_BDD_OMNICANAL_DBNAME");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("SCHEMA_DB" + " = " + "context.g_BDD_OMNICANAL_SCHEMA");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("USER" + " = " + "context.g_BDD_OMNICANAL_USERNAME");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("PASS" + " = "
											+ String.valueOf(routines.system.PasswordEncryptUtil
													.encryptPassword(context.g_BDD_OMNICANAL_PWD)).substring(0, 4)
											+ "...");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append("TABLE" + " = " + "\"files\"");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("TABLE_ACTION" + " = " + "NONE");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("DATA_ACTION" + " = " + "UPDATE");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("SPECIFY_DATASOURCE_ALIAS" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("DIE_ON_ERROR" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append("PROPERTIES" + " = " + "\"\"");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("COMMIT_EVERY" + " = " + "10000");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append("ADD_COLS" + " = " + "[]");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("USE_FIELD_OPTIONS" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("ENABLE_DEBUG_MODE" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("SUPPORT_NULL_WHERE" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("CONVERT_COLUMN_TABLE_TO_LOWERCASE" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("USE_BATCH_SIZE" + " = " + "true");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("BATCH_SIZE" + " = " + "10000");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlOutput");
							log4jParamters_j_catch_unexpected_error_1_tDBOutput_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_catch_unexpected_error_1_tDBOutput_1 - "
										+ (log4jParamters_j_catch_unexpected_error_1_tDBOutput_1));
						}
					}
					new BytesLimit65535_j_catch_unexpected_error_1_tDBOutput_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_catch_unexpected_error_1_tDBOutput_1",
							"__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>", "tPostgresqlOutput");
					talendJobLogProcess(globalMap);
				}

				String dbschema_j_catch_unexpected_error_1_tDBOutput_1 = null;
				dbschema_j_catch_unexpected_error_1_tDBOutput_1 = context.g_BDD_OMNICANAL_SCHEMA;

				String tableName_j_catch_unexpected_error_1_tDBOutput_1 = null;
				if (dbschema_j_catch_unexpected_error_1_tDBOutput_1 == null
						|| dbschema_j_catch_unexpected_error_1_tDBOutput_1.trim().length() == 0) {
					tableName_j_catch_unexpected_error_1_tDBOutput_1 = ("files");
				} else {
					tableName_j_catch_unexpected_error_1_tDBOutput_1 = dbschema_j_catch_unexpected_error_1_tDBOutput_1
							+ "\".\"" + ("files");
				}

				int updateKeyCount_j_catch_unexpected_error_1_tDBOutput_1 = 1;
				if (updateKeyCount_j_catch_unexpected_error_1_tDBOutput_1 < 1) {
					throw new RuntimeException("For update, Schema must have a key");
				} else if (updateKeyCount_j_catch_unexpected_error_1_tDBOutput_1 == 2 && true) {
					throw new RuntimeException("For update, every Schema column can not be a key");
				}

				int nb_line_j_catch_unexpected_error_1_tDBOutput_1 = 0;
				int nb_line_update_j_catch_unexpected_error_1_tDBOutput_1 = 0;
				int nb_line_inserted_j_catch_unexpected_error_1_tDBOutput_1 = 0;
				int nb_line_deleted_j_catch_unexpected_error_1_tDBOutput_1 = 0;
				int nb_line_rejected_j_catch_unexpected_error_1_tDBOutput_1 = 0;

				int deletedCount_j_catch_unexpected_error_1_tDBOutput_1 = 0;
				int updatedCount_j_catch_unexpected_error_1_tDBOutput_1 = 0;
				int insertedCount_j_catch_unexpected_error_1_tDBOutput_1 = 0;
				int rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_1 = 0;
				int rejectedCount_j_catch_unexpected_error_1_tDBOutput_1 = 0;

				boolean whetherReject_j_catch_unexpected_error_1_tDBOutput_1 = false;

				java.sql.Connection conn_j_catch_unexpected_error_1_tDBOutput_1 = null;
				String dbUser_j_catch_unexpected_error_1_tDBOutput_1 = null;

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("Driver ClassName: ")
							+ ("org.postgresql.Driver") + ("."));
				java.lang.Class.forName("org.postgresql.Driver");

				String url_j_catch_unexpected_error_1_tDBOutput_1 = "jdbc:postgresql://" + context.g_BDD_OMNICANAL_HOST
						+ ":" + context.g_BDD_OMNICANAL_PORT + "/" + context.g_BDD_OMNICANAL_DBNAME;
				dbUser_j_catch_unexpected_error_1_tDBOutput_1 = context.g_BDD_OMNICANAL_USERNAME;

				final String decryptedPassword_j_catch_unexpected_error_1_tDBOutput_1 = context.g_BDD_OMNICANAL_PWD;

				String dbPwd_j_catch_unexpected_error_1_tDBOutput_1 = decryptedPassword_j_catch_unexpected_error_1_tDBOutput_1;

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("Connection attempts to '")
							+ (url_j_catch_unexpected_error_1_tDBOutput_1) + ("' with the username '")
							+ (dbUser_j_catch_unexpected_error_1_tDBOutput_1) + ("'."));
				conn_j_catch_unexpected_error_1_tDBOutput_1 = java.sql.DriverManager.getConnection(
						url_j_catch_unexpected_error_1_tDBOutput_1, dbUser_j_catch_unexpected_error_1_tDBOutput_1,
						dbPwd_j_catch_unexpected_error_1_tDBOutput_1);
				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("Connection to '")
							+ (url_j_catch_unexpected_error_1_tDBOutput_1) + ("' has succeeded."));

				resourceMap.put("conn_j_catch_unexpected_error_1_tDBOutput_1",
						conn_j_catch_unexpected_error_1_tDBOutput_1);
				conn_j_catch_unexpected_error_1_tDBOutput_1.setAutoCommit(false);
				int commitEvery_j_catch_unexpected_error_1_tDBOutput_1 = 10000;
				int commitCounter_j_catch_unexpected_error_1_tDBOutput_1 = 0;
				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("Connection is set auto commit to '")
							+ (conn_j_catch_unexpected_error_1_tDBOutput_1.getAutoCommit()) + ("'."));

				int batchSize_j_catch_unexpected_error_1_tDBOutput_1 = 10000;
				int batchSizeCounter_j_catch_unexpected_error_1_tDBOutput_1 = 0;

				int count_j_catch_unexpected_error_1_tDBOutput_1 = 0;
				String update_j_catch_unexpected_error_1_tDBOutput_1 = "UPDATE \""
						+ tableName_j_catch_unexpected_error_1_tDBOutput_1 + "\" SET \"error\" = ? WHERE \"id\" = ?";
				java.sql.PreparedStatement pstmt_j_catch_unexpected_error_1_tDBOutput_1 = conn_j_catch_unexpected_error_1_tDBOutput_1
						.prepareStatement(update_j_catch_unexpected_error_1_tDBOutput_1);
				resourceMap.put("pstmt_j_catch_unexpected_error_1_tDBOutput_1",
						pstmt_j_catch_unexpected_error_1_tDBOutput_1);

				/**
				 * [j_catch_unexpected_error_1_tDBOutput_1 begin ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tMap_1 begin ] start
				 */

				ok_Hash.put("j_catch_unexpected_error_1_tMap_1", false);
				start_Hash.put("j_catch_unexpected_error_1_tMap_1", System.currentTimeMillis());

				currentComponent = "j_catch_unexpected_error_1_tMap_1";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0,
						"j_catch_unexpected_error_1_row2");

				int tos_count_j_catch_unexpected_error_1_tMap_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tMap_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_catch_unexpected_error_1_tMap_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_catch_unexpected_error_1_tMap_1 = new StringBuilder();
							log4jParamters_j_catch_unexpected_error_1_tMap_1.append("Parameters:");
							log4jParamters_j_catch_unexpected_error_1_tMap_1.append("LINK_STYLE" + " = " + "AUTO");
							log4jParamters_j_catch_unexpected_error_1_tMap_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tMap_1
									.append("TEMPORARY_DATA_DIRECTORY" + " = " + "");
							log4jParamters_j_catch_unexpected_error_1_tMap_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tMap_1
									.append("ROWS_BUFFER_SIZE" + " = " + "2000000");
							log4jParamters_j_catch_unexpected_error_1_tMap_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tMap_1
									.append("CHANGE_HASH_AND_EQUALS_FOR_BIGDECIMAL" + " = " + "true");
							log4jParamters_j_catch_unexpected_error_1_tMap_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_catch_unexpected_error_1_tMap_1 - "
										+ (log4jParamters_j_catch_unexpected_error_1_tMap_1));
						}
					}
					new BytesLimit65535_j_catch_unexpected_error_1_tMap_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_catch_unexpected_error_1_tMap_1", "j_catch_unexpected_error_1_tMap_1",
							"tMap");
					talendJobLogProcess(globalMap);
				}

// ###############################
// # Lookup's keys initialization
				int count_j_catch_unexpected_error_1_row2_j_catch_unexpected_error_1_tMap_1 = 0;

// ###############################        

// ###############################
// # Vars initialization
// ###############################

// ###############################
// # Outputs initialization
				int count_j_catch_unexpected_error_1_toFile_j_catch_unexpected_error_1_tMap_1 = 0;

				j_catch_unexpected_error_1_toFileStruct j_catch_unexpected_error_1_toFile_tmp = new j_catch_unexpected_error_1_toFileStruct();
// ###############################

				/**
				 * [j_catch_unexpected_error_1_tMap_1 begin ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tDBInput_1 begin ] start
				 */

				ok_Hash.put("j_catch_unexpected_error_1_tDBInput_1", false);
				start_Hash.put("j_catch_unexpected_error_1_tDBInput_1", System.currentTimeMillis());

				currentComponent = "j_catch_unexpected_error_1_tDBInput_1";

				int tos_count_j_catch_unexpected_error_1_tDBInput_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBInput_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_catch_unexpected_error_1_tDBInput_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_catch_unexpected_error_1_tDBInput_1 = new StringBuilder();
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append("Parameters:");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1
									.append("USE_EXISTING_CONNECTION" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append("DB_VERSION" + " = " + "V9_X");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1
									.append("HOST" + " = " + "context.g_BDD_OMNICANAL_HOST");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1
									.append("PORT" + " = " + "context.g_BDD_OMNICANAL_PORT");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1
									.append("DBNAME" + " = " + "context.g_BDD_OMNICANAL_DBNAME");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1
									.append("SCHEMA_DB" + " = " + "context.g_BDD_OMNICANAL_SCHEMA");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1
									.append("USER" + " = " + "context.g_BDD_OMNICANAL_USERNAME");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1
									.append("PASS" + " = "
											+ String.valueOf(routines.system.PasswordEncryptUtil
													.encryptPassword(context.g_BDD_OMNICANAL_PWD)).substring(0, 4)
											+ "...");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append("QUERYSTORE" + " = " + "\"\"");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append("QUERY" + " = "
									+ "\" SELECT  	id FROM files WHERE guid = '\"+ (String)globalMap.get(\"guid\") +\"' 	AND filename = '\"+ (String)globalMap.get(\"currentFile_Name\") +\"' ORDER BY id DESC \"");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1
									.append("SPECIFY_DATASOURCE_ALIAS" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append("PROPERTIES" + " = " + "\"\"");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append("USE_CURSOR" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1
									.append("TRIM_ALL_COLUMN" + " = " + "false");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(
									"TRIM_COLUMN" + " = " + "[{TRIM=" + ("false") + ", SCHEMA_COLUMN=" + ("id") + "}]");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlInput");
							log4jParamters_j_catch_unexpected_error_1_tDBInput_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_catch_unexpected_error_1_tDBInput_1 - "
										+ (log4jParamters_j_catch_unexpected_error_1_tDBInput_1));
						}
					}
					new BytesLimit65535_j_catch_unexpected_error_1_tDBInput_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_catch_unexpected_error_1_tDBInput_1", "__UNIQUE_NAME__<br><b>__TABLE__</b>",
							"tPostgresqlInput");
					talendJobLogProcess(globalMap);
				}

				int nb_line_j_catch_unexpected_error_1_tDBInput_1 = 0;
				java.sql.Connection conn_j_catch_unexpected_error_1_tDBInput_1 = null;
				String driverClass_j_catch_unexpected_error_1_tDBInput_1 = "org.postgresql.Driver";
				java.lang.Class jdbcclazz_j_catch_unexpected_error_1_tDBInput_1 = java.lang.Class
						.forName(driverClass_j_catch_unexpected_error_1_tDBInput_1);
				String dbUser_j_catch_unexpected_error_1_tDBInput_1 = context.g_BDD_OMNICANAL_USERNAME;

				final String decryptedPassword_j_catch_unexpected_error_1_tDBInput_1 = context.g_BDD_OMNICANAL_PWD;

				String dbPwd_j_catch_unexpected_error_1_tDBInput_1 = decryptedPassword_j_catch_unexpected_error_1_tDBInput_1;

				String url_j_catch_unexpected_error_1_tDBInput_1 = "jdbc:postgresql://" + context.g_BDD_OMNICANAL_HOST
						+ ":" + context.g_BDD_OMNICANAL_PORT + "/" + context.g_BDD_OMNICANAL_DBNAME;

				log.debug("j_catch_unexpected_error_1_tDBInput_1 - Driver ClassName: "
						+ driverClass_j_catch_unexpected_error_1_tDBInput_1 + ".");

				log.debug("j_catch_unexpected_error_1_tDBInput_1 - Connection attempt to '"
						+ url_j_catch_unexpected_error_1_tDBInput_1 + "' with the username '"
						+ dbUser_j_catch_unexpected_error_1_tDBInput_1 + "'.");

				conn_j_catch_unexpected_error_1_tDBInput_1 = java.sql.DriverManager.getConnection(
						url_j_catch_unexpected_error_1_tDBInput_1, dbUser_j_catch_unexpected_error_1_tDBInput_1,
						dbPwd_j_catch_unexpected_error_1_tDBInput_1);
				log.debug("j_catch_unexpected_error_1_tDBInput_1 - Connection to '"
						+ url_j_catch_unexpected_error_1_tDBInput_1 + "' has succeeded.");

				log.debug("j_catch_unexpected_error_1_tDBInput_1 - Connection is set auto commit to 'false'.");

				conn_j_catch_unexpected_error_1_tDBInput_1.setAutoCommit(false);

				java.sql.Statement stmt_j_catch_unexpected_error_1_tDBInput_1 = conn_j_catch_unexpected_error_1_tDBInput_1
						.createStatement();

				String dbquery_j_catch_unexpected_error_1_tDBInput_1 = "\nSELECT \n	id\nFROM files\nWHERE guid = '"
						+ (String) globalMap.get("guid") + "'\n	AND filename = '"
						+ (String) globalMap.get("currentFile_Name") + "'\nORDER BY id DESC\n";

				log.debug("j_catch_unexpected_error_1_tDBInput_1 - Executing the query: '"
						+ dbquery_j_catch_unexpected_error_1_tDBInput_1 + "'.");

				globalMap.put("j_catch_unexpected_error_1_tDBInput_1_QUERY",
						dbquery_j_catch_unexpected_error_1_tDBInput_1);
				java.sql.ResultSet rs_j_catch_unexpected_error_1_tDBInput_1 = null;

				try {
					rs_j_catch_unexpected_error_1_tDBInput_1 = stmt_j_catch_unexpected_error_1_tDBInput_1
							.executeQuery(dbquery_j_catch_unexpected_error_1_tDBInput_1);
					java.sql.ResultSetMetaData rsmd_j_catch_unexpected_error_1_tDBInput_1 = rs_j_catch_unexpected_error_1_tDBInput_1
							.getMetaData();
					int colQtyInRs_j_catch_unexpected_error_1_tDBInput_1 = rsmd_j_catch_unexpected_error_1_tDBInput_1
							.getColumnCount();

					String tmpContent_j_catch_unexpected_error_1_tDBInput_1 = null;

					log.debug("j_catch_unexpected_error_1_tDBInput_1 - Retrieving records from the database.");

					while (rs_j_catch_unexpected_error_1_tDBInput_1.next()) {
						nb_line_j_catch_unexpected_error_1_tDBInput_1++;

						if (colQtyInRs_j_catch_unexpected_error_1_tDBInput_1 < 1) {
							j_catch_unexpected_error_1_row2.id = null;
						} else {

							j_catch_unexpected_error_1_row2.id = rs_j_catch_unexpected_error_1_tDBInput_1.getInt(1);
							if (rs_j_catch_unexpected_error_1_tDBInput_1.wasNull()) {
								j_catch_unexpected_error_1_row2.id = null;
							}
						}

						log.debug("j_catch_unexpected_error_1_tDBInput_1 - Retrieving the record "
								+ nb_line_j_catch_unexpected_error_1_tDBInput_1 + ".");

						/**
						 * [j_catch_unexpected_error_1_tDBInput_1 begin ] stop
						 */

						/**
						 * [j_catch_unexpected_error_1_tDBInput_1 main ] start
						 */

						currentComponent = "j_catch_unexpected_error_1_tDBInput_1";

						tos_count_j_catch_unexpected_error_1_tDBInput_1++;

						/**
						 * [j_catch_unexpected_error_1_tDBInput_1 main ] stop
						 */

						/**
						 * [j_catch_unexpected_error_1_tDBInput_1 process_data_begin ] start
						 */

						currentComponent = "j_catch_unexpected_error_1_tDBInput_1";

						/**
						 * [j_catch_unexpected_error_1_tDBInput_1 process_data_begin ] stop
						 */

						/**
						 * [j_catch_unexpected_error_1_tMap_1 main ] start
						 */

						currentComponent = "j_catch_unexpected_error_1_tMap_1";

						if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

								, "j_catch_unexpected_error_1_row2", "j_catch_unexpected_error_1_tDBInput_1",
								"__UNIQUE_NAME__<br><b>__TABLE__</b>", "tPostgresqlInput",
								"j_catch_unexpected_error_1_tMap_1", "j_catch_unexpected_error_1_tMap_1", "tMap"

						)) {
							talendJobLogProcess(globalMap);
						}

						if (log.isTraceEnabled()) {
							log.trace(
									"j_catch_unexpected_error_1_row2 - " + (j_catch_unexpected_error_1_row2 == null ? ""
											: j_catch_unexpected_error_1_row2.toLogString()));
						}

						boolean hasCasePrimitiveKeyWithNull_j_catch_unexpected_error_1_tMap_1 = false;

						// ###############################
						// # Input tables (lookups)
						boolean rejectedInnerJoin_j_catch_unexpected_error_1_tMap_1 = false;
						boolean mainRowRejected_j_catch_unexpected_error_1_tMap_1 = false;

						// ###############################
						{ // start of Var scope

							// ###############################
							// # Vars tables
							// ###############################
							// ###############################
							// # Output tables

							j_catch_unexpected_error_1_toFile = null;

// # Output table : 'j_catch_unexpected_error_1_toFile'
							count_j_catch_unexpected_error_1_toFile_j_catch_unexpected_error_1_tMap_1++;

							j_catch_unexpected_error_1_toFile_tmp.id = j_catch_unexpected_error_1_row2.id;
							j_catch_unexpected_error_1_toFile_tmp.error = true;
							j_catch_unexpected_error_1_toFile = j_catch_unexpected_error_1_toFile_tmp;
							log.debug("j_catch_unexpected_error_1_tMap_1 - Outputting the record "
									+ count_j_catch_unexpected_error_1_toFile_j_catch_unexpected_error_1_tMap_1
									+ " of the output table 'j_catch_unexpected_error_1_toFile'.");

// ###############################

						} // end of Var scope

						rejectedInnerJoin_j_catch_unexpected_error_1_tMap_1 = false;

						tos_count_j_catch_unexpected_error_1_tMap_1++;

						/**
						 * [j_catch_unexpected_error_1_tMap_1 main ] stop
						 */

						/**
						 * [j_catch_unexpected_error_1_tMap_1 process_data_begin ] start
						 */

						currentComponent = "j_catch_unexpected_error_1_tMap_1";

						/**
						 * [j_catch_unexpected_error_1_tMap_1 process_data_begin ] stop
						 */
// Start of branch "j_catch_unexpected_error_1_toFile"
						if (j_catch_unexpected_error_1_toFile != null) {

							/**
							 * [j_catch_unexpected_error_1_tDBOutput_1 main ] start
							 */

							currentComponent = "j_catch_unexpected_error_1_tDBOutput_1";

							if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

									, "j_catch_unexpected_error_1_toFile", "j_catch_unexpected_error_1_tMap_1",
									"j_catch_unexpected_error_1_tMap_1", "tMap",
									"j_catch_unexpected_error_1_tDBOutput_1",
									"__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>", "tPostgresqlOutput"

							)) {
								talendJobLogProcess(globalMap);
							}

							if (log.isTraceEnabled()) {
								log.trace("j_catch_unexpected_error_1_toFile - "
										+ (j_catch_unexpected_error_1_toFile == null ? ""
												: j_catch_unexpected_error_1_toFile.toLogString()));
							}

							whetherReject_j_catch_unexpected_error_1_tDBOutput_1 = false;
							if (j_catch_unexpected_error_1_toFile.error == null) {
								pstmt_j_catch_unexpected_error_1_tDBOutput_1.setNull(1, java.sql.Types.BOOLEAN);
							} else {
								pstmt_j_catch_unexpected_error_1_tDBOutput_1.setBoolean(1,
										j_catch_unexpected_error_1_toFile.error);
							}

							if (j_catch_unexpected_error_1_toFile.id == null) {
								pstmt_j_catch_unexpected_error_1_tDBOutput_1.setNull(
										2 + count_j_catch_unexpected_error_1_tDBOutput_1, java.sql.Types.INTEGER);
							} else {
								pstmt_j_catch_unexpected_error_1_tDBOutput_1.setInt(
										2 + count_j_catch_unexpected_error_1_tDBOutput_1,
										j_catch_unexpected_error_1_toFile.id);
							}

							pstmt_j_catch_unexpected_error_1_tDBOutput_1.addBatch();
							nb_line_j_catch_unexpected_error_1_tDBOutput_1++;

							if (log.isDebugEnabled())
								log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("Adding the record ")
										+ (nb_line_j_catch_unexpected_error_1_tDBOutput_1) + (" to the ") + ("UPDATE")
										+ (" batch."));
							batchSizeCounter_j_catch_unexpected_error_1_tDBOutput_1++;

							if ((batchSize_j_catch_unexpected_error_1_tDBOutput_1 > 0)
									&& (batchSize_j_catch_unexpected_error_1_tDBOutput_1 <= batchSizeCounter_j_catch_unexpected_error_1_tDBOutput_1)) {
								try {
									int countSum_j_catch_unexpected_error_1_tDBOutput_1 = 0;

									if (log.isDebugEnabled())
										log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("Executing the ")
												+ ("UPDATE") + (" batch."));
									for (int countEach_j_catch_unexpected_error_1_tDBOutput_1 : pstmt_j_catch_unexpected_error_1_tDBOutput_1
											.executeBatch()) {
										countSum_j_catch_unexpected_error_1_tDBOutput_1 += (countEach_j_catch_unexpected_error_1_tDBOutput_1 < 0
												? 0
												: countEach_j_catch_unexpected_error_1_tDBOutput_1);
									}
									if (log.isDebugEnabled())
										log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("The ") + ("UPDATE")
												+ (" batch execution has succeeded."));
									rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_1 += countSum_j_catch_unexpected_error_1_tDBOutput_1;

									updatedCount_j_catch_unexpected_error_1_tDBOutput_1 += countSum_j_catch_unexpected_error_1_tDBOutput_1;

									batchSizeCounter_j_catch_unexpected_error_1_tDBOutput_1 = 0;
								} catch (java.sql.BatchUpdateException e_j_catch_unexpected_error_1_tDBOutput_1) {
									globalMap.put("j_catch_unexpected_error_1_tDBOutput_1_ERROR_MESSAGE",
											e_j_catch_unexpected_error_1_tDBOutput_1.getMessage());
									java.sql.SQLException ne_j_catch_unexpected_error_1_tDBOutput_1 = e_j_catch_unexpected_error_1_tDBOutput_1
											.getNextException(), sqle_j_catch_unexpected_error_1_tDBOutput_1 = null;
									String errormessage_j_catch_unexpected_error_1_tDBOutput_1;
									if (ne_j_catch_unexpected_error_1_tDBOutput_1 != null) {
										// build new exception to provide the original cause
										sqle_j_catch_unexpected_error_1_tDBOutput_1 = new java.sql.SQLException(
												e_j_catch_unexpected_error_1_tDBOutput_1.getMessage() + "\ncaused by: "
														+ ne_j_catch_unexpected_error_1_tDBOutput_1.getMessage(),
												ne_j_catch_unexpected_error_1_tDBOutput_1.getSQLState(),
												ne_j_catch_unexpected_error_1_tDBOutput_1.getErrorCode(),
												ne_j_catch_unexpected_error_1_tDBOutput_1);
										errormessage_j_catch_unexpected_error_1_tDBOutput_1 = sqle_j_catch_unexpected_error_1_tDBOutput_1
												.getMessage();
									} else {
										errormessage_j_catch_unexpected_error_1_tDBOutput_1 = e_j_catch_unexpected_error_1_tDBOutput_1
												.getMessage();
									}

									int countSum_j_catch_unexpected_error_1_tDBOutput_1 = 0;
									for (int countEach_j_catch_unexpected_error_1_tDBOutput_1 : e_j_catch_unexpected_error_1_tDBOutput_1
											.getUpdateCounts()) {
										countSum_j_catch_unexpected_error_1_tDBOutput_1 += (countEach_j_catch_unexpected_error_1_tDBOutput_1 < 0
												? 0
												: countEach_j_catch_unexpected_error_1_tDBOutput_1);
									}
									rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_1 += countSum_j_catch_unexpected_error_1_tDBOutput_1;

									updatedCount_j_catch_unexpected_error_1_tDBOutput_1 += countSum_j_catch_unexpected_error_1_tDBOutput_1;

									log.error("j_catch_unexpected_error_1_tDBOutput_1 - "
											+ (errormessage_j_catch_unexpected_error_1_tDBOutput_1));
									System.err.println(errormessage_j_catch_unexpected_error_1_tDBOutput_1);

								}
							}

							commitCounter_j_catch_unexpected_error_1_tDBOutput_1++;
							if (commitEvery_j_catch_unexpected_error_1_tDBOutput_1 <= commitCounter_j_catch_unexpected_error_1_tDBOutput_1) {
								if ((batchSize_j_catch_unexpected_error_1_tDBOutput_1 > 0)
										&& (batchSizeCounter_j_catch_unexpected_error_1_tDBOutput_1 > 0)) {
									try {
										int countSum_j_catch_unexpected_error_1_tDBOutput_1 = 0;

										if (log.isDebugEnabled())
											log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("Executing the ")
													+ ("UPDATE") + (" batch."));
										for (int countEach_j_catch_unexpected_error_1_tDBOutput_1 : pstmt_j_catch_unexpected_error_1_tDBOutput_1
												.executeBatch()) {
											countSum_j_catch_unexpected_error_1_tDBOutput_1 += (countEach_j_catch_unexpected_error_1_tDBOutput_1 < 0
													? 0
													: countEach_j_catch_unexpected_error_1_tDBOutput_1);
										}
										if (log.isDebugEnabled())
											log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("The ")
													+ ("UPDATE") + (" batch execution has succeeded."));
										rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_1 += countSum_j_catch_unexpected_error_1_tDBOutput_1;

										updatedCount_j_catch_unexpected_error_1_tDBOutput_1 += countSum_j_catch_unexpected_error_1_tDBOutput_1;

										batchSizeCounter_j_catch_unexpected_error_1_tDBOutput_1 = 0;
									} catch (java.sql.BatchUpdateException e_j_catch_unexpected_error_1_tDBOutput_1) {
										globalMap.put("j_catch_unexpected_error_1_tDBOutput_1_ERROR_MESSAGE",
												e_j_catch_unexpected_error_1_tDBOutput_1.getMessage());
										java.sql.SQLException ne_j_catch_unexpected_error_1_tDBOutput_1 = e_j_catch_unexpected_error_1_tDBOutput_1
												.getNextException(), sqle_j_catch_unexpected_error_1_tDBOutput_1 = null;
										String errormessage_j_catch_unexpected_error_1_tDBOutput_1;
										if (ne_j_catch_unexpected_error_1_tDBOutput_1 != null) {
											// build new exception to provide the original cause
											sqle_j_catch_unexpected_error_1_tDBOutput_1 = new java.sql.SQLException(
													e_j_catch_unexpected_error_1_tDBOutput_1.getMessage()
															+ "\ncaused by: "
															+ ne_j_catch_unexpected_error_1_tDBOutput_1.getMessage(),
													ne_j_catch_unexpected_error_1_tDBOutput_1.getSQLState(),
													ne_j_catch_unexpected_error_1_tDBOutput_1.getErrorCode(),
													ne_j_catch_unexpected_error_1_tDBOutput_1);
											errormessage_j_catch_unexpected_error_1_tDBOutput_1 = sqle_j_catch_unexpected_error_1_tDBOutput_1
													.getMessage();
										} else {
											errormessage_j_catch_unexpected_error_1_tDBOutput_1 = e_j_catch_unexpected_error_1_tDBOutput_1
													.getMessage();
										}

										int countSum_j_catch_unexpected_error_1_tDBOutput_1 = 0;
										for (int countEach_j_catch_unexpected_error_1_tDBOutput_1 : e_j_catch_unexpected_error_1_tDBOutput_1
												.getUpdateCounts()) {
											countSum_j_catch_unexpected_error_1_tDBOutput_1 += (countEach_j_catch_unexpected_error_1_tDBOutput_1 < 0
													? 0
													: countEach_j_catch_unexpected_error_1_tDBOutput_1);
										}
										rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_1 += countSum_j_catch_unexpected_error_1_tDBOutput_1;

										updatedCount_j_catch_unexpected_error_1_tDBOutput_1 += countSum_j_catch_unexpected_error_1_tDBOutput_1;

										log.error("j_catch_unexpected_error_1_tDBOutput_1 - "
												+ (errormessage_j_catch_unexpected_error_1_tDBOutput_1));
										System.err.println(errormessage_j_catch_unexpected_error_1_tDBOutput_1);

									}
								}
								if (rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_1 != 0) {

									if (log.isDebugEnabled())
										log.debug("j_catch_unexpected_error_1_tDBOutput_1 - "
												+ ("Connection starting to commit ")
												+ (rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_1)
												+ (" record(s)."));
								}
								conn_j_catch_unexpected_error_1_tDBOutput_1.commit();
								if (rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_1 != 0) {

									if (log.isDebugEnabled())
										log.debug("j_catch_unexpected_error_1_tDBOutput_1 - "
												+ ("Connection commit has succeeded."));
									rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_1 = 0;
								}
								commitCounter_j_catch_unexpected_error_1_tDBOutput_1 = 0;
							}

							tos_count_j_catch_unexpected_error_1_tDBOutput_1++;

							/**
							 * [j_catch_unexpected_error_1_tDBOutput_1 main ] stop
							 */

							/**
							 * [j_catch_unexpected_error_1_tDBOutput_1 process_data_begin ] start
							 */

							currentComponent = "j_catch_unexpected_error_1_tDBOutput_1";

							/**
							 * [j_catch_unexpected_error_1_tDBOutput_1 process_data_begin ] stop
							 */

							/**
							 * [j_catch_unexpected_error_1_tDBOutput_1 process_data_end ] start
							 */

							currentComponent = "j_catch_unexpected_error_1_tDBOutput_1";

							/**
							 * [j_catch_unexpected_error_1_tDBOutput_1 process_data_end ] stop
							 */

						} // End of branch "j_catch_unexpected_error_1_toFile"

						/**
						 * [j_catch_unexpected_error_1_tMap_1 process_data_end ] start
						 */

						currentComponent = "j_catch_unexpected_error_1_tMap_1";

						/**
						 * [j_catch_unexpected_error_1_tMap_1 process_data_end ] stop
						 */

						/**
						 * [j_catch_unexpected_error_1_tDBInput_1 process_data_end ] start
						 */

						currentComponent = "j_catch_unexpected_error_1_tDBInput_1";

						/**
						 * [j_catch_unexpected_error_1_tDBInput_1 process_data_end ] stop
						 */

						/**
						 * [j_catch_unexpected_error_1_tDBInput_1 end ] start
						 */

						currentComponent = "j_catch_unexpected_error_1_tDBInput_1";

					}
				} finally {
					if (rs_j_catch_unexpected_error_1_tDBInput_1 != null) {
						rs_j_catch_unexpected_error_1_tDBInput_1.close();
					}
					if (stmt_j_catch_unexpected_error_1_tDBInput_1 != null) {
						stmt_j_catch_unexpected_error_1_tDBInput_1.close();
					}
					if (conn_j_catch_unexpected_error_1_tDBInput_1 != null
							&& !conn_j_catch_unexpected_error_1_tDBInput_1.isClosed()) {

						log.debug("j_catch_unexpected_error_1_tDBInput_1 - Connection starting to commit.");

						conn_j_catch_unexpected_error_1_tDBInput_1.commit();

						log.debug("j_catch_unexpected_error_1_tDBInput_1 - Connection commit has succeeded.");

						log.debug("j_catch_unexpected_error_1_tDBInput_1 - Closing the connection to the database.");

						conn_j_catch_unexpected_error_1_tDBInput_1.close();

						if ("com.mysql.cj.jdbc.Driver".equals((String) globalMap.get("driverClass_"))
								&& routines.system.BundleUtils.inOSGi()) {
							Class.forName("com.mysql.cj.jdbc.AbandonedConnectionCleanupThread")
									.getMethod("checkedShutdown").invoke(null, (Object[]) null);
						}

						log.debug("j_catch_unexpected_error_1_tDBInput_1 - Connection to the database closed.");

					}

				}
				globalMap.put("j_catch_unexpected_error_1_tDBInput_1_NB_LINE",
						nb_line_j_catch_unexpected_error_1_tDBInput_1);
				log.debug("j_catch_unexpected_error_1_tDBInput_1 - Retrieved records count: "
						+ nb_line_j_catch_unexpected_error_1_tDBInput_1 + " .");

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBInput_1 - " + ("Done."));

				ok_Hash.put("j_catch_unexpected_error_1_tDBInput_1", true);
				end_Hash.put("j_catch_unexpected_error_1_tDBInput_1", System.currentTimeMillis());

				/**
				 * [j_catch_unexpected_error_1_tDBInput_1 end ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tMap_1 end ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tMap_1";

// ###############################
// # Lookup hashes releasing
// ###############################      
				log.debug(
						"j_catch_unexpected_error_1_tMap_1 - Written records count in the table 'j_catch_unexpected_error_1_toFile': "
								+ count_j_catch_unexpected_error_1_toFile_j_catch_unexpected_error_1_tMap_1 + ".");

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId,
						"j_catch_unexpected_error_1_row2", 2, 0, "j_catch_unexpected_error_1_tDBInput_1",
						"__UNIQUE_NAME__<br><b>__TABLE__</b>", "tPostgresqlInput", "j_catch_unexpected_error_1_tMap_1",
						"j_catch_unexpected_error_1_tMap_1", "tMap", "output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tMap_1 - " + ("Done."));

				ok_Hash.put("j_catch_unexpected_error_1_tMap_1", true);
				end_Hash.put("j_catch_unexpected_error_1_tMap_1", System.currentTimeMillis());

				/**
				 * [j_catch_unexpected_error_1_tMap_1 end ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tDBOutput_1 end ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tDBOutput_1";

				try {
					int countSum_j_catch_unexpected_error_1_tDBOutput_1 = 0;
					if (pstmt_j_catch_unexpected_error_1_tDBOutput_1 != null
							&& batchSizeCounter_j_catch_unexpected_error_1_tDBOutput_1 > 0) {

						if (log.isDebugEnabled())
							log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("Executing the ") + ("UPDATE")
									+ (" batch."));
						for (int countEach_j_catch_unexpected_error_1_tDBOutput_1 : pstmt_j_catch_unexpected_error_1_tDBOutput_1
								.executeBatch()) {
							countSum_j_catch_unexpected_error_1_tDBOutput_1 += (countEach_j_catch_unexpected_error_1_tDBOutput_1 < 0
									? 0
									: countEach_j_catch_unexpected_error_1_tDBOutput_1);
						}
						rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_1 += countSum_j_catch_unexpected_error_1_tDBOutput_1;

						if (log.isDebugEnabled())
							log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("The ") + ("UPDATE")
									+ (" batch execution has succeeded."));
					}

					updatedCount_j_catch_unexpected_error_1_tDBOutput_1 += countSum_j_catch_unexpected_error_1_tDBOutput_1;

				} catch (java.sql.BatchUpdateException e_j_catch_unexpected_error_1_tDBOutput_1) {
					globalMap.put("j_catch_unexpected_error_1_tDBOutput_1_ERROR_MESSAGE",
							e_j_catch_unexpected_error_1_tDBOutput_1.getMessage());
					java.sql.SQLException ne_j_catch_unexpected_error_1_tDBOutput_1 = e_j_catch_unexpected_error_1_tDBOutput_1
							.getNextException(), sqle_j_catch_unexpected_error_1_tDBOutput_1 = null;
					String errormessage_j_catch_unexpected_error_1_tDBOutput_1;
					if (ne_j_catch_unexpected_error_1_tDBOutput_1 != null) {
						// build new exception to provide the original cause
						sqle_j_catch_unexpected_error_1_tDBOutput_1 = new java.sql.SQLException(
								e_j_catch_unexpected_error_1_tDBOutput_1.getMessage() + "\ncaused by: "
										+ ne_j_catch_unexpected_error_1_tDBOutput_1.getMessage(),
								ne_j_catch_unexpected_error_1_tDBOutput_1.getSQLState(),
								ne_j_catch_unexpected_error_1_tDBOutput_1.getErrorCode(),
								ne_j_catch_unexpected_error_1_tDBOutput_1);
						errormessage_j_catch_unexpected_error_1_tDBOutput_1 = sqle_j_catch_unexpected_error_1_tDBOutput_1
								.getMessage();
					} else {
						errormessage_j_catch_unexpected_error_1_tDBOutput_1 = e_j_catch_unexpected_error_1_tDBOutput_1
								.getMessage();
					}

					int countSum_j_catch_unexpected_error_1_tDBOutput_1 = 0;
					for (int countEach_j_catch_unexpected_error_1_tDBOutput_1 : e_j_catch_unexpected_error_1_tDBOutput_1
							.getUpdateCounts()) {
						countSum_j_catch_unexpected_error_1_tDBOutput_1 += (countEach_j_catch_unexpected_error_1_tDBOutput_1 < 0
								? 0
								: countEach_j_catch_unexpected_error_1_tDBOutput_1);
					}
					rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_1 += countSum_j_catch_unexpected_error_1_tDBOutput_1;

					updatedCount_j_catch_unexpected_error_1_tDBOutput_1 += countSum_j_catch_unexpected_error_1_tDBOutput_1;

					log.error("j_catch_unexpected_error_1_tDBOutput_1 - "
							+ (errormessage_j_catch_unexpected_error_1_tDBOutput_1));
					System.err.println(errormessage_j_catch_unexpected_error_1_tDBOutput_1);

				}

				if (pstmt_j_catch_unexpected_error_1_tDBOutput_1 != null) {

					pstmt_j_catch_unexpected_error_1_tDBOutput_1.close();
					resourceMap.remove("pstmt_j_catch_unexpected_error_1_tDBOutput_1");
				}
				resourceMap.put("statementClosed_j_catch_unexpected_error_1_tDBOutput_1", true);
				if (rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_1 != 0) {

					if (log.isDebugEnabled())
						log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("Connection starting to commit ")
								+ (rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_1) + (" record(s)."));
				}
				conn_j_catch_unexpected_error_1_tDBOutput_1.commit();
				if (rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_1 != 0) {

					if (log.isDebugEnabled())
						log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("Connection commit has succeeded."));
					rowsToCommitCount_j_catch_unexpected_error_1_tDBOutput_1 = 0;
				}
				commitCounter_j_catch_unexpected_error_1_tDBOutput_1 = 0;

				if (log.isDebugEnabled())
					log.debug(
							"j_catch_unexpected_error_1_tDBOutput_1 - " + ("Closing the connection to the database."));
				conn_j_catch_unexpected_error_1_tDBOutput_1.close();

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("Connection to the database has closed."));
				resourceMap.put("finish_j_catch_unexpected_error_1_tDBOutput_1", true);

				nb_line_deleted_j_catch_unexpected_error_1_tDBOutput_1 = nb_line_deleted_j_catch_unexpected_error_1_tDBOutput_1
						+ deletedCount_j_catch_unexpected_error_1_tDBOutput_1;
				nb_line_update_j_catch_unexpected_error_1_tDBOutput_1 = nb_line_update_j_catch_unexpected_error_1_tDBOutput_1
						+ updatedCount_j_catch_unexpected_error_1_tDBOutput_1;
				nb_line_inserted_j_catch_unexpected_error_1_tDBOutput_1 = nb_line_inserted_j_catch_unexpected_error_1_tDBOutput_1
						+ insertedCount_j_catch_unexpected_error_1_tDBOutput_1;
				nb_line_rejected_j_catch_unexpected_error_1_tDBOutput_1 = nb_line_rejected_j_catch_unexpected_error_1_tDBOutput_1
						+ rejectedCount_j_catch_unexpected_error_1_tDBOutput_1;

				globalMap.put("j_catch_unexpected_error_1_tDBOutput_1_NB_LINE",
						nb_line_j_catch_unexpected_error_1_tDBOutput_1);
				globalMap.put("j_catch_unexpected_error_1_tDBOutput_1_NB_LINE_UPDATED",
						nb_line_update_j_catch_unexpected_error_1_tDBOutput_1);
				globalMap.put("j_catch_unexpected_error_1_tDBOutput_1_NB_LINE_INSERTED",
						nb_line_inserted_j_catch_unexpected_error_1_tDBOutput_1);
				globalMap.put("j_catch_unexpected_error_1_tDBOutput_1_NB_LINE_DELETED",
						nb_line_deleted_j_catch_unexpected_error_1_tDBOutput_1);
				globalMap.put("j_catch_unexpected_error_1_tDBOutput_1_NB_LINE_REJECTED",
						nb_line_rejected_j_catch_unexpected_error_1_tDBOutput_1);

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("Has ") + ("updated") + (" ")
							+ (nb_line_update_j_catch_unexpected_error_1_tDBOutput_1) + (" record(s)."));

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId,
						"j_catch_unexpected_error_1_toFile", 2, 0, "j_catch_unexpected_error_1_tMap_1",
						"j_catch_unexpected_error_1_tMap_1", "tMap", "j_catch_unexpected_error_1_tDBOutput_1",
						"__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>", "tPostgresqlOutput",
						"output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("j_catch_unexpected_error_1_tDBOutput_1 - " + ("Done."));

				ok_Hash.put("j_catch_unexpected_error_1_tDBOutput_1", true);
				end_Hash.put("j_catch_unexpected_error_1_tDBOutput_1", System.currentTimeMillis());

				/**
				 * [j_catch_unexpected_error_1_tDBOutput_1 end ] stop
				 */

			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_catch_unexpected_error_1_tDBInput_1 finally ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tDBInput_1";

				/**
				 * [j_catch_unexpected_error_1_tDBInput_1 finally ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tMap_1 finally ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tMap_1";

				/**
				 * [j_catch_unexpected_error_1_tMap_1 finally ] stop
				 */

				/**
				 * [j_catch_unexpected_error_1_tDBOutput_1 finally ] start
				 */

				currentComponent = "j_catch_unexpected_error_1_tDBOutput_1";

				try {
					if (resourceMap.get("statementClosed_j_catch_unexpected_error_1_tDBOutput_1") == null) {
						java.sql.PreparedStatement pstmtToClose_j_catch_unexpected_error_1_tDBOutput_1 = null;
						if ((pstmtToClose_j_catch_unexpected_error_1_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap
								.remove("pstmt_j_catch_unexpected_error_1_tDBOutput_1")) != null) {
							pstmtToClose_j_catch_unexpected_error_1_tDBOutput_1.close();
						}
					}
				} finally {
					if (resourceMap.get("finish_j_catch_unexpected_error_1_tDBOutput_1") == null) {
						java.sql.Connection ctn_j_catch_unexpected_error_1_tDBOutput_1 = null;
						if ((ctn_j_catch_unexpected_error_1_tDBOutput_1 = (java.sql.Connection) resourceMap
								.get("conn_j_catch_unexpected_error_1_tDBOutput_1")) != null) {
							try {
								if (log.isDebugEnabled())
									log.debug("j_catch_unexpected_error_1_tDBOutput_1 - "
											+ ("Closing the connection to the database."));
								ctn_j_catch_unexpected_error_1_tDBOutput_1.close();
								if (log.isDebugEnabled())
									log.debug("j_catch_unexpected_error_1_tDBOutput_1 - "
											+ ("Connection to the database has closed."));
							} catch (java.sql.SQLException sqlEx_j_catch_unexpected_error_1_tDBOutput_1) {
								String errorMessage_j_catch_unexpected_error_1_tDBOutput_1 = "failed to close the connection in j_catch_unexpected_error_1_tDBOutput_1 :"
										+ sqlEx_j_catch_unexpected_error_1_tDBOutput_1.getMessage();
								log.error("j_catch_unexpected_error_1_tDBOutput_1 - "
										+ (errorMessage_j_catch_unexpected_error_1_tDBOutput_1));
								System.err.println(errorMessage_j_catch_unexpected_error_1_tDBOutput_1);
							}
						}
					}
				}

				/**
				 * [j_catch_unexpected_error_1_tDBOutput_1 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_catch_unexpected_error_1_tDBInput_1_SUBPROCESS_STATE", 1);
	}

	public void tPostjob_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tPostjob_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tPostjob_1 begin ] start
				 */

				ok_Hash.put("tPostjob_1", false);
				start_Hash.put("tPostjob_1", System.currentTimeMillis());

				currentComponent = "tPostjob_1";

				int tos_count_tPostjob_1 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("tPostjob_1", "tPostjob_1", "tPostjob");
					talendJobLogProcess(globalMap);
				}

				/**
				 * [tPostjob_1 begin ] stop
				 */

				/**
				 * [tPostjob_1 main ] start
				 */

				currentComponent = "tPostjob_1";

				tos_count_tPostjob_1++;

				/**
				 * [tPostjob_1 main ] stop
				 */

				/**
				 * [tPostjob_1 process_data_begin ] start
				 */

				currentComponent = "tPostjob_1";

				/**
				 * [tPostjob_1 process_data_begin ] stop
				 */

				/**
				 * [tPostjob_1 process_data_end ] start
				 */

				currentComponent = "tPostjob_1";

				/**
				 * [tPostjob_1 process_data_end ] stop
				 */

				/**
				 * [tPostjob_1 end ] start
				 */

				currentComponent = "tPostjob_1";

				ok_Hash.put("tPostjob_1", true);
				end_Hash.put("tPostjob_1", System.currentTimeMillis());

				if (execStat) {
					runStat.updateStatOnConnection("OnComponentOk2", 0, "ok");
				}
				tJava_3Process(globalMap);

				/**
				 * [tPostjob_1 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tPostjob_1 finally ] start
				 */

				currentComponent = "tPostjob_1";

				/**
				 * [tPostjob_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tPostjob_1_SUBPROCESS_STATE", 1);
	}

	public void tJava_3Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tJava_3_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tJava_3 begin ] start
				 */

				ok_Hash.put("tJava_3", false);
				start_Hash.put("tJava_3", System.currentTimeMillis());

				currentComponent = "tJava_3";

				int tos_count_tJava_3 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("tJava_3", "__UNIQUE_NAME__<br><b>End</b>", "tJava");
					talendJobLogProcess(globalMap);
				}

// DEBUG
				System.out.println("=> " + jobName + " : end at " + TalendDate.getDate("CCYYMMDD hh:mm:ss"));

				/**
				 * [tJava_3 begin ] stop
				 */

				/**
				 * [tJava_3 main ] start
				 */

				currentComponent = "tJava_3";

				tos_count_tJava_3++;

				/**
				 * [tJava_3 main ] stop
				 */

				/**
				 * [tJava_3 process_data_begin ] start
				 */

				currentComponent = "tJava_3";

				/**
				 * [tJava_3 process_data_begin ] stop
				 */

				/**
				 * [tJava_3 process_data_end ] start
				 */

				currentComponent = "tJava_3";

				/**
				 * [tJava_3 process_data_end ] stop
				 */

				/**
				 * [tJava_3 end ] start
				 */

				currentComponent = "tJava_3";

				ok_Hash.put("tJava_3", true);
				end_Hash.put("tJava_3", System.currentTimeMillis());

				/**
				 * [tJava_3 end ] stop
				 */
			} // end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:tJava_3:OnSubjobOk", "",
						Thread.currentThread().getId() + "", "", "", "", "", "");
			}

			if (execStat) {
				runStat.updateStatOnConnection("OnSubjobOk3", 0, "ok");
			}

			j_end_exec_1_tDBConnection_1Process(globalMap);

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tJava_3 finally ] start
				 */

				currentComponent = "tJava_3";

				/**
				 * [tJava_3 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tJava_3_SUBPROCESS_STATE", 1);
	}

	public void j_end_exec_1_tDBConnection_1Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("j_end_exec_1_tDBConnection_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [j_end_exec_1_tDBConnection_1 begin ] start
				 */

				ok_Hash.put("j_end_exec_1_tDBConnection_1", false);
				start_Hash.put("j_end_exec_1_tDBConnection_1", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tDBConnection_1";

				int tos_count_j_end_exec_1_tDBConnection_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBConnection_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_end_exec_1_tDBConnection_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_end_exec_1_tDBConnection_1 = new StringBuilder();
							log4jParamters_j_end_exec_1_tDBConnection_1.append("Parameters:");
							log4jParamters_j_end_exec_1_tDBConnection_1.append("DB_VERSION" + " = " + "V9_X");
							log4jParamters_j_end_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBConnection_1
									.append("HOST" + " = " + "context.g_BDD_OMNICANAL_HOST");
							log4jParamters_j_end_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBConnection_1
									.append("PORT" + " = " + "context.g_BDD_OMNICANAL_PORT");
							log4jParamters_j_end_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBConnection_1
									.append("DBNAME" + " = " + "context.g_BDD_OMNICANAL_DBNAME");
							log4jParamters_j_end_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBConnection_1
									.append("SCHEMA_DB" + " = " + "context.g_BDD_OMNICANAL_SCHEMA");
							log4jParamters_j_end_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBConnection_1
									.append("USER" + " = " + "context.g_BDD_OMNICANAL_USERNAME");
							log4jParamters_j_end_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBConnection_1
									.append("PASS" + " = "
											+ String.valueOf(routines.system.PasswordEncryptUtil
													.encryptPassword(context.g_BDD_OMNICANAL_PWD)).substring(0, 4)
											+ "...");
							log4jParamters_j_end_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBConnection_1
									.append("USE_SHARED_CONNECTION" + " = " + "true");
							log4jParamters_j_end_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBConnection_1
									.append("SHARED_CONNECTION_NAME" + " = " + "\"BDD_OMNICANAL\"");
							log4jParamters_j_end_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBConnection_1
									.append("PROPERTIES" + " = " + "\"noDatetimeStringSync=true\"");
							log4jParamters_j_end_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBConnection_1.append("AUTO_COMMIT" + " = " + "true");
							log4jParamters_j_end_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBConnection_1
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlConnection");
							log4jParamters_j_end_exec_1_tDBConnection_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_end_exec_1_tDBConnection_1 - "
										+ (log4jParamters_j_end_exec_1_tDBConnection_1));
						}
					}
					new BytesLimit65535_j_end_exec_1_tDBConnection_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tDBConnection_1",
							"__UNIQUE_NAME__<br><b>BDD_OMNICANAL</b><br><i>CommitAuto</i>", "tPostgresqlConnection");
					talendJobLogProcess(globalMap);
				}

				String dbProperties_j_end_exec_1_tDBConnection_1 = "noDatetimeStringSync=true";
				String url_j_end_exec_1_tDBConnection_1 = "jdbc:postgresql://" + context.g_BDD_OMNICANAL_HOST + ":"
						+ context.g_BDD_OMNICANAL_PORT + "/" + context.g_BDD_OMNICANAL_DBNAME;

				if (dbProperties_j_end_exec_1_tDBConnection_1 != null
						&& !"".equals(dbProperties_j_end_exec_1_tDBConnection_1.trim())) {
					url_j_end_exec_1_tDBConnection_1 = url_j_end_exec_1_tDBConnection_1 + "?"
							+ dbProperties_j_end_exec_1_tDBConnection_1;
				}
				String dbUser_j_end_exec_1_tDBConnection_1 = context.g_BDD_OMNICANAL_USERNAME;

				final String decryptedPassword_j_end_exec_1_tDBConnection_1 = context.g_BDD_OMNICANAL_PWD;
				String dbPwd_j_end_exec_1_tDBConnection_1 = decryptedPassword_j_end_exec_1_tDBConnection_1;

				java.sql.Connection conn_j_end_exec_1_tDBConnection_1 = null;

				java.util.Enumeration<java.sql.Driver> drivers_j_end_exec_1_tDBConnection_1 = java.sql.DriverManager
						.getDrivers();
				java.util.Set<String> redShiftDriverNames_j_end_exec_1_tDBConnection_1 = new java.util.HashSet<String>(
						java.util.Arrays.asList("com.amazon.redshift.jdbc.Driver", "com.amazon.redshift.jdbc41.Driver",
								"com.amazon.redshift.jdbc42.Driver"));
				while (drivers_j_end_exec_1_tDBConnection_1.hasMoreElements()) {
					java.sql.Driver d_j_end_exec_1_tDBConnection_1 = drivers_j_end_exec_1_tDBConnection_1.nextElement();
					if (redShiftDriverNames_j_end_exec_1_tDBConnection_1
							.contains(d_j_end_exec_1_tDBConnection_1.getClass().getName())) {
						try {
							java.sql.DriverManager.deregisterDriver(d_j_end_exec_1_tDBConnection_1);
							java.sql.DriverManager.registerDriver(d_j_end_exec_1_tDBConnection_1);
						} catch (java.lang.Exception e_j_end_exec_1_tDBConnection_1) {
							globalMap.put("j_end_exec_1_tDBConnection_1_ERROR_MESSAGE",
									e_j_end_exec_1_tDBConnection_1.getMessage());
							// do nothing
						}
					}
				}

				SharedDBConnectionLog4j.initLogger(log.getName(), "j_end_exec_1_tDBConnection_1");
				String sharedConnectionName_j_end_exec_1_tDBConnection_1 = "BDD_OMNICANAL";
				conn_j_end_exec_1_tDBConnection_1 = SharedDBConnectionLog4j.getDBConnection("org.postgresql.Driver",
						url_j_end_exec_1_tDBConnection_1, dbUser_j_end_exec_1_tDBConnection_1,
						dbPwd_j_end_exec_1_tDBConnection_1, sharedConnectionName_j_end_exec_1_tDBConnection_1);
				globalMap.put("conn_j_end_exec_1_tDBConnection_1", conn_j_end_exec_1_tDBConnection_1);
				if (null != conn_j_end_exec_1_tDBConnection_1) {

					log.debug("j_end_exec_1_tDBConnection_1 - Connection is set auto commit to 'true'.");
					conn_j_end_exec_1_tDBConnection_1.setAutoCommit(true);
				}

				globalMap.put("schema_" + "j_end_exec_1_tDBConnection_1", context.g_BDD_OMNICANAL_SCHEMA);

				/**
				 * [j_end_exec_1_tDBConnection_1 begin ] stop
				 */

				/**
				 * [j_end_exec_1_tDBConnection_1 main ] start
				 */

				currentComponent = "j_end_exec_1_tDBConnection_1";

				tos_count_j_end_exec_1_tDBConnection_1++;

				/**
				 * [j_end_exec_1_tDBConnection_1 main ] stop
				 */

				/**
				 * [j_end_exec_1_tDBConnection_1 process_data_begin ] start
				 */

				currentComponent = "j_end_exec_1_tDBConnection_1";

				/**
				 * [j_end_exec_1_tDBConnection_1 process_data_begin ] stop
				 */

				/**
				 * [j_end_exec_1_tDBConnection_1 process_data_end ] start
				 */

				currentComponent = "j_end_exec_1_tDBConnection_1";

				/**
				 * [j_end_exec_1_tDBConnection_1 process_data_end ] stop
				 */

				/**
				 * [j_end_exec_1_tDBConnection_1 end ] start
				 */

				currentComponent = "j_end_exec_1_tDBConnection_1";

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBConnection_1 - " + ("Done."));

				ok_Hash.put("j_end_exec_1_tDBConnection_1", true);
				end_Hash.put("j_end_exec_1_tDBConnection_1", System.currentTimeMillis());

				/**
				 * [j_end_exec_1_tDBConnection_1 end ] stop
				 */
			} // end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:j_end_exec_1_tDBConnection_1:OnSubjobOk", "",
						Thread.currentThread().getId() + "", "", "", "", "", "");
			}

			if (execStat) {
				runStat.updateStatOnConnection("j_end_exec_1_OnSubjobOk4", 0, "ok");
			}

			j_end_exec_1_tDBInput_1Process(globalMap);

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_end_exec_1_tDBConnection_1 finally ] start
				 */

				currentComponent = "j_end_exec_1_tDBConnection_1";

				/**
				 * [j_end_exec_1_tDBConnection_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_end_exec_1_tDBConnection_1_SUBPROCESS_STATE", 1);
	}

	public static class j_end_exec_1_row3Struct implements routines.system.IPersistableRow<j_end_exec_1_row3Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public Integer id;

		public Integer getId() {
			return this.id;
		}

		public String filename;

		public String getFilename() {
			return this.filename;
		}

		public String message;

		public String getMessage() {
			return this.message;
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

					this.filename = readString(dis);

					this.message = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

					this.filename = readString(dis);

					this.message = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

				// String

				writeString(this.filename, dos);

				// String

				writeString(this.message, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

				// String

				writeString(this.filename, dos);

				// String

				writeString(this.message, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("id=" + String.valueOf(id));
			sb.append(",filename=" + filename);
			sb.append(",message=" + message);
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (id == null) {
				sb.append("<null>");
			} else {
				sb.append(id);
			}

			sb.append("|");

			if (filename == null) {
				sb.append("<null>");
			} else {
				sb.append(filename);
			}

			sb.append("|");

			if (message == null) {
				sb.append("<null>");
			} else {
				sb.append(message);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(j_end_exec_1_row3Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void j_end_exec_1_tDBInput_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("j_end_exec_1_tDBInput_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				j_end_exec_1_row3Struct j_end_exec_1_row3 = new j_end_exec_1_row3Struct();

				/**
				 * [j_end_exec_1_tFlowToIterate_1 begin ] start
				 */

				ok_Hash.put("j_end_exec_1_tFlowToIterate_1", false);
				start_Hash.put("j_end_exec_1_tFlowToIterate_1", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tFlowToIterate_1";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "j_end_exec_1_row3");

				int tos_count_j_end_exec_1_tFlowToIterate_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tFlowToIterate_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_end_exec_1_tFlowToIterate_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_end_exec_1_tFlowToIterate_1 = new StringBuilder();
							log4jParamters_j_end_exec_1_tFlowToIterate_1.append("Parameters:");
							log4jParamters_j_end_exec_1_tFlowToIterate_1.append("DEFAULT_MAP" + " = " + "false");
							log4jParamters_j_end_exec_1_tFlowToIterate_1.append(" | ");
							log4jParamters_j_end_exec_1_tFlowToIterate_1.append("MAP" + " = " + "[{VALUE=" + ("id")
									+ ", KEY=" + ("\"currentError_id\"") + "}, {VALUE=" + ("message") + ", KEY="
									+ ("\"currentError_message\"") + "}, {VALUE=" + ("filename") + ", KEY="
									+ ("\"currentError_filename\"") + "}]");
							log4jParamters_j_end_exec_1_tFlowToIterate_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_end_exec_1_tFlowToIterate_1 - "
										+ (log4jParamters_j_end_exec_1_tFlowToIterate_1));
						}
					}
					new BytesLimit65535_j_end_exec_1_tFlowToIterate_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tFlowToIterate_1", "j_end_exec_1_tFlowToIterate_1",
							"tFlowToIterate");
					talendJobLogProcess(globalMap);
				}

				int nb_line_j_end_exec_1_tFlowToIterate_1 = 0;
				int counter_j_end_exec_1_tFlowToIterate_1 = 0;

				/**
				 * [j_end_exec_1_tFlowToIterate_1 begin ] stop
				 */

				/**
				 * [j_end_exec_1_tDBInput_1 begin ] start
				 */

				ok_Hash.put("j_end_exec_1_tDBInput_1", false);
				start_Hash.put("j_end_exec_1_tDBInput_1", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tDBInput_1";

				int tos_count_j_end_exec_1_tDBInput_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBInput_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_end_exec_1_tDBInput_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_end_exec_1_tDBInput_1 = new StringBuilder();
							log4jParamters_j_end_exec_1_tDBInput_1.append("Parameters:");
							log4jParamters_j_end_exec_1_tDBInput_1.append("USE_EXISTING_CONNECTION" + " = " + "true");
							log4jParamters_j_end_exec_1_tDBInput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_1
									.append("CONNECTION" + " = " + "j_end_exec_1_tDBConnection_1");
							log4jParamters_j_end_exec_1_tDBInput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_1.append("QUERYSTORE" + " = " + "\"\"");
							log4jParamters_j_end_exec_1_tDBInput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_1.append("QUERY" + " = "
									+ "\" SELECT  	id, 	filename, 	message FROM errors WHERE guid = '\"+ (String)globalMap.get(\"guid\") +\"' 	AND send_alert = true ORDER BY id DESC \"");
							log4jParamters_j_end_exec_1_tDBInput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_1.append("USE_CURSOR" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBInput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_1.append("TRIM_ALL_COLUMN" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBInput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_1.append("TRIM_COLUMN" + " = " + "[{TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("id") + "}, {TRIM=" + ("false") + ", SCHEMA_COLUMN="
									+ ("filename") + "}, {TRIM=" + ("false") + ", SCHEMA_COLUMN=" + ("message") + "}]");
							log4jParamters_j_end_exec_1_tDBInput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_1
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlInput");
							log4jParamters_j_end_exec_1_tDBInput_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_end_exec_1_tDBInput_1 - " + (log4jParamters_j_end_exec_1_tDBInput_1));
						}
					}
					new BytesLimit65535_j_end_exec_1_tDBInput_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tDBInput_1", "__UNIQUE_NAME__<br><b>__TABLE__</b>",
							"tPostgresqlInput");
					talendJobLogProcess(globalMap);
				}

				int nb_line_j_end_exec_1_tDBInput_1 = 0;
				java.sql.Connection conn_j_end_exec_1_tDBInput_1 = null;
				conn_j_end_exec_1_tDBInput_1 = (java.sql.Connection) globalMap.get("conn_j_end_exec_1_tDBConnection_1");

				if (conn_j_end_exec_1_tDBInput_1 != null) {
					if (conn_j_end_exec_1_tDBInput_1.getMetaData() != null) {

						log.debug("j_end_exec_1_tDBInput_1 - Uses an existing connection with username '"
								+ conn_j_end_exec_1_tDBInput_1.getMetaData().getUserName() + "'. Connection URL: "
								+ conn_j_end_exec_1_tDBInput_1.getMetaData().getURL() + ".");

					}
				}

				java.sql.Statement stmt_j_end_exec_1_tDBInput_1 = conn_j_end_exec_1_tDBInput_1.createStatement();

				String dbquery_j_end_exec_1_tDBInput_1 = "\nSELECT \n	id,\n	filename,\n	message\nFROM errors\nWHERE guid = '"
						+ (String) globalMap.get("guid") + "'\n	AND send_alert = true\nORDER BY id DESC\n";

				log.debug("j_end_exec_1_tDBInput_1 - Executing the query: '" + dbquery_j_end_exec_1_tDBInput_1 + "'.");

				globalMap.put("j_end_exec_1_tDBInput_1_QUERY", dbquery_j_end_exec_1_tDBInput_1);
				java.sql.ResultSet rs_j_end_exec_1_tDBInput_1 = null;

				try {
					rs_j_end_exec_1_tDBInput_1 = stmt_j_end_exec_1_tDBInput_1
							.executeQuery(dbquery_j_end_exec_1_tDBInput_1);
					java.sql.ResultSetMetaData rsmd_j_end_exec_1_tDBInput_1 = rs_j_end_exec_1_tDBInput_1.getMetaData();
					int colQtyInRs_j_end_exec_1_tDBInput_1 = rsmd_j_end_exec_1_tDBInput_1.getColumnCount();

					String tmpContent_j_end_exec_1_tDBInput_1 = null;

					log.debug("j_end_exec_1_tDBInput_1 - Retrieving records from the database.");

					while (rs_j_end_exec_1_tDBInput_1.next()) {
						nb_line_j_end_exec_1_tDBInput_1++;

						if (colQtyInRs_j_end_exec_1_tDBInput_1 < 1) {
							j_end_exec_1_row3.id = null;
						} else {

							j_end_exec_1_row3.id = rs_j_end_exec_1_tDBInput_1.getInt(1);
							if (rs_j_end_exec_1_tDBInput_1.wasNull()) {
								j_end_exec_1_row3.id = null;
							}
						}
						if (colQtyInRs_j_end_exec_1_tDBInput_1 < 2) {
							j_end_exec_1_row3.filename = null;
						} else {

							j_end_exec_1_row3.filename = routines.system.JDBCUtil.getString(rs_j_end_exec_1_tDBInput_1,
									2, false);
						}
						if (colQtyInRs_j_end_exec_1_tDBInput_1 < 3) {
							j_end_exec_1_row3.message = null;
						} else {

							j_end_exec_1_row3.message = routines.system.JDBCUtil.getString(rs_j_end_exec_1_tDBInput_1,
									3, false);
						}

						log.debug("j_end_exec_1_tDBInput_1 - Retrieving the record " + nb_line_j_end_exec_1_tDBInput_1
								+ ".");

						/**
						 * [j_end_exec_1_tDBInput_1 begin ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_1 main ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_1";

						tos_count_j_end_exec_1_tDBInput_1++;

						/**
						 * [j_end_exec_1_tDBInput_1 main ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_1 process_data_begin ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_1";

						/**
						 * [j_end_exec_1_tDBInput_1 process_data_begin ] stop
						 */

						/**
						 * [j_end_exec_1_tFlowToIterate_1 main ] start
						 */

						currentComponent = "j_end_exec_1_tFlowToIterate_1";

						if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

								, "j_end_exec_1_row3", "j_end_exec_1_tDBInput_1", "__UNIQUE_NAME__<br><b>__TABLE__</b>",
								"tPostgresqlInput", "j_end_exec_1_tFlowToIterate_1", "j_end_exec_1_tFlowToIterate_1",
								"tFlowToIterate"

						)) {
							talendJobLogProcess(globalMap);
						}

						if (log.isTraceEnabled()) {
							log.trace("j_end_exec_1_row3 - "
									+ (j_end_exec_1_row3 == null ? "" : j_end_exec_1_row3.toLogString()));
						}

						if (log.isTraceEnabled())
							log.trace("j_end_exec_1_tFlowToIterate_1 - " + ("Set global var, key=")
									+ ("currentError_id") + (", value=") + (j_end_exec_1_row3.id) + ("."));
						globalMap.put("currentError_id", j_end_exec_1_row3.id);
						if (log.isTraceEnabled())
							log.trace("j_end_exec_1_tFlowToIterate_1 - " + ("Set global var, key=")
									+ ("currentError_message") + (", value=") + (j_end_exec_1_row3.message) + ("."));
						globalMap.put("currentError_message", j_end_exec_1_row3.message);
						if (log.isTraceEnabled())
							log.trace("j_end_exec_1_tFlowToIterate_1 - " + ("Set global var, key=")
									+ ("currentError_filename") + (", value=") + (j_end_exec_1_row3.filename) + ("."));
						globalMap.put("currentError_filename", j_end_exec_1_row3.filename);
						nb_line_j_end_exec_1_tFlowToIterate_1++;
						counter_j_end_exec_1_tFlowToIterate_1++;
						if (log.isDebugEnabled())
							log.debug("j_end_exec_1_tFlowToIterate_1 - " + ("Current iteration is: ")
									+ (counter_j_end_exec_1_tFlowToIterate_1) + ("."));
						globalMap.put("j_end_exec_1_tFlowToIterate_1_CURRENT_ITERATION",
								counter_j_end_exec_1_tFlowToIterate_1);

						tos_count_j_end_exec_1_tFlowToIterate_1++;

						/**
						 * [j_end_exec_1_tFlowToIterate_1 main ] stop
						 */

						/**
						 * [j_end_exec_1_tFlowToIterate_1 process_data_begin ] start
						 */

						currentComponent = "j_end_exec_1_tFlowToIterate_1";

						/**
						 * [j_end_exec_1_tFlowToIterate_1 process_data_begin ] stop
						 */

						/**
						 * [j_end_exec_1_tFlowToIterate_1 process_data_end ] start
						 */

						currentComponent = "j_end_exec_1_tFlowToIterate_1";

						/**
						 * [j_end_exec_1_tFlowToIterate_1 process_data_end ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_1 process_data_end ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_1";

						/**
						 * [j_end_exec_1_tDBInput_1 process_data_end ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_1 end ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_1";

					}
				} finally {
					if (rs_j_end_exec_1_tDBInput_1 != null) {
						rs_j_end_exec_1_tDBInput_1.close();
					}
					if (stmt_j_end_exec_1_tDBInput_1 != null) {
						stmt_j_end_exec_1_tDBInput_1.close();
					}
				}
				globalMap.put("j_end_exec_1_tDBInput_1_NB_LINE", nb_line_j_end_exec_1_tDBInput_1);
				log.debug(
						"j_end_exec_1_tDBInput_1 - Retrieved records count: " + nb_line_j_end_exec_1_tDBInput_1 + " .");

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBInput_1 - " + ("Done."));

				ok_Hash.put("j_end_exec_1_tDBInput_1", true);
				end_Hash.put("j_end_exec_1_tDBInput_1", System.currentTimeMillis());

				/**
				 * [j_end_exec_1_tDBInput_1 end ] stop
				 */

				/**
				 * [j_end_exec_1_tFlowToIterate_1 end ] start
				 */

				currentComponent = "j_end_exec_1_tFlowToIterate_1";

				globalMap.put("j_end_exec_1_tFlowToIterate_1_NB_LINE", nb_line_j_end_exec_1_tFlowToIterate_1);
				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "j_end_exec_1_row3", 2,
						0, "j_end_exec_1_tDBInput_1", "__UNIQUE_NAME__<br><b>__TABLE__</b>", "tPostgresqlInput",
						"j_end_exec_1_tFlowToIterate_1", "j_end_exec_1_tFlowToIterate_1", "tFlowToIterate", "output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tFlowToIterate_1 - " + ("Done."));

				ok_Hash.put("j_end_exec_1_tFlowToIterate_1", true);
				end_Hash.put("j_end_exec_1_tFlowToIterate_1", System.currentTimeMillis());

				/**
				 * [j_end_exec_1_tFlowToIterate_1 end ] stop
				 */

			} // end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:j_end_exec_1_tDBInput_1:OnSubjobOk", "",
						Thread.currentThread().getId() + "", "", "", "", "", "");
			}

			if (execStat) {
				runStat.updateStatOnConnection("j_end_exec_1_OnSubjobOk1", 0, "ok");
			}

			j_end_exec_1_tDBInput_2Process(globalMap);

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_end_exec_1_tDBInput_1 finally ] start
				 */

				currentComponent = "j_end_exec_1_tDBInput_1";

				/**
				 * [j_end_exec_1_tDBInput_1 finally ] stop
				 */

				/**
				 * [j_end_exec_1_tFlowToIterate_1 finally ] start
				 */

				currentComponent = "j_end_exec_1_tFlowToIterate_1";

				/**
				 * [j_end_exec_1_tFlowToIterate_1 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_end_exec_1_tDBInput_1_SUBPROCESS_STATE", 1);
	}

	public static class j_end_exec_1_row4Struct implements routines.system.IPersistableRow<j_end_exec_1_row4Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public Integer id;

		public Integer getId() {
			return this.id;
		}

		public String filename;

		public String getFilename() {
			return this.filename;
		}

		public String filepath;

		public String getFilepath() {
			return this.filepath;
		}

		public String contextname;

		public String getContextname() {
			return this.contextname;
		}

		public Boolean error;

		public Boolean getError() {
			return this.error;
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

					this.filename = readString(dis);

					this.filepath = readString(dis);

					this.contextname = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.error = null;
					} else {
						this.error = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

					this.filename = readString(dis);

					this.filepath = readString(dis);

					this.contextname = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.error = null;
					} else {
						this.error = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

				// String

				writeString(this.filename, dos);

				// String

				writeString(this.filepath, dos);

				// String

				writeString(this.contextname, dos);

				// Boolean

				if (this.error == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.error);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

				// String

				writeString(this.filename, dos);

				// String

				writeString(this.filepath, dos);

				// String

				writeString(this.contextname, dos);

				// Boolean

				if (this.error == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.error);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("id=" + String.valueOf(id));
			sb.append(",filename=" + filename);
			sb.append(",filepath=" + filepath);
			sb.append(",contextname=" + contextname);
			sb.append(",error=" + String.valueOf(error));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (id == null) {
				sb.append("<null>");
			} else {
				sb.append(id);
			}

			sb.append("|");

			if (filename == null) {
				sb.append("<null>");
			} else {
				sb.append(filename);
			}

			sb.append("|");

			if (filepath == null) {
				sb.append("<null>");
			} else {
				sb.append(filepath);
			}

			sb.append("|");

			if (contextname == null) {
				sb.append("<null>");
			} else {
				sb.append(contextname);
			}

			sb.append("|");

			if (error == null) {
				sb.append("<null>");
			} else {
				sb.append(error);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(j_end_exec_1_row4Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void j_end_exec_1_tDBInput_2Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("j_end_exec_1_tDBInput_2_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				j_end_exec_1_row4Struct j_end_exec_1_row4 = new j_end_exec_1_row4Struct();

				/**
				 * [j_end_exec_1_tFlowToIterate_2 begin ] start
				 */

				int NB_ITERATE_j_end_exec_1_tJava_1 = 0; // for statistics

				ok_Hash.put("j_end_exec_1_tFlowToIterate_2", false);
				start_Hash.put("j_end_exec_1_tFlowToIterate_2", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tFlowToIterate_2";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "j_end_exec_1_row4");

				int tos_count_j_end_exec_1_tFlowToIterate_2 = 0;

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tFlowToIterate_2 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_end_exec_1_tFlowToIterate_2 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_end_exec_1_tFlowToIterate_2 = new StringBuilder();
							log4jParamters_j_end_exec_1_tFlowToIterate_2.append("Parameters:");
							log4jParamters_j_end_exec_1_tFlowToIterate_2.append("DEFAULT_MAP" + " = " + "false");
							log4jParamters_j_end_exec_1_tFlowToIterate_2.append(" | ");
							log4jParamters_j_end_exec_1_tFlowToIterate_2.append("MAP" + " = " + "[{VALUE=" + ("id")
									+ ", KEY=" + ("\"currentFile_id\"") + "}, {VALUE=" + ("filename") + ", KEY="
									+ ("\"currentFile_filename\"") + "}, {VALUE=" + ("filepath") + ", KEY="
									+ ("\"currentFile_filepath\"") + "}, {VALUE=" + ("contextname") + ", KEY="
									+ ("\"currentFile_contextname\"") + "}, {VALUE=" + ("error") + ", KEY="
									+ ("\"currentFile_error\"") + "}]");
							log4jParamters_j_end_exec_1_tFlowToIterate_2.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_end_exec_1_tFlowToIterate_2 - "
										+ (log4jParamters_j_end_exec_1_tFlowToIterate_2));
						}
					}
					new BytesLimit65535_j_end_exec_1_tFlowToIterate_2().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tFlowToIterate_2", "j_end_exec_1_tFlowToIterate_2",
							"tFlowToIterate");
					talendJobLogProcess(globalMap);
				}

				int nb_line_j_end_exec_1_tFlowToIterate_2 = 0;
				int counter_j_end_exec_1_tFlowToIterate_2 = 0;

				/**
				 * [j_end_exec_1_tFlowToIterate_2 begin ] stop
				 */

				/**
				 * [j_end_exec_1_tDBInput_2 begin ] start
				 */

				ok_Hash.put("j_end_exec_1_tDBInput_2", false);
				start_Hash.put("j_end_exec_1_tDBInput_2", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tDBInput_2";

				int tos_count_j_end_exec_1_tDBInput_2 = 0;

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBInput_2 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_end_exec_1_tDBInput_2 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_end_exec_1_tDBInput_2 = new StringBuilder();
							log4jParamters_j_end_exec_1_tDBInput_2.append("Parameters:");
							log4jParamters_j_end_exec_1_tDBInput_2.append("USE_EXISTING_CONNECTION" + " = " + "true");
							log4jParamters_j_end_exec_1_tDBInput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_2
									.append("CONNECTION" + " = " + "j_end_exec_1_tDBConnection_1");
							log4jParamters_j_end_exec_1_tDBInput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_2.append("QUERYSTORE" + " = " + "\"\"");
							log4jParamters_j_end_exec_1_tDBInput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_2.append("QUERY" + " = "
									+ "\" SELECT  	id, 	filename, 	filepath, 	contextname, 	error FROM files WHERE guid = '\"+ (String)globalMap.get(\"guid\") +\"' ORDER BY id DESC \"");
							log4jParamters_j_end_exec_1_tDBInput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_2.append("USE_CURSOR" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBInput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_2.append("TRIM_ALL_COLUMN" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBInput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_2.append("TRIM_COLUMN" + " = " + "[{TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("id") + "}, {TRIM=" + ("false") + ", SCHEMA_COLUMN="
									+ ("filename") + "}, {TRIM=" + ("false") + ", SCHEMA_COLUMN=" + ("filepath")
									+ "}, {TRIM=" + ("false") + ", SCHEMA_COLUMN=" + ("contextname") + "}, {TRIM="
									+ ("false") + ", SCHEMA_COLUMN=" + ("error") + "}]");
							log4jParamters_j_end_exec_1_tDBInput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_2
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlInput");
							log4jParamters_j_end_exec_1_tDBInput_2.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_end_exec_1_tDBInput_2 - " + (log4jParamters_j_end_exec_1_tDBInput_2));
						}
					}
					new BytesLimit65535_j_end_exec_1_tDBInput_2().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tDBInput_2", "__UNIQUE_NAME__<br><b>__TABLE__</b>",
							"tPostgresqlInput");
					talendJobLogProcess(globalMap);
				}

				int nb_line_j_end_exec_1_tDBInput_2 = 0;
				java.sql.Connection conn_j_end_exec_1_tDBInput_2 = null;
				conn_j_end_exec_1_tDBInput_2 = (java.sql.Connection) globalMap.get("conn_j_end_exec_1_tDBConnection_1");

				if (conn_j_end_exec_1_tDBInput_2 != null) {
					if (conn_j_end_exec_1_tDBInput_2.getMetaData() != null) {

						log.debug("j_end_exec_1_tDBInput_2 - Uses an existing connection with username '"
								+ conn_j_end_exec_1_tDBInput_2.getMetaData().getUserName() + "'. Connection URL: "
								+ conn_j_end_exec_1_tDBInput_2.getMetaData().getURL() + ".");

					}
				}

				java.sql.Statement stmt_j_end_exec_1_tDBInput_2 = conn_j_end_exec_1_tDBInput_2.createStatement();

				String dbquery_j_end_exec_1_tDBInput_2 = "\nSELECT \n	id,\n	filename,\n	filepath,\n	contextname,\n	error\nFROM files\nWHERE guid = '"
						+ (String) globalMap.get("guid") + "'\nORDER BY id DESC\n";

				log.debug("j_end_exec_1_tDBInput_2 - Executing the query: '" + dbquery_j_end_exec_1_tDBInput_2 + "'.");

				globalMap.put("j_end_exec_1_tDBInput_2_QUERY", dbquery_j_end_exec_1_tDBInput_2);
				java.sql.ResultSet rs_j_end_exec_1_tDBInput_2 = null;

				try {
					rs_j_end_exec_1_tDBInput_2 = stmt_j_end_exec_1_tDBInput_2
							.executeQuery(dbquery_j_end_exec_1_tDBInput_2);
					java.sql.ResultSetMetaData rsmd_j_end_exec_1_tDBInput_2 = rs_j_end_exec_1_tDBInput_2.getMetaData();
					int colQtyInRs_j_end_exec_1_tDBInput_2 = rsmd_j_end_exec_1_tDBInput_2.getColumnCount();

					String tmpContent_j_end_exec_1_tDBInput_2 = null;

					log.debug("j_end_exec_1_tDBInput_2 - Retrieving records from the database.");

					while (rs_j_end_exec_1_tDBInput_2.next()) {
						nb_line_j_end_exec_1_tDBInput_2++;

						if (colQtyInRs_j_end_exec_1_tDBInput_2 < 1) {
							j_end_exec_1_row4.id = null;
						} else {

							j_end_exec_1_row4.id = rs_j_end_exec_1_tDBInput_2.getInt(1);
							if (rs_j_end_exec_1_tDBInput_2.wasNull()) {
								j_end_exec_1_row4.id = null;
							}
						}
						if (colQtyInRs_j_end_exec_1_tDBInput_2 < 2) {
							j_end_exec_1_row4.filename = null;
						} else {

							j_end_exec_1_row4.filename = routines.system.JDBCUtil.getString(rs_j_end_exec_1_tDBInput_2,
									2, false);
						}
						if (colQtyInRs_j_end_exec_1_tDBInput_2 < 3) {
							j_end_exec_1_row4.filepath = null;
						} else {

							j_end_exec_1_row4.filepath = routines.system.JDBCUtil.getString(rs_j_end_exec_1_tDBInput_2,
									3, false);
						}
						if (colQtyInRs_j_end_exec_1_tDBInput_2 < 4) {
							j_end_exec_1_row4.contextname = null;
						} else {

							j_end_exec_1_row4.contextname = routines.system.JDBCUtil
									.getString(rs_j_end_exec_1_tDBInput_2, 4, false);
						}
						if (colQtyInRs_j_end_exec_1_tDBInput_2 < 5) {
							j_end_exec_1_row4.error = null;
						} else {

							j_end_exec_1_row4.error = rs_j_end_exec_1_tDBInput_2.getBoolean(5);
							if (rs_j_end_exec_1_tDBInput_2.wasNull()) {
								j_end_exec_1_row4.error = null;
							}
						}

						log.debug("j_end_exec_1_tDBInput_2 - Retrieving the record " + nb_line_j_end_exec_1_tDBInput_2
								+ ".");

						/**
						 * [j_end_exec_1_tDBInput_2 begin ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_2 main ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_2";

						tos_count_j_end_exec_1_tDBInput_2++;

						/**
						 * [j_end_exec_1_tDBInput_2 main ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_2 process_data_begin ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_2";

						/**
						 * [j_end_exec_1_tDBInput_2 process_data_begin ] stop
						 */

						/**
						 * [j_end_exec_1_tFlowToIterate_2 main ] start
						 */

						currentComponent = "j_end_exec_1_tFlowToIterate_2";

						if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

								, "j_end_exec_1_row4", "j_end_exec_1_tDBInput_2", "__UNIQUE_NAME__<br><b>__TABLE__</b>",
								"tPostgresqlInput", "j_end_exec_1_tFlowToIterate_2", "j_end_exec_1_tFlowToIterate_2",
								"tFlowToIterate"

						)) {
							talendJobLogProcess(globalMap);
						}

						if (log.isTraceEnabled()) {
							log.trace("j_end_exec_1_row4 - "
									+ (j_end_exec_1_row4 == null ? "" : j_end_exec_1_row4.toLogString()));
						}

						if (log.isTraceEnabled())
							log.trace("j_end_exec_1_tFlowToIterate_2 - " + ("Set global var, key=") + ("currentFile_id")
									+ (", value=") + (j_end_exec_1_row4.id) + ("."));
						globalMap.put("currentFile_id", j_end_exec_1_row4.id);
						if (log.isTraceEnabled())
							log.trace("j_end_exec_1_tFlowToIterate_2 - " + ("Set global var, key=")
									+ ("currentFile_filename") + (", value=") + (j_end_exec_1_row4.filename) + ("."));
						globalMap.put("currentFile_filename", j_end_exec_1_row4.filename);
						if (log.isTraceEnabled())
							log.trace("j_end_exec_1_tFlowToIterate_2 - " + ("Set global var, key=")
									+ ("currentFile_filepath") + (", value=") + (j_end_exec_1_row4.filepath) + ("."));
						globalMap.put("currentFile_filepath", j_end_exec_1_row4.filepath);
						if (log.isTraceEnabled())
							log.trace("j_end_exec_1_tFlowToIterate_2 - " + ("Set global var, key=")
									+ ("currentFile_contextname") + (", value=") + (j_end_exec_1_row4.contextname)
									+ ("."));
						globalMap.put("currentFile_contextname", j_end_exec_1_row4.contextname);
						if (log.isTraceEnabled())
							log.trace("j_end_exec_1_tFlowToIterate_2 - " + ("Set global var, key=")
									+ ("currentFile_error") + (", value=") + (j_end_exec_1_row4.error) + ("."));
						globalMap.put("currentFile_error", j_end_exec_1_row4.error);
						nb_line_j_end_exec_1_tFlowToIterate_2++;
						counter_j_end_exec_1_tFlowToIterate_2++;
						if (log.isDebugEnabled())
							log.debug("j_end_exec_1_tFlowToIterate_2 - " + ("Current iteration is: ")
									+ (counter_j_end_exec_1_tFlowToIterate_2) + ("."));
						globalMap.put("j_end_exec_1_tFlowToIterate_2_CURRENT_ITERATION",
								counter_j_end_exec_1_tFlowToIterate_2);

						tos_count_j_end_exec_1_tFlowToIterate_2++;

						/**
						 * [j_end_exec_1_tFlowToIterate_2 main ] stop
						 */

						/**
						 * [j_end_exec_1_tFlowToIterate_2 process_data_begin ] start
						 */

						currentComponent = "j_end_exec_1_tFlowToIterate_2";

						/**
						 * [j_end_exec_1_tFlowToIterate_2 process_data_begin ] stop
						 */
						NB_ITERATE_j_end_exec_1_tJava_1++;

						if (execStat) {
							runStat.updateStatOnConnection("j_end_exec_1_OnComponentOk2", 3, 0);
						}

						if (execStat) {
							runStat.updateStatOnConnection("j_end_exec_1_If2", 3, 0);
						}

						if (execStat) {
							runStat.updateStatOnConnection("j_end_exec_1_iterate2", 1,
									"exec" + NB_ITERATE_j_end_exec_1_tJava_1);
							// Thread.sleep(1000);
						}

						/**
						 * [j_end_exec_1_tJava_1 begin ] start
						 */

						ok_Hash.put("j_end_exec_1_tJava_1", false);
						start_Hash.put("j_end_exec_1_tJava_1", System.currentTimeMillis());

						currentComponent = "j_end_exec_1_tJava_1";

						int tos_count_j_end_exec_1_tJava_1 = 0;

						if (enableLogStash) {
							talendJobLog.addCM("j_end_exec_1_tJava_1", "__UNIQUE_NAME__<br><b>Set FTP variables</b>",
									"tJava");
							talendJobLogProcess(globalMap);
						}

// ONESTOCK
						globalMap.put("tFTP_Host", context.g_FTP_ONESTOCK_HOST);
						globalMap.put("tFTP_User", context.g_FTP_ONESTOCK_USER);
						globalMap.put("tFTP_Port", context.g_FTP_ONESTOCK_PORT);
						globalMap.put("tFTP_Pwd", context.g_FTP_ONESTOCK_PWD);
						globalMap.put("tFTP_RemotePath", context.g_FTP_ONESTOCK_PATH);

// MAGENTO
						/*
						 * if("MAGENTO".equals((String)glogalMap.get("currentFile_contextname"))){
						 * globalMap.put("tFTP_Host", context.g_FTP_MAGENTO_HOST);
						 * globalMap.put("tFTP_User", context.g_FTP_MAGENTO_USER);
						 * globalMap.put("tFTP_Port", context.g_FTP_MAGENTO_PORT);
						 * globalMap.put("tFTP_Pwd", context.g_FTP_MAGENTO_PWD);
						 * globalMap.put("tFTP_RemotePath", context.g_FTP_MAGENTO_PATH);
						 * 
						 * // GCE }else
						 * if("GCE".equals((String)glogalMap.get("currentFile_contextname"))){
						 * globalMap.put("tFTP_Host", context.g_FTP_GCE_HOST);
						 * globalMap.put("tFTP_User", context.g_FTP_GCE_USER);
						 * globalMap.put("tFTP_Port", context.g_FTP_GCE_PORT); globalMap.put("tFTP_Pwd",
						 * context.g_FTP_GCE_PWD); globalMap.put("tFTP_RemotePath",
						 * context.g_FTP_GCE_PATH); }
						 */

						/**
						 * [j_end_exec_1_tJava_1 begin ] stop
						 */

						/**
						 * [j_end_exec_1_tJava_1 main ] start
						 */

						currentComponent = "j_end_exec_1_tJava_1";

						tos_count_j_end_exec_1_tJava_1++;

						/**
						 * [j_end_exec_1_tJava_1 main ] stop
						 */

						/**
						 * [j_end_exec_1_tJava_1 process_data_begin ] start
						 */

						currentComponent = "j_end_exec_1_tJava_1";

						/**
						 * [j_end_exec_1_tJava_1 process_data_begin ] stop
						 */

						/**
						 * [j_end_exec_1_tJava_1 process_data_end ] start
						 */

						currentComponent = "j_end_exec_1_tJava_1";

						/**
						 * [j_end_exec_1_tJava_1 process_data_end ] stop
						 */

						/**
						 * [j_end_exec_1_tJava_1 end ] start
						 */

						currentComponent = "j_end_exec_1_tJava_1";

						ok_Hash.put("j_end_exec_1_tJava_1", true);
						end_Hash.put("j_end_exec_1_tJava_1", System.currentTimeMillis());

						if (execStat) {
							runStat.updateStatOnConnection("j_end_exec_1_OnComponentOk2", 0, "ok");
						}
						j_end_exec_1_tFileExist_1Process(globalMap);

						/**
						 * [j_end_exec_1_tJava_1 end ] stop
						 */
						if (execStat) {
							runStat.updateStatOnConnection("j_end_exec_1_iterate2", 2,
									"exec" + NB_ITERATE_j_end_exec_1_tJava_1);
						}

						/**
						 * [j_end_exec_1_tFlowToIterate_2 process_data_end ] start
						 */

						currentComponent = "j_end_exec_1_tFlowToIterate_2";

						/**
						 * [j_end_exec_1_tFlowToIterate_2 process_data_end ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_2 process_data_end ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_2";

						/**
						 * [j_end_exec_1_tDBInput_2 process_data_end ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_2 end ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_2";

					}
				} finally {
					if (rs_j_end_exec_1_tDBInput_2 != null) {
						rs_j_end_exec_1_tDBInput_2.close();
					}
					if (stmt_j_end_exec_1_tDBInput_2 != null) {
						stmt_j_end_exec_1_tDBInput_2.close();
					}
				}
				globalMap.put("j_end_exec_1_tDBInput_2_NB_LINE", nb_line_j_end_exec_1_tDBInput_2);
				log.debug(
						"j_end_exec_1_tDBInput_2 - Retrieved records count: " + nb_line_j_end_exec_1_tDBInput_2 + " .");

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBInput_2 - " + ("Done."));

				ok_Hash.put("j_end_exec_1_tDBInput_2", true);
				end_Hash.put("j_end_exec_1_tDBInput_2", System.currentTimeMillis());

				/**
				 * [j_end_exec_1_tDBInput_2 end ] stop
				 */

				/**
				 * [j_end_exec_1_tFlowToIterate_2 end ] start
				 */

				currentComponent = "j_end_exec_1_tFlowToIterate_2";

				globalMap.put("j_end_exec_1_tFlowToIterate_2_NB_LINE", nb_line_j_end_exec_1_tFlowToIterate_2);
				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "j_end_exec_1_row4", 2,
						0, "j_end_exec_1_tDBInput_2", "__UNIQUE_NAME__<br><b>__TABLE__</b>", "tPostgresqlInput",
						"j_end_exec_1_tFlowToIterate_2", "j_end_exec_1_tFlowToIterate_2", "tFlowToIterate", "output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tFlowToIterate_2 - " + ("Done."));

				ok_Hash.put("j_end_exec_1_tFlowToIterate_2", true);
				end_Hash.put("j_end_exec_1_tFlowToIterate_2", System.currentTimeMillis());

				/**
				 * [j_end_exec_1_tFlowToIterate_2 end ] stop
				 */

			} // end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:j_end_exec_1_tDBInput_2:OnSubjobOk", "",
						Thread.currentThread().getId() + "", "", "", "", "", "");
			}

			if (execStat) {
				runStat.updateStatOnConnection("j_end_exec_1_OnSubjobOk5", 0, "ok");
			}

			j_end_exec_1_tDBInput_3Process(globalMap);

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_end_exec_1_tDBInput_2 finally ] start
				 */

				currentComponent = "j_end_exec_1_tDBInput_2";

				/**
				 * [j_end_exec_1_tDBInput_2 finally ] stop
				 */

				/**
				 * [j_end_exec_1_tFlowToIterate_2 finally ] start
				 */

				currentComponent = "j_end_exec_1_tFlowToIterate_2";

				/**
				 * [j_end_exec_1_tFlowToIterate_2 finally ] stop
				 */

				/**
				 * [j_end_exec_1_tJava_1 finally ] start
				 */

				currentComponent = "j_end_exec_1_tJava_1";

				/**
				 * [j_end_exec_1_tJava_1 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_end_exec_1_tDBInput_2_SUBPROCESS_STATE", 1);
	}

	public void j_end_exec_1_tFileExist_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("j_end_exec_1_tFileExist_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [j_end_exec_1_tFileExist_1 begin ] start
				 */

				ok_Hash.put("j_end_exec_1_tFileExist_1", false);
				start_Hash.put("j_end_exec_1_tFileExist_1", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tFileExist_1";

				int tos_count_j_end_exec_1_tFileExist_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tFileExist_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_end_exec_1_tFileExist_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_end_exec_1_tFileExist_1 = new StringBuilder();
							log4jParamters_j_end_exec_1_tFileExist_1.append("Parameters:");
							log4jParamters_j_end_exec_1_tFileExist_1.append("FILE_NAME" + " = "
									+ "context.g_VAR_TALEND_WORKSPACE + jobName +\"/\" + (String)globalMap.get(\"currentFile_filename\")");
							log4jParamters_j_end_exec_1_tFileExist_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_end_exec_1_tFileExist_1 - " + (log4jParamters_j_end_exec_1_tFileExist_1));
						}
					}
					new BytesLimit65535_j_end_exec_1_tFileExist_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tFileExist_1", "j_end_exec_1_tFileExist_1", "tFileExist");
					talendJobLogProcess(globalMap);
				}

				/**
				 * [j_end_exec_1_tFileExist_1 begin ] stop
				 */

				/**
				 * [j_end_exec_1_tFileExist_1 main ] start
				 */

				currentComponent = "j_end_exec_1_tFileExist_1";

				final StringBuffer log4jSb_j_end_exec_1_tFileExist_1 = new StringBuffer();

				java.io.File file_j_end_exec_1_tFileExist_1 = new java.io.File(context.g_VAR_TALEND_WORKSPACE + jobName
						+ "/" + (String) globalMap.get("currentFile_filename"));
				if (!file_j_end_exec_1_tFileExist_1.exists()) {
					globalMap.put("j_end_exec_1_tFileExist_1_EXISTS", false);
					log.info("j_end_exec_1_tFileExist_1 - Directory or file : "
							+ file_j_end_exec_1_tFileExist_1.getAbsolutePath() + " doesn't exist.");
				} else {
					globalMap.put("j_end_exec_1_tFileExist_1_EXISTS", true);
					log.info("j_end_exec_1_tFileExist_1 - Directory or file : "
							+ file_j_end_exec_1_tFileExist_1.getAbsolutePath() + " exists.");
				}

				globalMap.put("j_end_exec_1_tFileExist_1_FILENAME", context.g_VAR_TALEND_WORKSPACE + jobName + "/"
						+ (String) globalMap.get("currentFile_filename"));

				tos_count_j_end_exec_1_tFileExist_1++;

				/**
				 * [j_end_exec_1_tFileExist_1 main ] stop
				 */

				/**
				 * [j_end_exec_1_tFileExist_1 process_data_begin ] start
				 */

				currentComponent = "j_end_exec_1_tFileExist_1";

				/**
				 * [j_end_exec_1_tFileExist_1 process_data_begin ] stop
				 */

				/**
				 * [j_end_exec_1_tFileExist_1 process_data_end ] start
				 */

				currentComponent = "j_end_exec_1_tFileExist_1";

				/**
				 * [j_end_exec_1_tFileExist_1 process_data_end ] stop
				 */

				/**
				 * [j_end_exec_1_tFileExist_1 end ] start
				 */

				currentComponent = "j_end_exec_1_tFileExist_1";

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tFileExist_1 - " + ("Done."));

				ok_Hash.put("j_end_exec_1_tFileExist_1", true);
				end_Hash.put("j_end_exec_1_tFileExist_1", System.currentTimeMillis());

				if ((Boolean) globalMap.get("j_end_exec_1_tFileExist_1_EXISTS")) {

					if (execStat) {
						runStat.updateStatOnConnection("j_end_exec_1_If2", 0, "true");
					}
					j_end_exec_1_tFTPPut_1Process(globalMap);
				}

				else {
					if (execStat) {
						runStat.updateStatOnConnection("j_end_exec_1_If2", 0, "false");
					}
				}

				/**
				 * [j_end_exec_1_tFileExist_1 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_end_exec_1_tFileExist_1 finally ] start
				 */

				currentComponent = "j_end_exec_1_tFileExist_1";

				/**
				 * [j_end_exec_1_tFileExist_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_end_exec_1_tFileExist_1_SUBPROCESS_STATE", 1);
	}

	public void j_end_exec_1_tFTPPut_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("j_end_exec_1_tFTPPut_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [j_end_exec_1_tFTPPut_1 begin ] start
				 */

				ok_Hash.put("j_end_exec_1_tFTPPut_1", false);
				start_Hash.put("j_end_exec_1_tFTPPut_1", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tFTPPut_1";

				int tos_count_j_end_exec_1_tFTPPut_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tFTPPut_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_end_exec_1_tFTPPut_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_end_exec_1_tFTPPut_1 = new StringBuilder();
							log4jParamters_j_end_exec_1_tFTPPut_1.append("Parameters:");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("USE_EXISTING_CONNECTION" + " = " + "false");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1
									.append("HOST" + " = " + "(String)globalMap.get(\"tFTP_Host\")");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1
									.append("PORT" + " = " + "(Integer)globalMap.get(\"tFTP_Port\")");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1
									.append("USERNAME" + " = " + "(String)globalMap.get(\"tFTP_User\")");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("PASSWORD" + " = "
									+ String.valueOf(routines.system.PasswordEncryptUtil
											.encryptPassword((String) globalMap.get("tFTP_Pwd"))).substring(0, 4)
									+ "...");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("SFTP" + " = " + "false");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("FTPS" + " = " + "false");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1
									.append("LOCALDIR" + " = " + "context.g_VAR_TALEND_WORKSPACE + jobName +\"/\"");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("REMOTEDIR" + " = "
									+ "(String)globalMap.get(\"currentFile_filepath\") + ((Boolean)globalMap.get(\"currentFile_error\") ? \"Error/\" : \"Archived/\" )");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("CREATE_DIR_IF_NOT_EXIST" + " = " + "true");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("MODE" + " = " + "ascii");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("OVERWRITE" + " = " + "never");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("APPEND" + " = " + "false");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("PERL5_REGEX" + " = " + "false");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("FILES" + " = " + "[{FILEMASK="
									+ ("(String)globalMap.get(\"currentFile_filename\")") + ", NEWNAME="
									+ ("(String)globalMap.get(\"currentFile_filename\")") + "}]");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("CONNECT_MODE" + " = " + "PASSIVE");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("ENCODING" + " = " + "\"ISO-8859-15\"");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("DIE_ON_ERROR" + " = " + "true");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("USE_PROXY" + " = " + "false");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("IGNORE_FAILURE_AT_QUIT" + " = " + "false");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("CONNECTION_TIMEOUT" + " = " + "0");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							log4jParamters_j_end_exec_1_tFTPPut_1.append("USE_STRICT_REPLY_PARSING" + " = " + "true");
							log4jParamters_j_end_exec_1_tFTPPut_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_end_exec_1_tFTPPut_1 - " + (log4jParamters_j_end_exec_1_tFTPPut_1));
						}
					}
					new BytesLimit65535_j_end_exec_1_tFTPPut_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tFTPPut_1", "j_end_exec_1_tFTPPut_1", "tFTPPut");
					talendJobLogProcess(globalMap);
				}

				int connectionTimeout_j_end_exec_1_tFTPPut_1 = Integer.valueOf(0);
				int nb_file_j_end_exec_1_tFTPPut_1 = 0;
				// *** ftp *** //
				org.apache.commons.net.ftp.FTPClient ftp_j_end_exec_1_tFTPPut_1 = null;

				try {
					if (("true").equals(System.getProperty("http.proxySet"))) {

//check if the host is in the excludes for proxy
						boolean isHostIgnored_j_end_exec_1_tFTPPut_1 = false;
						String nonProxyHostsString_j_end_exec_1_tFTPPut_1 = System.getProperty("http.nonProxyHosts");
						String[] nonProxyHosts_j_end_exec_1_tFTPPut_1 = (nonProxyHostsString_j_end_exec_1_tFTPPut_1 == null)
								? new String[0]
								: nonProxyHostsString_j_end_exec_1_tFTPPut_1.split("\\|");
						for (String nonProxyHost : nonProxyHosts_j_end_exec_1_tFTPPut_1) {
							if (((String) globalMap.get("tFTP_Host")).matches(nonProxyHost.trim())) {
								isHostIgnored_j_end_exec_1_tFTPPut_1 = true;
								break;
							}
						}
						if (!isHostIgnored_j_end_exec_1_tFTPPut_1) {
							String httpProxyHost = System.getProperty("http.proxyHost");
							int httpProxyPort = Integer.getInteger("http.proxyPort");
							String httpProxyUser = System.getProperty("http.proxyUser");
							String httpProxyPass = System.getProperty("http.proxyPassword");
							ftp_j_end_exec_1_tFTPPut_1 = new org.apache.commons.net.ftp.FTPHTTPClient(httpProxyHost,
									httpProxyPort, httpProxyUser, httpProxyPass);
						} else {
							ftp_j_end_exec_1_tFTPPut_1 = new org.apache.commons.net.ftp.FTPClient();
						}
					} else if ("local".equals(System.getProperty("http.proxySet"))) {
						String uriString = (String) globalMap.get("tFTP_Host") + ":" + 990;
						java.net.Proxy proxyToUse = org.talend.proxy.TalendProxySelector.getInstance()
								.getProxyForUriString(uriString);

						if (!proxyToUse.equals(java.net.Proxy.NO_PROXY)) {
							java.net.InetSocketAddress proxyAddress = (java.net.InetSocketAddress) proxyToUse.address();

							String httpProxyHost = proxyAddress.getAddress().getHostAddress();
							int httpProxyPort = proxyAddress.getPort();
							String httpProxyUser = "";
							String httpProxyPass = ""; // leave it empty if proxy creds weren't specified

							org.talend.proxy.ProxyCreds proxyCreds = org.talend.proxy.TalendProxyAuthenticator
									.getInstance().getCredsForProxyURI(httpProxyHost + ":" + httpProxyPort);
							if (proxyCreds != null) {
								httpProxyUser = proxyCreds.getUser();
								httpProxyPass = proxyCreds.getPass();
							}

							ftp_j_end_exec_1_tFTPPut_1 = new org.apache.commons.net.ftp.FTPHTTPClient(httpProxyHost,
									httpProxyPort, httpProxyUser, httpProxyPass);

						} else { // no http proxy for ftp host defined
							ftp_j_end_exec_1_tFTPPut_1 = new org.apache.commons.net.ftp.FTPClient();
						}
					} else {
						ftp_j_end_exec_1_tFTPPut_1 = new org.apache.commons.net.ftp.FTPClient();
					}

					ftp_j_end_exec_1_tFTPPut_1.setControlEncoding("ISO-8859-15");

					log.info("j_end_exec_1_tFTPPut_1 - Attempt to connect to '" + (String) globalMap.get("tFTP_Host")
							+ "' with username '" + (String) globalMap.get("tFTP_User") + "'.");

					if (connectionTimeout_j_end_exec_1_tFTPPut_1 > 0) {
						ftp_j_end_exec_1_tFTPPut_1.setDefaultTimeout(connectionTimeout_j_end_exec_1_tFTPPut_1);
					}

					ftp_j_end_exec_1_tFTPPut_1.setStrictReplyParsing(true);
					ftp_j_end_exec_1_tFTPPut_1.connect((String) globalMap.get("tFTP_Host"),
							(Integer) globalMap.get("tFTP_Port"));
					log.info("j_end_exec_1_tFTPPut_1 - Connect to '" + (String) globalMap.get("tFTP_Host")
							+ "' has succeeded.");

					final String decryptedPassword_j_end_exec_1_tFTPPut_1 = (String) globalMap.get("tFTP_Pwd");

					boolean isLoginSuccessful_j_end_exec_1_tFTPPut_1 = ftp_j_end_exec_1_tFTPPut_1
							.login((String) globalMap.get("tFTP_User"), decryptedPassword_j_end_exec_1_tFTPPut_1);

					if (!isLoginSuccessful_j_end_exec_1_tFTPPut_1) {
						throw new RuntimeException("Login failed");
					}

					ftp_j_end_exec_1_tFTPPut_1.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);
				} catch (Exception e) {
					log.error("j_end_exec_1_tFTPPut_1 - Can't create connection: " + e.getMessage());
					throw e;
				}

				ftp_j_end_exec_1_tFTPPut_1.enterLocalPassiveMode();
				log.debug("j_end_exec_1_tFTPPut_1 - Using the passive mode.");
				// msg_j_end_exec_1_tFTPPut_1 likes a String[] to save the message from
				// transfer.
				java.util.List<String> msg_j_end_exec_1_tFTPPut_1 = new java.util.ArrayList<>();

				ftp_j_end_exec_1_tFTPPut_1.setFileType(org.apache.commons.net.ftp.FTP.ASCII_FILE_TYPE);

				String rootDir_j_end_exec_1_tFTPPut_1 = ftp_j_end_exec_1_tFTPPut_1.printWorkingDirectory();
				String remotedir_j_end_exec_1_tFTPPut_1 = ((String) globalMap.get("currentFile_filepath")
						+ ((Boolean) globalMap.get("currentFile_error") ? "Error/" : "Archived/")).replaceAll("\\\\",
								"/");
				boolean cwdSuccess_j_end_exec_1_tFTPPut_1 = ftp_j_end_exec_1_tFTPPut_1
						.changeWorkingDirectory(remotedir_j_end_exec_1_tFTPPut_1);
				if (!cwdSuccess_j_end_exec_1_tFTPPut_1) {
					String[] dirsTree_j_end_exec_1_tFTPPut_1 = remotedir_j_end_exec_1_tFTPPut_1.split("/");
					for (String dir : dirsTree_j_end_exec_1_tFTPPut_1) {
						ftp_j_end_exec_1_tFTPPut_1.makeDirectory(dir);
						ftp_j_end_exec_1_tFTPPut_1.changeWorkingDirectory(dir);
					}
					ftp_j_end_exec_1_tFTPPut_1.changeWorkingDirectory(rootDir_j_end_exec_1_tFTPPut_1);
					cwdSuccess_j_end_exec_1_tFTPPut_1 = ftp_j_end_exec_1_tFTPPut_1
							.changeWorkingDirectory(remotedir_j_end_exec_1_tFTPPut_1);
				}

				if (!cwdSuccess_j_end_exec_1_tFTPPut_1) {
					throw new RuntimeException(
							"Failed to change remote directory. " + ftp_j_end_exec_1_tFTPPut_1.getReplyString());
				}

				java.util.List<java.util.Map<String, String>> listj_end_exec_1_tFTPPut_1 = new java.util.ArrayList<java.util.Map<String, String>>();

				java.util.Map<String, String> mapj_end_exec_1_tFTPPut_10 = new java.util.HashMap<String, String>();
				mapj_end_exec_1_tFTPPut_10.put((String) globalMap.get("currentFile_filename"),
						(String) globalMap.get("currentFile_filename"));
				listj_end_exec_1_tFTPPut_1.add(mapj_end_exec_1_tFTPPut_10);
				String localdirj_end_exec_1_tFTPPut_1 = context.g_VAR_TALEND_WORKSPACE + jobName + "/";
				log.info("j_end_exec_1_tFTPPut_1 - Uploading files to the server.");
				for (java.util.Map<String, String> mapj_end_exec_1_tFTPPut_1 : listj_end_exec_1_tFTPPut_1) {

					/**
					 * [j_end_exec_1_tFTPPut_1 begin ] stop
					 */

					/**
					 * [j_end_exec_1_tFTPPut_1 main ] start
					 */

					currentComponent = "j_end_exec_1_tFTPPut_1";

					try {
						String currentStatus_j_end_exec_1_tFTPPut_1 = "No file transfered.";
						globalMap.put("j_end_exec_1_tFTPPut_1_CURRENT_STATUS", currentStatus_j_end_exec_1_tFTPPut_1);
						java.util.Set<String> keySetj_end_exec_1_tFTPPut_1 = mapj_end_exec_1_tFTPPut_1.keySet();

						for (String keyj_end_exec_1_tFTPPut_1 : keySetj_end_exec_1_tFTPPut_1) {
							if (keyj_end_exec_1_tFTPPut_1 == null || "".equals(keyj_end_exec_1_tFTPPut_1)) {
								log.error("j_end_exec_1_tFTPPut_1 - file name invalid!");
								System.err.println("file name invalid!");
								continue;
							}
							String tempdirj_end_exec_1_tFTPPut_1 = localdirj_end_exec_1_tFTPPut_1;
							String filemaskj_end_exec_1_tFTPPut_1 = keyj_end_exec_1_tFTPPut_1;
							String dirj_end_exec_1_tFTPPut_1 = null;
							String maskj_end_exec_1_tFTPPut_1 = filemaskj_end_exec_1_tFTPPut_1.replaceAll("\\\\", "/");
							int ij_end_exec_1_tFTPPut_1 = maskj_end_exec_1_tFTPPut_1.lastIndexOf('/');
							if (ij_end_exec_1_tFTPPut_1 != -1) {
								dirj_end_exec_1_tFTPPut_1 = maskj_end_exec_1_tFTPPut_1.substring(0,
										ij_end_exec_1_tFTPPut_1);
								maskj_end_exec_1_tFTPPut_1 = maskj_end_exec_1_tFTPPut_1
										.substring(ij_end_exec_1_tFTPPut_1 + 1);
							}
							if (dirj_end_exec_1_tFTPPut_1 != null && !"".equals(dirj_end_exec_1_tFTPPut_1))
								tempdirj_end_exec_1_tFTPPut_1 = tempdirj_end_exec_1_tFTPPut_1 + "/"
										+ dirj_end_exec_1_tFTPPut_1;
							maskj_end_exec_1_tFTPPut_1 = maskj_end_exec_1_tFTPPut_1.replaceAll("\\.", "\\\\.")
									.replaceAll("\\*", ".*");
							final String finalMaskj_end_exec_1_tFTPPut_1 = maskj_end_exec_1_tFTPPut_1;
							java.io.File[] listingsj_end_exec_1_tFTPPut_1 = null;
							java.io.File filej_end_exec_1_tFTPPut_1 = new java.io.File(tempdirj_end_exec_1_tFTPPut_1);
							if (filej_end_exec_1_tFTPPut_1.isDirectory()) {
								listingsj_end_exec_1_tFTPPut_1 = filej_end_exec_1_tFTPPut_1
										.listFiles(new java.io.FileFilter() {
											public boolean accept(java.io.File pathname) {
												boolean result = false;
												if (pathname != null && pathname.isFile()) {
													result = java.util.regex.Pattern
															.compile(finalMaskj_end_exec_1_tFTPPut_1)
															.matcher(pathname.getName()).find();
												}
												return result;
											}
										});
							}

							java.util.Set<String> remoteExistsFiles_j_end_exec_1_tFTPPut_1 = new java.util.HashSet<>();

							String[] ftpFileNames_j_end_exec_1_tFTPPut_1 = ftp_j_end_exec_1_tFTPPut_1.listNames();
							for (String ftpFileName : ftpFileNames_j_end_exec_1_tFTPPut_1) {
								if ("".equals(mapj_end_exec_1_tFTPPut_1.get(keyj_end_exec_1_tFTPPut_1))) {
									if (ftpFileName.matches(maskj_end_exec_1_tFTPPut_1)) {
										remoteExistsFiles_j_end_exec_1_tFTPPut_1.add(ftpFileName);
									}
								} else {
									if (ftpFileName.matches(mapj_end_exec_1_tFTPPut_1.get(keyj_end_exec_1_tFTPPut_1))) {
										remoteExistsFiles_j_end_exec_1_tFTPPut_1.add(ftpFileName);
									}
								}
							}

							if (listingsj_end_exec_1_tFTPPut_1 != null && listingsj_end_exec_1_tFTPPut_1.length > 0) {
								for (int mj_end_exec_1_tFTPPut_1 = 0; mj_end_exec_1_tFTPPut_1 < listingsj_end_exec_1_tFTPPut_1.length; mj_end_exec_1_tFTPPut_1++) {
									if (listingsj_end_exec_1_tFTPPut_1[mj_end_exec_1_tFTPPut_1].getName()
											.matches(maskj_end_exec_1_tFTPPut_1)) {
										java.io.File file_in_localDir_j_end_exec_1_tFTPPut_1 = listingsj_end_exec_1_tFTPPut_1[mj_end_exec_1_tFTPPut_1];
										java.io.FileInputStream file_stream_j_end_exec_1_tFTPPut_1 = new java.io.FileInputStream(
												file_in_localDir_j_end_exec_1_tFTPPut_1);

										final String destRename_j_end_exec_1_tFTPPut_1 = mapj_end_exec_1_tFTPPut_1
												.get(keyj_end_exec_1_tFTPPut_1);
										final String dest_j_end_exec_1_tFTPPut_1;
										if (destRename_j_end_exec_1_tFTPPut_1 == null
												|| destRename_j_end_exec_1_tFTPPut_1.isEmpty()) {
											dest_j_end_exec_1_tFTPPut_1 = listingsj_end_exec_1_tFTPPut_1[mj_end_exec_1_tFTPPut_1]
													.getName();
										} else {
											dest_j_end_exec_1_tFTPPut_1 = destRename_j_end_exec_1_tFTPPut_1;
										}
										globalMap.put("j_end_exec_1_tFTPPut_1_CURRENT_FILE_EXISTS",
												remoteExistsFiles_j_end_exec_1_tFTPPut_1
														.contains(dest_j_end_exec_1_tFTPPut_1));

										if (!(remoteExistsFiles_j_end_exec_1_tFTPPut_1
												.contains(dest_j_end_exec_1_tFTPPut_1))) {
											ftp_j_end_exec_1_tFTPPut_1.storeFile(dest_j_end_exec_1_tFTPPut_1,
													file_stream_j_end_exec_1_tFTPPut_1);
											log.debug("j_end_exec_1_tFTPPut_1 - Uploaded file '"
													+ dest_j_end_exec_1_tFTPPut_1 + "' successfully.");
											globalMap.put("j_end_exec_1_tFTPPut_1_CURRENT_FILE_EXISTS",
													remoteExistsFiles_j_end_exec_1_tFTPPut_1
															.contains(dest_j_end_exec_1_tFTPPut_1));
											remoteExistsFiles_j_end_exec_1_tFTPPut_1.add(dest_j_end_exec_1_tFTPPut_1);
										}
										file_stream_j_end_exec_1_tFTPPut_1.close();
										msg_j_end_exec_1_tFTPPut_1.add(
												"file: " + file_in_localDir_j_end_exec_1_tFTPPut_1.getAbsolutePath()
														+ ", size: " + file_in_localDir_j_end_exec_1_tFTPPut_1.length()
														+ " bytes upload successfully");
										nb_file_j_end_exec_1_tFTPPut_1++;
									}
								}
							} else {
								log.warn("j_end_exec_1_tFTPPut_1 - No matches found for mask '"
										+ keyj_end_exec_1_tFTPPut_1 + "'!");
								System.err.println("No matches found for mask '" + keyj_end_exec_1_tFTPPut_1 + "'!");
							}
						}
					} catch (java.lang.Exception e_j_end_exec_1_tFTPPut_1) {
						globalMap.put("j_end_exec_1_tFTPPut_1_ERROR_MESSAGE", e_j_end_exec_1_tFTPPut_1.getMessage());
						msg_j_end_exec_1_tFTPPut_1.add("file not found?: " + e_j_end_exec_1_tFTPPut_1.getMessage());
						throw (e_j_end_exec_1_tFTPPut_1);
					}

					tos_count_j_end_exec_1_tFTPPut_1++;

					/**
					 * [j_end_exec_1_tFTPPut_1 main ] stop
					 */

					/**
					 * [j_end_exec_1_tFTPPut_1 process_data_begin ] start
					 */

					currentComponent = "j_end_exec_1_tFTPPut_1";

					/**
					 * [j_end_exec_1_tFTPPut_1 process_data_begin ] stop
					 */

					/**
					 * [j_end_exec_1_tFTPPut_1 process_data_end ] start
					 */

					currentComponent = "j_end_exec_1_tFTPPut_1";

					/**
					 * [j_end_exec_1_tFTPPut_1 process_data_end ] stop
					 */

					/**
					 * [j_end_exec_1_tFTPPut_1 end ] start
					 */

					currentComponent = "j_end_exec_1_tFTPPut_1";

					// *** ftp *** //
				}

				msg_j_end_exec_1_tFTPPut_1.add(nb_file_j_end_exec_1_tFTPPut_1 + " files have been uploaded.");

				String[] msgAll_j_end_exec_1_tFTPPut_1 = msg_j_end_exec_1_tFTPPut_1.toArray(new String[0]);
				StringBuffer sb_j_end_exec_1_tFTPPut_1 = new StringBuffer();
				if (msgAll_j_end_exec_1_tFTPPut_1 != null) {
					for (String item_j_end_exec_1_tFTPPut_1 : msgAll_j_end_exec_1_tFTPPut_1) {
						sb_j_end_exec_1_tFTPPut_1.append(item_j_end_exec_1_tFTPPut_1).append("\n");
					}
				}
				globalMap.put("j_end_exec_1_tFTPPut_1_TRANSFER_MESSAGES", sb_j_end_exec_1_tFTPPut_1.toString());

				log.info("j_end_exec_1_tFTPPut_1 - Closing the connection to the server.");

				ftp_j_end_exec_1_tFTPPut_1.logout();
				ftp_j_end_exec_1_tFTPPut_1.disconnect();

				log.info("j_end_exec_1_tFTPPut_1 - Connection to the server closed.");

				if (nb_file_j_end_exec_1_tFTPPut_1 == 0 && !listj_end_exec_1_tFTPPut_1.isEmpty()) {
					throw new RuntimeException("Error during component operation!");
				}

				globalMap.put("j_end_exec_1_tFTPPut_1_NB_FILE", nb_file_j_end_exec_1_tFTPPut_1);
				log.info("j_end_exec_1_tFTPPut_1 - Uploaded files count: " + nb_file_j_end_exec_1_tFTPPut_1 + ".");
				if (nb_file_j_end_exec_1_tFTPPut_1 == 0 && !listj_end_exec_1_tFTPPut_1.isEmpty()) {
					throw new RuntimeException("Error during component operation!");
				}

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tFTPPut_1 - " + ("Done."));

				ok_Hash.put("j_end_exec_1_tFTPPut_1", true);
				end_Hash.put("j_end_exec_1_tFTPPut_1", System.currentTimeMillis());

				/**
				 * [j_end_exec_1_tFTPPut_1 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_end_exec_1_tFTPPut_1 finally ] start
				 */

				currentComponent = "j_end_exec_1_tFTPPut_1";

				/**
				 * [j_end_exec_1_tFTPPut_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_end_exec_1_tFTPPut_1_SUBPROCESS_STATE", 1);
	}

	public static class j_end_exec_1_toUpdateStruct
			implements routines.system.IPersistableRow<j_end_exec_1_toUpdateStruct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public Integer id;

		public Integer getId() {
			return this.id;
		}

		public String last_execution_end_date;

		public String getLast_execution_end_date() {
			return this.last_execution_end_date;
		}

		public String last_execution_result;

		public String getLast_execution_result() {
			return this.last_execution_result;
		}

		public Boolean is_running;

		public Boolean getIs_running() {
			return this.is_running;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final j_end_exec_1_toUpdateStruct other = (j_end_exec_1_toUpdateStruct) obj;

			if (this.id == null) {
				if (other.id != null)
					return false;

			} else if (!this.id.equals(other.id))

				return false;

			return true;
		}

		public void copyDataTo(j_end_exec_1_toUpdateStruct other) {

			other.id = this.id;
			other.last_execution_end_date = this.last_execution_end_date;
			other.last_execution_result = this.last_execution_result;
			other.is_running = this.is_running;

		}

		public void copyKeysDataTo(j_end_exec_1_toUpdateStruct other) {

			other.id = this.id;

		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

					this.last_execution_end_date = readString(dis);

					this.last_execution_result = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.is_running = null;
					} else {
						this.is_running = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

					this.last_execution_end_date = readString(dis);

					this.last_execution_result = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.is_running = null;
					} else {
						this.is_running = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

				// String

				writeString(this.last_execution_end_date, dos);

				// String

				writeString(this.last_execution_result, dos);

				// Boolean

				if (this.is_running == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.is_running);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

				// String

				writeString(this.last_execution_end_date, dos);

				// String

				writeString(this.last_execution_result, dos);

				// Boolean

				if (this.is_running == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.is_running);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("id=" + String.valueOf(id));
			sb.append(",last_execution_end_date=" + last_execution_end_date);
			sb.append(",last_execution_result=" + last_execution_result);
			sb.append(",is_running=" + String.valueOf(is_running));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (id == null) {
				sb.append("<null>");
			} else {
				sb.append(id);
			}

			sb.append("|");

			if (last_execution_end_date == null) {
				sb.append("<null>");
			} else {
				sb.append(last_execution_end_date);
			}

			sb.append("|");

			if (last_execution_result == null) {
				sb.append("<null>");
			} else {
				sb.append(last_execution_result);
			}

			sb.append("|");

			if (is_running == null) {
				sb.append("<null>");
			} else {
				sb.append(is_running);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(j_end_exec_1_toUpdateStruct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.id, other.id);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class j_end_exec_1_row2Struct implements routines.system.IPersistableRow<j_end_exec_1_row2Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public Integer id;

		public Integer getId() {
			return this.id;
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("id=" + String.valueOf(id));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (id == null) {
				sb.append("<null>");
			} else {
				sb.append(id);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(j_end_exec_1_row2Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void j_end_exec_1_tDBInput_3Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("j_end_exec_1_tDBInput_3_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				j_end_exec_1_row2Struct j_end_exec_1_row2 = new j_end_exec_1_row2Struct();
				j_end_exec_1_toUpdateStruct j_end_exec_1_toUpdate = new j_end_exec_1_toUpdateStruct();

				/**
				 * [j_end_exec_1_tDBOutput_1 begin ] start
				 */

				ok_Hash.put("j_end_exec_1_tDBOutput_1", false);
				start_Hash.put("j_end_exec_1_tDBOutput_1", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tDBOutput_1";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0,
						"j_end_exec_1_toUpdate");

				int tos_count_j_end_exec_1_tDBOutput_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBOutput_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_end_exec_1_tDBOutput_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_end_exec_1_tDBOutput_1 = new StringBuilder();
							log4jParamters_j_end_exec_1_tDBOutput_1.append("Parameters:");
							log4jParamters_j_end_exec_1_tDBOutput_1.append("USE_EXISTING_CONNECTION" + " = " + "true");
							log4jParamters_j_end_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_1
									.append("CONNECTION" + " = " + "j_end_exec_1_tDBConnection_1");
							log4jParamters_j_end_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_1.append("TABLE" + " = " + "\"locks\"");
							log4jParamters_j_end_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_1.append("TABLE_ACTION" + " = " + "NONE");
							log4jParamters_j_end_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_1.append("DATA_ACTION" + " = " + "UPDATE");
							log4jParamters_j_end_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_1.append("DIE_ON_ERROR" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_1.append("USE_ALTERNATE_SCHEMA" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_1.append("ADD_COLS" + " = " + "[]");
							log4jParamters_j_end_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_1.append("USE_FIELD_OPTIONS" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_1.append("ENABLE_DEBUG_MODE" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_1.append("SUPPORT_NULL_WHERE" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_1
									.append("CONVERT_COLUMN_TABLE_TO_LOWERCASE" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_1.append("USE_BATCH_SIZE" + " = " + "true");
							log4jParamters_j_end_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_1.append("BATCH_SIZE" + " = " + "10000");
							log4jParamters_j_end_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_1
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlOutput");
							log4jParamters_j_end_exec_1_tDBOutput_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_end_exec_1_tDBOutput_1 - " + (log4jParamters_j_end_exec_1_tDBOutput_1));
						}
					}
					new BytesLimit65535_j_end_exec_1_tDBOutput_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tDBOutput_1",
							"__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>", "tPostgresqlOutput");
					talendJobLogProcess(globalMap);
				}

				String dbschema_j_end_exec_1_tDBOutput_1 = null;
				dbschema_j_end_exec_1_tDBOutput_1 = (String) globalMap.get("schema_" + "j_end_exec_1_tDBConnection_1");

				String tableName_j_end_exec_1_tDBOutput_1 = null;
				if (dbschema_j_end_exec_1_tDBOutput_1 == null
						|| dbschema_j_end_exec_1_tDBOutput_1.trim().length() == 0) {
					tableName_j_end_exec_1_tDBOutput_1 = ("locks");
				} else {
					tableName_j_end_exec_1_tDBOutput_1 = dbschema_j_end_exec_1_tDBOutput_1 + "\".\"" + ("locks");
				}

				int updateKeyCount_j_end_exec_1_tDBOutput_1 = 1;
				if (updateKeyCount_j_end_exec_1_tDBOutput_1 < 1) {
					throw new RuntimeException("For update, Schema must have a key");
				} else if (updateKeyCount_j_end_exec_1_tDBOutput_1 == 4 && true) {
					throw new RuntimeException("For update, every Schema column can not be a key");
				}

				int nb_line_j_end_exec_1_tDBOutput_1 = 0;
				int nb_line_update_j_end_exec_1_tDBOutput_1 = 0;
				int nb_line_inserted_j_end_exec_1_tDBOutput_1 = 0;
				int nb_line_deleted_j_end_exec_1_tDBOutput_1 = 0;
				int nb_line_rejected_j_end_exec_1_tDBOutput_1 = 0;

				int deletedCount_j_end_exec_1_tDBOutput_1 = 0;
				int updatedCount_j_end_exec_1_tDBOutput_1 = 0;
				int insertedCount_j_end_exec_1_tDBOutput_1 = 0;
				int rowsToCommitCount_j_end_exec_1_tDBOutput_1 = 0;
				int rejectedCount_j_end_exec_1_tDBOutput_1 = 0;

				boolean whetherReject_j_end_exec_1_tDBOutput_1 = false;

				java.sql.Connection conn_j_end_exec_1_tDBOutput_1 = null;
				String dbUser_j_end_exec_1_tDBOutput_1 = null;

				conn_j_end_exec_1_tDBOutput_1 = (java.sql.Connection) globalMap
						.get("conn_j_end_exec_1_tDBConnection_1");

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBOutput_1 - " + ("Uses an existing connection with username '")
							+ (conn_j_end_exec_1_tDBOutput_1.getMetaData().getUserName()) + ("'. Connection URL: ")
							+ (conn_j_end_exec_1_tDBOutput_1.getMetaData().getURL()) + ("."));

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBOutput_1 - " + ("Connection is set auto commit to '")
							+ (conn_j_end_exec_1_tDBOutput_1.getAutoCommit()) + ("'."));

				int batchSize_j_end_exec_1_tDBOutput_1 = 10000;
				int batchSizeCounter_j_end_exec_1_tDBOutput_1 = 0;

				int count_j_end_exec_1_tDBOutput_1 = 0;
				String update_j_end_exec_1_tDBOutput_1 = "UPDATE \"" + tableName_j_end_exec_1_tDBOutput_1
						+ "\" SET \"last_execution_end_date\" = ?,\"last_execution_result\" = ?,\"is_running\" = ? WHERE \"id\" = ?";
				java.sql.PreparedStatement pstmt_j_end_exec_1_tDBOutput_1 = conn_j_end_exec_1_tDBOutput_1
						.prepareStatement(update_j_end_exec_1_tDBOutput_1);
				resourceMap.put("pstmt_j_end_exec_1_tDBOutput_1", pstmt_j_end_exec_1_tDBOutput_1);

				/**
				 * [j_end_exec_1_tDBOutput_1 begin ] stop
				 */

				/**
				 * [j_end_exec_1_tMap_1 begin ] start
				 */

				ok_Hash.put("j_end_exec_1_tMap_1", false);
				start_Hash.put("j_end_exec_1_tMap_1", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tMap_1";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "j_end_exec_1_row2");

				int tos_count_j_end_exec_1_tMap_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tMap_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_end_exec_1_tMap_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_end_exec_1_tMap_1 = new StringBuilder();
							log4jParamters_j_end_exec_1_tMap_1.append("Parameters:");
							log4jParamters_j_end_exec_1_tMap_1.append("LINK_STYLE" + " = " + "AUTO");
							log4jParamters_j_end_exec_1_tMap_1.append(" | ");
							log4jParamters_j_end_exec_1_tMap_1.append("TEMPORARY_DATA_DIRECTORY" + " = " + "");
							log4jParamters_j_end_exec_1_tMap_1.append(" | ");
							log4jParamters_j_end_exec_1_tMap_1.append("ROWS_BUFFER_SIZE" + " = " + "2000000");
							log4jParamters_j_end_exec_1_tMap_1.append(" | ");
							log4jParamters_j_end_exec_1_tMap_1
									.append("CHANGE_HASH_AND_EQUALS_FOR_BIGDECIMAL" + " = " + "true");
							log4jParamters_j_end_exec_1_tMap_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_end_exec_1_tMap_1 - " + (log4jParamters_j_end_exec_1_tMap_1));
						}
					}
					new BytesLimit65535_j_end_exec_1_tMap_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tMap_1", "j_end_exec_1_tMap_1", "tMap");
					talendJobLogProcess(globalMap);
				}

// ###############################
// # Lookup's keys initialization
				int count_j_end_exec_1_row2_j_end_exec_1_tMap_1 = 0;

// ###############################        

// ###############################
// # Vars initialization
				class Var__j_end_exec_1_tMap_1__Struct {
					String result;
				}
				Var__j_end_exec_1_tMap_1__Struct Var__j_end_exec_1_tMap_1 = new Var__j_end_exec_1_tMap_1__Struct();
// ###############################

// ###############################
// # Outputs initialization
				int count_j_end_exec_1_toUpdate_j_end_exec_1_tMap_1 = 0;

				j_end_exec_1_toUpdateStruct j_end_exec_1_toUpdate_tmp = new j_end_exec_1_toUpdateStruct();
// ###############################

				/**
				 * [j_end_exec_1_tMap_1 begin ] stop
				 */

				/**
				 * [j_end_exec_1_tDBInput_3 begin ] start
				 */

				ok_Hash.put("j_end_exec_1_tDBInput_3", false);
				start_Hash.put("j_end_exec_1_tDBInput_3", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tDBInput_3";

				int tos_count_j_end_exec_1_tDBInput_3 = 0;

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBInput_3 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_end_exec_1_tDBInput_3 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_end_exec_1_tDBInput_3 = new StringBuilder();
							log4jParamters_j_end_exec_1_tDBInput_3.append("Parameters:");
							log4jParamters_j_end_exec_1_tDBInput_3.append("USE_EXISTING_CONNECTION" + " = " + "true");
							log4jParamters_j_end_exec_1_tDBInput_3.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_3
									.append("CONNECTION" + " = " + "j_end_exec_1_tDBConnection_1");
							log4jParamters_j_end_exec_1_tDBInput_3.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_3.append("QUERYSTORE" + " = " + "\"\"");
							log4jParamters_j_end_exec_1_tDBInput_3.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_3.append("QUERY" + " = "
									+ "\" SELECT  	id FROM locks WHERE guid = '\"+ (String)globalMap.get(\"guid\") +\"' ORDER BY id DESC \"");
							log4jParamters_j_end_exec_1_tDBInput_3.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_3.append("USE_CURSOR" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBInput_3.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_3.append("TRIM_ALL_COLUMN" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBInput_3.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_3.append(
									"TRIM_COLUMN" + " = " + "[{TRIM=" + ("false") + ", SCHEMA_COLUMN=" + ("id") + "}]");
							log4jParamters_j_end_exec_1_tDBInput_3.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_3
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlInput");
							log4jParamters_j_end_exec_1_tDBInput_3.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_end_exec_1_tDBInput_3 - " + (log4jParamters_j_end_exec_1_tDBInput_3));
						}
					}
					new BytesLimit65535_j_end_exec_1_tDBInput_3().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tDBInput_3", "__UNIQUE_NAME__<br><b>__TABLE__</b>",
							"tPostgresqlInput");
					talendJobLogProcess(globalMap);
				}

				int nb_line_j_end_exec_1_tDBInput_3 = 0;
				java.sql.Connection conn_j_end_exec_1_tDBInput_3 = null;
				conn_j_end_exec_1_tDBInput_3 = (java.sql.Connection) globalMap.get("conn_j_end_exec_1_tDBConnection_1");

				if (conn_j_end_exec_1_tDBInput_3 != null) {
					if (conn_j_end_exec_1_tDBInput_3.getMetaData() != null) {

						log.debug("j_end_exec_1_tDBInput_3 - Uses an existing connection with username '"
								+ conn_j_end_exec_1_tDBInput_3.getMetaData().getUserName() + "'. Connection URL: "
								+ conn_j_end_exec_1_tDBInput_3.getMetaData().getURL() + ".");

					}
				}

				java.sql.Statement stmt_j_end_exec_1_tDBInput_3 = conn_j_end_exec_1_tDBInput_3.createStatement();

				String dbquery_j_end_exec_1_tDBInput_3 = "\nSELECT \n	id\nFROM locks\nWHERE guid = '"
						+ (String) globalMap.get("guid") + "'\nORDER BY id DESC\n";

				log.debug("j_end_exec_1_tDBInput_3 - Executing the query: '" + dbquery_j_end_exec_1_tDBInput_3 + "'.");

				globalMap.put("j_end_exec_1_tDBInput_3_QUERY", dbquery_j_end_exec_1_tDBInput_3);
				java.sql.ResultSet rs_j_end_exec_1_tDBInput_3 = null;

				try {
					rs_j_end_exec_1_tDBInput_3 = stmt_j_end_exec_1_tDBInput_3
							.executeQuery(dbquery_j_end_exec_1_tDBInput_3);
					java.sql.ResultSetMetaData rsmd_j_end_exec_1_tDBInput_3 = rs_j_end_exec_1_tDBInput_3.getMetaData();
					int colQtyInRs_j_end_exec_1_tDBInput_3 = rsmd_j_end_exec_1_tDBInput_3.getColumnCount();

					String tmpContent_j_end_exec_1_tDBInput_3 = null;

					log.debug("j_end_exec_1_tDBInput_3 - Retrieving records from the database.");

					while (rs_j_end_exec_1_tDBInput_3.next()) {
						nb_line_j_end_exec_1_tDBInput_3++;

						if (colQtyInRs_j_end_exec_1_tDBInput_3 < 1) {
							j_end_exec_1_row2.id = null;
						} else {

							j_end_exec_1_row2.id = rs_j_end_exec_1_tDBInput_3.getInt(1);
							if (rs_j_end_exec_1_tDBInput_3.wasNull()) {
								j_end_exec_1_row2.id = null;
							}
						}

						log.debug("j_end_exec_1_tDBInput_3 - Retrieving the record " + nb_line_j_end_exec_1_tDBInput_3
								+ ".");

						/**
						 * [j_end_exec_1_tDBInput_3 begin ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_3 main ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_3";

						tos_count_j_end_exec_1_tDBInput_3++;

						/**
						 * [j_end_exec_1_tDBInput_3 main ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_3 process_data_begin ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_3";

						/**
						 * [j_end_exec_1_tDBInput_3 process_data_begin ] stop
						 */

						/**
						 * [j_end_exec_1_tMap_1 main ] start
						 */

						currentComponent = "j_end_exec_1_tMap_1";

						if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

								, "j_end_exec_1_row2", "j_end_exec_1_tDBInput_3", "__UNIQUE_NAME__<br><b>__TABLE__</b>",
								"tPostgresqlInput", "j_end_exec_1_tMap_1", "j_end_exec_1_tMap_1", "tMap"

						)) {
							talendJobLogProcess(globalMap);
						}

						if (log.isTraceEnabled()) {
							log.trace("j_end_exec_1_row2 - "
									+ (j_end_exec_1_row2 == null ? "" : j_end_exec_1_row2.toLogString()));
						}

						boolean hasCasePrimitiveKeyWithNull_j_end_exec_1_tMap_1 = false;

						// ###############################
						// # Input tables (lookups)
						boolean rejectedInnerJoin_j_end_exec_1_tMap_1 = false;
						boolean mainRowRejected_j_end_exec_1_tMap_1 = false;

						// ###############################
						{ // start of Var scope

							// ###############################
							// # Vars tables

							Var__j_end_exec_1_tMap_1__Struct Var = Var__j_end_exec_1_tMap_1;
							Var.result = ((Integer) globalMap.get("j_end_exec_1_tDBInput_2_NB_LINE") > 0
									? "The job ended with "
											+ String.valueOf((Integer) globalMap.get("j_end_exec_1_tDBInput_2_NB_LINE"))
											+ " errors"
									: "The job ended without error");// ###############################
																		// ###############################
																		// # Output tables

							j_end_exec_1_toUpdate = null;

// # Output table : 'j_end_exec_1_toUpdate'
							count_j_end_exec_1_toUpdate_j_end_exec_1_tMap_1++;

							j_end_exec_1_toUpdate_tmp.id = j_end_exec_1_row2.id;
							j_end_exec_1_toUpdate_tmp.last_execution_end_date = TalendDate
									.getDate("CCYY-MM-DD hh:mm:ss");
							j_end_exec_1_toUpdate_tmp.last_execution_result = Var.result;
							j_end_exec_1_toUpdate_tmp.is_running = false;
							j_end_exec_1_toUpdate = j_end_exec_1_toUpdate_tmp;
							log.debug("j_end_exec_1_tMap_1 - Outputting the record "
									+ count_j_end_exec_1_toUpdate_j_end_exec_1_tMap_1
									+ " of the output table 'j_end_exec_1_toUpdate'.");

// ###############################

						} // end of Var scope

						rejectedInnerJoin_j_end_exec_1_tMap_1 = false;

						tos_count_j_end_exec_1_tMap_1++;

						/**
						 * [j_end_exec_1_tMap_1 main ] stop
						 */

						/**
						 * [j_end_exec_1_tMap_1 process_data_begin ] start
						 */

						currentComponent = "j_end_exec_1_tMap_1";

						/**
						 * [j_end_exec_1_tMap_1 process_data_begin ] stop
						 */
// Start of branch "j_end_exec_1_toUpdate"
						if (j_end_exec_1_toUpdate != null) {

							/**
							 * [j_end_exec_1_tDBOutput_1 main ] start
							 */

							currentComponent = "j_end_exec_1_tDBOutput_1";

							if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

									, "j_end_exec_1_toUpdate", "j_end_exec_1_tMap_1", "j_end_exec_1_tMap_1", "tMap",
									"j_end_exec_1_tDBOutput_1",
									"__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>", "tPostgresqlOutput"

							)) {
								talendJobLogProcess(globalMap);
							}

							if (log.isTraceEnabled()) {
								log.trace("j_end_exec_1_toUpdate - "
										+ (j_end_exec_1_toUpdate == null ? "" : j_end_exec_1_toUpdate.toLogString()));
							}

							whetherReject_j_end_exec_1_tDBOutput_1 = false;
							if (j_end_exec_1_toUpdate.last_execution_end_date == null) {
								pstmt_j_end_exec_1_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
							} else {
								pstmt_j_end_exec_1_tDBOutput_1.setString(1,
										j_end_exec_1_toUpdate.last_execution_end_date);
							}

							if (j_end_exec_1_toUpdate.last_execution_result == null) {
								pstmt_j_end_exec_1_tDBOutput_1.setNull(2, java.sql.Types.VARCHAR);
							} else {
								pstmt_j_end_exec_1_tDBOutput_1.setString(2,
										j_end_exec_1_toUpdate.last_execution_result);
							}

							if (j_end_exec_1_toUpdate.is_running == null) {
								pstmt_j_end_exec_1_tDBOutput_1.setNull(3, java.sql.Types.BOOLEAN);
							} else {
								pstmt_j_end_exec_1_tDBOutput_1.setBoolean(3, j_end_exec_1_toUpdate.is_running);
							}

							if (j_end_exec_1_toUpdate.id == null) {
								pstmt_j_end_exec_1_tDBOutput_1.setNull(4 + count_j_end_exec_1_tDBOutput_1,
										java.sql.Types.INTEGER);
							} else {
								pstmt_j_end_exec_1_tDBOutput_1.setInt(4 + count_j_end_exec_1_tDBOutput_1,
										j_end_exec_1_toUpdate.id);
							}

							pstmt_j_end_exec_1_tDBOutput_1.addBatch();
							nb_line_j_end_exec_1_tDBOutput_1++;

							if (log.isDebugEnabled())
								log.debug("j_end_exec_1_tDBOutput_1 - " + ("Adding the record ")
										+ (nb_line_j_end_exec_1_tDBOutput_1) + (" to the ") + ("UPDATE") + (" batch."));
							batchSizeCounter_j_end_exec_1_tDBOutput_1++;

							if ((batchSize_j_end_exec_1_tDBOutput_1 > 0)
									&& (batchSize_j_end_exec_1_tDBOutput_1 <= batchSizeCounter_j_end_exec_1_tDBOutput_1)) {
								try {
									int countSum_j_end_exec_1_tDBOutput_1 = 0;

									if (log.isDebugEnabled())
										log.debug("j_end_exec_1_tDBOutput_1 - " + ("Executing the ") + ("UPDATE")
												+ (" batch."));
									for (int countEach_j_end_exec_1_tDBOutput_1 : pstmt_j_end_exec_1_tDBOutput_1
											.executeBatch()) {
										countSum_j_end_exec_1_tDBOutput_1 += (countEach_j_end_exec_1_tDBOutput_1 < 0 ? 0
												: countEach_j_end_exec_1_tDBOutput_1);
									}
									if (log.isDebugEnabled())
										log.debug("j_end_exec_1_tDBOutput_1 - " + ("The ") + ("UPDATE")
												+ (" batch execution has succeeded."));
									rowsToCommitCount_j_end_exec_1_tDBOutput_1 += countSum_j_end_exec_1_tDBOutput_1;

									updatedCount_j_end_exec_1_tDBOutput_1 += countSum_j_end_exec_1_tDBOutput_1;

									batchSizeCounter_j_end_exec_1_tDBOutput_1 = 0;
								} catch (java.sql.BatchUpdateException e_j_end_exec_1_tDBOutput_1) {
									globalMap.put("j_end_exec_1_tDBOutput_1_ERROR_MESSAGE",
											e_j_end_exec_1_tDBOutput_1.getMessage());
									java.sql.SQLException ne_j_end_exec_1_tDBOutput_1 = e_j_end_exec_1_tDBOutput_1
											.getNextException(), sqle_j_end_exec_1_tDBOutput_1 = null;
									String errormessage_j_end_exec_1_tDBOutput_1;
									if (ne_j_end_exec_1_tDBOutput_1 != null) {
										// build new exception to provide the original cause
										sqle_j_end_exec_1_tDBOutput_1 = new java.sql.SQLException(
												e_j_end_exec_1_tDBOutput_1.getMessage() + "\ncaused by: "
														+ ne_j_end_exec_1_tDBOutput_1.getMessage(),
												ne_j_end_exec_1_tDBOutput_1.getSQLState(),
												ne_j_end_exec_1_tDBOutput_1.getErrorCode(),
												ne_j_end_exec_1_tDBOutput_1);
										errormessage_j_end_exec_1_tDBOutput_1 = sqle_j_end_exec_1_tDBOutput_1
												.getMessage();
									} else {
										errormessage_j_end_exec_1_tDBOutput_1 = e_j_end_exec_1_tDBOutput_1.getMessage();
									}

									int countSum_j_end_exec_1_tDBOutput_1 = 0;
									for (int countEach_j_end_exec_1_tDBOutput_1 : e_j_end_exec_1_tDBOutput_1
											.getUpdateCounts()) {
										countSum_j_end_exec_1_tDBOutput_1 += (countEach_j_end_exec_1_tDBOutput_1 < 0 ? 0
												: countEach_j_end_exec_1_tDBOutput_1);
									}
									rowsToCommitCount_j_end_exec_1_tDBOutput_1 += countSum_j_end_exec_1_tDBOutput_1;

									updatedCount_j_end_exec_1_tDBOutput_1 += countSum_j_end_exec_1_tDBOutput_1;

									log.error("j_end_exec_1_tDBOutput_1 - " + (errormessage_j_end_exec_1_tDBOutput_1));
									System.err.println(errormessage_j_end_exec_1_tDBOutput_1);

								}
							}

							tos_count_j_end_exec_1_tDBOutput_1++;

							/**
							 * [j_end_exec_1_tDBOutput_1 main ] stop
							 */

							/**
							 * [j_end_exec_1_tDBOutput_1 process_data_begin ] start
							 */

							currentComponent = "j_end_exec_1_tDBOutput_1";

							/**
							 * [j_end_exec_1_tDBOutput_1 process_data_begin ] stop
							 */

							/**
							 * [j_end_exec_1_tDBOutput_1 process_data_end ] start
							 */

							currentComponent = "j_end_exec_1_tDBOutput_1";

							/**
							 * [j_end_exec_1_tDBOutput_1 process_data_end ] stop
							 */

						} // End of branch "j_end_exec_1_toUpdate"

						/**
						 * [j_end_exec_1_tMap_1 process_data_end ] start
						 */

						currentComponent = "j_end_exec_1_tMap_1";

						/**
						 * [j_end_exec_1_tMap_1 process_data_end ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_3 process_data_end ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_3";

						/**
						 * [j_end_exec_1_tDBInput_3 process_data_end ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_3 end ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_3";

					}
				} finally {
					if (rs_j_end_exec_1_tDBInput_3 != null) {
						rs_j_end_exec_1_tDBInput_3.close();
					}
					if (stmt_j_end_exec_1_tDBInput_3 != null) {
						stmt_j_end_exec_1_tDBInput_3.close();
					}
				}
				globalMap.put("j_end_exec_1_tDBInput_3_NB_LINE", nb_line_j_end_exec_1_tDBInput_3);
				log.debug(
						"j_end_exec_1_tDBInput_3 - Retrieved records count: " + nb_line_j_end_exec_1_tDBInput_3 + " .");

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBInput_3 - " + ("Done."));

				ok_Hash.put("j_end_exec_1_tDBInput_3", true);
				end_Hash.put("j_end_exec_1_tDBInput_3", System.currentTimeMillis());

				/**
				 * [j_end_exec_1_tDBInput_3 end ] stop
				 */

				/**
				 * [j_end_exec_1_tMap_1 end ] start
				 */

				currentComponent = "j_end_exec_1_tMap_1";

// ###############################
// # Lookup hashes releasing
// ###############################      
				log.debug("j_end_exec_1_tMap_1 - Written records count in the table 'j_end_exec_1_toUpdate': "
						+ count_j_end_exec_1_toUpdate_j_end_exec_1_tMap_1 + ".");

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "j_end_exec_1_row2", 2,
						0, "j_end_exec_1_tDBInput_3", "__UNIQUE_NAME__<br><b>__TABLE__</b>", "tPostgresqlInput",
						"j_end_exec_1_tMap_1", "j_end_exec_1_tMap_1", "tMap", "output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tMap_1 - " + ("Done."));

				ok_Hash.put("j_end_exec_1_tMap_1", true);
				end_Hash.put("j_end_exec_1_tMap_1", System.currentTimeMillis());

				/**
				 * [j_end_exec_1_tMap_1 end ] stop
				 */

				/**
				 * [j_end_exec_1_tDBOutput_1 end ] start
				 */

				currentComponent = "j_end_exec_1_tDBOutput_1";

				try {
					int countSum_j_end_exec_1_tDBOutput_1 = 0;
					if (pstmt_j_end_exec_1_tDBOutput_1 != null && batchSizeCounter_j_end_exec_1_tDBOutput_1 > 0) {

						if (log.isDebugEnabled())
							log.debug("j_end_exec_1_tDBOutput_1 - " + ("Executing the ") + ("UPDATE") + (" batch."));
						for (int countEach_j_end_exec_1_tDBOutput_1 : pstmt_j_end_exec_1_tDBOutput_1.executeBatch()) {
							countSum_j_end_exec_1_tDBOutput_1 += (countEach_j_end_exec_1_tDBOutput_1 < 0 ? 0
									: countEach_j_end_exec_1_tDBOutput_1);
						}
						rowsToCommitCount_j_end_exec_1_tDBOutput_1 += countSum_j_end_exec_1_tDBOutput_1;

						if (log.isDebugEnabled())
							log.debug("j_end_exec_1_tDBOutput_1 - " + ("The ") + ("UPDATE")
									+ (" batch execution has succeeded."));
					}

					updatedCount_j_end_exec_1_tDBOutput_1 += countSum_j_end_exec_1_tDBOutput_1;

				} catch (java.sql.BatchUpdateException e_j_end_exec_1_tDBOutput_1) {
					globalMap.put("j_end_exec_1_tDBOutput_1_ERROR_MESSAGE", e_j_end_exec_1_tDBOutput_1.getMessage());
					java.sql.SQLException ne_j_end_exec_1_tDBOutput_1 = e_j_end_exec_1_tDBOutput_1.getNextException(),
							sqle_j_end_exec_1_tDBOutput_1 = null;
					String errormessage_j_end_exec_1_tDBOutput_1;
					if (ne_j_end_exec_1_tDBOutput_1 != null) {
						// build new exception to provide the original cause
						sqle_j_end_exec_1_tDBOutput_1 = new java.sql.SQLException(
								e_j_end_exec_1_tDBOutput_1.getMessage() + "\ncaused by: "
										+ ne_j_end_exec_1_tDBOutput_1.getMessage(),
								ne_j_end_exec_1_tDBOutput_1.getSQLState(), ne_j_end_exec_1_tDBOutput_1.getErrorCode(),
								ne_j_end_exec_1_tDBOutput_1);
						errormessage_j_end_exec_1_tDBOutput_1 = sqle_j_end_exec_1_tDBOutput_1.getMessage();
					} else {
						errormessage_j_end_exec_1_tDBOutput_1 = e_j_end_exec_1_tDBOutput_1.getMessage();
					}

					int countSum_j_end_exec_1_tDBOutput_1 = 0;
					for (int countEach_j_end_exec_1_tDBOutput_1 : e_j_end_exec_1_tDBOutput_1.getUpdateCounts()) {
						countSum_j_end_exec_1_tDBOutput_1 += (countEach_j_end_exec_1_tDBOutput_1 < 0 ? 0
								: countEach_j_end_exec_1_tDBOutput_1);
					}
					rowsToCommitCount_j_end_exec_1_tDBOutput_1 += countSum_j_end_exec_1_tDBOutput_1;

					updatedCount_j_end_exec_1_tDBOutput_1 += countSum_j_end_exec_1_tDBOutput_1;

					log.error("j_end_exec_1_tDBOutput_1 - " + (errormessage_j_end_exec_1_tDBOutput_1));
					System.err.println(errormessage_j_end_exec_1_tDBOutput_1);

				}

				if (pstmt_j_end_exec_1_tDBOutput_1 != null) {

					pstmt_j_end_exec_1_tDBOutput_1.close();
					resourceMap.remove("pstmt_j_end_exec_1_tDBOutput_1");
				}
				resourceMap.put("statementClosed_j_end_exec_1_tDBOutput_1", true);

				nb_line_deleted_j_end_exec_1_tDBOutput_1 = nb_line_deleted_j_end_exec_1_tDBOutput_1
						+ deletedCount_j_end_exec_1_tDBOutput_1;
				nb_line_update_j_end_exec_1_tDBOutput_1 = nb_line_update_j_end_exec_1_tDBOutput_1
						+ updatedCount_j_end_exec_1_tDBOutput_1;
				nb_line_inserted_j_end_exec_1_tDBOutput_1 = nb_line_inserted_j_end_exec_1_tDBOutput_1
						+ insertedCount_j_end_exec_1_tDBOutput_1;
				nb_line_rejected_j_end_exec_1_tDBOutput_1 = nb_line_rejected_j_end_exec_1_tDBOutput_1
						+ rejectedCount_j_end_exec_1_tDBOutput_1;

				globalMap.put("j_end_exec_1_tDBOutput_1_NB_LINE", nb_line_j_end_exec_1_tDBOutput_1);
				globalMap.put("j_end_exec_1_tDBOutput_1_NB_LINE_UPDATED", nb_line_update_j_end_exec_1_tDBOutput_1);
				globalMap.put("j_end_exec_1_tDBOutput_1_NB_LINE_INSERTED", nb_line_inserted_j_end_exec_1_tDBOutput_1);
				globalMap.put("j_end_exec_1_tDBOutput_1_NB_LINE_DELETED", nb_line_deleted_j_end_exec_1_tDBOutput_1);
				globalMap.put("j_end_exec_1_tDBOutput_1_NB_LINE_REJECTED", nb_line_rejected_j_end_exec_1_tDBOutput_1);

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBOutput_1 - " + ("Has ") + ("updated") + (" ")
							+ (nb_line_update_j_end_exec_1_tDBOutput_1) + (" record(s)."));

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "j_end_exec_1_toUpdate",
						2, 0, "j_end_exec_1_tMap_1", "j_end_exec_1_tMap_1", "tMap", "j_end_exec_1_tDBOutput_1",
						"__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>", "tPostgresqlOutput",
						"output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBOutput_1 - " + ("Done."));

				ok_Hash.put("j_end_exec_1_tDBOutput_1", true);
				end_Hash.put("j_end_exec_1_tDBOutput_1", System.currentTimeMillis());

				/**
				 * [j_end_exec_1_tDBOutput_1 end ] stop
				 */

			} // end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:j_end_exec_1_tDBInput_3:OnSubjobOk", "",
						Thread.currentThread().getId() + "", "", "", "", "", "");
			}

			if (execStat) {
				runStat.updateStatOnConnection("j_end_exec_1_OnSubjobOk6", 0, "ok");
			}

			j_end_exec_1_tDBClose_1Process(globalMap);

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_end_exec_1_tDBInput_3 finally ] start
				 */

				currentComponent = "j_end_exec_1_tDBInput_3";

				/**
				 * [j_end_exec_1_tDBInput_3 finally ] stop
				 */

				/**
				 * [j_end_exec_1_tMap_1 finally ] start
				 */

				currentComponent = "j_end_exec_1_tMap_1";

				/**
				 * [j_end_exec_1_tMap_1 finally ] stop
				 */

				/**
				 * [j_end_exec_1_tDBOutput_1 finally ] start
				 */

				currentComponent = "j_end_exec_1_tDBOutput_1";

				if (resourceMap.get("statementClosed_j_end_exec_1_tDBOutput_1") == null) {
					java.sql.PreparedStatement pstmtToClose_j_end_exec_1_tDBOutput_1 = null;
					if ((pstmtToClose_j_end_exec_1_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap
							.remove("pstmt_j_end_exec_1_tDBOutput_1")) != null) {
						pstmtToClose_j_end_exec_1_tDBOutput_1.close();
					}
				}

				/**
				 * [j_end_exec_1_tDBOutput_1 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_end_exec_1_tDBInput_3_SUBPROCESS_STATE", 1);
	}

	public void j_end_exec_1_tDBClose_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("j_end_exec_1_tDBClose_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [j_end_exec_1_tDBClose_1 begin ] start
				 */

				ok_Hash.put("j_end_exec_1_tDBClose_1", false);
				start_Hash.put("j_end_exec_1_tDBClose_1", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tDBClose_1";

				int tos_count_j_end_exec_1_tDBClose_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBClose_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_end_exec_1_tDBClose_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_end_exec_1_tDBClose_1 = new StringBuilder();
							log4jParamters_j_end_exec_1_tDBClose_1.append("Parameters:");
							log4jParamters_j_end_exec_1_tDBClose_1
									.append("CONNECTION" + " = " + "j_end_exec_1_tDBConnection_1");
							log4jParamters_j_end_exec_1_tDBClose_1.append(" | ");
							log4jParamters_j_end_exec_1_tDBClose_1
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlClose");
							log4jParamters_j_end_exec_1_tDBClose_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_end_exec_1_tDBClose_1 - " + (log4jParamters_j_end_exec_1_tDBClose_1));
						}
					}
					new BytesLimit65535_j_end_exec_1_tDBClose_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tDBClose_1", "__UNIQUE_NAME__<br><b>BDD_OMNICANAL</b>",
							"tPostgresqlClose");
					talendJobLogProcess(globalMap);
				}

				/**
				 * [j_end_exec_1_tDBClose_1 begin ] stop
				 */

				/**
				 * [j_end_exec_1_tDBClose_1 main ] start
				 */

				currentComponent = "j_end_exec_1_tDBClose_1";

				java.sql.Connection conn_j_end_exec_1_tDBClose_1 = (java.sql.Connection) globalMap
						.get("conn_j_end_exec_1_tDBConnection_1");
				if (conn_j_end_exec_1_tDBClose_1 != null && !conn_j_end_exec_1_tDBClose_1.isClosed()) {
					if (log.isDebugEnabled())
						log.debug("j_end_exec_1_tDBClose_1 - " + ("Closing the connection ")
								+ ("conn_j_end_exec_1_tDBConnection_1") + (" to the database."));
					conn_j_end_exec_1_tDBClose_1.close();
					if (log.isDebugEnabled())
						log.debug("j_end_exec_1_tDBClose_1 - " + ("Connection ") + ("conn_j_end_exec_1_tDBConnection_1")
								+ (" to the database has closed."));
				}

				tos_count_j_end_exec_1_tDBClose_1++;

				/**
				 * [j_end_exec_1_tDBClose_1 main ] stop
				 */

				/**
				 * [j_end_exec_1_tDBClose_1 process_data_begin ] start
				 */

				currentComponent = "j_end_exec_1_tDBClose_1";

				/**
				 * [j_end_exec_1_tDBClose_1 process_data_begin ] stop
				 */

				/**
				 * [j_end_exec_1_tDBClose_1 process_data_end ] start
				 */

				currentComponent = "j_end_exec_1_tDBClose_1";

				/**
				 * [j_end_exec_1_tDBClose_1 process_data_end ] stop
				 */

				/**
				 * [j_end_exec_1_tDBClose_1 end ] start
				 */

				currentComponent = "j_end_exec_1_tDBClose_1";

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBClose_1 - " + ("Done."));

				ok_Hash.put("j_end_exec_1_tDBClose_1", true);
				end_Hash.put("j_end_exec_1_tDBClose_1", System.currentTimeMillis());

				if (execStat) {
					runStat.updateStatOnConnection("j_end_exec_1_OnComponentOk5", 0, "ok");
				}
				j_end_exec_1_tJava_2Process(globalMap);

				/**
				 * [j_end_exec_1_tDBClose_1 end ] stop
				 */
			} // end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil.addLog("CHECKPOINT",
						"CONNECTION:SUBJOB_OK:j_end_exec_1_tDBClose_1:OnSubjobOk (TRIGGER_OUTPUT_1)", "",
						Thread.currentThread().getId() + "", "", "", "", "", "");
			}

			if (execStat) {
				runStat.updateStatOnConnection("OnSubjobOk4", 0, "ok");
			}

			tDBClose_1Process(globalMap);

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_end_exec_1_tDBClose_1 finally ] start
				 */

				currentComponent = "j_end_exec_1_tDBClose_1";

				/**
				 * [j_end_exec_1_tDBClose_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_end_exec_1_tDBClose_1_SUBPROCESS_STATE", 1);
	}

	public void tDBClose_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tDBClose_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tDBClose_1 begin ] start
				 */

				ok_Hash.put("tDBClose_1", false);
				start_Hash.put("tDBClose_1", System.currentTimeMillis());

				currentComponent = "tDBClose_1";

				int tos_count_tDBClose_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tDBClose_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tDBClose_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tDBClose_1 = new StringBuilder();
							log4jParamters_tDBClose_1.append("Parameters:");
							log4jParamters_tDBClose_1.append("CONNECTION" + " = " + "tDBConnection_1");
							log4jParamters_tDBClose_1.append(" | ");
							log4jParamters_tDBClose_1.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlClose");
							log4jParamters_tDBClose_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tDBClose_1 - " + (log4jParamters_tDBClose_1));
						}
					}
					new BytesLimit65535_tDBClose_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tDBClose_1", "__UNIQUE_NAME__<br><b>BDD_OMNICANAL</b>", "tPostgresqlClose");
					talendJobLogProcess(globalMap);
				}

				/**
				 * [tDBClose_1 begin ] stop
				 */

				/**
				 * [tDBClose_1 main ] start
				 */

				currentComponent = "tDBClose_1";

				java.sql.Connection conn_tDBClose_1 = (java.sql.Connection) globalMap.get("conn_tDBConnection_1");
				if (conn_tDBClose_1 != null && !conn_tDBClose_1.isClosed()) {
					if (log.isDebugEnabled())
						log.debug("tDBClose_1 - " + ("Closing the connection ") + ("conn_tDBConnection_1")
								+ (" to the database."));
					conn_tDBClose_1.close();
					if (log.isDebugEnabled())
						log.debug("tDBClose_1 - " + ("Connection ") + ("conn_tDBConnection_1")
								+ (" to the database has closed."));
				}

				tos_count_tDBClose_1++;

				/**
				 * [tDBClose_1 main ] stop
				 */

				/**
				 * [tDBClose_1 process_data_begin ] start
				 */

				currentComponent = "tDBClose_1";

				/**
				 * [tDBClose_1 process_data_begin ] stop
				 */

				/**
				 * [tDBClose_1 process_data_end ] start
				 */

				currentComponent = "tDBClose_1";

				/**
				 * [tDBClose_1 process_data_end ] stop
				 */

				/**
				 * [tDBClose_1 end ] start
				 */

				currentComponent = "tDBClose_1";

				if (log.isDebugEnabled())
					log.debug("tDBClose_1 - " + ("Done."));

				ok_Hash.put("tDBClose_1", true);
				end_Hash.put("tDBClose_1", System.currentTimeMillis());

				if (execStat) {
					runStat.updateStatOnConnection("OnComponentOk4", 0, "ok");
				}
				tFTPClose_1Process(globalMap);

				/**
				 * [tDBClose_1 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBClose_1 finally ] start
				 */

				currentComponent = "tDBClose_1";

				/**
				 * [tDBClose_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBClose_1_SUBPROCESS_STATE", 1);
	}

	public void tFTPClose_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tFTPClose_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tFTPClose_1 begin ] start
				 */

				ok_Hash.put("tFTPClose_1", false);
				start_Hash.put("tFTPClose_1", System.currentTimeMillis());

				currentComponent = "tFTPClose_1";

				int tos_count_tFTPClose_1 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("tFTPClose_1", "__UNIQUE_NAME__<br><b>GCE</b>", "tFTPClose");
					talendJobLogProcess(globalMap);
				}

				Object connObj = globalMap.get("conn_tFTPConnection_1");
				if (connObj != null) {
					try {

						org.apache.commons.net.ftp.FTPClient conn = (org.apache.commons.net.ftp.FTPClient) connObj;
						conn.logout();
						conn.disconnect();

					} catch (Exception e) {
						globalMap.put("tFTPClose_1_ERROR_MESSAGE", e.getMessage());
						throw e;
					}
				}

				/**
				 * [tFTPClose_1 begin ] stop
				 */

				/**
				 * [tFTPClose_1 main ] start
				 */

				currentComponent = "tFTPClose_1";

				tos_count_tFTPClose_1++;

				/**
				 * [tFTPClose_1 main ] stop
				 */

				/**
				 * [tFTPClose_1 process_data_begin ] start
				 */

				currentComponent = "tFTPClose_1";

				/**
				 * [tFTPClose_1 process_data_begin ] stop
				 */

				/**
				 * [tFTPClose_1 process_data_end ] start
				 */

				currentComponent = "tFTPClose_1";

				/**
				 * [tFTPClose_1 process_data_end ] stop
				 */

				/**
				 * [tFTPClose_1 end ] start
				 */

				currentComponent = "tFTPClose_1";

				ok_Hash.put("tFTPClose_1", true);
				end_Hash.put("tFTPClose_1", System.currentTimeMillis());

				if (execStat) {
					runStat.updateStatOnConnection("OnComponentOk6", 0, "ok");
				}
				tFTPClose_2Process(globalMap);

				/**
				 * [tFTPClose_1 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tFTPClose_1 finally ] start
				 */

				currentComponent = "tFTPClose_1";

				Object connObj = globalMap.get("conn_tFTPConnection_1");
				if (connObj != null) {
					org.apache.commons.net.ftp.FTPClient conn = (org.apache.commons.net.ftp.FTPClient) connObj;
					conn.disconnect();

				}

				/**
				 * [tFTPClose_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tFTPClose_1_SUBPROCESS_STATE", 1);
	}

	public void tFTPClose_2Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tFTPClose_2_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tFTPClose_2 begin ] start
				 */

				ok_Hash.put("tFTPClose_2", false);
				start_Hash.put("tFTPClose_2", System.currentTimeMillis());

				currentComponent = "tFTPClose_2";

				int tos_count_tFTPClose_2 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("tFTPClose_2", "__UNIQUE_NAME__<br><b>ONESTOCK</b>", "tFTPClose");
					talendJobLogProcess(globalMap);
				}

				Object connObj = globalMap.get("conn_tFTPConnection_2");
				if (connObj != null) {
					try {

						org.apache.commons.net.ftp.FTPClient conn = (org.apache.commons.net.ftp.FTPClient) connObj;
						conn.logout();
						conn.disconnect();

					} catch (Exception e) {
						globalMap.put("tFTPClose_2_ERROR_MESSAGE", e.getMessage());
						throw e;
					}
				}

				/**
				 * [tFTPClose_2 begin ] stop
				 */

				/**
				 * [tFTPClose_2 main ] start
				 */

				currentComponent = "tFTPClose_2";

				tos_count_tFTPClose_2++;

				/**
				 * [tFTPClose_2 main ] stop
				 */

				/**
				 * [tFTPClose_2 process_data_begin ] start
				 */

				currentComponent = "tFTPClose_2";

				/**
				 * [tFTPClose_2 process_data_begin ] stop
				 */

				/**
				 * [tFTPClose_2 process_data_end ] start
				 */

				currentComponent = "tFTPClose_2";

				/**
				 * [tFTPClose_2 process_data_end ] stop
				 */

				/**
				 * [tFTPClose_2 end ] start
				 */

				currentComponent = "tFTPClose_2";

				ok_Hash.put("tFTPClose_2", true);
				end_Hash.put("tFTPClose_2", System.currentTimeMillis());

				/**
				 * [tFTPClose_2 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tFTPClose_2 finally ] start
				 */

				currentComponent = "tFTPClose_2";

				Object connObj = globalMap.get("conn_tFTPConnection_2");
				if (connObj != null) {
					org.apache.commons.net.ftp.FTPClient conn = (org.apache.commons.net.ftp.FTPClient) connObj;
					conn.disconnect();

				}

				/**
				 * [tFTPClose_2 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tFTPClose_2_SUBPROCESS_STATE", 1);
	}

	public void j_end_exec_1_tJava_2Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("j_end_exec_1_tJava_2_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [j_end_exec_1_tJava_2 begin ] start
				 */

				ok_Hash.put("j_end_exec_1_tJava_2", false);
				start_Hash.put("j_end_exec_1_tJava_2", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tJava_2";

				int tos_count_j_end_exec_1_tJava_2 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tJava_2", "j_end_exec_1_tJava_2", "tJava");
					talendJobLogProcess(globalMap);
				}

				System.out.println("END_EXEC => END");

				/**
				 * [j_end_exec_1_tJava_2 begin ] stop
				 */

				/**
				 * [j_end_exec_1_tJava_2 main ] start
				 */

				currentComponent = "j_end_exec_1_tJava_2";

				tos_count_j_end_exec_1_tJava_2++;

				/**
				 * [j_end_exec_1_tJava_2 main ] stop
				 */

				/**
				 * [j_end_exec_1_tJava_2 process_data_begin ] start
				 */

				currentComponent = "j_end_exec_1_tJava_2";

				/**
				 * [j_end_exec_1_tJava_2 process_data_begin ] stop
				 */

				/**
				 * [j_end_exec_1_tJava_2 process_data_end ] start
				 */

				currentComponent = "j_end_exec_1_tJava_2";

				/**
				 * [j_end_exec_1_tJava_2 process_data_end ] stop
				 */

				/**
				 * [j_end_exec_1_tJava_2 end ] start
				 */

				currentComponent = "j_end_exec_1_tJava_2";

				ok_Hash.put("j_end_exec_1_tJava_2", true);
				end_Hash.put("j_end_exec_1_tJava_2", System.currentTimeMillis());

				/**
				 * [j_end_exec_1_tJava_2 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_end_exec_1_tJava_2 finally ] start
				 */

				currentComponent = "j_end_exec_1_tJava_2";

				/**
				 * [j_end_exec_1_tJava_2 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_end_exec_1_tJava_2_SUBPROCESS_STATE", 1);
	}

	public static class j_end_exec_1_toUpdateErrorStruct
			implements routines.system.IPersistableRow<j_end_exec_1_toUpdateErrorStruct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public Integer id;

		public Integer getId() {
			return this.id;
		}

		public String last_execution_end_date;

		public String getLast_execution_end_date() {
			return this.last_execution_end_date;
		}

		public String last_execution_result;

		public String getLast_execution_result() {
			return this.last_execution_result;
		}

		public Boolean is_running;

		public Boolean getIs_running() {
			return this.is_running;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final j_end_exec_1_toUpdateErrorStruct other = (j_end_exec_1_toUpdateErrorStruct) obj;

			if (this.id == null) {
				if (other.id != null)
					return false;

			} else if (!this.id.equals(other.id))

				return false;

			return true;
		}

		public void copyDataTo(j_end_exec_1_toUpdateErrorStruct other) {

			other.id = this.id;
			other.last_execution_end_date = this.last_execution_end_date;
			other.last_execution_result = this.last_execution_result;
			other.is_running = this.is_running;

		}

		public void copyKeysDataTo(j_end_exec_1_toUpdateErrorStruct other) {

			other.id = this.id;

		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

					this.last_execution_end_date = readString(dis);

					this.last_execution_result = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.is_running = null;
					} else {
						this.is_running = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

					this.last_execution_end_date = readString(dis);

					this.last_execution_result = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.is_running = null;
					} else {
						this.is_running = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

				// String

				writeString(this.last_execution_end_date, dos);

				// String

				writeString(this.last_execution_result, dos);

				// Boolean

				if (this.is_running == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.is_running);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

				// String

				writeString(this.last_execution_end_date, dos);

				// String

				writeString(this.last_execution_result, dos);

				// Boolean

				if (this.is_running == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.is_running);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("id=" + String.valueOf(id));
			sb.append(",last_execution_end_date=" + last_execution_end_date);
			sb.append(",last_execution_result=" + last_execution_result);
			sb.append(",is_running=" + String.valueOf(is_running));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (id == null) {
				sb.append("<null>");
			} else {
				sb.append(id);
			}

			sb.append("|");

			if (last_execution_end_date == null) {
				sb.append("<null>");
			} else {
				sb.append(last_execution_end_date);
			}

			sb.append("|");

			if (last_execution_result == null) {
				sb.append("<null>");
			} else {
				sb.append(last_execution_result);
			}

			sb.append("|");

			if (is_running == null) {
				sb.append("<null>");
			} else {
				sb.append(is_running);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(j_end_exec_1_toUpdateErrorStruct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.id, other.id);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class j_end_exec_1_row8Struct implements routines.system.IPersistableRow<j_end_exec_1_row8Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public java.util.Date moment;

		public java.util.Date getMoment() {
			return this.moment;
		}

		public String pid;

		public String getPid() {
			return this.pid;
		}

		public String root_pid;

		public String getRoot_pid() {
			return this.root_pid;
		}

		public String father_pid;

		public String getFather_pid() {
			return this.father_pid;
		}

		public String project;

		public String getProject() {
			return this.project;
		}

		public String job;

		public String getJob() {
			return this.job;
		}

		public String context;

		public String getContext() {
			return this.context;
		}

		public Integer priority;

		public Integer getPriority() {
			return this.priority;
		}

		public String type;

		public String getType() {
			return this.type;
		}

		public String origin;

		public String getOrigin() {
			return this.origin;
		}

		public String message;

		public String getMessage() {
			return this.message;
		}

		public Integer code;

		public Integer getCode() {
			return this.code;
		}

		private java.util.Date readDate(ObjectInputStream dis) throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = unmarshaller.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(unmarshaller.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (date1 == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeLong(date1.getTime());
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.moment = readDate(dis);

					this.pid = readString(dis);

					this.root_pid = readString(dis);

					this.father_pid = readString(dis);

					this.project = readString(dis);

					this.job = readString(dis);

					this.context = readString(dis);

					this.priority = readInteger(dis);

					this.type = readString(dis);

					this.origin = readString(dis);

					this.message = readString(dis);

					this.code = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.moment = readDate(dis);

					this.pid = readString(dis);

					this.root_pid = readString(dis);

					this.father_pid = readString(dis);

					this.project = readString(dis);

					this.job = readString(dis);

					this.context = readString(dis);

					this.priority = readInteger(dis);

					this.type = readString(dis);

					this.origin = readString(dis);

					this.message = readString(dis);

					this.code = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// java.util.Date

				writeDate(this.moment, dos);

				// String

				writeString(this.pid, dos);

				// String

				writeString(this.root_pid, dos);

				// String

				writeString(this.father_pid, dos);

				// String

				writeString(this.project, dos);

				// String

				writeString(this.job, dos);

				// String

				writeString(this.context, dos);

				// Integer

				writeInteger(this.priority, dos);

				// String

				writeString(this.type, dos);

				// String

				writeString(this.origin, dos);

				// String

				writeString(this.message, dos);

				// Integer

				writeInteger(this.code, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// java.util.Date

				writeDate(this.moment, dos);

				// String

				writeString(this.pid, dos);

				// String

				writeString(this.root_pid, dos);

				// String

				writeString(this.father_pid, dos);

				// String

				writeString(this.project, dos);

				// String

				writeString(this.job, dos);

				// String

				writeString(this.context, dos);

				// Integer

				writeInteger(this.priority, dos);

				// String

				writeString(this.type, dos);

				// String

				writeString(this.origin, dos);

				// String

				writeString(this.message, dos);

				// Integer

				writeInteger(this.code, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("moment=" + String.valueOf(moment));
			sb.append(",pid=" + pid);
			sb.append(",root_pid=" + root_pid);
			sb.append(",father_pid=" + father_pid);
			sb.append(",project=" + project);
			sb.append(",job=" + job);
			sb.append(",context=" + context);
			sb.append(",priority=" + String.valueOf(priority));
			sb.append(",type=" + type);
			sb.append(",origin=" + origin);
			sb.append(",message=" + message);
			sb.append(",code=" + String.valueOf(code));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (moment == null) {
				sb.append("<null>");
			} else {
				sb.append(moment);
			}

			sb.append("|");

			if (pid == null) {
				sb.append("<null>");
			} else {
				sb.append(pid);
			}

			sb.append("|");

			if (root_pid == null) {
				sb.append("<null>");
			} else {
				sb.append(root_pid);
			}

			sb.append("|");

			if (father_pid == null) {
				sb.append("<null>");
			} else {
				sb.append(father_pid);
			}

			sb.append("|");

			if (project == null) {
				sb.append("<null>");
			} else {
				sb.append(project);
			}

			sb.append("|");

			if (job == null) {
				sb.append("<null>");
			} else {
				sb.append(job);
			}

			sb.append("|");

			if (context == null) {
				sb.append("<null>");
			} else {
				sb.append(context);
			}

			sb.append("|");

			if (priority == null) {
				sb.append("<null>");
			} else {
				sb.append(priority);
			}

			sb.append("|");

			if (type == null) {
				sb.append("<null>");
			} else {
				sb.append(type);
			}

			sb.append("|");

			if (origin == null) {
				sb.append("<null>");
			} else {
				sb.append(origin);
			}

			sb.append("|");

			if (message == null) {
				sb.append("<null>");
			} else {
				sb.append(message);
			}

			sb.append("|");

			if (code == null) {
				sb.append("<null>");
			} else {
				sb.append(code);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(j_end_exec_1_row8Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class after_j_end_exec_1_tLogCatcher_1Struct
			implements routines.system.IPersistableRow<after_j_end_exec_1_tLogCatcher_1Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public java.util.Date moment;

		public java.util.Date getMoment() {
			return this.moment;
		}

		public String pid;

		public String getPid() {
			return this.pid;
		}

		public String root_pid;

		public String getRoot_pid() {
			return this.root_pid;
		}

		public String father_pid;

		public String getFather_pid() {
			return this.father_pid;
		}

		public String project;

		public String getProject() {
			return this.project;
		}

		public String job;

		public String getJob() {
			return this.job;
		}

		public String context;

		public String getContext() {
			return this.context;
		}

		public Integer priority;

		public Integer getPriority() {
			return this.priority;
		}

		public String type;

		public String getType() {
			return this.type;
		}

		public String origin;

		public String getOrigin() {
			return this.origin;
		}

		public String message;

		public String getMessage() {
			return this.message;
		}

		public Integer code;

		public Integer getCode() {
			return this.code;
		}

		private java.util.Date readDate(ObjectInputStream dis) throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = unmarshaller.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(unmarshaller.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (date1 == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeLong(date1.getTime());
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.moment = readDate(dis);

					this.pid = readString(dis);

					this.root_pid = readString(dis);

					this.father_pid = readString(dis);

					this.project = readString(dis);

					this.job = readString(dis);

					this.context = readString(dis);

					this.priority = readInteger(dis);

					this.type = readString(dis);

					this.origin = readString(dis);

					this.message = readString(dis);

					this.code = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.moment = readDate(dis);

					this.pid = readString(dis);

					this.root_pid = readString(dis);

					this.father_pid = readString(dis);

					this.project = readString(dis);

					this.job = readString(dis);

					this.context = readString(dis);

					this.priority = readInteger(dis);

					this.type = readString(dis);

					this.origin = readString(dis);

					this.message = readString(dis);

					this.code = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// java.util.Date

				writeDate(this.moment, dos);

				// String

				writeString(this.pid, dos);

				// String

				writeString(this.root_pid, dos);

				// String

				writeString(this.father_pid, dos);

				// String

				writeString(this.project, dos);

				// String

				writeString(this.job, dos);

				// String

				writeString(this.context, dos);

				// Integer

				writeInteger(this.priority, dos);

				// String

				writeString(this.type, dos);

				// String

				writeString(this.origin, dos);

				// String

				writeString(this.message, dos);

				// Integer

				writeInteger(this.code, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// java.util.Date

				writeDate(this.moment, dos);

				// String

				writeString(this.pid, dos);

				// String

				writeString(this.root_pid, dos);

				// String

				writeString(this.father_pid, dos);

				// String

				writeString(this.project, dos);

				// String

				writeString(this.job, dos);

				// String

				writeString(this.context, dos);

				// Integer

				writeInteger(this.priority, dos);

				// String

				writeString(this.type, dos);

				// String

				writeString(this.origin, dos);

				// String

				writeString(this.message, dos);

				// Integer

				writeInteger(this.code, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("moment=" + String.valueOf(moment));
			sb.append(",pid=" + pid);
			sb.append(",root_pid=" + root_pid);
			sb.append(",father_pid=" + father_pid);
			sb.append(",project=" + project);
			sb.append(",job=" + job);
			sb.append(",context=" + context);
			sb.append(",priority=" + String.valueOf(priority));
			sb.append(",type=" + type);
			sb.append(",origin=" + origin);
			sb.append(",message=" + message);
			sb.append(",code=" + String.valueOf(code));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (moment == null) {
				sb.append("<null>");
			} else {
				sb.append(moment);
			}

			sb.append("|");

			if (pid == null) {
				sb.append("<null>");
			} else {
				sb.append(pid);
			}

			sb.append("|");

			if (root_pid == null) {
				sb.append("<null>");
			} else {
				sb.append(root_pid);
			}

			sb.append("|");

			if (father_pid == null) {
				sb.append("<null>");
			} else {
				sb.append(father_pid);
			}

			sb.append("|");

			if (project == null) {
				sb.append("<null>");
			} else {
				sb.append(project);
			}

			sb.append("|");

			if (job == null) {
				sb.append("<null>");
			} else {
				sb.append(job);
			}

			sb.append("|");

			if (context == null) {
				sb.append("<null>");
			} else {
				sb.append(context);
			}

			sb.append("|");

			if (priority == null) {
				sb.append("<null>");
			} else {
				sb.append(priority);
			}

			sb.append("|");

			if (type == null) {
				sb.append("<null>");
			} else {
				sb.append(type);
			}

			sb.append("|");

			if (origin == null) {
				sb.append("<null>");
			} else {
				sb.append(origin);
			}

			sb.append("|");

			if (message == null) {
				sb.append("<null>");
			} else {
				sb.append(message);
			}

			sb.append("|");

			if (code == null) {
				sb.append("<null>");
			} else {
				sb.append(code);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(after_j_end_exec_1_tLogCatcher_1Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void j_end_exec_1_tLogCatcher_1Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("j_end_exec_1_tLogCatcher_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				j_end_exec_1_tDBInput_4Process(globalMap);

				j_end_exec_1_row8Struct j_end_exec_1_row8 = new j_end_exec_1_row8Struct();
				j_end_exec_1_toUpdateErrorStruct j_end_exec_1_toUpdateError = new j_end_exec_1_toUpdateErrorStruct();

				/**
				 * [j_end_exec_1_tDBOutput_2 begin ] start
				 */

				ok_Hash.put("j_end_exec_1_tDBOutput_2", false);
				start_Hash.put("j_end_exec_1_tDBOutput_2", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tDBOutput_2";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0,
						"j_end_exec_1_toUpdateError");

				int tos_count_j_end_exec_1_tDBOutput_2 = 0;

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBOutput_2 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_end_exec_1_tDBOutput_2 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_end_exec_1_tDBOutput_2 = new StringBuilder();
							log4jParamters_j_end_exec_1_tDBOutput_2.append("Parameters:");
							log4jParamters_j_end_exec_1_tDBOutput_2.append("USE_EXISTING_CONNECTION" + " = " + "true");
							log4jParamters_j_end_exec_1_tDBOutput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_2
									.append("CONNECTION" + " = " + "j_end_exec_1_tDBConnection_1");
							log4jParamters_j_end_exec_1_tDBOutput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_2.append("TABLE" + " = " + "\"locks\"");
							log4jParamters_j_end_exec_1_tDBOutput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_2.append("TABLE_ACTION" + " = " + "NONE");
							log4jParamters_j_end_exec_1_tDBOutput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_2.append("DATA_ACTION" + " = " + "UPDATE");
							log4jParamters_j_end_exec_1_tDBOutput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_2.append("DIE_ON_ERROR" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBOutput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_2.append("USE_ALTERNATE_SCHEMA" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBOutput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_2.append("ADD_COLS" + " = " + "[]");
							log4jParamters_j_end_exec_1_tDBOutput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_2.append("USE_FIELD_OPTIONS" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBOutput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_2.append("ENABLE_DEBUG_MODE" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBOutput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_2.append("SUPPORT_NULL_WHERE" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBOutput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_2
									.append("CONVERT_COLUMN_TABLE_TO_LOWERCASE" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBOutput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_2.append("USE_BATCH_SIZE" + " = " + "true");
							log4jParamters_j_end_exec_1_tDBOutput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_2.append("BATCH_SIZE" + " = " + "10000");
							log4jParamters_j_end_exec_1_tDBOutput_2.append(" | ");
							log4jParamters_j_end_exec_1_tDBOutput_2
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlOutput");
							log4jParamters_j_end_exec_1_tDBOutput_2.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_end_exec_1_tDBOutput_2 - " + (log4jParamters_j_end_exec_1_tDBOutput_2));
						}
					}
					new BytesLimit65535_j_end_exec_1_tDBOutput_2().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tDBOutput_2",
							"__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>", "tPostgresqlOutput");
					talendJobLogProcess(globalMap);
				}

				String dbschema_j_end_exec_1_tDBOutput_2 = null;
				dbschema_j_end_exec_1_tDBOutput_2 = (String) globalMap.get("schema_" + "j_end_exec_1_tDBConnection_1");

				String tableName_j_end_exec_1_tDBOutput_2 = null;
				if (dbschema_j_end_exec_1_tDBOutput_2 == null
						|| dbschema_j_end_exec_1_tDBOutput_2.trim().length() == 0) {
					tableName_j_end_exec_1_tDBOutput_2 = ("locks");
				} else {
					tableName_j_end_exec_1_tDBOutput_2 = dbschema_j_end_exec_1_tDBOutput_2 + "\".\"" + ("locks");
				}

				int updateKeyCount_j_end_exec_1_tDBOutput_2 = 1;
				if (updateKeyCount_j_end_exec_1_tDBOutput_2 < 1) {
					throw new RuntimeException("For update, Schema must have a key");
				} else if (updateKeyCount_j_end_exec_1_tDBOutput_2 == 4 && true) {
					throw new RuntimeException("For update, every Schema column can not be a key");
				}

				int nb_line_j_end_exec_1_tDBOutput_2 = 0;
				int nb_line_update_j_end_exec_1_tDBOutput_2 = 0;
				int nb_line_inserted_j_end_exec_1_tDBOutput_2 = 0;
				int nb_line_deleted_j_end_exec_1_tDBOutput_2 = 0;
				int nb_line_rejected_j_end_exec_1_tDBOutput_2 = 0;

				int deletedCount_j_end_exec_1_tDBOutput_2 = 0;
				int updatedCount_j_end_exec_1_tDBOutput_2 = 0;
				int insertedCount_j_end_exec_1_tDBOutput_2 = 0;
				int rowsToCommitCount_j_end_exec_1_tDBOutput_2 = 0;
				int rejectedCount_j_end_exec_1_tDBOutput_2 = 0;

				boolean whetherReject_j_end_exec_1_tDBOutput_2 = false;

				java.sql.Connection conn_j_end_exec_1_tDBOutput_2 = null;
				String dbUser_j_end_exec_1_tDBOutput_2 = null;

				conn_j_end_exec_1_tDBOutput_2 = (java.sql.Connection) globalMap
						.get("conn_j_end_exec_1_tDBConnection_1");

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBOutput_2 - " + ("Uses an existing connection with username '")
							+ (conn_j_end_exec_1_tDBOutput_2.getMetaData().getUserName()) + ("'. Connection URL: ")
							+ (conn_j_end_exec_1_tDBOutput_2.getMetaData().getURL()) + ("."));

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBOutput_2 - " + ("Connection is set auto commit to '")
							+ (conn_j_end_exec_1_tDBOutput_2.getAutoCommit()) + ("'."));

				int batchSize_j_end_exec_1_tDBOutput_2 = 10000;
				int batchSizeCounter_j_end_exec_1_tDBOutput_2 = 0;

				int count_j_end_exec_1_tDBOutput_2 = 0;
				String update_j_end_exec_1_tDBOutput_2 = "UPDATE \"" + tableName_j_end_exec_1_tDBOutput_2
						+ "\" SET \"last_execution_end_date\" = ?,\"last_execution_result\" = ?,\"is_running\" = ? WHERE \"id\" = ?";
				java.sql.PreparedStatement pstmt_j_end_exec_1_tDBOutput_2 = conn_j_end_exec_1_tDBOutput_2
						.prepareStatement(update_j_end_exec_1_tDBOutput_2);
				resourceMap.put("pstmt_j_end_exec_1_tDBOutput_2", pstmt_j_end_exec_1_tDBOutput_2);

				/**
				 * [j_end_exec_1_tDBOutput_2 begin ] stop
				 */

				/**
				 * [j_end_exec_1_tMap_2 begin ] start
				 */

				ok_Hash.put("j_end_exec_1_tMap_2", false);
				start_Hash.put("j_end_exec_1_tMap_2", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tMap_2";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "j_end_exec_1_row8");

				int tos_count_j_end_exec_1_tMap_2 = 0;

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tMap_2 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_end_exec_1_tMap_2 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_end_exec_1_tMap_2 = new StringBuilder();
							log4jParamters_j_end_exec_1_tMap_2.append("Parameters:");
							log4jParamters_j_end_exec_1_tMap_2.append("LINK_STYLE" + " = " + "AUTO");
							log4jParamters_j_end_exec_1_tMap_2.append(" | ");
							log4jParamters_j_end_exec_1_tMap_2.append("TEMPORARY_DATA_DIRECTORY" + " = " + "");
							log4jParamters_j_end_exec_1_tMap_2.append(" | ");
							log4jParamters_j_end_exec_1_tMap_2.append("ROWS_BUFFER_SIZE" + " = " + "2000000");
							log4jParamters_j_end_exec_1_tMap_2.append(" | ");
							log4jParamters_j_end_exec_1_tMap_2
									.append("CHANGE_HASH_AND_EQUALS_FOR_BIGDECIMAL" + " = " + "true");
							log4jParamters_j_end_exec_1_tMap_2.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_end_exec_1_tMap_2 - " + (log4jParamters_j_end_exec_1_tMap_2));
						}
					}
					new BytesLimit65535_j_end_exec_1_tMap_2().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tMap_2", "j_end_exec_1_tMap_2", "tMap");
					talendJobLogProcess(globalMap);
				}

// ###############################
// # Lookup's keys initialization
				int count_j_end_exec_1_row8_j_end_exec_1_tMap_2 = 0;

				int count_j_end_exec_1_row6_j_end_exec_1_tMap_2 = 0;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<j_end_exec_1_row6Struct> tHash_Lookup_j_end_exec_1_row6 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<j_end_exec_1_row6Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<j_end_exec_1_row6Struct>) globalMap
						.get("tHash_Lookup_j_end_exec_1_row6"));

				tHash_Lookup_j_end_exec_1_row6.initGet();

				j_end_exec_1_row6Struct j_end_exec_1_row6HashKey = new j_end_exec_1_row6Struct();
				j_end_exec_1_row6Struct j_end_exec_1_row6Default = new j_end_exec_1_row6Struct();
// ###############################        

// ###############################
// # Vars initialization
				class Var__j_end_exec_1_tMap_2__Struct {
					String result;
				}
				Var__j_end_exec_1_tMap_2__Struct Var__j_end_exec_1_tMap_2 = new Var__j_end_exec_1_tMap_2__Struct();
// ###############################

// ###############################
// # Outputs initialization
				int count_j_end_exec_1_toUpdateError_j_end_exec_1_tMap_2 = 0;

				j_end_exec_1_toUpdateErrorStruct j_end_exec_1_toUpdateError_tmp = new j_end_exec_1_toUpdateErrorStruct();
// ###############################

				/**
				 * [j_end_exec_1_tMap_2 begin ] stop
				 */

				/**
				 * [j_end_exec_1_tLogCatcher_1 begin ] start
				 */

				ok_Hash.put("j_end_exec_1_tLogCatcher_1", false);
				start_Hash.put("j_end_exec_1_tLogCatcher_1", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tLogCatcher_1";

				int tos_count_j_end_exec_1_tLogCatcher_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tLogCatcher_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_end_exec_1_tLogCatcher_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_end_exec_1_tLogCatcher_1 = new StringBuilder();
							log4jParamters_j_end_exec_1_tLogCatcher_1.append("Parameters:");
							log4jParamters_j_end_exec_1_tLogCatcher_1.append("CATCH_JAVA_EXCEPTION" + " = " + "true");
							log4jParamters_j_end_exec_1_tLogCatcher_1.append(" | ");
							log4jParamters_j_end_exec_1_tLogCatcher_1.append("CATCH_TDIE" + " = " + "true");
							log4jParamters_j_end_exec_1_tLogCatcher_1.append(" | ");
							log4jParamters_j_end_exec_1_tLogCatcher_1.append("CATCH_TWARN" + " = " + "true");
							log4jParamters_j_end_exec_1_tLogCatcher_1.append(" | ");
							log4jParamters_j_end_exec_1_tLogCatcher_1.append("CATCH_TACTIONFAILURE" + " = " + "true");
							log4jParamters_j_end_exec_1_tLogCatcher_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug(
										"j_end_exec_1_tLogCatcher_1 - " + (log4jParamters_j_end_exec_1_tLogCatcher_1));
						}
					}
					new BytesLimit65535_j_end_exec_1_tLogCatcher_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tLogCatcher_1", "j_end_exec_1_tLogCatcher_1", "tLogCatcher");
					talendJobLogProcess(globalMap);
				}

				try {
					for (LogCatcherUtils.LogCatcherMessage lcm : j_end_exec_1_tLogCatcher_1.getMessages()) {
						j_end_exec_1_row8.type = lcm.getType();
						j_end_exec_1_row8.origin = (lcm.getOrigin() == null || lcm.getOrigin().length() < 1 ? null
								: lcm.getOrigin());
						j_end_exec_1_row8.priority = lcm.getPriority();
						j_end_exec_1_row8.message = lcm.getMessage();
						j_end_exec_1_row8.code = lcm.getCode();

						j_end_exec_1_row8.moment = java.util.Calendar.getInstance().getTime();

						j_end_exec_1_row8.pid = pid;
						j_end_exec_1_row8.root_pid = rootPid;
						j_end_exec_1_row8.father_pid = fatherPid;

						j_end_exec_1_row8.project = projectName;
						j_end_exec_1_row8.job = jobName;
						j_end_exec_1_row8.context = contextStr;

						/**
						 * [j_end_exec_1_tLogCatcher_1 begin ] stop
						 */

						/**
						 * [j_end_exec_1_tLogCatcher_1 main ] start
						 */

						currentComponent = "j_end_exec_1_tLogCatcher_1";

						tos_count_j_end_exec_1_tLogCatcher_1++;

						/**
						 * [j_end_exec_1_tLogCatcher_1 main ] stop
						 */

						/**
						 * [j_end_exec_1_tLogCatcher_1 process_data_begin ] start
						 */

						currentComponent = "j_end_exec_1_tLogCatcher_1";

						/**
						 * [j_end_exec_1_tLogCatcher_1 process_data_begin ] stop
						 */

						/**
						 * [j_end_exec_1_tMap_2 main ] start
						 */

						currentComponent = "j_end_exec_1_tMap_2";

						if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

								, "j_end_exec_1_row8", "j_end_exec_1_tLogCatcher_1", "j_end_exec_1_tLogCatcher_1",
								"tLogCatcher", "j_end_exec_1_tMap_2", "j_end_exec_1_tMap_2", "tMap"

						)) {
							talendJobLogProcess(globalMap);
						}

						if (log.isTraceEnabled()) {
							log.trace("j_end_exec_1_row8 - "
									+ (j_end_exec_1_row8 == null ? "" : j_end_exec_1_row8.toLogString()));
						}

						boolean hasCasePrimitiveKeyWithNull_j_end_exec_1_tMap_2 = false;

						// ###############################
						// # Input tables (lookups)
						boolean rejectedInnerJoin_j_end_exec_1_tMap_2 = false;
						boolean mainRowRejected_j_end_exec_1_tMap_2 = false;

						///////////////////////////////////////////////
						// Starting Lookup Table "j_end_exec_1_row6"
						///////////////////////////////////////////////

						boolean forceLoopj_end_exec_1_row6 = false;

						j_end_exec_1_row6Struct j_end_exec_1_row6ObjectFromLookup = null;

						if (!rejectedInnerJoin_j_end_exec_1_tMap_2) { // G_TM_M_020

							tHash_Lookup_j_end_exec_1_row6.lookup(j_end_exec_1_row6HashKey);

							if (!tHash_Lookup_j_end_exec_1_row6.hasNext()) { // G_TM_M_090

								forceLoopj_end_exec_1_row6 = true;

							} // G_TM_M_090

						} // G_TM_M_020

						else { // G 20 - G 21
							forceLoopj_end_exec_1_row6 = true;
						} // G 21

						j_end_exec_1_row6Struct j_end_exec_1_row6 = null;

						while ((tHash_Lookup_j_end_exec_1_row6 != null && tHash_Lookup_j_end_exec_1_row6.hasNext())
								|| forceLoopj_end_exec_1_row6) { // G_TM_M_043

							// CALL close loop of lookup 'j_end_exec_1_row6'

							j_end_exec_1_row6Struct fromLookup_j_end_exec_1_row6 = null;
							j_end_exec_1_row6 = j_end_exec_1_row6Default;

							if (!forceLoopj_end_exec_1_row6) { // G 46

								fromLookup_j_end_exec_1_row6 = tHash_Lookup_j_end_exec_1_row6.next();

								if (fromLookup_j_end_exec_1_row6 != null) {
									j_end_exec_1_row6 = fromLookup_j_end_exec_1_row6;
								}

							} // G 46

							forceLoopj_end_exec_1_row6 = false;

							// ###############################
							{ // start of Var scope

								// ###############################
								// # Vars tables

								Var__j_end_exec_1_tMap_2__Struct Var = Var__j_end_exec_1_tMap_2;
								Var.result = "The job ended with error in the joblet J_END_EXEC ("
										+ j_end_exec_1_row8.origin + ") : " + j_end_exec_1_row8.message;// ###############################
								// ###############################
								// # Output tables

								j_end_exec_1_toUpdateError = null;

// # Output table : 'j_end_exec_1_toUpdateError'
								count_j_end_exec_1_toUpdateError_j_end_exec_1_tMap_2++;

								j_end_exec_1_toUpdateError_tmp.id = j_end_exec_1_row6.id;
								j_end_exec_1_toUpdateError_tmp.last_execution_end_date = TalendDate
										.getDate("CCYY-MM-DD hh:mm:ss");
								j_end_exec_1_toUpdateError_tmp.last_execution_result = Var.result;
								j_end_exec_1_toUpdateError_tmp.is_running = (Boolean) globalMap.get("isRunning");
								j_end_exec_1_toUpdateError = j_end_exec_1_toUpdateError_tmp;
								log.debug("j_end_exec_1_tMap_2 - Outputting the record "
										+ count_j_end_exec_1_toUpdateError_j_end_exec_1_tMap_2
										+ " of the output table 'j_end_exec_1_toUpdateError'.");

// ###############################

							} // end of Var scope

							rejectedInnerJoin_j_end_exec_1_tMap_2 = false;

							tos_count_j_end_exec_1_tMap_2++;

							/**
							 * [j_end_exec_1_tMap_2 main ] stop
							 */

							/**
							 * [j_end_exec_1_tMap_2 process_data_begin ] start
							 */

							currentComponent = "j_end_exec_1_tMap_2";

							/**
							 * [j_end_exec_1_tMap_2 process_data_begin ] stop
							 */
// Start of branch "j_end_exec_1_toUpdateError"
							if (j_end_exec_1_toUpdateError != null) {

								/**
								 * [j_end_exec_1_tDBOutput_2 main ] start
								 */

								currentComponent = "j_end_exec_1_tDBOutput_2";

								if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

										, "j_end_exec_1_toUpdateError", "j_end_exec_1_tMap_2", "j_end_exec_1_tMap_2",
										"tMap", "j_end_exec_1_tDBOutput_2",
										"__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>",
										"tPostgresqlOutput"

								)) {
									talendJobLogProcess(globalMap);
								}

								if (log.isTraceEnabled()) {
									log.trace("j_end_exec_1_toUpdateError - " + (j_end_exec_1_toUpdateError == null ? ""
											: j_end_exec_1_toUpdateError.toLogString()));
								}

								whetherReject_j_end_exec_1_tDBOutput_2 = false;
								if (j_end_exec_1_toUpdateError.last_execution_end_date == null) {
									pstmt_j_end_exec_1_tDBOutput_2.setNull(1, java.sql.Types.VARCHAR);
								} else {
									pstmt_j_end_exec_1_tDBOutput_2.setString(1,
											j_end_exec_1_toUpdateError.last_execution_end_date);
								}

								if (j_end_exec_1_toUpdateError.last_execution_result == null) {
									pstmt_j_end_exec_1_tDBOutput_2.setNull(2, java.sql.Types.VARCHAR);
								} else {
									pstmt_j_end_exec_1_tDBOutput_2.setString(2,
											j_end_exec_1_toUpdateError.last_execution_result);
								}

								if (j_end_exec_1_toUpdateError.is_running == null) {
									pstmt_j_end_exec_1_tDBOutput_2.setNull(3, java.sql.Types.BOOLEAN);
								} else {
									pstmt_j_end_exec_1_tDBOutput_2.setBoolean(3, j_end_exec_1_toUpdateError.is_running);
								}

								if (j_end_exec_1_toUpdateError.id == null) {
									pstmt_j_end_exec_1_tDBOutput_2.setNull(4 + count_j_end_exec_1_tDBOutput_2,
											java.sql.Types.INTEGER);
								} else {
									pstmt_j_end_exec_1_tDBOutput_2.setInt(4 + count_j_end_exec_1_tDBOutput_2,
											j_end_exec_1_toUpdateError.id);
								}

								pstmt_j_end_exec_1_tDBOutput_2.addBatch();
								nb_line_j_end_exec_1_tDBOutput_2++;

								if (log.isDebugEnabled())
									log.debug("j_end_exec_1_tDBOutput_2 - " + ("Adding the record ")
											+ (nb_line_j_end_exec_1_tDBOutput_2) + (" to the ") + ("UPDATE")
											+ (" batch."));
								batchSizeCounter_j_end_exec_1_tDBOutput_2++;

								if ((batchSize_j_end_exec_1_tDBOutput_2 > 0)
										&& (batchSize_j_end_exec_1_tDBOutput_2 <= batchSizeCounter_j_end_exec_1_tDBOutput_2)) {
									try {
										int countSum_j_end_exec_1_tDBOutput_2 = 0;

										if (log.isDebugEnabled())
											log.debug("j_end_exec_1_tDBOutput_2 - " + ("Executing the ") + ("UPDATE")
													+ (" batch."));
										for (int countEach_j_end_exec_1_tDBOutput_2 : pstmt_j_end_exec_1_tDBOutput_2
												.executeBatch()) {
											countSum_j_end_exec_1_tDBOutput_2 += (countEach_j_end_exec_1_tDBOutput_2 < 0
													? 0
													: countEach_j_end_exec_1_tDBOutput_2);
										}
										if (log.isDebugEnabled())
											log.debug("j_end_exec_1_tDBOutput_2 - " + ("The ") + ("UPDATE")
													+ (" batch execution has succeeded."));
										rowsToCommitCount_j_end_exec_1_tDBOutput_2 += countSum_j_end_exec_1_tDBOutput_2;

										updatedCount_j_end_exec_1_tDBOutput_2 += countSum_j_end_exec_1_tDBOutput_2;

										batchSizeCounter_j_end_exec_1_tDBOutput_2 = 0;
									} catch (java.sql.BatchUpdateException e_j_end_exec_1_tDBOutput_2) {
										globalMap.put("j_end_exec_1_tDBOutput_2_ERROR_MESSAGE",
												e_j_end_exec_1_tDBOutput_2.getMessage());
										java.sql.SQLException ne_j_end_exec_1_tDBOutput_2 = e_j_end_exec_1_tDBOutput_2
												.getNextException(), sqle_j_end_exec_1_tDBOutput_2 = null;
										String errormessage_j_end_exec_1_tDBOutput_2;
										if (ne_j_end_exec_1_tDBOutput_2 != null) {
											// build new exception to provide the original cause
											sqle_j_end_exec_1_tDBOutput_2 = new java.sql.SQLException(
													e_j_end_exec_1_tDBOutput_2.getMessage() + "\ncaused by: "
															+ ne_j_end_exec_1_tDBOutput_2.getMessage(),
													ne_j_end_exec_1_tDBOutput_2.getSQLState(),
													ne_j_end_exec_1_tDBOutput_2.getErrorCode(),
													ne_j_end_exec_1_tDBOutput_2);
											errormessage_j_end_exec_1_tDBOutput_2 = sqle_j_end_exec_1_tDBOutput_2
													.getMessage();
										} else {
											errormessage_j_end_exec_1_tDBOutput_2 = e_j_end_exec_1_tDBOutput_2
													.getMessage();
										}

										int countSum_j_end_exec_1_tDBOutput_2 = 0;
										for (int countEach_j_end_exec_1_tDBOutput_2 : e_j_end_exec_1_tDBOutput_2
												.getUpdateCounts()) {
											countSum_j_end_exec_1_tDBOutput_2 += (countEach_j_end_exec_1_tDBOutput_2 < 0
													? 0
													: countEach_j_end_exec_1_tDBOutput_2);
										}
										rowsToCommitCount_j_end_exec_1_tDBOutput_2 += countSum_j_end_exec_1_tDBOutput_2;

										updatedCount_j_end_exec_1_tDBOutput_2 += countSum_j_end_exec_1_tDBOutput_2;

										log.error("j_end_exec_1_tDBOutput_2 - "
												+ (errormessage_j_end_exec_1_tDBOutput_2));
										System.err.println(errormessage_j_end_exec_1_tDBOutput_2);

									}
								}

								tos_count_j_end_exec_1_tDBOutput_2++;

								/**
								 * [j_end_exec_1_tDBOutput_2 main ] stop
								 */

								/**
								 * [j_end_exec_1_tDBOutput_2 process_data_begin ] start
								 */

								currentComponent = "j_end_exec_1_tDBOutput_2";

								/**
								 * [j_end_exec_1_tDBOutput_2 process_data_begin ] stop
								 */

								/**
								 * [j_end_exec_1_tDBOutput_2 process_data_end ] start
								 */

								currentComponent = "j_end_exec_1_tDBOutput_2";

								/**
								 * [j_end_exec_1_tDBOutput_2 process_data_end ] stop
								 */

							} // End of branch "j_end_exec_1_toUpdateError"

						} // close loop of lookup 'j_end_exec_1_row6' // G_TM_M_043

						/**
						 * [j_end_exec_1_tMap_2 process_data_end ] start
						 */

						currentComponent = "j_end_exec_1_tMap_2";

						/**
						 * [j_end_exec_1_tMap_2 process_data_end ] stop
						 */

						/**
						 * [j_end_exec_1_tLogCatcher_1 process_data_end ] start
						 */

						currentComponent = "j_end_exec_1_tLogCatcher_1";

						/**
						 * [j_end_exec_1_tLogCatcher_1 process_data_end ] stop
						 */

						/**
						 * [j_end_exec_1_tLogCatcher_1 end ] start
						 */

						currentComponent = "j_end_exec_1_tLogCatcher_1";

					}
				} catch (Exception e_j_end_exec_1_tLogCatcher_1) {
					globalMap.put("j_end_exec_1_tLogCatcher_1_ERROR_MESSAGE",
							e_j_end_exec_1_tLogCatcher_1.getMessage());
					logIgnoredError(String.format(
							"j_end_exec_1_tLogCatcher_1 - tLogCatcher failed to process log message(s) due to internal error: %s",
							e_j_end_exec_1_tLogCatcher_1), e_j_end_exec_1_tLogCatcher_1);
				}

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tLogCatcher_1 - " + ("Done."));

				ok_Hash.put("j_end_exec_1_tLogCatcher_1", true);
				end_Hash.put("j_end_exec_1_tLogCatcher_1", System.currentTimeMillis());

				/**
				 * [j_end_exec_1_tLogCatcher_1 end ] stop
				 */

				/**
				 * [j_end_exec_1_tMap_2 end ] start
				 */

				currentComponent = "j_end_exec_1_tMap_2";

// ###############################
// # Lookup hashes releasing
				if (tHash_Lookup_j_end_exec_1_row6 != null) {
					tHash_Lookup_j_end_exec_1_row6.endGet();
				}
				globalMap.remove("tHash_Lookup_j_end_exec_1_row6");

// ###############################      
				log.debug("j_end_exec_1_tMap_2 - Written records count in the table 'j_end_exec_1_toUpdateError': "
						+ count_j_end_exec_1_toUpdateError_j_end_exec_1_tMap_2 + ".");

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "j_end_exec_1_row8", 2,
						0, "j_end_exec_1_tLogCatcher_1", "j_end_exec_1_tLogCatcher_1", "tLogCatcher",
						"j_end_exec_1_tMap_2", "j_end_exec_1_tMap_2", "tMap", "output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tMap_2 - " + ("Done."));

				ok_Hash.put("j_end_exec_1_tMap_2", true);
				end_Hash.put("j_end_exec_1_tMap_2", System.currentTimeMillis());

				/**
				 * [j_end_exec_1_tMap_2 end ] stop
				 */

				/**
				 * [j_end_exec_1_tDBOutput_2 end ] start
				 */

				currentComponent = "j_end_exec_1_tDBOutput_2";

				try {
					int countSum_j_end_exec_1_tDBOutput_2 = 0;
					if (pstmt_j_end_exec_1_tDBOutput_2 != null && batchSizeCounter_j_end_exec_1_tDBOutput_2 > 0) {

						if (log.isDebugEnabled())
							log.debug("j_end_exec_1_tDBOutput_2 - " + ("Executing the ") + ("UPDATE") + (" batch."));
						for (int countEach_j_end_exec_1_tDBOutput_2 : pstmt_j_end_exec_1_tDBOutput_2.executeBatch()) {
							countSum_j_end_exec_1_tDBOutput_2 += (countEach_j_end_exec_1_tDBOutput_2 < 0 ? 0
									: countEach_j_end_exec_1_tDBOutput_2);
						}
						rowsToCommitCount_j_end_exec_1_tDBOutput_2 += countSum_j_end_exec_1_tDBOutput_2;

						if (log.isDebugEnabled())
							log.debug("j_end_exec_1_tDBOutput_2 - " + ("The ") + ("UPDATE")
									+ (" batch execution has succeeded."));
					}

					updatedCount_j_end_exec_1_tDBOutput_2 += countSum_j_end_exec_1_tDBOutput_2;

				} catch (java.sql.BatchUpdateException e_j_end_exec_1_tDBOutput_2) {
					globalMap.put("j_end_exec_1_tDBOutput_2_ERROR_MESSAGE", e_j_end_exec_1_tDBOutput_2.getMessage());
					java.sql.SQLException ne_j_end_exec_1_tDBOutput_2 = e_j_end_exec_1_tDBOutput_2.getNextException(),
							sqle_j_end_exec_1_tDBOutput_2 = null;
					String errormessage_j_end_exec_1_tDBOutput_2;
					if (ne_j_end_exec_1_tDBOutput_2 != null) {
						// build new exception to provide the original cause
						sqle_j_end_exec_1_tDBOutput_2 = new java.sql.SQLException(
								e_j_end_exec_1_tDBOutput_2.getMessage() + "\ncaused by: "
										+ ne_j_end_exec_1_tDBOutput_2.getMessage(),
								ne_j_end_exec_1_tDBOutput_2.getSQLState(), ne_j_end_exec_1_tDBOutput_2.getErrorCode(),
								ne_j_end_exec_1_tDBOutput_2);
						errormessage_j_end_exec_1_tDBOutput_2 = sqle_j_end_exec_1_tDBOutput_2.getMessage();
					} else {
						errormessage_j_end_exec_1_tDBOutput_2 = e_j_end_exec_1_tDBOutput_2.getMessage();
					}

					int countSum_j_end_exec_1_tDBOutput_2 = 0;
					for (int countEach_j_end_exec_1_tDBOutput_2 : e_j_end_exec_1_tDBOutput_2.getUpdateCounts()) {
						countSum_j_end_exec_1_tDBOutput_2 += (countEach_j_end_exec_1_tDBOutput_2 < 0 ? 0
								: countEach_j_end_exec_1_tDBOutput_2);
					}
					rowsToCommitCount_j_end_exec_1_tDBOutput_2 += countSum_j_end_exec_1_tDBOutput_2;

					updatedCount_j_end_exec_1_tDBOutput_2 += countSum_j_end_exec_1_tDBOutput_2;

					log.error("j_end_exec_1_tDBOutput_2 - " + (errormessage_j_end_exec_1_tDBOutput_2));
					System.err.println(errormessage_j_end_exec_1_tDBOutput_2);

				}

				if (pstmt_j_end_exec_1_tDBOutput_2 != null) {

					pstmt_j_end_exec_1_tDBOutput_2.close();
					resourceMap.remove("pstmt_j_end_exec_1_tDBOutput_2");
				}
				resourceMap.put("statementClosed_j_end_exec_1_tDBOutput_2", true);

				nb_line_deleted_j_end_exec_1_tDBOutput_2 = nb_line_deleted_j_end_exec_1_tDBOutput_2
						+ deletedCount_j_end_exec_1_tDBOutput_2;
				nb_line_update_j_end_exec_1_tDBOutput_2 = nb_line_update_j_end_exec_1_tDBOutput_2
						+ updatedCount_j_end_exec_1_tDBOutput_2;
				nb_line_inserted_j_end_exec_1_tDBOutput_2 = nb_line_inserted_j_end_exec_1_tDBOutput_2
						+ insertedCount_j_end_exec_1_tDBOutput_2;
				nb_line_rejected_j_end_exec_1_tDBOutput_2 = nb_line_rejected_j_end_exec_1_tDBOutput_2
						+ rejectedCount_j_end_exec_1_tDBOutput_2;

				globalMap.put("j_end_exec_1_tDBOutput_2_NB_LINE", nb_line_j_end_exec_1_tDBOutput_2);
				globalMap.put("j_end_exec_1_tDBOutput_2_NB_LINE_UPDATED", nb_line_update_j_end_exec_1_tDBOutput_2);
				globalMap.put("j_end_exec_1_tDBOutput_2_NB_LINE_INSERTED", nb_line_inserted_j_end_exec_1_tDBOutput_2);
				globalMap.put("j_end_exec_1_tDBOutput_2_NB_LINE_DELETED", nb_line_deleted_j_end_exec_1_tDBOutput_2);
				globalMap.put("j_end_exec_1_tDBOutput_2_NB_LINE_REJECTED", nb_line_rejected_j_end_exec_1_tDBOutput_2);

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBOutput_2 - " + ("Has ") + ("updated") + (" ")
							+ (nb_line_update_j_end_exec_1_tDBOutput_2) + (" record(s)."));

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId,
						"j_end_exec_1_toUpdateError", 2, 0, "j_end_exec_1_tMap_2", "j_end_exec_1_tMap_2", "tMap",
						"j_end_exec_1_tDBOutput_2", "__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>",
						"tPostgresqlOutput", "output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBOutput_2 - " + ("Done."));

				ok_Hash.put("j_end_exec_1_tDBOutput_2", true);
				end_Hash.put("j_end_exec_1_tDBOutput_2", System.currentTimeMillis());

				/**
				 * [j_end_exec_1_tDBOutput_2 end ] stop
				 */

			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			// free memory for "j_end_exec_1_tMap_2"
			globalMap.remove("tHash_Lookup_j_end_exec_1_row6");

			try {

				/**
				 * [j_end_exec_1_tLogCatcher_1 finally ] start
				 */

				currentComponent = "j_end_exec_1_tLogCatcher_1";

				/**
				 * [j_end_exec_1_tLogCatcher_1 finally ] stop
				 */

				/**
				 * [j_end_exec_1_tMap_2 finally ] start
				 */

				currentComponent = "j_end_exec_1_tMap_2";

				/**
				 * [j_end_exec_1_tMap_2 finally ] stop
				 */

				/**
				 * [j_end_exec_1_tDBOutput_2 finally ] start
				 */

				currentComponent = "j_end_exec_1_tDBOutput_2";

				if (resourceMap.get("statementClosed_j_end_exec_1_tDBOutput_2") == null) {
					java.sql.PreparedStatement pstmtToClose_j_end_exec_1_tDBOutput_2 = null;
					if ((pstmtToClose_j_end_exec_1_tDBOutput_2 = (java.sql.PreparedStatement) resourceMap
							.remove("pstmt_j_end_exec_1_tDBOutput_2")) != null) {
						pstmtToClose_j_end_exec_1_tDBOutput_2.close();
					}
				}

				/**
				 * [j_end_exec_1_tDBOutput_2 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_end_exec_1_tLogCatcher_1_SUBPROCESS_STATE", 1);
	}

	public static class j_end_exec_1_row6Struct implements routines.system.IPersistableRow<j_end_exec_1_row6Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public Integer id;

		public Integer getId() {
			return this.id;
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("id=" + String.valueOf(id));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (id == null) {
				sb.append("<null>");
			} else {
				sb.append(id);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(j_end_exec_1_row6Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void j_end_exec_1_tDBInput_4Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("j_end_exec_1_tDBInput_4_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				j_end_exec_1_row6Struct j_end_exec_1_row6 = new j_end_exec_1_row6Struct();

				/**
				 * [tAdvancedHash_j_end_exec_1_row6 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_j_end_exec_1_row6", false);
				start_Hash.put("tAdvancedHash_j_end_exec_1_row6", System.currentTimeMillis());

				currentComponent = "tAdvancedHash_j_end_exec_1_row6";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "j_end_exec_1_row6");

				int tos_count_tAdvancedHash_j_end_exec_1_row6 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("tAdvancedHash_j_end_exec_1_row6", "tAdvancedHash_j_end_exec_1_row6",
							"tAdvancedHash");
					talendJobLogProcess(globalMap);
				}

				// connection name:j_end_exec_1_row6
				// source node:j_end_exec_1_tDBInput_4 -
				// inputs:(after_j_end_exec_1_tLogCatcher_1)
				// outputs:(j_end_exec_1_row6,j_end_exec_1_row6) | target
				// node:tAdvancedHash_j_end_exec_1_row6 - inputs:(j_end_exec_1_row6) outputs:()
				// linked node: j_end_exec_1_tMap_2 -
				// inputs:(j_end_exec_1_row8,j_end_exec_1_row6)
				// outputs:(j_end_exec_1_toUpdateError)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_j_end_exec_1_row6 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.ALL_ROWS;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<j_end_exec_1_row6Struct> tHash_Lookup_j_end_exec_1_row6 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<j_end_exec_1_row6Struct>getLookup(matchingModeEnum_j_end_exec_1_row6);

				globalMap.put("tHash_Lookup_j_end_exec_1_row6", tHash_Lookup_j_end_exec_1_row6);

				/**
				 * [tAdvancedHash_j_end_exec_1_row6 begin ] stop
				 */

				/**
				 * [j_end_exec_1_tDBInput_4 begin ] start
				 */

				ok_Hash.put("j_end_exec_1_tDBInput_4", false);
				start_Hash.put("j_end_exec_1_tDBInput_4", System.currentTimeMillis());

				currentComponent = "j_end_exec_1_tDBInput_4";

				int tos_count_j_end_exec_1_tDBInput_4 = 0;

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBInput_4 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_end_exec_1_tDBInput_4 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_end_exec_1_tDBInput_4 = new StringBuilder();
							log4jParamters_j_end_exec_1_tDBInput_4.append("Parameters:");
							log4jParamters_j_end_exec_1_tDBInput_4.append("USE_EXISTING_CONNECTION" + " = " + "true");
							log4jParamters_j_end_exec_1_tDBInput_4.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_4
									.append("CONNECTION" + " = " + "j_end_exec_1_tDBConnection_1");
							log4jParamters_j_end_exec_1_tDBInput_4.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_4.append("QUERYSTORE" + " = " + "\"\"");
							log4jParamters_j_end_exec_1_tDBInput_4.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_4.append("QUERY" + " = "
									+ "\" SELECT  	id FROM locks WHERE guid = '\"+ (String)globalMap.get(\"guid\") +\"' ORDER BY id DESC \"");
							log4jParamters_j_end_exec_1_tDBInput_4.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_4.append("USE_CURSOR" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBInput_4.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_4.append("TRIM_ALL_COLUMN" + " = " + "false");
							log4jParamters_j_end_exec_1_tDBInput_4.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_4.append(
									"TRIM_COLUMN" + " = " + "[{TRIM=" + ("false") + ", SCHEMA_COLUMN=" + ("id") + "}]");
							log4jParamters_j_end_exec_1_tDBInput_4.append(" | ");
							log4jParamters_j_end_exec_1_tDBInput_4
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlInput");
							log4jParamters_j_end_exec_1_tDBInput_4.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_end_exec_1_tDBInput_4 - " + (log4jParamters_j_end_exec_1_tDBInput_4));
						}
					}
					new BytesLimit65535_j_end_exec_1_tDBInput_4().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_end_exec_1_tDBInput_4", "__UNIQUE_NAME__<br><b>__TABLE__</b>",
							"tPostgresqlInput");
					talendJobLogProcess(globalMap);
				}

				int nb_line_j_end_exec_1_tDBInput_4 = 0;
				java.sql.Connection conn_j_end_exec_1_tDBInput_4 = null;
				conn_j_end_exec_1_tDBInput_4 = (java.sql.Connection) globalMap.get("conn_j_end_exec_1_tDBConnection_1");

				if (conn_j_end_exec_1_tDBInput_4 != null) {
					if (conn_j_end_exec_1_tDBInput_4.getMetaData() != null) {

						log.debug("j_end_exec_1_tDBInput_4 - Uses an existing connection with username '"
								+ conn_j_end_exec_1_tDBInput_4.getMetaData().getUserName() + "'. Connection URL: "
								+ conn_j_end_exec_1_tDBInput_4.getMetaData().getURL() + ".");

					}
				}

				java.sql.Statement stmt_j_end_exec_1_tDBInput_4 = conn_j_end_exec_1_tDBInput_4.createStatement();

				String dbquery_j_end_exec_1_tDBInput_4 = "\nSELECT \n	id\nFROM locks\nWHERE guid = '"
						+ (String) globalMap.get("guid") + "'\nORDER BY id DESC\n";

				log.debug("j_end_exec_1_tDBInput_4 - Executing the query: '" + dbquery_j_end_exec_1_tDBInput_4 + "'.");

				globalMap.put("j_end_exec_1_tDBInput_4_QUERY", dbquery_j_end_exec_1_tDBInput_4);
				java.sql.ResultSet rs_j_end_exec_1_tDBInput_4 = null;

				try {
					rs_j_end_exec_1_tDBInput_4 = stmt_j_end_exec_1_tDBInput_4
							.executeQuery(dbquery_j_end_exec_1_tDBInput_4);
					java.sql.ResultSetMetaData rsmd_j_end_exec_1_tDBInput_4 = rs_j_end_exec_1_tDBInput_4.getMetaData();
					int colQtyInRs_j_end_exec_1_tDBInput_4 = rsmd_j_end_exec_1_tDBInput_4.getColumnCount();

					String tmpContent_j_end_exec_1_tDBInput_4 = null;

					log.debug("j_end_exec_1_tDBInput_4 - Retrieving records from the database.");

					while (rs_j_end_exec_1_tDBInput_4.next()) {
						nb_line_j_end_exec_1_tDBInput_4++;

						if (colQtyInRs_j_end_exec_1_tDBInput_4 < 1) {
							j_end_exec_1_row6.id = null;
						} else {

							j_end_exec_1_row6.id = rs_j_end_exec_1_tDBInput_4.getInt(1);
							if (rs_j_end_exec_1_tDBInput_4.wasNull()) {
								j_end_exec_1_row6.id = null;
							}
						}

						log.debug("j_end_exec_1_tDBInput_4 - Retrieving the record " + nb_line_j_end_exec_1_tDBInput_4
								+ ".");

						/**
						 * [j_end_exec_1_tDBInput_4 begin ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_4 main ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_4";

						tos_count_j_end_exec_1_tDBInput_4++;

						/**
						 * [j_end_exec_1_tDBInput_4 main ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_4 process_data_begin ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_4";

						/**
						 * [j_end_exec_1_tDBInput_4 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_j_end_exec_1_row6 main ] start
						 */

						currentComponent = "tAdvancedHash_j_end_exec_1_row6";

						if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

								, "j_end_exec_1_row6", "j_end_exec_1_tDBInput_4", "__UNIQUE_NAME__<br><b>__TABLE__</b>",
								"tPostgresqlInput", "tAdvancedHash_j_end_exec_1_row6",
								"tAdvancedHash_j_end_exec_1_row6", "tAdvancedHash"

						)) {
							talendJobLogProcess(globalMap);
						}

						if (log.isTraceEnabled()) {
							log.trace("j_end_exec_1_row6 - "
									+ (j_end_exec_1_row6 == null ? "" : j_end_exec_1_row6.toLogString()));
						}

						j_end_exec_1_row6Struct j_end_exec_1_row6_HashRow = new j_end_exec_1_row6Struct();

						j_end_exec_1_row6_HashRow.id = j_end_exec_1_row6.id;

						tHash_Lookup_j_end_exec_1_row6.put(j_end_exec_1_row6_HashRow);

						tos_count_tAdvancedHash_j_end_exec_1_row6++;

						/**
						 * [tAdvancedHash_j_end_exec_1_row6 main ] stop
						 */

						/**
						 * [tAdvancedHash_j_end_exec_1_row6 process_data_begin ] start
						 */

						currentComponent = "tAdvancedHash_j_end_exec_1_row6";

						/**
						 * [tAdvancedHash_j_end_exec_1_row6 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_j_end_exec_1_row6 process_data_end ] start
						 */

						currentComponent = "tAdvancedHash_j_end_exec_1_row6";

						/**
						 * [tAdvancedHash_j_end_exec_1_row6 process_data_end ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_4 process_data_end ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_4";

						/**
						 * [j_end_exec_1_tDBInput_4 process_data_end ] stop
						 */

						/**
						 * [j_end_exec_1_tDBInput_4 end ] start
						 */

						currentComponent = "j_end_exec_1_tDBInput_4";

					}
				} finally {
					if (rs_j_end_exec_1_tDBInput_4 != null) {
						rs_j_end_exec_1_tDBInput_4.close();
					}
					if (stmt_j_end_exec_1_tDBInput_4 != null) {
						stmt_j_end_exec_1_tDBInput_4.close();
					}
				}
				globalMap.put("j_end_exec_1_tDBInput_4_NB_LINE", nb_line_j_end_exec_1_tDBInput_4);
				log.debug(
						"j_end_exec_1_tDBInput_4 - Retrieved records count: " + nb_line_j_end_exec_1_tDBInput_4 + " .");

				if (log.isDebugEnabled())
					log.debug("j_end_exec_1_tDBInput_4 - " + ("Done."));

				ok_Hash.put("j_end_exec_1_tDBInput_4", true);
				end_Hash.put("j_end_exec_1_tDBInput_4", System.currentTimeMillis());

				/**
				 * [j_end_exec_1_tDBInput_4 end ] stop
				 */

				/**
				 * [tAdvancedHash_j_end_exec_1_row6 end ] start
				 */

				currentComponent = "tAdvancedHash_j_end_exec_1_row6";

				tHash_Lookup_j_end_exec_1_row6.endPut();

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "j_end_exec_1_row6", 2,
						0, "j_end_exec_1_tDBInput_4", "__UNIQUE_NAME__<br><b>__TABLE__</b>", "tPostgresqlInput",
						"tAdvancedHash_j_end_exec_1_row6", "tAdvancedHash_j_end_exec_1_row6", "tAdvancedHash",
						"output")) {
					talendJobLogProcess(globalMap);
				}

				ok_Hash.put("tAdvancedHash_j_end_exec_1_row6", true);
				end_Hash.put("tAdvancedHash_j_end_exec_1_row6", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_j_end_exec_1_row6 end ] stop
				 */

			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_end_exec_1_tDBInput_4 finally ] start
				 */

				currentComponent = "j_end_exec_1_tDBInput_4";

				/**
				 * [j_end_exec_1_tDBInput_4 finally ] stop
				 */

				/**
				 * [tAdvancedHash_j_end_exec_1_row6 finally ] start
				 */

				currentComponent = "tAdvancedHash_j_end_exec_1_row6";

				/**
				 * [tAdvancedHash_j_end_exec_1_row6 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_end_exec_1_tDBInput_4_SUBPROCESS_STATE", 1);
	}

	public void tPrejob_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tPrejob_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tPrejob_1 begin ] start
				 */

				ok_Hash.put("tPrejob_1", false);
				start_Hash.put("tPrejob_1", System.currentTimeMillis());

				currentComponent = "tPrejob_1";

				int tos_count_tPrejob_1 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("tPrejob_1", "tPrejob_1", "tPrejob");
					talendJobLogProcess(globalMap);
				}

				/**
				 * [tPrejob_1 begin ] stop
				 */

				/**
				 * [tPrejob_1 main ] start
				 */

				currentComponent = "tPrejob_1";

				tos_count_tPrejob_1++;

				/**
				 * [tPrejob_1 main ] stop
				 */

				/**
				 * [tPrejob_1 process_data_begin ] start
				 */

				currentComponent = "tPrejob_1";

				/**
				 * [tPrejob_1 process_data_begin ] stop
				 */

				/**
				 * [tPrejob_1 process_data_end ] start
				 */

				currentComponent = "tPrejob_1";

				/**
				 * [tPrejob_1 process_data_end ] stop
				 */

				/**
				 * [tPrejob_1 end ] start
				 */

				currentComponent = "tPrejob_1";

				ok_Hash.put("tPrejob_1", true);
				end_Hash.put("tPrejob_1", System.currentTimeMillis());

				if (execStat) {
					runStat.updateStatOnConnection("OnComponentOk1", 0, "ok");
				}
				tJava_1Process(globalMap);

				/**
				 * [tPrejob_1 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tPrejob_1 finally ] start
				 */

				currentComponent = "tPrejob_1";

				/**
				 * [tPrejob_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tPrejob_1_SUBPROCESS_STATE", 1);
	}

	public void tJava_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tJava_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tJava_1 begin ] start
				 */

				ok_Hash.put("tJava_1", false);
				start_Hash.put("tJava_1", System.currentTimeMillis());

				currentComponent = "tJava_1";

				int tos_count_tJava_1 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("tJava_1", "__UNIQUE_NAME__<br><b>Init</b>", "tJava");
					talendJobLogProcess(globalMap);
				}

// DEBUG
				System.out.println("******************************************");
				System.out.println("=> " + jobName + " : start at " + TalendDate.getDate("CCYYMMDD hh:mm:ss"));

				/**
				 * [tJava_1 begin ] stop
				 */

				/**
				 * [tJava_1 main ] start
				 */

				currentComponent = "tJava_1";

				tos_count_tJava_1++;

				/**
				 * [tJava_1 main ] stop
				 */

				/**
				 * [tJava_1 process_data_begin ] start
				 */

				currentComponent = "tJava_1";

				/**
				 * [tJava_1 process_data_begin ] stop
				 */

				/**
				 * [tJava_1 process_data_end ] start
				 */

				currentComponent = "tJava_1";

				/**
				 * [tJava_1 process_data_end ] stop
				 */

				/**
				 * [tJava_1 end ] start
				 */

				currentComponent = "tJava_1";

				ok_Hash.put("tJava_1", true);
				end_Hash.put("tJava_1", System.currentTimeMillis());

				/**
				 * [tJava_1 end ] stop
				 */
			} // end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:tJava_1:OnSubjobOk", "",
						Thread.currentThread().getId() + "", "", "", "", "", "");
			}

			if (execStat) {
				runStat.updateStatOnConnection("OnSubjobOk1", 0, "ok");
			}

			j_init_exec_1_tJava_1Process(globalMap);

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tJava_1 finally ] start
				 */

				currentComponent = "tJava_1";

				/**
				 * [tJava_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tJava_1_SUBPROCESS_STATE", 1);
	}

	public void j_init_exec_1_tJava_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("j_init_exec_1_tJava_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [j_init_exec_1_tJava_1 begin ] start
				 */

				ok_Hash.put("j_init_exec_1_tJava_1", false);
				start_Hash.put("j_init_exec_1_tJava_1", System.currentTimeMillis());

				currentComponent = "j_init_exec_1_tJava_1";

				int tos_count_j_init_exec_1_tJava_1 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("j_init_exec_1_tJava_1", "__UNIQUE_NAME__<br><b>Init</b>", "tJava");
					talendJobLogProcess(globalMap);
				}

// See advanced params => import java.util.UUID;

// Init globalMap
				globalMap.put("isRunning", false);
				globalMap.put("guid", UUID.randomUUID().toString());
				globalMap.put("guid", "aaa");
				globalMap.put("currentExecutionBeginDate", TalendDate.getDate("CCYY-MM-DD hh:mm:ss"));

				/**
				 * [j_init_exec_1_tJava_1 begin ] stop
				 */

				/**
				 * [j_init_exec_1_tJava_1 main ] start
				 */

				currentComponent = "j_init_exec_1_tJava_1";

				tos_count_j_init_exec_1_tJava_1++;

				/**
				 * [j_init_exec_1_tJava_1 main ] stop
				 */

				/**
				 * [j_init_exec_1_tJava_1 process_data_begin ] start
				 */

				currentComponent = "j_init_exec_1_tJava_1";

				/**
				 * [j_init_exec_1_tJava_1 process_data_begin ] stop
				 */

				/**
				 * [j_init_exec_1_tJava_1 process_data_end ] start
				 */

				currentComponent = "j_init_exec_1_tJava_1";

				/**
				 * [j_init_exec_1_tJava_1 process_data_end ] stop
				 */

				/**
				 * [j_init_exec_1_tJava_1 end ] start
				 */

				currentComponent = "j_init_exec_1_tJava_1";

				ok_Hash.put("j_init_exec_1_tJava_1", true);
				end_Hash.put("j_init_exec_1_tJava_1", System.currentTimeMillis());

				if (execStat) {
					runStat.updateStatOnConnection("j_init_exec_1_OnComponentOk1", 0, "ok");
				}
				j_init_exec_1_tDBConnection_1Process(globalMap);

				/**
				 * [j_init_exec_1_tJava_1 end ] stop
				 */
			} // end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil.addLog("CHECKPOINT", "CONNECTION:SUBJOB_OK:j_init_exec_1_tJava_1:OnSubjobOk", "",
						Thread.currentThread().getId() + "", "", "", "", "", "");
			}

			if (execStat) {
				runStat.updateStatOnConnection("j_init_exec_1_OnSubjobOk2", 0, "ok");
			}

			j_init_exec_1_tDBClose_1Process(globalMap);

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_init_exec_1_tJava_1 finally ] start
				 */

				currentComponent = "j_init_exec_1_tJava_1";

				/**
				 * [j_init_exec_1_tJava_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_init_exec_1_tJava_1_SUBPROCESS_STATE", 1);
	}

	public void j_init_exec_1_tDBConnection_1Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("j_init_exec_1_tDBConnection_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [j_init_exec_1_tDBConnection_1 begin ] start
				 */

				ok_Hash.put("j_init_exec_1_tDBConnection_1", false);
				start_Hash.put("j_init_exec_1_tDBConnection_1", System.currentTimeMillis());

				currentComponent = "j_init_exec_1_tDBConnection_1";

				int tos_count_j_init_exec_1_tDBConnection_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_init_exec_1_tDBConnection_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_init_exec_1_tDBConnection_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_init_exec_1_tDBConnection_1 = new StringBuilder();
							log4jParamters_j_init_exec_1_tDBConnection_1.append("Parameters:");
							log4jParamters_j_init_exec_1_tDBConnection_1.append("DB_VERSION" + " = " + "V9_X");
							log4jParamters_j_init_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBConnection_1
									.append("HOST" + " = " + "context.g_BDD_OMNICANAL_HOST");
							log4jParamters_j_init_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBConnection_1
									.append("PORT" + " = " + "context.g_BDD_OMNICANAL_PORT");
							log4jParamters_j_init_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBConnection_1
									.append("DBNAME" + " = " + "context.g_BDD_OMNICANAL_DBNAME");
							log4jParamters_j_init_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBConnection_1
									.append("SCHEMA_DB" + " = " + "context.g_BDD_OMNICANAL_SCHEMA");
							log4jParamters_j_init_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBConnection_1
									.append("USER" + " = " + "context.g_BDD_OMNICANAL_USERNAME");
							log4jParamters_j_init_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBConnection_1
									.append("PASS" + " = "
											+ String.valueOf(routines.system.PasswordEncryptUtil
													.encryptPassword(context.g_BDD_OMNICANAL_PWD)).substring(0, 4)
											+ "...");
							log4jParamters_j_init_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBConnection_1
									.append("USE_SHARED_CONNECTION" + " = " + "true");
							log4jParamters_j_init_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBConnection_1
									.append("SHARED_CONNECTION_NAME" + " = " + "\"BDD_OMNICANAL\"");
							log4jParamters_j_init_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBConnection_1
									.append("PROPERTIES" + " = " + "\"noDatetimeStringSync=true\"");
							log4jParamters_j_init_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBConnection_1.append("AUTO_COMMIT" + " = " + "true");
							log4jParamters_j_init_exec_1_tDBConnection_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBConnection_1
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlConnection");
							log4jParamters_j_init_exec_1_tDBConnection_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_init_exec_1_tDBConnection_1 - "
										+ (log4jParamters_j_init_exec_1_tDBConnection_1));
						}
					}
					new BytesLimit65535_j_init_exec_1_tDBConnection_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_init_exec_1_tDBConnection_1",
							"__UNIQUE_NAME__<br><b>BDD_OMNICANAL</b><br><i>CommitAuto</i>", "tPostgresqlConnection");
					talendJobLogProcess(globalMap);
				}

				String dbProperties_j_init_exec_1_tDBConnection_1 = "noDatetimeStringSync=true";
				String url_j_init_exec_1_tDBConnection_1 = "jdbc:postgresql://" + context.g_BDD_OMNICANAL_HOST + ":"
						+ context.g_BDD_OMNICANAL_PORT + "/" + context.g_BDD_OMNICANAL_DBNAME;

				if (dbProperties_j_init_exec_1_tDBConnection_1 != null
						&& !"".equals(dbProperties_j_init_exec_1_tDBConnection_1.trim())) {
					url_j_init_exec_1_tDBConnection_1 = url_j_init_exec_1_tDBConnection_1 + "?"
							+ dbProperties_j_init_exec_1_tDBConnection_1;
				}
				String dbUser_j_init_exec_1_tDBConnection_1 = context.g_BDD_OMNICANAL_USERNAME;

				final String decryptedPassword_j_init_exec_1_tDBConnection_1 = context.g_BDD_OMNICANAL_PWD;
				String dbPwd_j_init_exec_1_tDBConnection_1 = decryptedPassword_j_init_exec_1_tDBConnection_1;

				java.sql.Connection conn_j_init_exec_1_tDBConnection_1 = null;

				java.util.Enumeration<java.sql.Driver> drivers_j_init_exec_1_tDBConnection_1 = java.sql.DriverManager
						.getDrivers();
				java.util.Set<String> redShiftDriverNames_j_init_exec_1_tDBConnection_1 = new java.util.HashSet<String>(
						java.util.Arrays.asList("com.amazon.redshift.jdbc.Driver", "com.amazon.redshift.jdbc41.Driver",
								"com.amazon.redshift.jdbc42.Driver"));
				while (drivers_j_init_exec_1_tDBConnection_1.hasMoreElements()) {
					java.sql.Driver d_j_init_exec_1_tDBConnection_1 = drivers_j_init_exec_1_tDBConnection_1
							.nextElement();
					if (redShiftDriverNames_j_init_exec_1_tDBConnection_1
							.contains(d_j_init_exec_1_tDBConnection_1.getClass().getName())) {
						try {
							java.sql.DriverManager.deregisterDriver(d_j_init_exec_1_tDBConnection_1);
							java.sql.DriverManager.registerDriver(d_j_init_exec_1_tDBConnection_1);
						} catch (java.lang.Exception e_j_init_exec_1_tDBConnection_1) {
							globalMap.put("j_init_exec_1_tDBConnection_1_ERROR_MESSAGE",
									e_j_init_exec_1_tDBConnection_1.getMessage());
							// do nothing
						}
					}
				}

				SharedDBConnectionLog4j.initLogger(log.getName(), "j_init_exec_1_tDBConnection_1");
				String sharedConnectionName_j_init_exec_1_tDBConnection_1 = "BDD_OMNICANAL";
				conn_j_init_exec_1_tDBConnection_1 = SharedDBConnectionLog4j.getDBConnection("org.postgresql.Driver",
						url_j_init_exec_1_tDBConnection_1, dbUser_j_init_exec_1_tDBConnection_1,
						dbPwd_j_init_exec_1_tDBConnection_1, sharedConnectionName_j_init_exec_1_tDBConnection_1);
				globalMap.put("conn_j_init_exec_1_tDBConnection_1", conn_j_init_exec_1_tDBConnection_1);
				if (null != conn_j_init_exec_1_tDBConnection_1) {

					log.debug("j_init_exec_1_tDBConnection_1 - Connection is set auto commit to 'true'.");
					conn_j_init_exec_1_tDBConnection_1.setAutoCommit(true);
				}

				globalMap.put("schema_" + "j_init_exec_1_tDBConnection_1", context.g_BDD_OMNICANAL_SCHEMA);

				/**
				 * [j_init_exec_1_tDBConnection_1 begin ] stop
				 */

				/**
				 * [j_init_exec_1_tDBConnection_1 main ] start
				 */

				currentComponent = "j_init_exec_1_tDBConnection_1";

				tos_count_j_init_exec_1_tDBConnection_1++;

				/**
				 * [j_init_exec_1_tDBConnection_1 main ] stop
				 */

				/**
				 * [j_init_exec_1_tDBConnection_1 process_data_begin ] start
				 */

				currentComponent = "j_init_exec_1_tDBConnection_1";

				/**
				 * [j_init_exec_1_tDBConnection_1 process_data_begin ] stop
				 */

				/**
				 * [j_init_exec_1_tDBConnection_1 process_data_end ] start
				 */

				currentComponent = "j_init_exec_1_tDBConnection_1";

				/**
				 * [j_init_exec_1_tDBConnection_1 process_data_end ] stop
				 */

				/**
				 * [j_init_exec_1_tDBConnection_1 end ] start
				 */

				currentComponent = "j_init_exec_1_tDBConnection_1";

				if (log.isDebugEnabled())
					log.debug("j_init_exec_1_tDBConnection_1 - " + ("Done."));

				ok_Hash.put("j_init_exec_1_tDBConnection_1", true);
				end_Hash.put("j_init_exec_1_tDBConnection_1", System.currentTimeMillis());

				if (execStat) {
					runStat.updateStatOnConnection("j_init_exec_1_OnComponentOk2", 0, "ok");
				}
				j_init_exec_1_tDBInput_1Process(globalMap);

				/**
				 * [j_init_exec_1_tDBConnection_1 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_init_exec_1_tDBConnection_1 finally ] start
				 */

				currentComponent = "j_init_exec_1_tDBConnection_1";

				/**
				 * [j_init_exec_1_tDBConnection_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_init_exec_1_tDBConnection_1_SUBPROCESS_STATE", 1);
	}

	public static class j_init_exec_1_row5Struct implements routines.system.IPersistableRow<j_init_exec_1_row5Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public Integer id;

		public Integer getId() {
			return this.id;
		}

		public String jobname;

		public String getJobname() {
			return this.jobname;
		}

		public String guid;

		public String getGuid() {
			return this.guid;
		}

		public String last_execution_begin_date;

		public String getLast_execution_begin_date() {
			return this.last_execution_begin_date;
		}

		public String last_execution_end_date;

		public String getLast_execution_end_date() {
			return this.last_execution_end_date;
		}

		public String last_execution_result;

		public String getLast_execution_result() {
			return this.last_execution_result;
		}

		public Boolean is_running;

		public Boolean getIs_running() {
			return this.is_running;
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

					this.jobname = readString(dis);

					this.guid = readString(dis);

					this.last_execution_begin_date = readString(dis);

					this.last_execution_end_date = readString(dis);

					this.last_execution_result = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.is_running = null;
					} else {
						this.is_running = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

					this.jobname = readString(dis);

					this.guid = readString(dis);

					this.last_execution_begin_date = readString(dis);

					this.last_execution_end_date = readString(dis);

					this.last_execution_result = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.is_running = null;
					} else {
						this.is_running = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

				// String

				writeString(this.jobname, dos);

				// String

				writeString(this.guid, dos);

				// String

				writeString(this.last_execution_begin_date, dos);

				// String

				writeString(this.last_execution_end_date, dos);

				// String

				writeString(this.last_execution_result, dos);

				// Boolean

				if (this.is_running == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.is_running);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

				// String

				writeString(this.jobname, dos);

				// String

				writeString(this.guid, dos);

				// String

				writeString(this.last_execution_begin_date, dos);

				// String

				writeString(this.last_execution_end_date, dos);

				// String

				writeString(this.last_execution_result, dos);

				// Boolean

				if (this.is_running == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.is_running);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("id=" + String.valueOf(id));
			sb.append(",jobname=" + jobname);
			sb.append(",guid=" + guid);
			sb.append(",last_execution_begin_date=" + last_execution_begin_date);
			sb.append(",last_execution_end_date=" + last_execution_end_date);
			sb.append(",last_execution_result=" + last_execution_result);
			sb.append(",is_running=" + String.valueOf(is_running));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (id == null) {
				sb.append("<null>");
			} else {
				sb.append(id);
			}

			sb.append("|");

			if (jobname == null) {
				sb.append("<null>");
			} else {
				sb.append(jobname);
			}

			sb.append("|");

			if (guid == null) {
				sb.append("<null>");
			} else {
				sb.append(guid);
			}

			sb.append("|");

			if (last_execution_begin_date == null) {
				sb.append("<null>");
			} else {
				sb.append(last_execution_begin_date);
			}

			sb.append("|");

			if (last_execution_end_date == null) {
				sb.append("<null>");
			} else {
				sb.append(last_execution_end_date);
			}

			sb.append("|");

			if (last_execution_result == null) {
				sb.append("<null>");
			} else {
				sb.append(last_execution_result);
			}

			sb.append("|");

			if (is_running == null) {
				sb.append("<null>");
			} else {
				sb.append(is_running);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(j_init_exec_1_row5Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class j_init_exec_1_row1Struct implements routines.system.IPersistableRow<j_init_exec_1_row1Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];

		public Integer id;

		public Integer getId() {
			return this.id;
		}

		public String jobname;

		public String getJobname() {
			return this.jobname;
		}

		public String guid;

		public String getGuid() {
			return this.guid;
		}

		public String last_execution_begin_date;

		public String getLast_execution_begin_date() {
			return this.last_execution_begin_date;
		}

		public String last_execution_end_date;

		public String getLast_execution_end_date() {
			return this.last_execution_end_date;
		}

		public String last_execution_result;

		public String getLast_execution_result() {
			return this.last_execution_result;
		}

		public Boolean is_running;

		public Boolean getIs_running() {
			return this.is_running;
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

					this.jobname = readString(dis);

					this.guid = readString(dis);

					this.last_execution_begin_date = readString(dis);

					this.last_execution_end_date = readString(dis);

					this.last_execution_result = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.is_running = null;
					} else {
						this.is_running = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.id = readInteger(dis);

					this.jobname = readString(dis);

					this.guid = readString(dis);

					this.last_execution_begin_date = readString(dis);

					this.last_execution_end_date = readString(dis);

					this.last_execution_result = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.is_running = null;
					} else {
						this.is_running = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

				// String

				writeString(this.jobname, dos);

				// String

				writeString(this.guid, dos);

				// String

				writeString(this.last_execution_begin_date, dos);

				// String

				writeString(this.last_execution_end_date, dos);

				// String

				writeString(this.last_execution_result, dos);

				// Boolean

				if (this.is_running == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.is_running);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// Integer

				writeInteger(this.id, dos);

				// String

				writeString(this.jobname, dos);

				// String

				writeString(this.guid, dos);

				// String

				writeString(this.last_execution_begin_date, dos);

				// String

				writeString(this.last_execution_end_date, dos);

				// String

				writeString(this.last_execution_result, dos);

				// Boolean

				if (this.is_running == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.is_running);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("id=" + String.valueOf(id));
			sb.append(",jobname=" + jobname);
			sb.append(",guid=" + guid);
			sb.append(",last_execution_begin_date=" + last_execution_begin_date);
			sb.append(",last_execution_end_date=" + last_execution_end_date);
			sb.append(",last_execution_result=" + last_execution_result);
			sb.append(",is_running=" + String.valueOf(is_running));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (id == null) {
				sb.append("<null>");
			} else {
				sb.append(id);
			}

			sb.append("|");

			if (jobname == null) {
				sb.append("<null>");
			} else {
				sb.append(jobname);
			}

			sb.append("|");

			if (guid == null) {
				sb.append("<null>");
			} else {
				sb.append(guid);
			}

			sb.append("|");

			if (last_execution_begin_date == null) {
				sb.append("<null>");
			} else {
				sb.append(last_execution_begin_date);
			}

			sb.append("|");

			if (last_execution_end_date == null) {
				sb.append("<null>");
			} else {
				sb.append(last_execution_end_date);
			}

			sb.append("|");

			if (last_execution_result == null) {
				sb.append("<null>");
			} else {
				sb.append(last_execution_result);
			}

			sb.append("|");

			if (is_running == null) {
				sb.append("<null>");
			} else {
				sb.append(is_running);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(j_init_exec_1_row1Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void j_init_exec_1_tDBInput_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("j_init_exec_1_tDBInput_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				j_init_exec_1_row1Struct j_init_exec_1_row1 = new j_init_exec_1_row1Struct();
				j_init_exec_1_row5Struct j_init_exec_1_row5 = new j_init_exec_1_row5Struct();

				/**
				 * [j_init_exec_1_tJavaRow_1 begin ] start
				 */

				ok_Hash.put("j_init_exec_1_tJavaRow_1", false);
				start_Hash.put("j_init_exec_1_tJavaRow_1", System.currentTimeMillis());

				currentComponent = "j_init_exec_1_tJavaRow_1";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "j_init_exec_1_row5");

				int tos_count_j_init_exec_1_tJavaRow_1 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("j_init_exec_1_tJavaRow_1", "__UNIQUE_NAME__<br><b>isRunning</b>", "tJavaRow");
					talendJobLogProcess(globalMap);
				}

				int nb_line_j_init_exec_1_tJavaRow_1 = 0;

				/**
				 * [j_init_exec_1_tJavaRow_1 begin ] stop
				 */

				/**
				 * [j_init_exec_1_tUniqRow_1 begin ] start
				 */

				ok_Hash.put("j_init_exec_1_tUniqRow_1", false);
				start_Hash.put("j_init_exec_1_tUniqRow_1", System.currentTimeMillis());

				currentComponent = "j_init_exec_1_tUniqRow_1";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "j_init_exec_1_row1");

				int tos_count_j_init_exec_1_tUniqRow_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_init_exec_1_tUniqRow_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_init_exec_1_tUniqRow_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_init_exec_1_tUniqRow_1 = new StringBuilder();
							log4jParamters_j_init_exec_1_tUniqRow_1.append("Parameters:");
							log4jParamters_j_init_exec_1_tUniqRow_1.append("UNIQUE_KEY" + " = " + "[{CASE_SENSITIVE="
									+ ("false") + ", KEY_ATTRIBUTE=" + ("false") + ", SCHEMA_COLUMN=" + ("id")
									+ "}, {CASE_SENSITIVE=" + ("false") + ", KEY_ATTRIBUTE=" + ("true")
									+ ", SCHEMA_COLUMN=" + ("jobname") + "}, {CASE_SENSITIVE=" + ("false")
									+ ", KEY_ATTRIBUTE=" + ("false") + ", SCHEMA_COLUMN=" + ("guid")
									+ "}, {CASE_SENSITIVE=" + ("false") + ", KEY_ATTRIBUTE=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("last_execution_begin_date") + "}, {CASE_SENSITIVE="
									+ ("false") + ", KEY_ATTRIBUTE=" + ("false") + ", SCHEMA_COLUMN="
									+ ("last_execution_end_date") + "}, {CASE_SENSITIVE=" + ("false")
									+ ", KEY_ATTRIBUTE=" + ("false") + ", SCHEMA_COLUMN=" + ("last_execution_result")
									+ "}, {CASE_SENSITIVE=" + ("false") + ", KEY_ATTRIBUTE=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("is_running") + "}]");
							log4jParamters_j_init_exec_1_tUniqRow_1.append(" | ");
							log4jParamters_j_init_exec_1_tUniqRow_1
									.append("ONLY_ONCE_EACH_DUPLICATED_KEY" + " = " + "false");
							log4jParamters_j_init_exec_1_tUniqRow_1.append(" | ");
							log4jParamters_j_init_exec_1_tUniqRow_1.append("IS_VIRTUAL_COMPONENT" + " = " + "false");
							log4jParamters_j_init_exec_1_tUniqRow_1.append(" | ");
							log4jParamters_j_init_exec_1_tUniqRow_1
									.append("CHANGE_HASH_AND_EQUALS_FOR_BIGDECIMAL" + " = " + "false");
							log4jParamters_j_init_exec_1_tUniqRow_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_init_exec_1_tUniqRow_1 - " + (log4jParamters_j_init_exec_1_tUniqRow_1));
						}
					}
					new BytesLimit65535_j_init_exec_1_tUniqRow_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_init_exec_1_tUniqRow_1", "__UNIQUE_NAME__<br><b>Filter jobName</b>",
							"tUniqRow");
					talendJobLogProcess(globalMap);
				}

				class KeyStruct_j_init_exec_1_tUniqRow_1 {

					private static final int DEFAULT_HASHCODE = 1;
					private static final int PRIME = 31;
					private int hashCode = DEFAULT_HASHCODE;
					public boolean hashCodeDirty = true;

					String jobname;

					@Override
					public int hashCode() {
						if (this.hashCodeDirty) {
							final int prime = PRIME;
							int result = DEFAULT_HASHCODE;

							result = prime * result + ((this.jobname == null) ? 0 : this.jobname.hashCode());

							this.hashCode = result;
							this.hashCodeDirty = false;
						}
						return this.hashCode;
					}

					@Override
					public boolean equals(Object obj) {
						if (this == obj)
							return true;
						if (obj == null)
							return false;
						if (getClass() != obj.getClass())
							return false;
						final KeyStruct_j_init_exec_1_tUniqRow_1 other = (KeyStruct_j_init_exec_1_tUniqRow_1) obj;

						if (this.jobname == null) {
							if (other.jobname != null)
								return false;

						} else if (!this.jobname.equals(other.jobname))

							return false;

						return true;
					}

				}

				int nb_uniques_j_init_exec_1_tUniqRow_1 = 0;
				int nb_duplicates_j_init_exec_1_tUniqRow_1 = 0;
				log.debug("j_init_exec_1_tUniqRow_1 - Start to process the data from datasource.");
				KeyStruct_j_init_exec_1_tUniqRow_1 finder_j_init_exec_1_tUniqRow_1 = new KeyStruct_j_init_exec_1_tUniqRow_1();
				java.util.Set<KeyStruct_j_init_exec_1_tUniqRow_1> keysj_init_exec_1_tUniqRow_1 = new java.util.HashSet<KeyStruct_j_init_exec_1_tUniqRow_1>();

				/**
				 * [j_init_exec_1_tUniqRow_1 begin ] stop
				 */

				/**
				 * [j_init_exec_1_tDBInput_1 begin ] start
				 */

				ok_Hash.put("j_init_exec_1_tDBInput_1", false);
				start_Hash.put("j_init_exec_1_tDBInput_1", System.currentTimeMillis());

				currentComponent = "j_init_exec_1_tDBInput_1";

				int tos_count_j_init_exec_1_tDBInput_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_init_exec_1_tDBInput_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_init_exec_1_tDBInput_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_init_exec_1_tDBInput_1 = new StringBuilder();
							log4jParamters_j_init_exec_1_tDBInput_1.append("Parameters:");
							log4jParamters_j_init_exec_1_tDBInput_1.append("USE_EXISTING_CONNECTION" + " = " + "true");
							log4jParamters_j_init_exec_1_tDBInput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBInput_1
									.append("CONNECTION" + " = " + "j_init_exec_1_tDBConnection_1");
							log4jParamters_j_init_exec_1_tDBInput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBInput_1.append("QUERYSTORE" + " = " + "\"\"");
							log4jParamters_j_init_exec_1_tDBInput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBInput_1.append("QUERY" + " = "
									+ "\" SELECT  	id, 	jobname, 	guid, 	last_execution_begin_date, 	last_execution_end_date, 	last_execution_result, 	is_running FROM locks WHERE jobname = '\"+ jobName +\"' ORDER BY id DESC \"");
							log4jParamters_j_init_exec_1_tDBInput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBInput_1.append("USE_CURSOR" + " = " + "false");
							log4jParamters_j_init_exec_1_tDBInput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBInput_1.append("TRIM_ALL_COLUMN" + " = " + "false");
							log4jParamters_j_init_exec_1_tDBInput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBInput_1.append("TRIM_COLUMN" + " = " + "[{TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("id") + "}, {TRIM=" + ("false") + ", SCHEMA_COLUMN="
									+ ("jobname") + "}, {TRIM=" + ("false") + ", SCHEMA_COLUMN=" + ("guid")
									+ "}, {TRIM=" + ("false") + ", SCHEMA_COLUMN=" + ("last_execution_begin_date")
									+ "}, {TRIM=" + ("false") + ", SCHEMA_COLUMN=" + ("last_execution_end_date")
									+ "}, {TRIM=" + ("false") + ", SCHEMA_COLUMN=" + ("last_execution_result")
									+ "}, {TRIM=" + ("false") + ", SCHEMA_COLUMN=" + ("is_running") + "}]");
							log4jParamters_j_init_exec_1_tDBInput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBInput_1
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlInput");
							log4jParamters_j_init_exec_1_tDBInput_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_init_exec_1_tDBInput_1 - " + (log4jParamters_j_init_exec_1_tDBInput_1));
						}
					}
					new BytesLimit65535_j_init_exec_1_tDBInput_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_init_exec_1_tDBInput_1", "__UNIQUE_NAME__<br><b>__TABLE__</b>",
							"tPostgresqlInput");
					talendJobLogProcess(globalMap);
				}

				int nb_line_j_init_exec_1_tDBInput_1 = 0;
				java.sql.Connection conn_j_init_exec_1_tDBInput_1 = null;
				conn_j_init_exec_1_tDBInput_1 = (java.sql.Connection) globalMap
						.get("conn_j_init_exec_1_tDBConnection_1");

				if (conn_j_init_exec_1_tDBInput_1 != null) {
					if (conn_j_init_exec_1_tDBInput_1.getMetaData() != null) {

						log.debug("j_init_exec_1_tDBInput_1 - Uses an existing connection with username '"
								+ conn_j_init_exec_1_tDBInput_1.getMetaData().getUserName() + "'. Connection URL: "
								+ conn_j_init_exec_1_tDBInput_1.getMetaData().getURL() + ".");

					}
				}

				java.sql.Statement stmt_j_init_exec_1_tDBInput_1 = conn_j_init_exec_1_tDBInput_1.createStatement();

				String dbquery_j_init_exec_1_tDBInput_1 = "\nSELECT \n	id,\n	jobname,\n	guid,\n	last_execution_begin_date,\n	last_execution_end_date,\n	last_execution_result,\n	is_runnin"
						+ "g\nFROM locks\nWHERE jobname = '" + jobName + "'\nORDER BY id DESC\n";

				log.debug(
						"j_init_exec_1_tDBInput_1 - Executing the query: '" + dbquery_j_init_exec_1_tDBInput_1 + "'.");

				globalMap.put("j_init_exec_1_tDBInput_1_QUERY", dbquery_j_init_exec_1_tDBInput_1);
				java.sql.ResultSet rs_j_init_exec_1_tDBInput_1 = null;

				try {
					rs_j_init_exec_1_tDBInput_1 = stmt_j_init_exec_1_tDBInput_1
							.executeQuery(dbquery_j_init_exec_1_tDBInput_1);
					java.sql.ResultSetMetaData rsmd_j_init_exec_1_tDBInput_1 = rs_j_init_exec_1_tDBInput_1
							.getMetaData();
					int colQtyInRs_j_init_exec_1_tDBInput_1 = rsmd_j_init_exec_1_tDBInput_1.getColumnCount();

					String tmpContent_j_init_exec_1_tDBInput_1 = null;

					log.debug("j_init_exec_1_tDBInput_1 - Retrieving records from the database.");

					while (rs_j_init_exec_1_tDBInput_1.next()) {
						nb_line_j_init_exec_1_tDBInput_1++;

						if (colQtyInRs_j_init_exec_1_tDBInput_1 < 1) {
							j_init_exec_1_row1.id = null;
						} else {

							j_init_exec_1_row1.id = rs_j_init_exec_1_tDBInput_1.getInt(1);
							if (rs_j_init_exec_1_tDBInput_1.wasNull()) {
								j_init_exec_1_row1.id = null;
							}
						}
						if (colQtyInRs_j_init_exec_1_tDBInput_1 < 2) {
							j_init_exec_1_row1.jobname = null;
						} else {

							j_init_exec_1_row1.jobname = routines.system.JDBCUtil.getString(rs_j_init_exec_1_tDBInput_1,
									2, false);
						}
						if (colQtyInRs_j_init_exec_1_tDBInput_1 < 3) {
							j_init_exec_1_row1.guid = null;
						} else {

							j_init_exec_1_row1.guid = routines.system.JDBCUtil.getString(rs_j_init_exec_1_tDBInput_1, 3,
									false);
						}
						if (colQtyInRs_j_init_exec_1_tDBInput_1 < 4) {
							j_init_exec_1_row1.last_execution_begin_date = null;
						} else {

							j_init_exec_1_row1.last_execution_begin_date = routines.system.JDBCUtil
									.getString(rs_j_init_exec_1_tDBInput_1, 4, false);
						}
						if (colQtyInRs_j_init_exec_1_tDBInput_1 < 5) {
							j_init_exec_1_row1.last_execution_end_date = null;
						} else {

							j_init_exec_1_row1.last_execution_end_date = routines.system.JDBCUtil
									.getString(rs_j_init_exec_1_tDBInput_1, 5, false);
						}
						if (colQtyInRs_j_init_exec_1_tDBInput_1 < 6) {
							j_init_exec_1_row1.last_execution_result = null;
						} else {

							j_init_exec_1_row1.last_execution_result = routines.system.JDBCUtil
									.getString(rs_j_init_exec_1_tDBInput_1, 6, false);
						}
						if (colQtyInRs_j_init_exec_1_tDBInput_1 < 7) {
							j_init_exec_1_row1.is_running = null;
						} else {

							j_init_exec_1_row1.is_running = rs_j_init_exec_1_tDBInput_1.getBoolean(7);
							if (rs_j_init_exec_1_tDBInput_1.wasNull()) {
								j_init_exec_1_row1.is_running = null;
							}
						}

						log.debug("j_init_exec_1_tDBInput_1 - Retrieving the record " + nb_line_j_init_exec_1_tDBInput_1
								+ ".");

						/**
						 * [j_init_exec_1_tDBInput_1 begin ] stop
						 */

						/**
						 * [j_init_exec_1_tDBInput_1 main ] start
						 */

						currentComponent = "j_init_exec_1_tDBInput_1";

						tos_count_j_init_exec_1_tDBInput_1++;

						/**
						 * [j_init_exec_1_tDBInput_1 main ] stop
						 */

						/**
						 * [j_init_exec_1_tDBInput_1 process_data_begin ] start
						 */

						currentComponent = "j_init_exec_1_tDBInput_1";

						/**
						 * [j_init_exec_1_tDBInput_1 process_data_begin ] stop
						 */

						/**
						 * [j_init_exec_1_tUniqRow_1 main ] start
						 */

						currentComponent = "j_init_exec_1_tUniqRow_1";

						if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

								, "j_init_exec_1_row1", "j_init_exec_1_tDBInput_1",
								"__UNIQUE_NAME__<br><b>__TABLE__</b>", "tPostgresqlInput", "j_init_exec_1_tUniqRow_1",
								"__UNIQUE_NAME__<br><b>Filter jobName</b>", "tUniqRow"

						)) {
							talendJobLogProcess(globalMap);
						}

						if (log.isTraceEnabled()) {
							log.trace("j_init_exec_1_row1 - "
									+ (j_init_exec_1_row1 == null ? "" : j_init_exec_1_row1.toLogString()));
						}

						j_init_exec_1_row5 = null;
						if (j_init_exec_1_row1.jobname == null) {
							finder_j_init_exec_1_tUniqRow_1.jobname = null;
						} else {
							finder_j_init_exec_1_tUniqRow_1.jobname = j_init_exec_1_row1.jobname.toLowerCase();
						}
						finder_j_init_exec_1_tUniqRow_1.hashCodeDirty = true;
						if (!keysj_init_exec_1_tUniqRow_1.contains(finder_j_init_exec_1_tUniqRow_1)) {
							KeyStruct_j_init_exec_1_tUniqRow_1 new_j_init_exec_1_tUniqRow_1 = new KeyStruct_j_init_exec_1_tUniqRow_1();

							if (j_init_exec_1_row1.jobname == null) {
								new_j_init_exec_1_tUniqRow_1.jobname = null;
							} else {
								new_j_init_exec_1_tUniqRow_1.jobname = j_init_exec_1_row1.jobname.toLowerCase();
							}

							keysj_init_exec_1_tUniqRow_1.add(new_j_init_exec_1_tUniqRow_1);
							if (j_init_exec_1_row5 == null) {

								log.trace("j_init_exec_1_tUniqRow_1 - Writing the unique record "
										+ (nb_uniques_j_init_exec_1_tUniqRow_1 + 1) + " into j_init_exec_1_row5.");

								j_init_exec_1_row5 = new j_init_exec_1_row5Struct();
							}
							j_init_exec_1_row5.id = j_init_exec_1_row1.id;
							j_init_exec_1_row5.jobname = j_init_exec_1_row1.jobname;
							j_init_exec_1_row5.guid = j_init_exec_1_row1.guid;
							j_init_exec_1_row5.last_execution_begin_date = j_init_exec_1_row1.last_execution_begin_date;
							j_init_exec_1_row5.last_execution_end_date = j_init_exec_1_row1.last_execution_end_date;
							j_init_exec_1_row5.last_execution_result = j_init_exec_1_row1.last_execution_result;
							j_init_exec_1_row5.is_running = j_init_exec_1_row1.is_running;
							nb_uniques_j_init_exec_1_tUniqRow_1++;
						} else {
							nb_duplicates_j_init_exec_1_tUniqRow_1++;
						}

						tos_count_j_init_exec_1_tUniqRow_1++;

						/**
						 * [j_init_exec_1_tUniqRow_1 main ] stop
						 */

						/**
						 * [j_init_exec_1_tUniqRow_1 process_data_begin ] start
						 */

						currentComponent = "j_init_exec_1_tUniqRow_1";

						/**
						 * [j_init_exec_1_tUniqRow_1 process_data_begin ] stop
						 */
// Start of branch "j_init_exec_1_row5"
						if (j_init_exec_1_row5 != null) {

							/**
							 * [j_init_exec_1_tJavaRow_1 main ] start
							 */

							currentComponent = "j_init_exec_1_tJavaRow_1";

							if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

									, "j_init_exec_1_row5", "j_init_exec_1_tUniqRow_1",
									"__UNIQUE_NAME__<br><b>Filter jobName</b>", "tUniqRow", "j_init_exec_1_tJavaRow_1",
									"__UNIQUE_NAME__<br><b>isRunning</b>", "tJavaRow"

							)) {
								talendJobLogProcess(globalMap);
							}

							if (log.isTraceEnabled()) {
								log.trace("j_init_exec_1_row5 - "
										+ (j_init_exec_1_row5 == null ? "" : j_init_exec_1_row5.toLogString()));
							}

							// Save values in globalMap
							globalMap.put("isRunning", j_init_exec_1_row5.is_running);

							nb_line_j_init_exec_1_tJavaRow_1++;

							tos_count_j_init_exec_1_tJavaRow_1++;

							/**
							 * [j_init_exec_1_tJavaRow_1 main ] stop
							 */

							/**
							 * [j_init_exec_1_tJavaRow_1 process_data_begin ] start
							 */

							currentComponent = "j_init_exec_1_tJavaRow_1";

							/**
							 * [j_init_exec_1_tJavaRow_1 process_data_begin ] stop
							 */

							/**
							 * [j_init_exec_1_tJavaRow_1 process_data_end ] start
							 */

							currentComponent = "j_init_exec_1_tJavaRow_1";

							/**
							 * [j_init_exec_1_tJavaRow_1 process_data_end ] stop
							 */

						} // End of branch "j_init_exec_1_row5"

						/**
						 * [j_init_exec_1_tUniqRow_1 process_data_end ] start
						 */

						currentComponent = "j_init_exec_1_tUniqRow_1";

						/**
						 * [j_init_exec_1_tUniqRow_1 process_data_end ] stop
						 */

						/**
						 * [j_init_exec_1_tDBInput_1 process_data_end ] start
						 */

						currentComponent = "j_init_exec_1_tDBInput_1";

						/**
						 * [j_init_exec_1_tDBInput_1 process_data_end ] stop
						 */

						/**
						 * [j_init_exec_1_tDBInput_1 end ] start
						 */

						currentComponent = "j_init_exec_1_tDBInput_1";

					}
				} finally {
					if (rs_j_init_exec_1_tDBInput_1 != null) {
						rs_j_init_exec_1_tDBInput_1.close();
					}
					if (stmt_j_init_exec_1_tDBInput_1 != null) {
						stmt_j_init_exec_1_tDBInput_1.close();
					}
				}
				globalMap.put("j_init_exec_1_tDBInput_1_NB_LINE", nb_line_j_init_exec_1_tDBInput_1);
				log.debug("j_init_exec_1_tDBInput_1 - Retrieved records count: " + nb_line_j_init_exec_1_tDBInput_1
						+ " .");

				if (log.isDebugEnabled())
					log.debug("j_init_exec_1_tDBInput_1 - " + ("Done."));

				ok_Hash.put("j_init_exec_1_tDBInput_1", true);
				end_Hash.put("j_init_exec_1_tDBInput_1", System.currentTimeMillis());

				/**
				 * [j_init_exec_1_tDBInput_1 end ] stop
				 */

				/**
				 * [j_init_exec_1_tUniqRow_1 end ] start
				 */

				currentComponent = "j_init_exec_1_tUniqRow_1";

				globalMap.put("j_init_exec_1_tUniqRow_1_NB_UNIQUES", nb_uniques_j_init_exec_1_tUniqRow_1);
				globalMap.put("j_init_exec_1_tUniqRow_1_NB_DUPLICATES", nb_duplicates_j_init_exec_1_tUniqRow_1);
				log.info("j_init_exec_1_tUniqRow_1 - Unique records count: " + (nb_uniques_j_init_exec_1_tUniqRow_1)
						+ " .");
				log.info("j_init_exec_1_tUniqRow_1 - Duplicate records count: "
						+ (nb_duplicates_j_init_exec_1_tUniqRow_1) + " .");

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "j_init_exec_1_row1", 2,
						0, "j_init_exec_1_tDBInput_1", "__UNIQUE_NAME__<br><b>__TABLE__</b>", "tPostgresqlInput",
						"j_init_exec_1_tUniqRow_1", "__UNIQUE_NAME__<br><b>Filter jobName</b>", "tUniqRow", "output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("j_init_exec_1_tUniqRow_1 - " + ("Done."));

				ok_Hash.put("j_init_exec_1_tUniqRow_1", true);
				end_Hash.put("j_init_exec_1_tUniqRow_1", System.currentTimeMillis());

				/**
				 * [j_init_exec_1_tUniqRow_1 end ] stop
				 */

				/**
				 * [j_init_exec_1_tJavaRow_1 end ] start
				 */

				currentComponent = "j_init_exec_1_tJavaRow_1";

				globalMap.put("j_init_exec_1_tJavaRow_1_NB_LINE", nb_line_j_init_exec_1_tJavaRow_1);
				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "j_init_exec_1_row5", 2,
						0, "j_init_exec_1_tUniqRow_1", "__UNIQUE_NAME__<br><b>Filter jobName</b>", "tUniqRow",
						"j_init_exec_1_tJavaRow_1", "__UNIQUE_NAME__<br><b>isRunning</b>", "tJavaRow", "output")) {
					talendJobLogProcess(globalMap);
				}

				ok_Hash.put("j_init_exec_1_tJavaRow_1", true);
				end_Hash.put("j_init_exec_1_tJavaRow_1", System.currentTimeMillis());

				if (!(Boolean) globalMap.get("isRunning")) {

					if (execStat) {
						runStat.updateStatOnConnection("j_init_exec_1_If1", 0, "true");
					}
					j_init_exec_1_tFixedFlowInput_1Process(globalMap);
				}

				else {
					if (execStat) {
						runStat.updateStatOnConnection("j_init_exec_1_If1", 0, "false");
					}
				}

				/**
				 * [j_init_exec_1_tJavaRow_1 end ] stop
				 */

			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_init_exec_1_tDBInput_1 finally ] start
				 */

				currentComponent = "j_init_exec_1_tDBInput_1";

				/**
				 * [j_init_exec_1_tDBInput_1 finally ] stop
				 */

				/**
				 * [j_init_exec_1_tUniqRow_1 finally ] start
				 */

				currentComponent = "j_init_exec_1_tUniqRow_1";

				/**
				 * [j_init_exec_1_tUniqRow_1 finally ] stop
				 */

				/**
				 * [j_init_exec_1_tJavaRow_1 finally ] start
				 */

				currentComponent = "j_init_exec_1_tJavaRow_1";

				/**
				 * [j_init_exec_1_tJavaRow_1 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_init_exec_1_tDBInput_1_SUBPROCESS_STATE", 1);
	}

	public static class j_init_exec_1_row2Struct implements routines.system.IPersistableRow<j_init_exec_1_row2Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public String jobname;

		public String getJobname() {
			return this.jobname;
		}

		public String guid;

		public String getGuid() {
			return this.guid;
		}

		public String last_execution_begin_date;

		public String getLast_execution_begin_date() {
			return this.last_execution_begin_date;
		}

		public String last_execution_end_date;

		public String getLast_execution_end_date() {
			return this.last_execution_end_date;
		}

		public String last_execution_result;

		public String getLast_execution_result() {
			return this.last_execution_result;
		}

		public Boolean is_running;

		public Boolean getIs_running() {
			return this.is_running;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + ((this.jobname == null) ? 0 : this.jobname.hashCode());

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final j_init_exec_1_row2Struct other = (j_init_exec_1_row2Struct) obj;

			if (this.jobname == null) {
				if (other.jobname != null)
					return false;

			} else if (!this.jobname.equals(other.jobname))

				return false;

			return true;
		}

		public void copyDataTo(j_init_exec_1_row2Struct other) {

			other.jobname = this.jobname;
			other.guid = this.guid;
			other.last_execution_begin_date = this.last_execution_begin_date;
			other.last_execution_end_date = this.last_execution_end_date;
			other.last_execution_result = this.last_execution_result;
			other.is_running = this.is_running;

		}

		public void copyKeysDataTo(j_init_exec_1_row2Struct other) {

			other.jobname = this.jobname;

		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_j_confirmation_odp.length == 0) {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_j_confirmation_odp = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_j_confirmation_odp, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.jobname = readString(dis);

					this.guid = readString(dis);

					this.last_execution_begin_date = readString(dis);

					this.last_execution_end_date = readString(dis);

					this.last_execution_result = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.is_running = null;
					} else {
						this.is_running = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_j_confirmation_odp) {

				try {

					int length = 0;

					this.jobname = readString(dis);

					this.guid = readString(dis);

					this.last_execution_begin_date = readString(dis);

					this.last_execution_end_date = readString(dis);

					this.last_execution_result = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.is_running = null;
					} else {
						this.is_running = dis.readBoolean();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.jobname, dos);

				// String

				writeString(this.guid, dos);

				// String

				writeString(this.last_execution_begin_date, dos);

				// String

				writeString(this.last_execution_end_date, dos);

				// String

				writeString(this.last_execution_result, dos);

				// Boolean

				if (this.is_running == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.is_running);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// String

				writeString(this.jobname, dos);

				// String

				writeString(this.guid, dos);

				// String

				writeString(this.last_execution_begin_date, dos);

				// String

				writeString(this.last_execution_end_date, dos);

				// String

				writeString(this.last_execution_result, dos);

				// Boolean

				if (this.is_running == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeBoolean(this.is_running);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("jobname=" + jobname);
			sb.append(",guid=" + guid);
			sb.append(",last_execution_begin_date=" + last_execution_begin_date);
			sb.append(",last_execution_end_date=" + last_execution_end_date);
			sb.append(",last_execution_result=" + last_execution_result);
			sb.append(",is_running=" + String.valueOf(is_running));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (jobname == null) {
				sb.append("<null>");
			} else {
				sb.append(jobname);
			}

			sb.append("|");

			if (guid == null) {
				sb.append("<null>");
			} else {
				sb.append(guid);
			}

			sb.append("|");

			if (last_execution_begin_date == null) {
				sb.append("<null>");
			} else {
				sb.append(last_execution_begin_date);
			}

			sb.append("|");

			if (last_execution_end_date == null) {
				sb.append("<null>");
			} else {
				sb.append(last_execution_end_date);
			}

			sb.append("|");

			if (last_execution_result == null) {
				sb.append("<null>");
			} else {
				sb.append(last_execution_result);
			}

			sb.append("|");

			if (is_running == null) {
				sb.append("<null>");
			} else {
				sb.append(is_running);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(j_init_exec_1_row2Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.jobname, other.jobname);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void j_init_exec_1_tFixedFlowInput_1Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("j_init_exec_1_tFixedFlowInput_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				j_init_exec_1_row2Struct j_init_exec_1_row2 = new j_init_exec_1_row2Struct();

				/**
				 * [j_init_exec_1_tDBOutput_1 begin ] start
				 */

				ok_Hash.put("j_init_exec_1_tDBOutput_1", false);
				start_Hash.put("j_init_exec_1_tDBOutput_1", System.currentTimeMillis());

				currentComponent = "j_init_exec_1_tDBOutput_1";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "j_init_exec_1_row2");

				int tos_count_j_init_exec_1_tDBOutput_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_init_exec_1_tDBOutput_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_init_exec_1_tDBOutput_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_init_exec_1_tDBOutput_1 = new StringBuilder();
							log4jParamters_j_init_exec_1_tDBOutput_1.append("Parameters:");
							log4jParamters_j_init_exec_1_tDBOutput_1.append("USE_EXISTING_CONNECTION" + " = " + "true");
							log4jParamters_j_init_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBOutput_1
									.append("CONNECTION" + " = " + "j_init_exec_1_tDBConnection_1");
							log4jParamters_j_init_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBOutput_1.append("TABLE" + " = " + "\"locks\"");
							log4jParamters_j_init_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBOutput_1.append("TABLE_ACTION" + " = " + "NONE");
							log4jParamters_j_init_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBOutput_1.append("DATA_ACTION" + " = " + "UPDATE_OR_INSERT");
							log4jParamters_j_init_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBOutput_1.append("DIE_ON_ERROR" + " = " + "false");
							log4jParamters_j_init_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBOutput_1.append("USE_ALTERNATE_SCHEMA" + " = " + "false");
							log4jParamters_j_init_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBOutput_1.append("ADD_COLS" + " = " + "[]");
							log4jParamters_j_init_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBOutput_1.append("USE_FIELD_OPTIONS" + " = " + "false");
							log4jParamters_j_init_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBOutput_1.append("ENABLE_DEBUG_MODE" + " = " + "false");
							log4jParamters_j_init_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBOutput_1.append("SUPPORT_NULL_WHERE" + " = " + "false");
							log4jParamters_j_init_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBOutput_1
									.append("CONVERT_COLUMN_TABLE_TO_LOWERCASE" + " = " + "false");
							log4jParamters_j_init_exec_1_tDBOutput_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBOutput_1
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlOutput");
							log4jParamters_j_init_exec_1_tDBOutput_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_init_exec_1_tDBOutput_1 - " + (log4jParamters_j_init_exec_1_tDBOutput_1));
						}
					}
					new BytesLimit65535_j_init_exec_1_tDBOutput_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_init_exec_1_tDBOutput_1",
							"__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>", "tPostgresqlOutput");
					talendJobLogProcess(globalMap);
				}

				String dbschema_j_init_exec_1_tDBOutput_1 = null;
				dbschema_j_init_exec_1_tDBOutput_1 = (String) globalMap
						.get("schema_" + "j_init_exec_1_tDBConnection_1");

				String tableName_j_init_exec_1_tDBOutput_1 = null;
				if (dbschema_j_init_exec_1_tDBOutput_1 == null
						|| dbschema_j_init_exec_1_tDBOutput_1.trim().length() == 0) {
					tableName_j_init_exec_1_tDBOutput_1 = ("locks");
				} else {
					tableName_j_init_exec_1_tDBOutput_1 = dbschema_j_init_exec_1_tDBOutput_1 + "\".\"" + ("locks");
				}

				int updateKeyCount_j_init_exec_1_tDBOutput_1 = 1;
				if (updateKeyCount_j_init_exec_1_tDBOutput_1 < 1) {
					throw new RuntimeException("For update, Schema must have a key");
				} else if (updateKeyCount_j_init_exec_1_tDBOutput_1 == 6 && true) {
					log.warn("For update, every Schema column can not be a key");
				}

				int nb_line_j_init_exec_1_tDBOutput_1 = 0;
				int nb_line_update_j_init_exec_1_tDBOutput_1 = 0;
				int nb_line_inserted_j_init_exec_1_tDBOutput_1 = 0;
				int nb_line_deleted_j_init_exec_1_tDBOutput_1 = 0;
				int nb_line_rejected_j_init_exec_1_tDBOutput_1 = 0;

				int deletedCount_j_init_exec_1_tDBOutput_1 = 0;
				int updatedCount_j_init_exec_1_tDBOutput_1 = 0;
				int insertedCount_j_init_exec_1_tDBOutput_1 = 0;
				int rowsToCommitCount_j_init_exec_1_tDBOutput_1 = 0;
				int rejectedCount_j_init_exec_1_tDBOutput_1 = 0;

				boolean whetherReject_j_init_exec_1_tDBOutput_1 = false;

				java.sql.Connection conn_j_init_exec_1_tDBOutput_1 = null;
				String dbUser_j_init_exec_1_tDBOutput_1 = null;

				conn_j_init_exec_1_tDBOutput_1 = (java.sql.Connection) globalMap
						.get("conn_j_init_exec_1_tDBConnection_1");

				if (log.isDebugEnabled())
					log.debug("j_init_exec_1_tDBOutput_1 - " + ("Uses an existing connection with username '")
							+ (conn_j_init_exec_1_tDBOutput_1.getMetaData().getUserName()) + ("'. Connection URL: ")
							+ (conn_j_init_exec_1_tDBOutput_1.getMetaData().getURL()) + ("."));

				if (log.isDebugEnabled())
					log.debug("j_init_exec_1_tDBOutput_1 - " + ("Connection is set auto commit to '")
							+ (conn_j_init_exec_1_tDBOutput_1.getAutoCommit()) + ("'."));

				int count_j_init_exec_1_tDBOutput_1 = 0;
				String update_j_init_exec_1_tDBOutput_1 = "UPDATE \"" + tableName_j_init_exec_1_tDBOutput_1
						+ "\" SET \"guid\" = ?,\"last_execution_begin_date\" = ?,\"last_execution_end_date\" = ?,\"last_execution_result\" = ?,\"is_running\" = ? WHERE \"jobname\" = ?";
				java.sql.PreparedStatement pstmtUpdate_j_init_exec_1_tDBOutput_1 = conn_j_init_exec_1_tDBOutput_1
						.prepareStatement(update_j_init_exec_1_tDBOutput_1);
				resourceMap.put("pstmtUpdate_j_init_exec_1_tDBOutput_1", pstmtUpdate_j_init_exec_1_tDBOutput_1);
				String insert_j_init_exec_1_tDBOutput_1 = "INSERT INTO \"" + tableName_j_init_exec_1_tDBOutput_1
						+ "\" (\"jobname\",\"guid\",\"last_execution_begin_date\",\"last_execution_end_date\",\"last_execution_result\",\"is_running\") VALUES (?,?,?,?,?,?)";
				java.sql.PreparedStatement pstmtInsert_j_init_exec_1_tDBOutput_1 = conn_j_init_exec_1_tDBOutput_1
						.prepareStatement(insert_j_init_exec_1_tDBOutput_1);
				resourceMap.put("pstmtInsert_j_init_exec_1_tDBOutput_1", pstmtInsert_j_init_exec_1_tDBOutput_1);

				/**
				 * [j_init_exec_1_tDBOutput_1 begin ] stop
				 */

				/**
				 * [j_init_exec_1_tFixedFlowInput_1 begin ] start
				 */

				ok_Hash.put("j_init_exec_1_tFixedFlowInput_1", false);
				start_Hash.put("j_init_exec_1_tFixedFlowInput_1", System.currentTimeMillis());

				currentComponent = "j_init_exec_1_tFixedFlowInput_1";

				int tos_count_j_init_exec_1_tFixedFlowInput_1 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("j_init_exec_1_tFixedFlowInput_1", "j_init_exec_1_tFixedFlowInput_1",
							"tFixedFlowInput");
					talendJobLogProcess(globalMap);
				}

				for (int i_j_init_exec_1_tFixedFlowInput_1 = 0; i_j_init_exec_1_tFixedFlowInput_1 < 1; i_j_init_exec_1_tFixedFlowInput_1++) {

					j_init_exec_1_row2.jobname = jobName;

					j_init_exec_1_row2.guid = (String) globalMap.get("guid");

					j_init_exec_1_row2.last_execution_begin_date = (String) globalMap.get("currentExecutionBeginDate");

					j_init_exec_1_row2.last_execution_end_date = null;

					j_init_exec_1_row2.last_execution_result = null;

					j_init_exec_1_row2.is_running = true;

					/**
					 * [j_init_exec_1_tFixedFlowInput_1 begin ] stop
					 */

					/**
					 * [j_init_exec_1_tFixedFlowInput_1 main ] start
					 */

					currentComponent = "j_init_exec_1_tFixedFlowInput_1";

					tos_count_j_init_exec_1_tFixedFlowInput_1++;

					/**
					 * [j_init_exec_1_tFixedFlowInput_1 main ] stop
					 */

					/**
					 * [j_init_exec_1_tFixedFlowInput_1 process_data_begin ] start
					 */

					currentComponent = "j_init_exec_1_tFixedFlowInput_1";

					/**
					 * [j_init_exec_1_tFixedFlowInput_1 process_data_begin ] stop
					 */

					/**
					 * [j_init_exec_1_tDBOutput_1 main ] start
					 */

					currentComponent = "j_init_exec_1_tDBOutput_1";

					if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

							, "j_init_exec_1_row2", "j_init_exec_1_tFixedFlowInput_1",
							"j_init_exec_1_tFixedFlowInput_1", "tFixedFlowInput", "j_init_exec_1_tDBOutput_1",
							"__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>", "tPostgresqlOutput"

					)) {
						talendJobLogProcess(globalMap);
					}

					if (log.isTraceEnabled()) {
						log.trace("j_init_exec_1_row2 - "
								+ (j_init_exec_1_row2 == null ? "" : j_init_exec_1_row2.toLogString()));
					}

					whetherReject_j_init_exec_1_tDBOutput_1 = false;
					int updateFlag_j_init_exec_1_tDBOutput_1 = 0;
					if (j_init_exec_1_row2.guid == null) {
						pstmtUpdate_j_init_exec_1_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
					} else {
						pstmtUpdate_j_init_exec_1_tDBOutput_1.setString(1, j_init_exec_1_row2.guid);
					}

					if (j_init_exec_1_row2.last_execution_begin_date == null) {
						pstmtUpdate_j_init_exec_1_tDBOutput_1.setNull(2, java.sql.Types.VARCHAR);
					} else {
						pstmtUpdate_j_init_exec_1_tDBOutput_1.setString(2,
								j_init_exec_1_row2.last_execution_begin_date);
					}

					if (j_init_exec_1_row2.last_execution_end_date == null) {
						pstmtUpdate_j_init_exec_1_tDBOutput_1.setNull(3, java.sql.Types.VARCHAR);
					} else {
						pstmtUpdate_j_init_exec_1_tDBOutput_1.setString(3, j_init_exec_1_row2.last_execution_end_date);
					}

					if (j_init_exec_1_row2.last_execution_result == null) {
						pstmtUpdate_j_init_exec_1_tDBOutput_1.setNull(4, java.sql.Types.VARCHAR);
					} else {
						pstmtUpdate_j_init_exec_1_tDBOutput_1.setString(4, j_init_exec_1_row2.last_execution_result);
					}

					if (j_init_exec_1_row2.is_running == null) {
						pstmtUpdate_j_init_exec_1_tDBOutput_1.setNull(5, java.sql.Types.BOOLEAN);
					} else {
						pstmtUpdate_j_init_exec_1_tDBOutput_1.setBoolean(5, j_init_exec_1_row2.is_running);
					}

					if (j_init_exec_1_row2.jobname == null) {
						pstmtUpdate_j_init_exec_1_tDBOutput_1.setNull(6 + count_j_init_exec_1_tDBOutput_1,
								java.sql.Types.VARCHAR);
					} else {
						pstmtUpdate_j_init_exec_1_tDBOutput_1.setString(6 + count_j_init_exec_1_tDBOutput_1,
								j_init_exec_1_row2.jobname);
					}

					try {

						updateFlag_j_init_exec_1_tDBOutput_1 = pstmtUpdate_j_init_exec_1_tDBOutput_1.executeUpdate();
						updatedCount_j_init_exec_1_tDBOutput_1 = updatedCount_j_init_exec_1_tDBOutput_1
								+ updateFlag_j_init_exec_1_tDBOutput_1;
						rowsToCommitCount_j_init_exec_1_tDBOutput_1 += updateFlag_j_init_exec_1_tDBOutput_1;

						if (updateFlag_j_init_exec_1_tDBOutput_1 == 0) {

							if (j_init_exec_1_row2.jobname == null) {
								pstmtInsert_j_init_exec_1_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
							} else {
								pstmtInsert_j_init_exec_1_tDBOutput_1.setString(1, j_init_exec_1_row2.jobname);
							}

							if (j_init_exec_1_row2.guid == null) {
								pstmtInsert_j_init_exec_1_tDBOutput_1.setNull(2, java.sql.Types.VARCHAR);
							} else {
								pstmtInsert_j_init_exec_1_tDBOutput_1.setString(2, j_init_exec_1_row2.guid);
							}

							if (j_init_exec_1_row2.last_execution_begin_date == null) {
								pstmtInsert_j_init_exec_1_tDBOutput_1.setNull(3, java.sql.Types.VARCHAR);
							} else {
								pstmtInsert_j_init_exec_1_tDBOutput_1.setString(3,
										j_init_exec_1_row2.last_execution_begin_date);
							}

							if (j_init_exec_1_row2.last_execution_end_date == null) {
								pstmtInsert_j_init_exec_1_tDBOutput_1.setNull(4, java.sql.Types.VARCHAR);
							} else {
								pstmtInsert_j_init_exec_1_tDBOutput_1.setString(4,
										j_init_exec_1_row2.last_execution_end_date);
							}

							if (j_init_exec_1_row2.last_execution_result == null) {
								pstmtInsert_j_init_exec_1_tDBOutput_1.setNull(5, java.sql.Types.VARCHAR);
							} else {
								pstmtInsert_j_init_exec_1_tDBOutput_1.setString(5,
										j_init_exec_1_row2.last_execution_result);
							}

							if (j_init_exec_1_row2.is_running == null) {
								pstmtInsert_j_init_exec_1_tDBOutput_1.setNull(6, java.sql.Types.BOOLEAN);
							} else {
								pstmtInsert_j_init_exec_1_tDBOutput_1.setBoolean(6, j_init_exec_1_row2.is_running);
							}

							int processedCount_j_init_exec_1_tDBOutput_1 = pstmtInsert_j_init_exec_1_tDBOutput_1
									.executeUpdate();
							insertedCount_j_init_exec_1_tDBOutput_1 += processedCount_j_init_exec_1_tDBOutput_1;
							rowsToCommitCount_j_init_exec_1_tDBOutput_1 += processedCount_j_init_exec_1_tDBOutput_1;
							nb_line_j_init_exec_1_tDBOutput_1++;
							if (log.isDebugEnabled())
								log.debug("j_init_exec_1_tDBOutput_1 - " + ("Inserting") + (" the record ")
										+ (nb_line_j_init_exec_1_tDBOutput_1) + ("."));

						} else {
							nb_line_j_init_exec_1_tDBOutput_1++;

							if (log.isDebugEnabled())
								log.debug("j_init_exec_1_tDBOutput_1 - " + ("Updating") + (" the record ")
										+ (nb_line_j_init_exec_1_tDBOutput_1) + ("."));
						}
					} catch (java.lang.Exception e) {
						globalMap.put("j_init_exec_1_tDBOutput_1_ERROR_MESSAGE", e.getMessage());

						whetherReject_j_init_exec_1_tDBOutput_1 = true;
						nb_line_j_init_exec_1_tDBOutput_1++;
						log.error("j_init_exec_1_tDBOutput_1 - " + (e.getMessage()));
						System.err.print(e.getMessage());
					}

					tos_count_j_init_exec_1_tDBOutput_1++;

					/**
					 * [j_init_exec_1_tDBOutput_1 main ] stop
					 */

					/**
					 * [j_init_exec_1_tDBOutput_1 process_data_begin ] start
					 */

					currentComponent = "j_init_exec_1_tDBOutput_1";

					/**
					 * [j_init_exec_1_tDBOutput_1 process_data_begin ] stop
					 */

					/**
					 * [j_init_exec_1_tDBOutput_1 process_data_end ] start
					 */

					currentComponent = "j_init_exec_1_tDBOutput_1";

					/**
					 * [j_init_exec_1_tDBOutput_1 process_data_end ] stop
					 */

					/**
					 * [j_init_exec_1_tFixedFlowInput_1 process_data_end ] start
					 */

					currentComponent = "j_init_exec_1_tFixedFlowInput_1";

					/**
					 * [j_init_exec_1_tFixedFlowInput_1 process_data_end ] stop
					 */

					/**
					 * [j_init_exec_1_tFixedFlowInput_1 end ] start
					 */

					currentComponent = "j_init_exec_1_tFixedFlowInput_1";

				}
				globalMap.put("j_init_exec_1_tFixedFlowInput_1_NB_LINE", 1);

				ok_Hash.put("j_init_exec_1_tFixedFlowInput_1", true);
				end_Hash.put("j_init_exec_1_tFixedFlowInput_1", System.currentTimeMillis());

				/**
				 * [j_init_exec_1_tFixedFlowInput_1 end ] stop
				 */

				/**
				 * [j_init_exec_1_tDBOutput_1 end ] start
				 */

				currentComponent = "j_init_exec_1_tDBOutput_1";

				if (pstmtUpdate_j_init_exec_1_tDBOutput_1 != null) {
					pstmtUpdate_j_init_exec_1_tDBOutput_1.close();
					resourceMap.remove("pstmtUpdate_j_init_exec_1_tDBOutput_1");
				}
				if (pstmtInsert_j_init_exec_1_tDBOutput_1 != null) {
					pstmtInsert_j_init_exec_1_tDBOutput_1.close();
					resourceMap.remove("pstmtInsert_j_init_exec_1_tDBOutput_1");
				}
				resourceMap.put("statementClosed_j_init_exec_1_tDBOutput_1", true);

				nb_line_deleted_j_init_exec_1_tDBOutput_1 = nb_line_deleted_j_init_exec_1_tDBOutput_1
						+ deletedCount_j_init_exec_1_tDBOutput_1;
				nb_line_update_j_init_exec_1_tDBOutput_1 = nb_line_update_j_init_exec_1_tDBOutput_1
						+ updatedCount_j_init_exec_1_tDBOutput_1;
				nb_line_inserted_j_init_exec_1_tDBOutput_1 = nb_line_inserted_j_init_exec_1_tDBOutput_1
						+ insertedCount_j_init_exec_1_tDBOutput_1;
				nb_line_rejected_j_init_exec_1_tDBOutput_1 = nb_line_rejected_j_init_exec_1_tDBOutput_1
						+ rejectedCount_j_init_exec_1_tDBOutput_1;

				globalMap.put("j_init_exec_1_tDBOutput_1_NB_LINE", nb_line_j_init_exec_1_tDBOutput_1);
				globalMap.put("j_init_exec_1_tDBOutput_1_NB_LINE_UPDATED", nb_line_update_j_init_exec_1_tDBOutput_1);
				globalMap.put("j_init_exec_1_tDBOutput_1_NB_LINE_INSERTED", nb_line_inserted_j_init_exec_1_tDBOutput_1);
				globalMap.put("j_init_exec_1_tDBOutput_1_NB_LINE_DELETED", nb_line_deleted_j_init_exec_1_tDBOutput_1);
				globalMap.put("j_init_exec_1_tDBOutput_1_NB_LINE_REJECTED", nb_line_rejected_j_init_exec_1_tDBOutput_1);

				if (log.isDebugEnabled())
					log.debug("j_init_exec_1_tDBOutput_1 - " + ("Has ") + ("updated") + (" ")
							+ (nb_line_update_j_init_exec_1_tDBOutput_1) + (" record(s)."));
				if (log.isDebugEnabled())
					log.debug("j_init_exec_1_tDBOutput_1 - " + ("Has ") + ("inserted") + (" ")
							+ (nb_line_inserted_j_init_exec_1_tDBOutput_1) + (" record(s)."));

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "j_init_exec_1_row2", 2,
						0, "j_init_exec_1_tFixedFlowInput_1", "j_init_exec_1_tFixedFlowInput_1", "tFixedFlowInput",
						"j_init_exec_1_tDBOutput_1", "__UNIQUE_NAME__<br><b>__TABLE__</b><br><i>__DATA_ACTION__</i>",
						"tPostgresqlOutput", "output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("j_init_exec_1_tDBOutput_1 - " + ("Done."));

				ok_Hash.put("j_init_exec_1_tDBOutput_1", true);
				end_Hash.put("j_init_exec_1_tDBOutput_1", System.currentTimeMillis());

				/**
				 * [j_init_exec_1_tDBOutput_1 end ] stop
				 */

			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_init_exec_1_tFixedFlowInput_1 finally ] start
				 */

				currentComponent = "j_init_exec_1_tFixedFlowInput_1";

				/**
				 * [j_init_exec_1_tFixedFlowInput_1 finally ] stop
				 */

				/**
				 * [j_init_exec_1_tDBOutput_1 finally ] start
				 */

				currentComponent = "j_init_exec_1_tDBOutput_1";

				if (resourceMap.get("statementClosed_j_init_exec_1_tDBOutput_1") == null) {
					java.sql.PreparedStatement pstmtUpdateToClose_j_init_exec_1_tDBOutput_1 = null;
					if ((pstmtUpdateToClose_j_init_exec_1_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap
							.remove("pstmtUpdate_j_init_exec_1_tDBOutput_1")) != null) {
						pstmtUpdateToClose_j_init_exec_1_tDBOutput_1.close();
					}
					java.sql.PreparedStatement pstmtInsertToClose_j_init_exec_1_tDBOutput_1 = null;
					if ((pstmtInsertToClose_j_init_exec_1_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap
							.remove("pstmtInsert_j_init_exec_1_tDBOutput_1")) != null) {
						pstmtInsertToClose_j_init_exec_1_tDBOutput_1.close();
					}
				}

				/**
				 * [j_init_exec_1_tDBOutput_1 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_init_exec_1_tFixedFlowInput_1_SUBPROCESS_STATE", 1);
	}

	public void j_init_exec_1_tDBClose_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("j_init_exec_1_tDBClose_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [j_init_exec_1_tDBClose_1 begin ] start
				 */

				ok_Hash.put("j_init_exec_1_tDBClose_1", false);
				start_Hash.put("j_init_exec_1_tDBClose_1", System.currentTimeMillis());

				currentComponent = "j_init_exec_1_tDBClose_1";

				int tos_count_j_init_exec_1_tDBClose_1 = 0;

				if (log.isDebugEnabled())
					log.debug("j_init_exec_1_tDBClose_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_j_init_exec_1_tDBClose_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_j_init_exec_1_tDBClose_1 = new StringBuilder();
							log4jParamters_j_init_exec_1_tDBClose_1.append("Parameters:");
							log4jParamters_j_init_exec_1_tDBClose_1
									.append("CONNECTION" + " = " + "j_init_exec_1_tDBConnection_1");
							log4jParamters_j_init_exec_1_tDBClose_1.append(" | ");
							log4jParamters_j_init_exec_1_tDBClose_1
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlClose");
							log4jParamters_j_init_exec_1_tDBClose_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("j_init_exec_1_tDBClose_1 - " + (log4jParamters_j_init_exec_1_tDBClose_1));
						}
					}
					new BytesLimit65535_j_init_exec_1_tDBClose_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("j_init_exec_1_tDBClose_1", "__UNIQUE_NAME__<br><b>BDD_OMNICANAL</b>",
							"tPostgresqlClose");
					talendJobLogProcess(globalMap);
				}

				/**
				 * [j_init_exec_1_tDBClose_1 begin ] stop
				 */

				/**
				 * [j_init_exec_1_tDBClose_1 main ] start
				 */

				currentComponent = "j_init_exec_1_tDBClose_1";

				java.sql.Connection conn_j_init_exec_1_tDBClose_1 = (java.sql.Connection) globalMap
						.get("conn_j_init_exec_1_tDBConnection_1");
				if (conn_j_init_exec_1_tDBClose_1 != null && !conn_j_init_exec_1_tDBClose_1.isClosed()) {
					if (log.isDebugEnabled())
						log.debug("j_init_exec_1_tDBClose_1 - " + ("Closing the connection ")
								+ ("conn_j_init_exec_1_tDBConnection_1") + (" to the database."));
					conn_j_init_exec_1_tDBClose_1.close();
					if (log.isDebugEnabled())
						log.debug("j_init_exec_1_tDBClose_1 - " + ("Connection ")
								+ ("conn_j_init_exec_1_tDBConnection_1") + (" to the database has closed."));
				}

				tos_count_j_init_exec_1_tDBClose_1++;

				/**
				 * [j_init_exec_1_tDBClose_1 main ] stop
				 */

				/**
				 * [j_init_exec_1_tDBClose_1 process_data_begin ] start
				 */

				currentComponent = "j_init_exec_1_tDBClose_1";

				/**
				 * [j_init_exec_1_tDBClose_1 process_data_begin ] stop
				 */

				/**
				 * [j_init_exec_1_tDBClose_1 process_data_end ] start
				 */

				currentComponent = "j_init_exec_1_tDBClose_1";

				/**
				 * [j_init_exec_1_tDBClose_1 process_data_end ] stop
				 */

				/**
				 * [j_init_exec_1_tDBClose_1 end ] start
				 */

				currentComponent = "j_init_exec_1_tDBClose_1";

				if (log.isDebugEnabled())
					log.debug("j_init_exec_1_tDBClose_1 - " + ("Done."));

				ok_Hash.put("j_init_exec_1_tDBClose_1", true);
				end_Hash.put("j_init_exec_1_tDBClose_1", System.currentTimeMillis());

				/**
				 * [j_init_exec_1_tDBClose_1 end ] stop
				 */
			} // end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil.addLog("CHECKPOINT",
						"CONNECTION:SUBJOB_OK:j_init_exec_1_tDBClose_1:OnSubjobOk (TRIGGER_OUTPUT_1)", "",
						Thread.currentThread().getId() + "", "", "", "", "", "");
			}

			if (execStat) {
				runStat.updateStatOnConnection("OnSubjobOk2", 0, "ok");
			}

			tDBConnection_1Process(globalMap);

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [j_init_exec_1_tDBClose_1 finally ] start
				 */

				currentComponent = "j_init_exec_1_tDBClose_1";

				/**
				 * [j_init_exec_1_tDBClose_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("j_init_exec_1_tDBClose_1_SUBPROCESS_STATE", 1);
	}

	public void tDBConnection_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tDBConnection_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tDBConnection_1 begin ] start
				 */

				ok_Hash.put("tDBConnection_1", false);
				start_Hash.put("tDBConnection_1", System.currentTimeMillis());

				currentComponent = "tDBConnection_1";

				int tos_count_tDBConnection_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tDBConnection_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tDBConnection_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tDBConnection_1 = new StringBuilder();
							log4jParamters_tDBConnection_1.append("Parameters:");
							log4jParamters_tDBConnection_1.append("DB_VERSION" + " = " + "V9_X");
							log4jParamters_tDBConnection_1.append(" | ");
							log4jParamters_tDBConnection_1.append("HOST" + " = " + "context.g_BDD_OMNICANAL_HOST");
							log4jParamters_tDBConnection_1.append(" | ");
							log4jParamters_tDBConnection_1.append("PORT" + " = " + "context.g_BDD_OMNICANAL_PORT");
							log4jParamters_tDBConnection_1.append(" | ");
							log4jParamters_tDBConnection_1.append("DBNAME" + " = " + "context.g_BDD_OMNICANAL_DBNAME");
							log4jParamters_tDBConnection_1.append(" | ");
							log4jParamters_tDBConnection_1
									.append("SCHEMA_DB" + " = " + "context.g_BDD_OMNICANAL_SCHEMA");
							log4jParamters_tDBConnection_1.append(" | ");
							log4jParamters_tDBConnection_1.append("USER" + " = " + "context.g_BDD_OMNICANAL_USERNAME");
							log4jParamters_tDBConnection_1.append(" | ");
							log4jParamters_tDBConnection_1
									.append("PASS" + " = "
											+ String.valueOf(routines.system.PasswordEncryptUtil
													.encryptPassword(context.g_BDD_OMNICANAL_PWD)).substring(0, 4)
											+ "...");
							log4jParamters_tDBConnection_1.append(" | ");
							log4jParamters_tDBConnection_1.append("USE_SHARED_CONNECTION" + " = " + "true");
							log4jParamters_tDBConnection_1.append(" | ");
							log4jParamters_tDBConnection_1
									.append("SHARED_CONNECTION_NAME" + " = " + "\"BDD_OMNICANAL\"");
							log4jParamters_tDBConnection_1.append(" | ");
							log4jParamters_tDBConnection_1
									.append("PROPERTIES" + " = " + "\"noDatetimeStringSync=true\"");
							log4jParamters_tDBConnection_1.append(" | ");
							log4jParamters_tDBConnection_1.append("AUTO_COMMIT" + " = " + "true");
							log4jParamters_tDBConnection_1.append(" | ");
							log4jParamters_tDBConnection_1
									.append("UNIFIED_COMPONENTS" + " = " + "tPostgresqlConnection");
							log4jParamters_tDBConnection_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tDBConnection_1 - " + (log4jParamters_tDBConnection_1));
						}
					}
					new BytesLimit65535_tDBConnection_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tDBConnection_1",
							"__UNIQUE_NAME__<br><b>BDD_OMNICANAL</b><br><i>CommitAuto</i>", "tPostgresqlConnection");
					talendJobLogProcess(globalMap);
				}

				String dbProperties_tDBConnection_1 = "noDatetimeStringSync=true";
				String url_tDBConnection_1 = "jdbc:postgresql://" + context.g_BDD_OMNICANAL_HOST + ":"
						+ context.g_BDD_OMNICANAL_PORT + "/" + context.g_BDD_OMNICANAL_DBNAME;

				if (dbProperties_tDBConnection_1 != null && !"".equals(dbProperties_tDBConnection_1.trim())) {
					url_tDBConnection_1 = url_tDBConnection_1 + "?" + dbProperties_tDBConnection_1;
				}
				String dbUser_tDBConnection_1 = context.g_BDD_OMNICANAL_USERNAME;

				final String decryptedPassword_tDBConnection_1 = context.g_BDD_OMNICANAL_PWD;
				String dbPwd_tDBConnection_1 = decryptedPassword_tDBConnection_1;

				java.sql.Connection conn_tDBConnection_1 = null;

				java.util.Enumeration<java.sql.Driver> drivers_tDBConnection_1 = java.sql.DriverManager.getDrivers();
				java.util.Set<String> redShiftDriverNames_tDBConnection_1 = new java.util.HashSet<String>(
						java.util.Arrays.asList("com.amazon.redshift.jdbc.Driver", "com.amazon.redshift.jdbc41.Driver",
								"com.amazon.redshift.jdbc42.Driver"));
				while (drivers_tDBConnection_1.hasMoreElements()) {
					java.sql.Driver d_tDBConnection_1 = drivers_tDBConnection_1.nextElement();
					if (redShiftDriverNames_tDBConnection_1.contains(d_tDBConnection_1.getClass().getName())) {
						try {
							java.sql.DriverManager.deregisterDriver(d_tDBConnection_1);
							java.sql.DriverManager.registerDriver(d_tDBConnection_1);
						} catch (java.lang.Exception e_tDBConnection_1) {
							globalMap.put("tDBConnection_1_ERROR_MESSAGE", e_tDBConnection_1.getMessage());
							// do nothing
						}
					}
				}

				SharedDBConnectionLog4j.initLogger(log.getName(), "tDBConnection_1");
				String sharedConnectionName_tDBConnection_1 = "BDD_OMNICANAL";
				conn_tDBConnection_1 = SharedDBConnectionLog4j.getDBConnection("org.postgresql.Driver",
						url_tDBConnection_1, dbUser_tDBConnection_1, dbPwd_tDBConnection_1,
						sharedConnectionName_tDBConnection_1);
				globalMap.put("conn_tDBConnection_1", conn_tDBConnection_1);
				if (null != conn_tDBConnection_1) {

					log.debug("tDBConnection_1 - Connection is set auto commit to 'true'.");
					conn_tDBConnection_1.setAutoCommit(true);
				}

				globalMap.put("schema_" + "tDBConnection_1", context.g_BDD_OMNICANAL_SCHEMA);

				/**
				 * [tDBConnection_1 begin ] stop
				 */

				/**
				 * [tDBConnection_1 main ] start
				 */

				currentComponent = "tDBConnection_1";

				tos_count_tDBConnection_1++;

				/**
				 * [tDBConnection_1 main ] stop
				 */

				/**
				 * [tDBConnection_1 process_data_begin ] start
				 */

				currentComponent = "tDBConnection_1";

				/**
				 * [tDBConnection_1 process_data_begin ] stop
				 */

				/**
				 * [tDBConnection_1 process_data_end ] start
				 */

				currentComponent = "tDBConnection_1";

				/**
				 * [tDBConnection_1 process_data_end ] stop
				 */

				/**
				 * [tDBConnection_1 end ] start
				 */

				currentComponent = "tDBConnection_1";

				if (log.isDebugEnabled())
					log.debug("tDBConnection_1 - " + ("Done."));

				ok_Hash.put("tDBConnection_1", true);
				end_Hash.put("tDBConnection_1", System.currentTimeMillis());

				if (execStat) {
					runStat.updateStatOnConnection("OnComponentOk3", 0, "ok");
				}
				tFTPConnection_1Process(globalMap);

				/**
				 * [tDBConnection_1 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBConnection_1 finally ] start
				 */

				currentComponent = "tDBConnection_1";

				/**
				 * [tDBConnection_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBConnection_1_SUBPROCESS_STATE", 1);
	}

	public void tFTPConnection_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tFTPConnection_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tFTPConnection_1 begin ] start
				 */

				ok_Hash.put("tFTPConnection_1", false);
				start_Hash.put("tFTPConnection_1", System.currentTimeMillis());

				currentComponent = "tFTPConnection_1";

				int tos_count_tFTPConnection_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tFTPConnection_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tFTPConnection_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tFTPConnection_1 = new StringBuilder();
							log4jParamters_tFTPConnection_1.append("Parameters:");
							log4jParamters_tFTPConnection_1.append("HOST" + " = " + "context.g_FTP_GCE_HOST");
							log4jParamters_tFTPConnection_1.append(" | ");
							log4jParamters_tFTPConnection_1.append("PORT" + " = " + "context.g_FTP_GCE_PORT");
							log4jParamters_tFTPConnection_1.append(" | ");
							log4jParamters_tFTPConnection_1.append("USER" + " = " + "context.g_FTP_GCE_USER");
							log4jParamters_tFTPConnection_1.append(" | ");
							log4jParamters_tFTPConnection_1.append("PASS" + " = "
									+ String.valueOf(
											routines.system.PasswordEncryptUtil.encryptPassword(context.g_FTP_GCE_PWD))
											.substring(0, 4)
									+ "...");
							log4jParamters_tFTPConnection_1.append(" | ");
							log4jParamters_tFTPConnection_1.append("SFTP" + " = " + "false");
							log4jParamters_tFTPConnection_1.append(" | ");
							log4jParamters_tFTPConnection_1.append("FTPS" + " = " + "false");
							log4jParamters_tFTPConnection_1.append(" | ");
							log4jParamters_tFTPConnection_1.append("CONNECT_MODE" + " = " + "PASSIVE");
							log4jParamters_tFTPConnection_1.append(" | ");
							log4jParamters_tFTPConnection_1.append("ENCODING" + " = " + "\"ISO-8859-15\"");
							log4jParamters_tFTPConnection_1.append(" | ");
							log4jParamters_tFTPConnection_1.append("USE_PROXY" + " = " + "false");
							log4jParamters_tFTPConnection_1.append(" | ");
							log4jParamters_tFTPConnection_1.append("CONNECTION_TIMEOUT" + " = " + "0");
							log4jParamters_tFTPConnection_1.append(" | ");
							log4jParamters_tFTPConnection_1.append("USE_STRICT_REPLY_PARSING" + " = " + "true");
							log4jParamters_tFTPConnection_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tFTPConnection_1 - " + (log4jParamters_tFTPConnection_1));
						}
					}
					new BytesLimit65535_tFTPConnection_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tFTPConnection_1", "__UNIQUE_NAME__<br><b>GCE</b>", "tFTPConnection");
					talendJobLogProcess(globalMap);
				}

				int connectionTimeout_tFTPConnection_1 = Integer.valueOf(0);
				org.apache.commons.net.ftp.FTPClient ftp_tFTPConnection_1 = null;

				try {
					if (("true").equals(System.getProperty("http.proxySet"))) {

//check if the host is in the excludes for proxy
						boolean isHostIgnored_tFTPConnection_1 = false;
						String nonProxyHostsString_tFTPConnection_1 = System.getProperty("http.nonProxyHosts");
						String[] nonProxyHosts_tFTPConnection_1 = (nonProxyHostsString_tFTPConnection_1 == null)
								? new String[0]
								: nonProxyHostsString_tFTPConnection_1.split("\\|");
						for (String nonProxyHost : nonProxyHosts_tFTPConnection_1) {
							if ((context.g_FTP_GCE_HOST).matches(nonProxyHost.trim())) {
								isHostIgnored_tFTPConnection_1 = true;
								break;
							}
						}
						if (!isHostIgnored_tFTPConnection_1) {
							String httpProxyHost = System.getProperty("http.proxyHost");
							int httpProxyPort = Integer.getInteger("http.proxyPort");
							String httpProxyUser = System.getProperty("http.proxyUser");
							String httpProxyPass = System.getProperty("http.proxyPassword");
							ftp_tFTPConnection_1 = new org.apache.commons.net.ftp.FTPHTTPClient(httpProxyHost,
									httpProxyPort, httpProxyUser, httpProxyPass);
						} else {
							ftp_tFTPConnection_1 = new org.apache.commons.net.ftp.FTPClient();
						}
					} else if ("local".equals(System.getProperty("http.proxySet"))) {
						String uriString = context.g_FTP_GCE_HOST + ":" + 990;
						java.net.Proxy proxyToUse = org.talend.proxy.TalendProxySelector.getInstance()
								.getProxyForUriString(uriString);

						if (!proxyToUse.equals(java.net.Proxy.NO_PROXY)) {
							java.net.InetSocketAddress proxyAddress = (java.net.InetSocketAddress) proxyToUse.address();

							String httpProxyHost = proxyAddress.getAddress().getHostAddress();
							int httpProxyPort = proxyAddress.getPort();
							String httpProxyUser = "";
							String httpProxyPass = ""; // leave it empty if proxy creds weren't specified

							org.talend.proxy.ProxyCreds proxyCreds = org.talend.proxy.TalendProxyAuthenticator
									.getInstance().getCredsForProxyURI(httpProxyHost + ":" + httpProxyPort);
							if (proxyCreds != null) {
								httpProxyUser = proxyCreds.getUser();
								httpProxyPass = proxyCreds.getPass();
							}

							ftp_tFTPConnection_1 = new org.apache.commons.net.ftp.FTPHTTPClient(httpProxyHost,
									httpProxyPort, httpProxyUser, httpProxyPass);

						} else { // no http proxy for ftp host defined
							ftp_tFTPConnection_1 = new org.apache.commons.net.ftp.FTPClient();
						}
					} else {
						ftp_tFTPConnection_1 = new org.apache.commons.net.ftp.FTPClient();
					}

					ftp_tFTPConnection_1.setControlEncoding("ISO-8859-15");

					log.info("tFTPConnection_1 - Attempt to connect to '" + context.g_FTP_GCE_HOST + "' with username '"
							+ context.g_FTP_GCE_USER + "'.");

					if (connectionTimeout_tFTPConnection_1 > 0) {
						ftp_tFTPConnection_1.setDefaultTimeout(connectionTimeout_tFTPConnection_1);
					}

					ftp_tFTPConnection_1.setStrictReplyParsing(true);
					ftp_tFTPConnection_1.connect(context.g_FTP_GCE_HOST, context.g_FTP_GCE_PORT);
					log.info("tFTPConnection_1 - Connect to '" + context.g_FTP_GCE_HOST + "' has succeeded.");

					final String decryptedPassword_tFTPConnection_1 = context.g_FTP_GCE_PWD;

					boolean isLoginSuccessful_tFTPConnection_1 = ftp_tFTPConnection_1.login(context.g_FTP_GCE_USER,
							decryptedPassword_tFTPConnection_1);

					if (!isLoginSuccessful_tFTPConnection_1) {
						throw new RuntimeException("Login failed");
					}

					ftp_tFTPConnection_1.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);
				} catch (Exception e) {
					log.error("tFTPConnection_1 - Can't create connection: " + e.getMessage());
					throw e;
				}

				ftp_tFTPConnection_1.enterLocalPassiveMode();
				log.debug("tFTPConnection_1 - Using the passive mode.");

				globalMap.put("conn_tFTPConnection_1", ftp_tFTPConnection_1);

				/**
				 * [tFTPConnection_1 begin ] stop
				 */

				/**
				 * [tFTPConnection_1 main ] start
				 */

				currentComponent = "tFTPConnection_1";

				tos_count_tFTPConnection_1++;

				/**
				 * [tFTPConnection_1 main ] stop
				 */

				/**
				 * [tFTPConnection_1 process_data_begin ] start
				 */

				currentComponent = "tFTPConnection_1";

				/**
				 * [tFTPConnection_1 process_data_begin ] stop
				 */

				/**
				 * [tFTPConnection_1 process_data_end ] start
				 */

				currentComponent = "tFTPConnection_1";

				/**
				 * [tFTPConnection_1 process_data_end ] stop
				 */

				/**
				 * [tFTPConnection_1 end ] start
				 */

				currentComponent = "tFTPConnection_1";

				if (log.isDebugEnabled())
					log.debug("tFTPConnection_1 - " + ("Done."));

				ok_Hash.put("tFTPConnection_1", true);
				end_Hash.put("tFTPConnection_1", System.currentTimeMillis());

				if (execStat) {
					runStat.updateStatOnConnection("OnComponentOk5", 0, "ok");
				}
				tFTPConnection_2Process(globalMap);

				/**
				 * [tFTPConnection_1 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tFTPConnection_1 finally ] start
				 */

				currentComponent = "tFTPConnection_1";

				/**
				 * [tFTPConnection_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tFTPConnection_1_SUBPROCESS_STATE", 1);
	}

	public void tFTPConnection_2Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tFTPConnection_2_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tFTPConnection_2 begin ] start
				 */

				ok_Hash.put("tFTPConnection_2", false);
				start_Hash.put("tFTPConnection_2", System.currentTimeMillis());

				currentComponent = "tFTPConnection_2";

				int tos_count_tFTPConnection_2 = 0;

				if (log.isDebugEnabled())
					log.debug("tFTPConnection_2 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tFTPConnection_2 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tFTPConnection_2 = new StringBuilder();
							log4jParamters_tFTPConnection_2.append("Parameters:");
							log4jParamters_tFTPConnection_2.append("HOST" + " = " + "context.g_FTP_ONESTOCK_HOST");
							log4jParamters_tFTPConnection_2.append(" | ");
							log4jParamters_tFTPConnection_2.append("PORT" + " = " + "context.g_FTP_ONESTOCK_PORT");
							log4jParamters_tFTPConnection_2.append(" | ");
							log4jParamters_tFTPConnection_2.append("USER" + " = " + "context.g_FTP_ONESTOCK_USER");
							log4jParamters_tFTPConnection_2.append(" | ");
							log4jParamters_tFTPConnection_2
									.append("PASS" + " = "
											+ String.valueOf(routines.system.PasswordEncryptUtil
													.encryptPassword(context.g_FTP_ONESTOCK_PWD)).substring(0, 4)
											+ "...");
							log4jParamters_tFTPConnection_2.append(" | ");
							log4jParamters_tFTPConnection_2.append("SFTP" + " = " + "false");
							log4jParamters_tFTPConnection_2.append(" | ");
							log4jParamters_tFTPConnection_2.append("FTPS" + " = " + "false");
							log4jParamters_tFTPConnection_2.append(" | ");
							log4jParamters_tFTPConnection_2.append("CONNECT_MODE" + " = " + "PASSIVE");
							log4jParamters_tFTPConnection_2.append(" | ");
							log4jParamters_tFTPConnection_2.append("ENCODING" + " = " + "\"ISO-8859-15\"");
							log4jParamters_tFTPConnection_2.append(" | ");
							log4jParamters_tFTPConnection_2.append("USE_PROXY" + " = " + "false");
							log4jParamters_tFTPConnection_2.append(" | ");
							log4jParamters_tFTPConnection_2.append("CONNECTION_TIMEOUT" + " = " + "0");
							log4jParamters_tFTPConnection_2.append(" | ");
							log4jParamters_tFTPConnection_2.append("USE_STRICT_REPLY_PARSING" + " = " + "true");
							log4jParamters_tFTPConnection_2.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tFTPConnection_2 - " + (log4jParamters_tFTPConnection_2));
						}
					}
					new BytesLimit65535_tFTPConnection_2().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tFTPConnection_2", "__UNIQUE_NAME__<br><b>ONESTOCK</b>", "tFTPConnection");
					talendJobLogProcess(globalMap);
				}

				int connectionTimeout_tFTPConnection_2 = Integer.valueOf(0);
				org.apache.commons.net.ftp.FTPClient ftp_tFTPConnection_2 = null;

				try {
					if (("true").equals(System.getProperty("http.proxySet"))) {

//check if the host is in the excludes for proxy
						boolean isHostIgnored_tFTPConnection_2 = false;
						String nonProxyHostsString_tFTPConnection_2 = System.getProperty("http.nonProxyHosts");
						String[] nonProxyHosts_tFTPConnection_2 = (nonProxyHostsString_tFTPConnection_2 == null)
								? new String[0]
								: nonProxyHostsString_tFTPConnection_2.split("\\|");
						for (String nonProxyHost : nonProxyHosts_tFTPConnection_2) {
							if ((context.g_FTP_ONESTOCK_HOST).matches(nonProxyHost.trim())) {
								isHostIgnored_tFTPConnection_2 = true;
								break;
							}
						}
						if (!isHostIgnored_tFTPConnection_2) {
							String httpProxyHost = System.getProperty("http.proxyHost");
							int httpProxyPort = Integer.getInteger("http.proxyPort");
							String httpProxyUser = System.getProperty("http.proxyUser");
							String httpProxyPass = System.getProperty("http.proxyPassword");
							ftp_tFTPConnection_2 = new org.apache.commons.net.ftp.FTPHTTPClient(httpProxyHost,
									httpProxyPort, httpProxyUser, httpProxyPass);
						} else {
							ftp_tFTPConnection_2 = new org.apache.commons.net.ftp.FTPClient();
						}
					} else if ("local".equals(System.getProperty("http.proxySet"))) {
						String uriString = context.g_FTP_ONESTOCK_HOST + ":" + 990;
						java.net.Proxy proxyToUse = org.talend.proxy.TalendProxySelector.getInstance()
								.getProxyForUriString(uriString);

						if (!proxyToUse.equals(java.net.Proxy.NO_PROXY)) {
							java.net.InetSocketAddress proxyAddress = (java.net.InetSocketAddress) proxyToUse.address();

							String httpProxyHost = proxyAddress.getAddress().getHostAddress();
							int httpProxyPort = proxyAddress.getPort();
							String httpProxyUser = "";
							String httpProxyPass = ""; // leave it empty if proxy creds weren't specified

							org.talend.proxy.ProxyCreds proxyCreds = org.talend.proxy.TalendProxyAuthenticator
									.getInstance().getCredsForProxyURI(httpProxyHost + ":" + httpProxyPort);
							if (proxyCreds != null) {
								httpProxyUser = proxyCreds.getUser();
								httpProxyPass = proxyCreds.getPass();
							}

							ftp_tFTPConnection_2 = new org.apache.commons.net.ftp.FTPHTTPClient(httpProxyHost,
									httpProxyPort, httpProxyUser, httpProxyPass);

						} else { // no http proxy for ftp host defined
							ftp_tFTPConnection_2 = new org.apache.commons.net.ftp.FTPClient();
						}
					} else {
						ftp_tFTPConnection_2 = new org.apache.commons.net.ftp.FTPClient();
					}

					ftp_tFTPConnection_2.setControlEncoding("ISO-8859-15");

					log.info("tFTPConnection_2 - Attempt to connect to '" + context.g_FTP_ONESTOCK_HOST
							+ "' with username '" + context.g_FTP_ONESTOCK_USER + "'.");

					if (connectionTimeout_tFTPConnection_2 > 0) {
						ftp_tFTPConnection_2.setDefaultTimeout(connectionTimeout_tFTPConnection_2);
					}

					ftp_tFTPConnection_2.setStrictReplyParsing(true);
					ftp_tFTPConnection_2.connect(context.g_FTP_ONESTOCK_HOST, context.g_FTP_ONESTOCK_PORT);
					log.info("tFTPConnection_2 - Connect to '" + context.g_FTP_ONESTOCK_HOST + "' has succeeded.");

					final String decryptedPassword_tFTPConnection_2 = context.g_FTP_ONESTOCK_PWD;

					boolean isLoginSuccessful_tFTPConnection_2 = ftp_tFTPConnection_2.login(context.g_FTP_ONESTOCK_USER,
							decryptedPassword_tFTPConnection_2);

					if (!isLoginSuccessful_tFTPConnection_2) {
						throw new RuntimeException("Login failed");
					}

					ftp_tFTPConnection_2.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);
				} catch (Exception e) {
					log.error("tFTPConnection_2 - Can't create connection: " + e.getMessage());
					throw e;
				}

				ftp_tFTPConnection_2.enterLocalPassiveMode();
				log.debug("tFTPConnection_2 - Using the passive mode.");

				globalMap.put("conn_tFTPConnection_2", ftp_tFTPConnection_2);

				/**
				 * [tFTPConnection_2 begin ] stop
				 */

				/**
				 * [tFTPConnection_2 main ] start
				 */

				currentComponent = "tFTPConnection_2";

				tos_count_tFTPConnection_2++;

				/**
				 * [tFTPConnection_2 main ] stop
				 */

				/**
				 * [tFTPConnection_2 process_data_begin ] start
				 */

				currentComponent = "tFTPConnection_2";

				/**
				 * [tFTPConnection_2 process_data_begin ] stop
				 */

				/**
				 * [tFTPConnection_2 process_data_end ] start
				 */

				currentComponent = "tFTPConnection_2";

				/**
				 * [tFTPConnection_2 process_data_end ] stop
				 */

				/**
				 * [tFTPConnection_2 end ] start
				 */

				currentComponent = "tFTPConnection_2";

				if (log.isDebugEnabled())
					log.debug("tFTPConnection_2 - " + ("Done."));

				ok_Hash.put("tFTPConnection_2", true);
				end_Hash.put("tFTPConnection_2", System.currentTimeMillis());

				/**
				 * [tFTPConnection_2 end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tFTPConnection_2 finally ] start
				 */

				currentComponent = "tFTPConnection_2";

				/**
				 * [tFTPConnection_2 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tFTPConnection_2_SUBPROCESS_STATE", 1);
	}

	public void talendJobLogProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("talendJobLog_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [talendJobLog begin ] start
				 */

				ok_Hash.put("talendJobLog", false);
				start_Hash.put("talendJobLog", System.currentTimeMillis());

				currentComponent = "talendJobLog";

				int tos_count_talendJobLog = 0;

				for (JobStructureCatcherUtils.JobStructureCatcherMessage jcm : talendJobLog.getMessages()) {
					org.talend.job.audit.JobContextBuilder builder_talendJobLog = org.talend.job.audit.JobContextBuilder
							.create().jobName(jcm.job_name).jobId(jcm.job_id).jobVersion(jcm.job_version)
							.custom("process_id", jcm.pid).custom("thread_id", jcm.tid).custom("pid", pid)
							.custom("father_pid", fatherPid).custom("root_pid", rootPid);
					org.talend.logging.audit.Context log_context_talendJobLog = null;

					if (jcm.log_type == JobStructureCatcherUtils.LogType.PERFORMANCE) {
						long timeMS = jcm.end_time - jcm.start_time;
						String duration = String.valueOf(timeMS);

						log_context_talendJobLog = builder_talendJobLog.sourceId(jcm.sourceId)
								.sourceLabel(jcm.sourceLabel).sourceConnectorType(jcm.sourceComponentName)
								.targetId(jcm.targetId).targetLabel(jcm.targetLabel)
								.targetConnectorType(jcm.targetComponentName).connectionName(jcm.current_connector)
								.rows(jcm.row_count).duration(duration).build();
						auditLogger_talendJobLog.flowExecution(log_context_talendJobLog);
					} else if (jcm.log_type == JobStructureCatcherUtils.LogType.JOBSTART) {
						log_context_talendJobLog = builder_talendJobLog.timestamp(jcm.moment).build();
						auditLogger_talendJobLog.jobstart(log_context_talendJobLog);
					} else if (jcm.log_type == JobStructureCatcherUtils.LogType.JOBEND) {
						long timeMS = jcm.end_time - jcm.start_time;
						String duration = String.valueOf(timeMS);

						log_context_talendJobLog = builder_talendJobLog.timestamp(jcm.moment).duration(duration)
								.status(jcm.status).build();
						auditLogger_talendJobLog.jobstop(log_context_talendJobLog);
					} else if (jcm.log_type == JobStructureCatcherUtils.LogType.RUNCOMPONENT) {
						log_context_talendJobLog = builder_talendJobLog.timestamp(jcm.moment)
								.connectorType(jcm.component_name).connectorId(jcm.component_id)
								.connectorLabel(jcm.component_label).build();
						auditLogger_talendJobLog.runcomponent(log_context_talendJobLog);
					} else if (jcm.log_type == JobStructureCatcherUtils.LogType.FLOWINPUT) {// log current component
																							// input line
						long timeMS = jcm.end_time - jcm.start_time;
						String duration = String.valueOf(timeMS);

						log_context_talendJobLog = builder_talendJobLog.connectorType(jcm.component_name)
								.connectorId(jcm.component_id).connectorLabel(jcm.component_label)
								.connectionName(jcm.current_connector).connectionType(jcm.current_connector_type)
								.rows(jcm.total_row_number).duration(duration).build();
						auditLogger_talendJobLog.flowInput(log_context_talendJobLog);
					} else if (jcm.log_type == JobStructureCatcherUtils.LogType.FLOWOUTPUT) {// log current component
																								// output/reject line
						long timeMS = jcm.end_time - jcm.start_time;
						String duration = String.valueOf(timeMS);

						log_context_talendJobLog = builder_talendJobLog.connectorType(jcm.component_name)
								.connectorId(jcm.component_id).connectorLabel(jcm.component_label)
								.connectionName(jcm.current_connector).connectionType(jcm.current_connector_type)
								.rows(jcm.total_row_number).duration(duration).build();
						auditLogger_talendJobLog.flowOutput(log_context_talendJobLog);
					}

				}

				/**
				 * [talendJobLog begin ] stop
				 */

				/**
				 * [talendJobLog main ] start
				 */

				currentComponent = "talendJobLog";

				tos_count_talendJobLog++;

				/**
				 * [talendJobLog main ] stop
				 */

				/**
				 * [talendJobLog process_data_begin ] start
				 */

				currentComponent = "talendJobLog";

				/**
				 * [talendJobLog process_data_begin ] stop
				 */

				/**
				 * [talendJobLog process_data_end ] start
				 */

				currentComponent = "talendJobLog";

				/**
				 * [talendJobLog process_data_end ] stop
				 */

				/**
				 * [talendJobLog end ] start
				 */

				currentComponent = "talendJobLog";

				ok_Hash.put("talendJobLog", true);
				end_Hash.put("talendJobLog", System.currentTimeMillis());

				/**
				 * [talendJobLog end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [talendJobLog finally ] start
				 */

				currentComponent = "talendJobLog";

				/**
				 * [talendJobLog finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("talendJobLog_SUBPROCESS_STATE", 1);
	}

	public String resuming_logs_dir_path = null;
	public String resuming_checkpoint_path = null;
	public String parent_part_launcher = null;
	private String resumeEntryMethodName = null;
	private boolean globalResumeTicket = false;

	public boolean watch = false;
	// portStats is null, it means don't execute the statistics
	public Integer portStats = null;
	public int portTraces = 4334;
	public String clientHost;
	public String defaultClientHost = "localhost";
	public String contextStr = "DEV";
	public boolean isDefaultContext = true;
	public String pid = "0";
	public String rootPid = null;
	public String fatherPid = null;
	public String fatherNode = null;
	public long startTime = 0;
	public boolean isChildJob = false;
	public String log4jLevel = "";

	private boolean enableLogStash;

	private boolean execStat = true;

	private ThreadLocal<java.util.Map<String, String>> threadLocal = new ThreadLocal<java.util.Map<String, String>>() {
		protected java.util.Map<String, String> initialValue() {
			java.util.Map<String, String> threadRunResultMap = new java.util.HashMap<String, String>();
			threadRunResultMap.put("errorCode", null);
			threadRunResultMap.put("status", "");
			return threadRunResultMap;
		};
	};

	protected PropertiesWithType context_param = new PropertiesWithType();
	public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();

	public String status = "";

	public static void main(String[] args) {
		final j_confirmation_odp j_confirmation_odpClass = new j_confirmation_odp();

		int exitCode = j_confirmation_odpClass.runJobInTOS(args);
		if (exitCode == 0) {
			log.info("TalendJob: 'j_confirmation_odp' - Done.");
		}

		System.exit(exitCode);
	}

	public String[][] runJob(String[] args) {

		int exitCode = runJobInTOS(args);
		String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };

		return bufferValue;
	}

	public boolean hastBufferOutputComponent() {
		boolean hastBufferOutput = false;

		return hastBufferOutput;
	}

	public int runJobInTOS(String[] args) {
		// reset status
		status = "";

		String lastStr = "";
		for (String arg : args) {
			if (arg.equalsIgnoreCase("--context_param")) {
				lastStr = arg;
			} else if (lastStr.equals("")) {
				evalParam(arg);
			} else {
				evalParam(lastStr + " " + arg);
				lastStr = "";
			}
		}
		enableLogStash = "true".equalsIgnoreCase(System.getProperty("audit.enabled"));

		if (!"".equals(log4jLevel)) {

			if ("trace".equalsIgnoreCase(log4jLevel)) {
				org.apache.logging.log4j.core.config.Configurator.setLevel(log.getName(),
						org.apache.logging.log4j.Level.TRACE);
			} else if ("debug".equalsIgnoreCase(log4jLevel)) {
				org.apache.logging.log4j.core.config.Configurator.setLevel(log.getName(),
						org.apache.logging.log4j.Level.DEBUG);
			} else if ("info".equalsIgnoreCase(log4jLevel)) {
				org.apache.logging.log4j.core.config.Configurator.setLevel(log.getName(),
						org.apache.logging.log4j.Level.INFO);
			} else if ("warn".equalsIgnoreCase(log4jLevel)) {
				org.apache.logging.log4j.core.config.Configurator.setLevel(log.getName(),
						org.apache.logging.log4j.Level.WARN);
			} else if ("error".equalsIgnoreCase(log4jLevel)) {
				org.apache.logging.log4j.core.config.Configurator.setLevel(log.getName(),
						org.apache.logging.log4j.Level.ERROR);
			} else if ("fatal".equalsIgnoreCase(log4jLevel)) {
				org.apache.logging.log4j.core.config.Configurator.setLevel(log.getName(),
						org.apache.logging.log4j.Level.FATAL);
			} else if ("off".equalsIgnoreCase(log4jLevel)) {
				org.apache.logging.log4j.core.config.Configurator.setLevel(log.getName(),
						org.apache.logging.log4j.Level.OFF);
			}
			org.apache.logging.log4j.core.config.Configurator
					.setLevel(org.apache.logging.log4j.LogManager.getRootLogger().getName(), log.getLevel());

		}
		log.info("TalendJob: 'j_confirmation_odp' - Start.");

		if (enableLogStash) {
			java.util.Properties properties_talendJobLog = new java.util.Properties();
			properties_talendJobLog.setProperty("root.logger", "audit");
			properties_talendJobLog.setProperty("encoding", "UTF-8");
			properties_talendJobLog.setProperty("application.name", "Talend Studio");
			properties_talendJobLog.setProperty("service.name", "Talend Studio Job");
			properties_talendJobLog.setProperty("instance.name", "Talend Studio Job Instance");
			properties_talendJobLog.setProperty("propagate.appender.exceptions", "none");
			properties_talendJobLog.setProperty("log.appender", "file");
			properties_talendJobLog.setProperty("appender.file.path", "audit.json");
			properties_talendJobLog.setProperty("appender.file.maxsize", "52428800");
			properties_talendJobLog.setProperty("appender.file.maxbackup", "20");
			properties_talendJobLog.setProperty("host", "false");

			System.getProperties().stringPropertyNames().stream().filter(it -> it.startsWith("audit.logger."))
					.forEach(key -> properties_talendJobLog.setProperty(key.substring("audit.logger.".length()),
							System.getProperty(key)));

			org.apache.logging.log4j.core.config.Configurator
					.setLevel(properties_talendJobLog.getProperty("root.logger"), org.apache.logging.log4j.Level.DEBUG);

			auditLogger_talendJobLog = org.talend.job.audit.JobEventAuditLoggerFactory
					.createJobAuditLogger(properties_talendJobLog);
		}

		if (clientHost == null) {
			clientHost = defaultClientHost;
		}

		if (pid == null || "0".equals(pid)) {
			pid = TalendString.getAsciiRandomString(6);
		}

		if (rootPid == null) {
			rootPid = pid;
		}
		if (fatherPid == null) {
			fatherPid = pid;
		} else {
			isChildJob = true;
		}

		if (portStats != null) {
			// portStats = -1; //for testing
			if (portStats < 0 || portStats > 65535) {
				// issue:10869, the portStats is invalid, so this client socket can't open
				System.err.println("The statistics socket port " + portStats + " is invalid.");
				execStat = false;
			}
		} else {
			execStat = false;
		}
		boolean inOSGi = routines.system.BundleUtils.inOSGi();

		if (inOSGi) {
			java.util.Dictionary<String, Object> jobProperties = routines.system.BundleUtils.getJobProperties(jobName);

			if (jobProperties != null && jobProperties.get("context") != null) {
				contextStr = (String) jobProperties.get("context");
			}
		}

		try {
			// call job/subjob with an existing context, like: --context=production. if
			// without this parameter, there will use the default context instead.
			java.io.InputStream inContext = j_confirmation_odp.class.getClassLoader().getResourceAsStream(
					"apex_omnicanal/j_confirmation_odp_0_1/contexts/" + contextStr + ".properties");
			if (inContext == null) {
				inContext = j_confirmation_odp.class.getClassLoader()
						.getResourceAsStream("config/contexts/" + contextStr + ".properties");
			}
			if (inContext != null) {
				try {
					// defaultProps is in order to keep the original context value
					if (context != null && context.isEmpty()) {
						defaultProps.load(inContext);
						context = new ContextProperties(defaultProps);
					}
				} finally {
					inContext.close();
				}
			} else if (!isDefaultContext) {
				// print info and job continue to run, for case: context_param is not empty.
				System.err.println("Could not find the context " + contextStr);
			}

			if (!context_param.isEmpty()) {
				context.putAll(context_param);
				// set types for params from parentJobs
				for (Object key : context_param.keySet()) {
					String context_key = key.toString();
					String context_type = context_param.getContextType(context_key);
					context.setContextType(context_key, context_type);

				}
			}
			class ContextProcessing {
				private void processContext_0() {
					context.setContextType("l_FTP_FILE_MASK", "id_String");
					if (context.getStringValue("l_FTP_FILE_MASK") == null) {
						context.l_FTP_FILE_MASK = null;
					} else {
						context.l_FTP_FILE_MASK = (String) context.getProperty("l_FTP_FILE_MASK");
					}
					context.setContextType("l_FTP_INPUT_PATH", "id_String");
					if (context.getStringValue("l_FTP_INPUT_PATH") == null) {
						context.l_FTP_INPUT_PATH = null;
					} else {
						context.l_FTP_INPUT_PATH = (String) context.getProperty("l_FTP_INPUT_PATH");
					}
					context.setContextType("l_FTP_OUPUT_PATH", "id_String");
					if (context.getStringValue("l_FTP_OUPUT_PATH") == null) {
						context.l_FTP_OUPUT_PATH = null;
					} else {
						context.l_FTP_OUPUT_PATH = (String) context.getProperty("l_FTP_OUPUT_PATH");
					}
					context.setContextType("g_ALERTING_BCC_EMAIL", "id_String");
					if (context.getStringValue("g_ALERTING_BCC_EMAIL") == null) {
						context.g_ALERTING_BCC_EMAIL = null;
					} else {
						context.g_ALERTING_BCC_EMAIL = (String) context.getProperty("g_ALERTING_BCC_EMAIL");
					}
					context.setContextType("g_ALERTING_CC_EMAIL", "id_String");
					if (context.getStringValue("g_ALERTING_CC_EMAIL") == null) {
						context.g_ALERTING_CC_EMAIL = null;
					} else {
						context.g_ALERTING_CC_EMAIL = (String) context.getProperty("g_ALERTING_CC_EMAIL");
					}
					context.setContextType("g_ALERTING_FROM_EMAIL", "id_String");
					if (context.getStringValue("g_ALERTING_FROM_EMAIL") == null) {
						context.g_ALERTING_FROM_EMAIL = null;
					} else {
						context.g_ALERTING_FROM_EMAIL = (String) context.getProperty("g_ALERTING_FROM_EMAIL");
					}
					context.setContextType("g_ALERTING_OBJECT", "id_String");
					if (context.getStringValue("g_ALERTING_OBJECT") == null) {
						context.g_ALERTING_OBJECT = null;
					} else {
						context.g_ALERTING_OBJECT = (String) context.getProperty("g_ALERTING_OBJECT");
					}
					context.setContextType("g_ALERTING_SMTP_HOST", "id_String");
					if (context.getStringValue("g_ALERTING_SMTP_HOST") == null) {
						context.g_ALERTING_SMTP_HOST = null;
					} else {
						context.g_ALERTING_SMTP_HOST = (String) context.getProperty("g_ALERTING_SMTP_HOST");
					}
					context.setContextType("g_ALERTING_SMTP_PORT", "id_Integer");
					if (context.getStringValue("g_ALERTING_SMTP_PORT") == null) {
						context.g_ALERTING_SMTP_PORT = null;
					} else {
						try {
							context.g_ALERTING_SMTP_PORT = routines.system.ParserUtils
									.parseTo_Integer(context.getProperty("g_ALERTING_SMTP_PORT"));
						} catch (NumberFormatException e) {
							log.warn(String.format("Null value will be used for context parameter %s: %s",
									"g_ALERTING_SMTP_PORT", e.getMessage()));
							context.g_ALERTING_SMTP_PORT = null;
						}
					}
					context.setContextType("g_ALERTING_SMTP_PWD", "id_Password");
					if (context.getStringValue("g_ALERTING_SMTP_PWD") == null) {
						context.g_ALERTING_SMTP_PWD = null;
					} else {
						String pwd_g_ALERTING_SMTP_PWD_value = context.getProperty("g_ALERTING_SMTP_PWD");
						context.g_ALERTING_SMTP_PWD = null;
						if (pwd_g_ALERTING_SMTP_PWD_value != null) {
							if (context_param.containsKey("g_ALERTING_SMTP_PWD")) {// no need to decrypt if it come from
																					// program argument or parent job
																					// runtime
								context.g_ALERTING_SMTP_PWD = pwd_g_ALERTING_SMTP_PWD_value;
							} else if (!pwd_g_ALERTING_SMTP_PWD_value.isEmpty()) {
								try {
									context.g_ALERTING_SMTP_PWD = routines.system.PasswordEncryptUtil
											.decryptPassword(pwd_g_ALERTING_SMTP_PWD_value);
									context.put("g_ALERTING_SMTP_PWD", context.g_ALERTING_SMTP_PWD);
								} catch (java.lang.RuntimeException e) {
									// do nothing
								}
							}
						}
					}
					context.setContextType("g_ALERTING_SMTP_USERNAME", "id_String");
					if (context.getStringValue("g_ALERTING_SMTP_USERNAME") == null) {
						context.g_ALERTING_SMTP_USERNAME = null;
					} else {
						context.g_ALERTING_SMTP_USERNAME = (String) context.getProperty("g_ALERTING_SMTP_USERNAME");
					}
					context.setContextType("g_ALERTING_TARGET_EMAIL", "id_String");
					if (context.getStringValue("g_ALERTING_TARGET_EMAIL") == null) {
						context.g_ALERTING_TARGET_EMAIL = null;
					} else {
						context.g_ALERTING_TARGET_EMAIL = (String) context.getProperty("g_ALERTING_TARGET_EMAIL");
					}
					context.setContextType("g_BDD_OMNICANAL_DBNAME", "id_String");
					if (context.getStringValue("g_BDD_OMNICANAL_DBNAME") == null) {
						context.g_BDD_OMNICANAL_DBNAME = null;
					} else {
						context.g_BDD_OMNICANAL_DBNAME = (String) context.getProperty("g_BDD_OMNICANAL_DBNAME");
					}
					context.setContextType("g_BDD_OMNICANAL_HOST", "id_String");
					if (context.getStringValue("g_BDD_OMNICANAL_HOST") == null) {
						context.g_BDD_OMNICANAL_HOST = null;
					} else {
						context.g_BDD_OMNICANAL_HOST = (String) context.getProperty("g_BDD_OMNICANAL_HOST");
					}
					context.setContextType("g_BDD_OMNICANAL_PORT", "id_String");
					if (context.getStringValue("g_BDD_OMNICANAL_PORT") == null) {
						context.g_BDD_OMNICANAL_PORT = null;
					} else {
						context.g_BDD_OMNICANAL_PORT = (String) context.getProperty("g_BDD_OMNICANAL_PORT");
					}
					context.setContextType("g_BDD_OMNICANAL_PWD", "id_Password");
					if (context.getStringValue("g_BDD_OMNICANAL_PWD") == null) {
						context.g_BDD_OMNICANAL_PWD = null;
					} else {
						String pwd_g_BDD_OMNICANAL_PWD_value = context.getProperty("g_BDD_OMNICANAL_PWD");
						context.g_BDD_OMNICANAL_PWD = null;
						if (pwd_g_BDD_OMNICANAL_PWD_value != null) {
							if (context_param.containsKey("g_BDD_OMNICANAL_PWD")) {// no need to decrypt if it come from
																					// program argument or parent job
																					// runtime
								context.g_BDD_OMNICANAL_PWD = pwd_g_BDD_OMNICANAL_PWD_value;
							} else if (!pwd_g_BDD_OMNICANAL_PWD_value.isEmpty()) {
								try {
									context.g_BDD_OMNICANAL_PWD = routines.system.PasswordEncryptUtil
											.decryptPassword(pwd_g_BDD_OMNICANAL_PWD_value);
									context.put("g_BDD_OMNICANAL_PWD", context.g_BDD_OMNICANAL_PWD);
								} catch (java.lang.RuntimeException e) {
									// do nothing
								}
							}
						}
					}
					context.setContextType("g_BDD_OMNICANAL_SCHEMA", "id_String");
					if (context.getStringValue("g_BDD_OMNICANAL_SCHEMA") == null) {
						context.g_BDD_OMNICANAL_SCHEMA = null;
					} else {
						context.g_BDD_OMNICANAL_SCHEMA = (String) context.getProperty("g_BDD_OMNICANAL_SCHEMA");
					}
					context.setContextType("g_BDD_OMNICANAL_USERNAME", "id_String");
					if (context.getStringValue("g_BDD_OMNICANAL_USERNAME") == null) {
						context.g_BDD_OMNICANAL_USERNAME = null;
					} else {
						context.g_BDD_OMNICANAL_USERNAME = (String) context.getProperty("g_BDD_OMNICANAL_USERNAME");
					}
					context.setContextType("g_FTP_GCE_HOST", "id_String");
					if (context.getStringValue("g_FTP_GCE_HOST") == null) {
						context.g_FTP_GCE_HOST = null;
					} else {
						context.g_FTP_GCE_HOST = (String) context.getProperty("g_FTP_GCE_HOST");
					}
					context.setContextType("g_FTP_GCE_PATH", "id_String");
					if (context.getStringValue("g_FTP_GCE_PATH") == null) {
						context.g_FTP_GCE_PATH = null;
					} else {
						context.g_FTP_GCE_PATH = (String) context.getProperty("g_FTP_GCE_PATH");
					}
					context.setContextType("g_FTP_GCE_PORT", "id_Integer");
					if (context.getStringValue("g_FTP_GCE_PORT") == null) {
						context.g_FTP_GCE_PORT = null;
					} else {
						try {
							context.g_FTP_GCE_PORT = routines.system.ParserUtils
									.parseTo_Integer(context.getProperty("g_FTP_GCE_PORT"));
						} catch (NumberFormatException e) {
							log.warn(String.format("Null value will be used for context parameter %s: %s",
									"g_FTP_GCE_PORT", e.getMessage()));
							context.g_FTP_GCE_PORT = null;
						}
					}
					context.setContextType("g_FTP_GCE_PWD", "id_String");
					if (context.getStringValue("g_FTP_GCE_PWD") == null) {
						context.g_FTP_GCE_PWD = null;
					} else {
						context.g_FTP_GCE_PWD = (String) context.getProperty("g_FTP_GCE_PWD");
					}
					context.setContextType("g_FTP_GCE_USER", "id_String");
					if (context.getStringValue("g_FTP_GCE_USER") == null) {
						context.g_FTP_GCE_USER = null;
					} else {
						context.g_FTP_GCE_USER = (String) context.getProperty("g_FTP_GCE_USER");
					}
					context.setContextType("g_FTP_ONESTOCK_HOST", "id_String");
					if (context.getStringValue("g_FTP_ONESTOCK_HOST") == null) {
						context.g_FTP_ONESTOCK_HOST = null;
					} else {
						context.g_FTP_ONESTOCK_HOST = (String) context.getProperty("g_FTP_ONESTOCK_HOST");
					}
					context.setContextType("g_FTP_ONESTOCK_PATH", "id_String");
					if (context.getStringValue("g_FTP_ONESTOCK_PATH") == null) {
						context.g_FTP_ONESTOCK_PATH = null;
					} else {
						context.g_FTP_ONESTOCK_PATH = (String) context.getProperty("g_FTP_ONESTOCK_PATH");
					}
					context.setContextType("g_FTP_ONESTOCK_PORT", "id_Integer");
					if (context.getStringValue("g_FTP_ONESTOCK_PORT") == null) {
						context.g_FTP_ONESTOCK_PORT = null;
					} else {
						try {
							context.g_FTP_ONESTOCK_PORT = routines.system.ParserUtils
									.parseTo_Integer(context.getProperty("g_FTP_ONESTOCK_PORT"));
						} catch (NumberFormatException e) {
							log.warn(String.format("Null value will be used for context parameter %s: %s",
									"g_FTP_ONESTOCK_PORT", e.getMessage()));
							context.g_FTP_ONESTOCK_PORT = null;
						}
					}
					context.setContextType("g_FTP_ONESTOCK_PWD", "id_String");
					if (context.getStringValue("g_FTP_ONESTOCK_PWD") == null) {
						context.g_FTP_ONESTOCK_PWD = null;
					} else {
						context.g_FTP_ONESTOCK_PWD = (String) context.getProperty("g_FTP_ONESTOCK_PWD");
					}
					context.setContextType("g_FTP_ONESTOCK_USER", "id_String");
					if (context.getStringValue("g_FTP_ONESTOCK_USER") == null) {
						context.g_FTP_ONESTOCK_USER = null;
					} else {
						context.g_FTP_ONESTOCK_USER = (String) context.getProperty("g_FTP_ONESTOCK_USER");
					}
					context.setContextType("g_SFTP_MAGENTO_HOST", "id_String");
					if (context.getStringValue("g_SFTP_MAGENTO_HOST") == null) {
						context.g_SFTP_MAGENTO_HOST = null;
					} else {
						context.g_SFTP_MAGENTO_HOST = (String) context.getProperty("g_SFTP_MAGENTO_HOST");
					}
					context.setContextType("g_SFTP_MAGENTO_LOGIN", "id_String");
					if (context.getStringValue("g_SFTP_MAGENTO_LOGIN") == null) {
						context.g_SFTP_MAGENTO_LOGIN = null;
					} else {
						context.g_SFTP_MAGENTO_LOGIN = (String) context.getProperty("g_SFTP_MAGENTO_LOGIN");
					}
					context.setContextType("g_SFTP_MAGENTO_PASSWORD", "id_String");
					if (context.getStringValue("g_SFTP_MAGENTO_PASSWORD") == null) {
						context.g_SFTP_MAGENTO_PASSWORD = null;
					} else {
						context.g_SFTP_MAGENTO_PASSWORD = (String) context.getProperty("g_SFTP_MAGENTO_PASSWORD");
					}
					context.setContextType("g_SFTP_MAGENTO_PATH", "id_String");
					if (context.getStringValue("g_SFTP_MAGENTO_PATH") == null) {
						context.g_SFTP_MAGENTO_PATH = null;
					} else {
						context.g_SFTP_MAGENTO_PATH = (String) context.getProperty("g_SFTP_MAGENTO_PATH");
					}
					context.setContextType("g_SFTP_MAGENTO_PORT", "id_String");
					if (context.getStringValue("g_SFTP_MAGENTO_PORT") == null) {
						context.g_SFTP_MAGENTO_PORT = null;
					} else {
						context.g_SFTP_MAGENTO_PORT = (String) context.getProperty("g_SFTP_MAGENTO_PORT");
					}
					context.setContextType("g_SFTP_ONESTOCK_HOST", "id_String");
					if (context.getStringValue("g_SFTP_ONESTOCK_HOST") == null) {
						context.g_SFTP_ONESTOCK_HOST = null;
					} else {
						context.g_SFTP_ONESTOCK_HOST = (String) context.getProperty("g_SFTP_ONESTOCK_HOST");
					}
					context.setContextType("g_SFTP_ONESTOCK_LOGIN", "id_String");
					if (context.getStringValue("g_SFTP_ONESTOCK_LOGIN") == null) {
						context.g_SFTP_ONESTOCK_LOGIN = null;
					} else {
						context.g_SFTP_ONESTOCK_LOGIN = (String) context.getProperty("g_SFTP_ONESTOCK_LOGIN");
					}
					context.setContextType("g_SFTP_ONESTOCK_PASSWORD", "id_String");
					if (context.getStringValue("g_SFTP_ONESTOCK_PASSWORD") == null) {
						context.g_SFTP_ONESTOCK_PASSWORD = null;
					} else {
						context.g_SFTP_ONESTOCK_PASSWORD = (String) context.getProperty("g_SFTP_ONESTOCK_PASSWORD");
					}
					context.setContextType("g_SFTP_ONESTOCK_PATH", "id_String");
					if (context.getStringValue("g_SFTP_ONESTOCK_PATH") == null) {
						context.g_SFTP_ONESTOCK_PATH = null;
					} else {
						context.g_SFTP_ONESTOCK_PATH = (String) context.getProperty("g_SFTP_ONESTOCK_PATH");
					}
					context.setContextType("g_SFTP_ONESTOCK_PORT", "id_String");
					if (context.getStringValue("g_SFTP_ONESTOCK_PORT") == null) {
						context.g_SFTP_ONESTOCK_PORT = null;
					} else {
						context.g_SFTP_ONESTOCK_PORT = (String) context.getProperty("g_SFTP_ONESTOCK_PORT");
					}
					context.setContextType("g_SFTP_TALEND_HOST", "id_String");
					if (context.getStringValue("g_SFTP_TALEND_HOST") == null) {
						context.g_SFTP_TALEND_HOST = null;
					} else {
						context.g_SFTP_TALEND_HOST = (String) context.getProperty("g_SFTP_TALEND_HOST");
					}
					context.setContextType("g_SFTP_TALEND_LOGIN", "id_String");
					if (context.getStringValue("g_SFTP_TALEND_LOGIN") == null) {
						context.g_SFTP_TALEND_LOGIN = null;
					} else {
						context.g_SFTP_TALEND_LOGIN = (String) context.getProperty("g_SFTP_TALEND_LOGIN");
					}
					context.setContextType("g_SFTP_TALEND_PASSWORD", "id_String");
					if (context.getStringValue("g_SFTP_TALEND_PASSWORD") == null) {
						context.g_SFTP_TALEND_PASSWORD = null;
					} else {
						context.g_SFTP_TALEND_PASSWORD = (String) context.getProperty("g_SFTP_TALEND_PASSWORD");
					}
					context.setContextType("g_SFTP_TALEND_PATH", "id_String");
					if (context.getStringValue("g_SFTP_TALEND_PATH") == null) {
						context.g_SFTP_TALEND_PATH = null;
					} else {
						context.g_SFTP_TALEND_PATH = (String) context.getProperty("g_SFTP_TALEND_PATH");
					}
					context.setContextType("g_SFTP_TALEND_PORT", "id_String");
					if (context.getStringValue("g_SFTP_TALEND_PORT") == null) {
						context.g_SFTP_TALEND_PORT = null;
					} else {
						context.g_SFTP_TALEND_PORT = (String) context.getProperty("g_SFTP_TALEND_PORT");
					}
					context.setContextType("g_VAR_TALEND_WORKSPACE", "id_String");
					if (context.getStringValue("g_VAR_TALEND_WORKSPACE") == null) {
						context.g_VAR_TALEND_WORKSPACE = null;
					} else {
						context.g_VAR_TALEND_WORKSPACE = (String) context.getProperty("g_VAR_TALEND_WORKSPACE");
					}
				}

				public void processAllContext() {
					processContext_0();
				}
			}

			new ContextProcessing().processAllContext();
		} catch (java.io.IOException ie) {
			System.err.println("Could not load context " + contextStr);
			ie.printStackTrace();
		}

		// get context value from parent directly
		if (parentContextMap != null && !parentContextMap.isEmpty()) {
			if (parentContextMap.containsKey("l_FTP_FILE_MASK")) {
				context.l_FTP_FILE_MASK = (String) parentContextMap.get("l_FTP_FILE_MASK");
			}
			if (parentContextMap.containsKey("l_FTP_INPUT_PATH")) {
				context.l_FTP_INPUT_PATH = (String) parentContextMap.get("l_FTP_INPUT_PATH");
			}
			if (parentContextMap.containsKey("l_FTP_OUPUT_PATH")) {
				context.l_FTP_OUPUT_PATH = (String) parentContextMap.get("l_FTP_OUPUT_PATH");
			}
			if (parentContextMap.containsKey("g_ALERTING_BCC_EMAIL")) {
				context.g_ALERTING_BCC_EMAIL = (String) parentContextMap.get("g_ALERTING_BCC_EMAIL");
			}
			if (parentContextMap.containsKey("g_ALERTING_CC_EMAIL")) {
				context.g_ALERTING_CC_EMAIL = (String) parentContextMap.get("g_ALERTING_CC_EMAIL");
			}
			if (parentContextMap.containsKey("g_ALERTING_FROM_EMAIL")) {
				context.g_ALERTING_FROM_EMAIL = (String) parentContextMap.get("g_ALERTING_FROM_EMAIL");
			}
			if (parentContextMap.containsKey("g_ALERTING_OBJECT")) {
				context.g_ALERTING_OBJECT = (String) parentContextMap.get("g_ALERTING_OBJECT");
			}
			if (parentContextMap.containsKey("g_ALERTING_SMTP_HOST")) {
				context.g_ALERTING_SMTP_HOST = (String) parentContextMap.get("g_ALERTING_SMTP_HOST");
			}
			if (parentContextMap.containsKey("g_ALERTING_SMTP_PORT")) {
				context.g_ALERTING_SMTP_PORT = (Integer) parentContextMap.get("g_ALERTING_SMTP_PORT");
			}
			if (parentContextMap.containsKey("g_ALERTING_SMTP_PWD")) {
				context.g_ALERTING_SMTP_PWD = (java.lang.String) parentContextMap.get("g_ALERTING_SMTP_PWD");
			}
			if (parentContextMap.containsKey("g_ALERTING_SMTP_USERNAME")) {
				context.g_ALERTING_SMTP_USERNAME = (String) parentContextMap.get("g_ALERTING_SMTP_USERNAME");
			}
			if (parentContextMap.containsKey("g_ALERTING_TARGET_EMAIL")) {
				context.g_ALERTING_TARGET_EMAIL = (String) parentContextMap.get("g_ALERTING_TARGET_EMAIL");
			}
			if (parentContextMap.containsKey("g_BDD_OMNICANAL_DBNAME")) {
				context.g_BDD_OMNICANAL_DBNAME = (String) parentContextMap.get("g_BDD_OMNICANAL_DBNAME");
			}
			if (parentContextMap.containsKey("g_BDD_OMNICANAL_HOST")) {
				context.g_BDD_OMNICANAL_HOST = (String) parentContextMap.get("g_BDD_OMNICANAL_HOST");
			}
			if (parentContextMap.containsKey("g_BDD_OMNICANAL_PORT")) {
				context.g_BDD_OMNICANAL_PORT = (String) parentContextMap.get("g_BDD_OMNICANAL_PORT");
			}
			if (parentContextMap.containsKey("g_BDD_OMNICANAL_PWD")) {
				context.g_BDD_OMNICANAL_PWD = (java.lang.String) parentContextMap.get("g_BDD_OMNICANAL_PWD");
			}
			if (parentContextMap.containsKey("g_BDD_OMNICANAL_SCHEMA")) {
				context.g_BDD_OMNICANAL_SCHEMA = (String) parentContextMap.get("g_BDD_OMNICANAL_SCHEMA");
			}
			if (parentContextMap.containsKey("g_BDD_OMNICANAL_USERNAME")) {
				context.g_BDD_OMNICANAL_USERNAME = (String) parentContextMap.get("g_BDD_OMNICANAL_USERNAME");
			}
			if (parentContextMap.containsKey("g_FTP_GCE_HOST")) {
				context.g_FTP_GCE_HOST = (String) parentContextMap.get("g_FTP_GCE_HOST");
			}
			if (parentContextMap.containsKey("g_FTP_GCE_PATH")) {
				context.g_FTP_GCE_PATH = (String) parentContextMap.get("g_FTP_GCE_PATH");
			}
			if (parentContextMap.containsKey("g_FTP_GCE_PORT")) {
				context.g_FTP_GCE_PORT = (Integer) parentContextMap.get("g_FTP_GCE_PORT");
			}
			if (parentContextMap.containsKey("g_FTP_GCE_PWD")) {
				context.g_FTP_GCE_PWD = (String) parentContextMap.get("g_FTP_GCE_PWD");
			}
			if (parentContextMap.containsKey("g_FTP_GCE_USER")) {
				context.g_FTP_GCE_USER = (String) parentContextMap.get("g_FTP_GCE_USER");
			}
			if (parentContextMap.containsKey("g_FTP_ONESTOCK_HOST")) {
				context.g_FTP_ONESTOCK_HOST = (String) parentContextMap.get("g_FTP_ONESTOCK_HOST");
			}
			if (parentContextMap.containsKey("g_FTP_ONESTOCK_PATH")) {
				context.g_FTP_ONESTOCK_PATH = (String) parentContextMap.get("g_FTP_ONESTOCK_PATH");
			}
			if (parentContextMap.containsKey("g_FTP_ONESTOCK_PORT")) {
				context.g_FTP_ONESTOCK_PORT = (Integer) parentContextMap.get("g_FTP_ONESTOCK_PORT");
			}
			if (parentContextMap.containsKey("g_FTP_ONESTOCK_PWD")) {
				context.g_FTP_ONESTOCK_PWD = (String) parentContextMap.get("g_FTP_ONESTOCK_PWD");
			}
			if (parentContextMap.containsKey("g_FTP_ONESTOCK_USER")) {
				context.g_FTP_ONESTOCK_USER = (String) parentContextMap.get("g_FTP_ONESTOCK_USER");
			}
			if (parentContextMap.containsKey("g_SFTP_MAGENTO_HOST")) {
				context.g_SFTP_MAGENTO_HOST = (String) parentContextMap.get("g_SFTP_MAGENTO_HOST");
			}
			if (parentContextMap.containsKey("g_SFTP_MAGENTO_LOGIN")) {
				context.g_SFTP_MAGENTO_LOGIN = (String) parentContextMap.get("g_SFTP_MAGENTO_LOGIN");
			}
			if (parentContextMap.containsKey("g_SFTP_MAGENTO_PASSWORD")) {
				context.g_SFTP_MAGENTO_PASSWORD = (String) parentContextMap.get("g_SFTP_MAGENTO_PASSWORD");
			}
			if (parentContextMap.containsKey("g_SFTP_MAGENTO_PATH")) {
				context.g_SFTP_MAGENTO_PATH = (String) parentContextMap.get("g_SFTP_MAGENTO_PATH");
			}
			if (parentContextMap.containsKey("g_SFTP_MAGENTO_PORT")) {
				context.g_SFTP_MAGENTO_PORT = (String) parentContextMap.get("g_SFTP_MAGENTO_PORT");
			}
			if (parentContextMap.containsKey("g_SFTP_ONESTOCK_HOST")) {
				context.g_SFTP_ONESTOCK_HOST = (String) parentContextMap.get("g_SFTP_ONESTOCK_HOST");
			}
			if (parentContextMap.containsKey("g_SFTP_ONESTOCK_LOGIN")) {
				context.g_SFTP_ONESTOCK_LOGIN = (String) parentContextMap.get("g_SFTP_ONESTOCK_LOGIN");
			}
			if (parentContextMap.containsKey("g_SFTP_ONESTOCK_PASSWORD")) {
				context.g_SFTP_ONESTOCK_PASSWORD = (String) parentContextMap.get("g_SFTP_ONESTOCK_PASSWORD");
			}
			if (parentContextMap.containsKey("g_SFTP_ONESTOCK_PATH")) {
				context.g_SFTP_ONESTOCK_PATH = (String) parentContextMap.get("g_SFTP_ONESTOCK_PATH");
			}
			if (parentContextMap.containsKey("g_SFTP_ONESTOCK_PORT")) {
				context.g_SFTP_ONESTOCK_PORT = (String) parentContextMap.get("g_SFTP_ONESTOCK_PORT");
			}
			if (parentContextMap.containsKey("g_SFTP_TALEND_HOST")) {
				context.g_SFTP_TALEND_HOST = (String) parentContextMap.get("g_SFTP_TALEND_HOST");
			}
			if (parentContextMap.containsKey("g_SFTP_TALEND_LOGIN")) {
				context.g_SFTP_TALEND_LOGIN = (String) parentContextMap.get("g_SFTP_TALEND_LOGIN");
			}
			if (parentContextMap.containsKey("g_SFTP_TALEND_PASSWORD")) {
				context.g_SFTP_TALEND_PASSWORD = (String) parentContextMap.get("g_SFTP_TALEND_PASSWORD");
			}
			if (parentContextMap.containsKey("g_SFTP_TALEND_PATH")) {
				context.g_SFTP_TALEND_PATH = (String) parentContextMap.get("g_SFTP_TALEND_PATH");
			}
			if (parentContextMap.containsKey("g_SFTP_TALEND_PORT")) {
				context.g_SFTP_TALEND_PORT = (String) parentContextMap.get("g_SFTP_TALEND_PORT");
			}
			if (parentContextMap.containsKey("g_VAR_TALEND_WORKSPACE")) {
				context.g_VAR_TALEND_WORKSPACE = (String) parentContextMap.get("g_VAR_TALEND_WORKSPACE");
			}
		}

		// Resume: init the resumeUtil
		resumeEntryMethodName = ResumeUtil.getResumeEntryMethodName(resuming_checkpoint_path);
		resumeUtil = new ResumeUtil(resuming_logs_dir_path, isChildJob, rootPid);
		resumeUtil.initCommonInfo(pid, rootPid, fatherPid, projectName, jobName, contextStr, jobVersion);

		List<String> parametersToEncrypt = new java.util.ArrayList<String>();
		parametersToEncrypt.add("g_ALERTING_SMTP_PWD");
		parametersToEncrypt.add("g_BDD_OMNICANAL_PWD");
		// Resume: jobStart
		resumeUtil.addLog("JOB_STARTED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "",
				"", "", "", "", resumeUtil.convertToJsonText(context, parametersToEncrypt));

		if (execStat) {
			try {
				runStat.openSocket(!isChildJob);
				runStat.setAllPID(rootPid, fatherPid, pid, jobName);
				runStat.startThreadStat(clientHost, portStats);
				runStat.updateStatOnJob(RunStat.JOBSTART, fatherNode);
			} catch (java.io.IOException ioException) {
				ioException.printStackTrace();
			}
		}

		java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap = new java.util.concurrent.ConcurrentHashMap<Object, Object>();
		globalMap.put("concurrentHashMap", concurrentHashMap);

		long startUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		long endUsedMemory = 0;
		long end = 0;

		startTime = System.currentTimeMillis();

		this.globalResumeTicket = true;// to run tPreJob

		try {
			errorCode = null;
			tPrejob_1Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}
		} catch (TalendException e_tPrejob_1) {
			globalMap.put("tPrejob_1_SUBPROCESS_STATE", -1);

			e_tPrejob_1.printStackTrace();

		}

		if (enableLogStash) {
			talendJobLog.addJobStartMessage();
			try {
				talendJobLogProcess(globalMap);
			} catch (java.lang.Exception e) {
				e.printStackTrace();
			}
		}

		this.globalResumeTicket = false;// to run others jobs

		try {
			errorCode = null;
			tJava_2Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}
		} catch (TalendException e_tJava_2) {
			globalMap.put("tJava_2_SUBPROCESS_STATE", -1);

			e_tJava_2.printStackTrace();

		}

		this.globalResumeTicket = true;// to run tPostJob

		try {
			errorCode = null;
			tPostjob_1Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}
		} catch (TalendException e_tPostjob_1) {
			globalMap.put("tPostjob_1_SUBPROCESS_STATE", -1);

			e_tPostjob_1.printStackTrace();

		}

		end = System.currentTimeMillis();

		if (watch) {
			System.out.println((end - startTime) + " milliseconds");
		}

		endUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		if (false) {
			System.out.println(
					(endUsedMemory - startUsedMemory) + " bytes memory increase when running : j_confirmation_odp");
		}
		if (enableLogStash) {
			talendJobLog.addJobEndMessage(startTime, end, status);
			try {
				talendJobLogProcess(globalMap);
			} catch (java.lang.Exception e) {
				e.printStackTrace();
			}
		}

		if (execStat) {
			runStat.updateStatOnJob(RunStat.JOBEND, fatherNode);
			runStat.stopThreadStat();
		}
		int returnCode = 0;

		if (errorCode == null) {
			returnCode = status != null && status.equals("failure") ? 1 : 0;
		} else {
			returnCode = errorCode.intValue();
		}
		resumeUtil.addLog("JOB_ENDED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "",
				"" + returnCode, "", "", "");

		return returnCode;

	}

	// only for OSGi env
	public void destroy() {
		closeSqlDbConnections();
		closeFtpConnections();

	}

	private void closeSqlDbConnections() {
		try {
			Object obj_conn;
			obj_conn = globalMap.remove("conn_j_catch_unexpected_error_1_tDBConnection_1");
			if (null != obj_conn) {
				((java.sql.Connection) obj_conn).close();
			}
			obj_conn = globalMap.remove("conn_j_end_exec_1_tDBConnection_1");
			if (null != obj_conn) {
				((java.sql.Connection) obj_conn).close();
			}
			obj_conn = globalMap.remove("conn_j_init_exec_1_tDBConnection_1");
			if (null != obj_conn) {
				((java.sql.Connection) obj_conn).close();
			}
			obj_conn = globalMap.remove("conn_tDBConnection_1");
			if (null != obj_conn) {
				((java.sql.Connection) obj_conn).close();
			}
		} catch (java.lang.Exception e) {
		}
	}

	private void closeFtpConnections() {
		try {
			Object obj_conn;
			obj_conn = globalMap.remove("conn_tFTPConnection_1");
			if (obj_conn != null) {
				((org.apache.commons.net.ftp.FTPClient) obj_conn).logout();
				((org.apache.commons.net.ftp.FTPClient) obj_conn).disconnect();
			}
			obj_conn = globalMap.remove("conn_tFTPConnection_2");
			if (obj_conn != null) {
				((org.apache.commons.net.ftp.FTPClient) obj_conn).logout();
				((org.apache.commons.net.ftp.FTPClient) obj_conn).disconnect();
			}
		} catch (java.lang.Exception e) {
		}
	}

	private java.util.Map<String, Object> getSharedConnections4REST() {
		java.util.Map<String, Object> connections = new java.util.HashMap<String, Object>();
		connections.put("conn_j_catch_unexpected_error_1_tDBConnection_1",
				globalMap.get("conn_j_catch_unexpected_error_1_tDBConnection_1"));
		connections.put("conn_j_end_exec_1_tDBConnection_1", globalMap.get("conn_j_end_exec_1_tDBConnection_1"));
		connections.put("conn_j_init_exec_1_tDBConnection_1", globalMap.get("conn_j_init_exec_1_tDBConnection_1"));
		connections.put("conn_tDBConnection_1", globalMap.get("conn_tDBConnection_1"));

		connections.put("conn_tFTPConnection_1", globalMap.get("conn_tFTPConnection_1"));
		connections.put("conn_tFTPConnection_2", globalMap.get("conn_tFTPConnection_2"));

		return connections;
	}

	private void evalParam(String arg) {
		if (arg.startsWith("--resuming_logs_dir_path")) {
			resuming_logs_dir_path = arg.substring(25);
		} else if (arg.startsWith("--resuming_checkpoint_path")) {
			resuming_checkpoint_path = arg.substring(27);
		} else if (arg.startsWith("--parent_part_launcher")) {
			parent_part_launcher = arg.substring(23);
		} else if (arg.startsWith("--watch")) {
			watch = true;
		} else if (arg.startsWith("--stat_port=")) {
			String portStatsStr = arg.substring(12);
			if (portStatsStr != null && !portStatsStr.equals("null")) {
				portStats = Integer.parseInt(portStatsStr);
			}
		} else if (arg.startsWith("--trace_port=")) {
			portTraces = Integer.parseInt(arg.substring(13));
		} else if (arg.startsWith("--client_host=")) {
			clientHost = arg.substring(14);
		} else if (arg.startsWith("--context=")) {
			contextStr = arg.substring(10);
			isDefaultContext = false;
		} else if (arg.startsWith("--father_pid=")) {
			fatherPid = arg.substring(13);
		} else if (arg.startsWith("--root_pid=")) {
			rootPid = arg.substring(11);
		} else if (arg.startsWith("--father_node=")) {
			fatherNode = arg.substring(14);
		} else if (arg.startsWith("--pid=")) {
			pid = arg.substring(6);
		} else if (arg.startsWith("--context_type")) {
			String keyValue = arg.substring(15);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.setContextType(keyValue.substring(0, index),
							replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.setContextType(keyValue.substring(0, index), keyValue.substring(index + 1));
				}

			}

		} else if (arg.startsWith("--context_param")) {
			String keyValue = arg.substring(16);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.put(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.put(keyValue.substring(0, index), keyValue.substring(index + 1));
				}
			}
		} else if (arg.startsWith("--log4jLevel=")) {
			log4jLevel = arg.substring(13);
		} else if (arg.startsWith("--audit.enabled") && arg.contains("=")) {// for trunjob call
			final int equal = arg.indexOf('=');
			final String key = arg.substring("--".length(), equal);
			System.setProperty(key, arg.substring(equal + 1));
		}
	}

	private static final String NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY = "<TALEND_NULL>";

	private final String[][] escapeChars = { { "\\\\", "\\" }, { "\\n", "\n" }, { "\\'", "\'" }, { "\\r", "\r" },
			{ "\\f", "\f" }, { "\\b", "\b" }, { "\\t", "\t" } };

	private String replaceEscapeChars(String keyValue) {

		if (keyValue == null || ("").equals(keyValue.trim())) {
			return keyValue;
		}

		StringBuilder result = new StringBuilder();
		int currIndex = 0;
		while (currIndex < keyValue.length()) {
			int index = -1;
			// judege if the left string includes escape chars
			for (String[] strArray : escapeChars) {
				index = keyValue.indexOf(strArray[0], currIndex);
				if (index >= 0) {

					result.append(keyValue.substring(currIndex, index + strArray[0].length()).replace(strArray[0],
							strArray[1]));
					currIndex = index + strArray[0].length();
					break;
				}
			}
			// if the left string doesn't include escape chars, append the left into the
			// result
			if (index < 0) {
				result.append(keyValue.substring(currIndex));
				currIndex = currIndex + keyValue.length();
			}
		}

		return result.toString();
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public String getStatus() {
		return status;
	}

	ResumeUtil resumeUtil = null;
}
/************************************************************************************************
 * 797225 characters generated by Talend Data Integration on the 23 mai 2024 à
 * 14:16:09 CEST
 ************************************************************************************************/