
package apex_omnicanal.token_0_1;

import routines.Numeric;
import routines.DataOperation;
import routines.TalendDataGenerator;
import routines.TalendStringUtil;
import routines.TalendString;
import routines.StringHandling;
import routines.Relational;
import routines.TalendDate;
import routines.Mathematical;
import routines.SQLike;
import routines.system.*;
import routines.system.api.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.Comparator;

@SuppressWarnings("unused")

/**
 * Job: token Purpose: <br>
 * Description: obtenir token <br>
 * 
 * @author user@talend.com
 * @version 8.0.1.20211109_1610
 * @status
 */
public class token implements TalendJob {
	static {
		System.setProperty("TalendJob.log", "token.log");
	}

	private static org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(token.class);

	protected static void logIgnoredError(String message, Throwable cause) {
		log.error(message, cause);

	}

	public final Object obj = new Object();

	// for transmiting parameters purpose
	private Object valueObject = null;

	public Object getValueObject() {
		return this.valueObject;
	}

	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}

	private final static String defaultCharset = java.nio.charset.Charset.defaultCharset().name();

	private final static String utf8Charset = "UTF-8";

	// contains type for every context property
	public class PropertiesWithType extends java.util.Properties {
		private static final long serialVersionUID = 1L;
		private java.util.Map<String, String> propertyTypes = new java.util.HashMap<>();

		public PropertiesWithType(java.util.Properties properties) {
			super(properties);
		}

		public PropertiesWithType() {
			super();
		}

		public void setContextType(String key, String type) {
			propertyTypes.put(key, type);
		}

		public String getContextType(String key) {
			return propertyTypes.get(key);
		}
	}

	// create and load default properties
	private java.util.Properties defaultProps = new java.util.Properties();

	// create application properties with default
	public class ContextProperties extends PropertiesWithType {

		private static final long serialVersionUID = 1L;

		public ContextProperties(java.util.Properties properties) {
			super(properties);
		}

		public ContextProperties() {
			super();
		}

		public void synchronizeContext() {

			if (authorization != null) {

				this.setProperty("authorization", authorization.toString());

			}

			if (password != null) {

				this.setProperty("password", password.toString());

			}

			if (password_akeneo != null) {

				this.setProperty("password_akeneo", password_akeneo.toString());

			}

			if (site_id != null) {

				this.setProperty("site_id", site_id.toString());

			}

			if (url_akeneo != null) {

				this.setProperty("url_akeneo", url_akeneo.toString());

			}

			if (url_onestock != null) {

				this.setProperty("url_onestock", url_onestock.toString());

			}

			if (user_id != null) {

				this.setProperty("user_id", user_id.toString());

			}

			if (username != null) {

				this.setProperty("username", username.toString());

			}

		}

		// if the stored or passed value is "<TALEND_NULL>" string, it mean null
		public String getStringValue(String key) {
			String origin_value = this.getProperty(key);
			if (NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY.equals(origin_value)) {
				return null;
			}
			return origin_value;
		}

		public String authorization;

		public String getAuthorization() {
			return this.authorization;
		}

		public java.lang.String password;

		public java.lang.String getPassword() {
			return this.password;
		}

		public String password_akeneo;

		public String getPassword_akeneo() {
			return this.password_akeneo;
		}

		public String site_id;

		public String getSite_id() {
			return this.site_id;
		}

		public String url_akeneo;

		public String getUrl_akeneo() {
			return this.url_akeneo;
		}

		public String url_onestock;

		public String getUrl_onestock() {
			return this.url_onestock;
		}

		public String user_id;

		public String getUser_id() {
			return this.user_id;
		}

		public String username;

		public String getUsername() {
			return this.username;
		}
	}

	protected ContextProperties context = new ContextProperties(); // will be instanciated by MS.

	public ContextProperties getContext() {
		return this.context;
	}

	private final String jobVersion = "0.1";
	private final String jobName = "token";
	private final String projectName = "APEX_OMNICANAL";
	public Integer errorCode = null;
	private String currentComponent = "";

	private final java.util.Map<String, Object> globalMap = java.util.Collections
			.synchronizedMap(new java.util.HashMap<String, Object>());

	private final java.util.Map<String, Long> start_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Long> end_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Boolean> ok_Hash = new java.util.HashMap<String, Boolean>();
	public final java.util.List<String[]> globalBuffer = new java.util.ArrayList<String[]>();

	private final JobStructureCatcherUtils talendJobLog = new JobStructureCatcherUtils(jobName,
			"_yJln8AIvEe-QRuIyeGtg9w", "0.1");
	private org.talend.job.audit.JobAuditLogger auditLogger_talendJobLog = null;

	private RunStat runStat = new RunStat(talendJobLog, System.getProperty("audit.interval"));

	// OSGi DataSource
	private final static String KEY_DB_DATASOURCES = "KEY_DB_DATASOURCES";

	private final static String KEY_DB_DATASOURCES_RAW = "KEY_DB_DATASOURCES_RAW";

	public void setDataSources(java.util.Map<String, javax.sql.DataSource> dataSources) {
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		for (java.util.Map.Entry<String, javax.sql.DataSource> dataSourceEntry : dataSources.entrySet()) {
			talendDataSources.put(dataSourceEntry.getKey(),
					new routines.system.TalendDataSource(dataSourceEntry.getValue()));
		}
		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap.put(KEY_DB_DATASOURCES_RAW, new java.util.HashMap<String, javax.sql.DataSource>(dataSources));
	}

	public void setDataSourceReferences(List serviceReferences) throws Exception {

		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		java.util.Map<String, javax.sql.DataSource> dataSources = new java.util.HashMap<String, javax.sql.DataSource>();

		for (java.util.Map.Entry<String, javax.sql.DataSource> entry : BundleUtils
				.getServices(serviceReferences, javax.sql.DataSource.class).entrySet()) {
			dataSources.put(entry.getKey(), entry.getValue());
			talendDataSources.put(entry.getKey(), new routines.system.TalendDataSource(entry.getValue()));
		}

		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap.put(KEY_DB_DATASOURCES_RAW, new java.util.HashMap<String, javax.sql.DataSource>(dataSources));
	}

	private final java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
	private final java.io.PrintStream errorMessagePS = new java.io.PrintStream(new java.io.BufferedOutputStream(baos));

	public String getExceptionStackTrace() {
		if ("failure".equals(this.getStatus())) {
			errorMessagePS.flush();
			return baos.toString();
		}
		return null;
	}

	private Exception exception;

	public Exception getException() {
		if ("failure".equals(this.getStatus())) {
			return this.exception;
		}
		return null;
	}

	private class TalendException extends Exception {

		private static final long serialVersionUID = 1L;

		private java.util.Map<String, Object> globalMap = null;
		private Exception e = null;
		private String currentComponent = null;
		private String virtualComponentName = null;

		public void setVirtualComponentName(String virtualComponentName) {
			this.virtualComponentName = virtualComponentName;
		}

		private TalendException(Exception e, String errorComponent, final java.util.Map<String, Object> globalMap) {
			this.currentComponent = errorComponent;
			this.globalMap = globalMap;
			this.e = e;
		}

		public Exception getException() {
			return this.e;
		}

		public String getCurrentComponent() {
			return this.currentComponent;
		}

		public String getExceptionCauseMessage(Exception e) {
			Throwable cause = e;
			String message = null;
			int i = 10;
			while (null != cause && 0 < i--) {
				message = cause.getMessage();
				if (null == message) {
					cause = cause.getCause();
				} else {
					break;
				}
			}
			if (null == message) {
				message = e.getClass().getName();
			}
			return message;
		}

		@Override
		public void printStackTrace() {
			if (!(e instanceof TalendException || e instanceof TDieException)) {
				if (virtualComponentName != null && currentComponent.indexOf(virtualComponentName + "_") == 0) {
					globalMap.put(virtualComponentName + "_ERROR_MESSAGE", getExceptionCauseMessage(e));
				}
				globalMap.put(currentComponent + "_ERROR_MESSAGE", getExceptionCauseMessage(e));
				System.err.println("Exception in component " + currentComponent + " (" + jobName + ")");
			}
			if (!(e instanceof TDieException)) {
				if (e instanceof TalendException) {
					e.printStackTrace();
				} else {
					e.printStackTrace();
					e.printStackTrace(errorMessagePS);
					token.this.exception = e;
				}
			}
			if (!(e instanceof TalendException)) {
				try {
					for (java.lang.reflect.Method m : this.getClass().getEnclosingClass().getMethods()) {
						if (m.getName().compareTo(currentComponent + "_error") == 0) {
							m.invoke(token.this, new Object[] { e, currentComponent, globalMap });
							break;
						}
					}

					if (!(e instanceof TDieException)) {
					}
				} catch (Exception e) {
					this.e.printStackTrace();
				}
			}
		}
	}

	public void tFixedFlowInput_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFixedFlowInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tRESTClient_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFixedFlowInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tLogRow_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFixedFlowInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tExtractJSONFields_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFixedFlowInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tLogRow_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFixedFlowInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tWriteJSONField_1_Out_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		tWriteJSONField_1_In_error(exception, errorComponent, globalMap);

	}

	public void tWriteJSONField_1_In_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFixedFlowInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void talendJobLog_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		talendJobLog_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFixedFlowInput_1_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tWriteJSONField_1_In_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void talendJobLog_onSubJobError(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap) throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public static class row1Struct implements routines.system.IPersistableRow<row1Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_token = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_token = new byte[0];

		public String password;

		public String getPassword() {
			return this.password;
		}

		public String site_id;

		public String getSite_id() {
			return this.site_id;
		}

		public String user_id;

		public String getUser_id() {
			return this.user_id;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_token.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_token.length == 0) {
						commonByteArray_APEX_OMNICANAL_token = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_token = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_token, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_token, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_token.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_token.length == 0) {
						commonByteArray_APEX_OMNICANAL_token = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_token = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_token, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_token, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_token) {

				try {

					int length = 0;

					this.password = readString(dis);

					this.site_id = readString(dis);

					this.user_id = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_token) {

				try {

					int length = 0;

					this.password = readString(dis);

					this.site_id = readString(dis);

					this.user_id = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.password, dos);

				// String

				writeString(this.site_id, dos);

				// String

				writeString(this.user_id, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// String

				writeString(this.password, dos);

				// String

				writeString(this.site_id, dos);

				// String

				writeString(this.user_id, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("password=" + password);
			sb.append(",site_id=" + site_id);
			sb.append(",user_id=" + user_id);
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (password == null) {
				sb.append("<null>");
			} else {
				sb.append(password);
			}

			sb.append("|");

			if (site_id == null) {
				sb.append("<null>");
			} else {
				sb.append(site_id);
			}

			sb.append("|");

			if (user_id == null) {
				sb.append("<null>");
			} else {
				sb.append(user_id);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row1Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tFixedFlowInput_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tFixedFlowInput_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;
		String currentVirtualComponent = null;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row1Struct row1 = new row1Struct();

				/**
				 * [tWriteJSONField_1_Out begin ] start
				 */

				ok_Hash.put("tWriteJSONField_1_Out", false);
				start_Hash.put("tWriteJSONField_1_Out", System.currentTimeMillis());

				currentVirtualComponent = "tWriteJSONField_1";

				currentComponent = "tWriteJSONField_1_Out";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "row1");

				int tos_count_tWriteJSONField_1_Out = 0;

				if (log.isDebugEnabled())
					log.debug("tWriteJSONField_1_Out - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tWriteJSONField_1_Out {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tWriteJSONField_1_Out = new StringBuilder();
							log4jParamters_tWriteJSONField_1_Out.append("Parameters:");
							log4jParamters_tWriteJSONField_1_Out.append("GROUPBYS" + " = " + "[]");
							log4jParamters_tWriteJSONField_1_Out.append(" | ");
							log4jParamters_tWriteJSONField_1_Out.append("REMOVE_HEADER" + " = " + "false");
							log4jParamters_tWriteJSONField_1_Out.append(" | ");
							log4jParamters_tWriteJSONField_1_Out.append("CREATE" + " = " + "true");
							log4jParamters_tWriteJSONField_1_Out.append(" | ");
							log4jParamters_tWriteJSONField_1_Out.append("CREATE_EMPTY_ELEMENT" + " = " + "true");
							log4jParamters_tWriteJSONField_1_Out.append(" | ");
							log4jParamters_tWriteJSONField_1_Out.append("EXPAND_EMPTY_ELM" + " = " + "false");
							log4jParamters_tWriteJSONField_1_Out.append(" | ");
							log4jParamters_tWriteJSONField_1_Out.append("ALLOW_EMPTY_STRINGS" + " = " + "false");
							log4jParamters_tWriteJSONField_1_Out.append(" | ");
							log4jParamters_tWriteJSONField_1_Out.append("OUTPUT_AS_XSD" + " = " + "false");
							log4jParamters_tWriteJSONField_1_Out.append(" | ");
							log4jParamters_tWriteJSONField_1_Out.append("ADVANCED_SEPARATOR" + " = " + "false");
							log4jParamters_tWriteJSONField_1_Out.append(" | ");
							log4jParamters_tWriteJSONField_1_Out.append("COMPACT_FORMAT" + " = " + "true");
							log4jParamters_tWriteJSONField_1_Out.append(" | ");
							log4jParamters_tWriteJSONField_1_Out.append("GENERATION_MODE" + " = " + "Dom4j");
							log4jParamters_tWriteJSONField_1_Out.append(" | ");
							log4jParamters_tWriteJSONField_1_Out.append("ENCODING" + " = " + "\"ISO-8859-15\"");
							log4jParamters_tWriteJSONField_1_Out.append(" | ");
							log4jParamters_tWriteJSONField_1_Out.append("DESTINATION" + " = " + "tWriteJSONField_1");
							log4jParamters_tWriteJSONField_1_Out.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tWriteJSONField_1_Out - " + (log4jParamters_tWriteJSONField_1_Out));
						}
					}
					new BytesLimit65535_tWriteJSONField_1_Out().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tWriteJSONField_1_Out", "tWriteJSONField_1_Out", "tWriteXMLFieldOut");
					talendJobLogProcess(globalMap);
				}

//tWriteXMLFieldOut_begin
				int nb_line_tWriteJSONField_1_Out = 0;
				boolean needRoot_tWriteJSONField_1_Out = true;

				String strCompCache_tWriteJSONField_1_Out = null;

				java.util.Queue<row2Struct> listGroupby_tWriteJSONField_1_Out = new java.util.concurrent.ConcurrentLinkedQueue<row2Struct>();

				class ThreadXMLField_tWriteJSONField_1_Out extends Thread {

					java.util.Queue<row2Struct> queue;

					java.util.List<java.util.Map<String, String>> flows;
					java.lang.Exception lastException;
					java.lang.Error lastError;
					String currentComponent;

					ThreadXMLField_tWriteJSONField_1_Out(java.util.Queue q) {
						this.queue = q;
						globalMap.put("queue_tWriteJSONField_1_In", queue);
						lastException = null;
					}

					ThreadXMLField_tWriteJSONField_1_Out(java.util.Queue q,
							java.util.List<java.util.Map<String, String>> l) {
						this.queue = q;
						this.flows = l;
						lastException = null;
						globalMap.put("queue_tWriteJSONField_1_In", queue);
						globalMap.put("flows_tWriteJSONField_1_In", flows);
					}

					public java.lang.Exception getLastException() {
						return this.lastException;
					}

					public java.lang.Error getLastError() {
						return this.lastError;
					}

					public String getCurrentComponent() {
						return this.currentComponent;
					}

					@Override
					public void run() {
						try {
							tWriteJSONField_1_InProcess(globalMap);
						} catch (TalendException te) {
							globalMap.put("tWriteJSONField_1_Out_ERROR_MESSAGE", te.getMessage());
							this.lastException = te.getException();
							this.currentComponent = te.getCurrentComponent();
						} catch (java.lang.Error error) {
							this.lastError = error;
						}
					}
				}

				ThreadXMLField_tWriteJSONField_1_Out txf_tWriteJSONField_1_Out = new ThreadXMLField_tWriteJSONField_1_Out(
						listGroupby_tWriteJSONField_1_Out);

				globalMap.put("wrtXMLFieldIn_tWriteJSONField_1_Out", txf_tWriteJSONField_1_Out);
				txf_tWriteJSONField_1_Out.start();

				java.util.List<java.util.List<String>> groupbyList_tWriteJSONField_1_Out = new java.util.ArrayList<java.util.List<String>>();
				java.util.Map<String, String> valueMap_tWriteJSONField_1_Out = new java.util.HashMap<String, String>();
				java.util.Map<String, String> arraysValueMap_tWriteJSONField_1_Out = new java.util.HashMap<String, String>();

				class NestXMLTool_tWriteJSONField_1_Out {
					public void parseAndAdd(org.dom4j.Element nestRoot, String value) {
						try {
							org.dom4j.Document doc4Str = org.dom4j.DocumentHelper
									.parseText("<root>" + value + "</root>");
							nestRoot.setContent(doc4Str.getRootElement().content());
						} catch (java.lang.Exception e) {
							globalMap.put("tWriteJSONField_1_Out_ERROR_MESSAGE", e.getMessage());
							e.printStackTrace();
							nestRoot.setText(value);
						}
					}

					public void setText(org.dom4j.Element element, String value) {
						if (value.startsWith("<![CDATA[") && value.endsWith("]]>")) {
							String text = value.substring(9, value.length() - 3);
							element.addCDATA(text);
						} else {
							element.setText(value);
						}
					}

					public void replaceDefaultNameSpace(org.dom4j.Element nestRoot) {
						if (nestRoot != null) {
							for (org.dom4j.Element tmp : (java.util.List<org.dom4j.Element>) nestRoot.elements()) {
								if (("").equals(tmp.getQName().getNamespace().getURI())
										&& ("").equals(tmp.getQName().getNamespace().getPrefix())) {
									tmp.setQName(org.dom4j.DocumentHelper.createQName(tmp.getName(),
											nestRoot.getQName().getNamespace()));
								}
								replaceDefaultNameSpace(tmp);
							}
						}
					}

					public void removeEmptyElement(org.dom4j.Element root) {
						if (root != null) {
							for (org.dom4j.Element tmp : (java.util.List<org.dom4j.Element>) root.elements()) {
								removeEmptyElement(tmp);
							}

							boolean noSignificantDataAnnotationsExist = root.attributes().isEmpty();
							if (root.content().isEmpty() && noSignificantDataAnnotationsExist
									&& root.declaredNamespaces().isEmpty()) {
								if (root.getParent() != null) {
									root.getParent().remove(root);
								}
							}
						}
					}

					public String objectToString(Object value) {
						if (value.getClass().isArray()) {
							StringBuilder sb = new StringBuilder();

							int length = java.lang.reflect.Array.getLength(value);
							for (int i = 0; i < length; i++) {
								Object obj = java.lang.reflect.Array.get(value, i);
								sb.append("<element>");
								sb.append(obj);
								sb.append("</element>");
							}
							return sb.toString();
						} else {
							return value.toString();
						}
					}
				}
				NestXMLTool_tWriteJSONField_1_Out nestXMLTool_tWriteJSONField_1_Out = new NestXMLTool_tWriteJSONField_1_Out();

				row1Struct rowStructOutput_tWriteJSONField_1_Out = new row1Struct();
// sort group root element for judgement of group
				java.util.List<org.dom4j.Element> groupElementList_tWriteJSONField_1_Out = new java.util.ArrayList<org.dom4j.Element>();
				org.dom4j.Element root4Group_tWriteJSONField_1_Out = null;
				org.dom4j.Document doc_tWriteJSONField_1_Out = org.dom4j.DocumentHelper.createDocument();
				org.dom4j.io.OutputFormat format_tWriteJSONField_1_Out = org.dom4j.io.OutputFormat
						.createCompactFormat();
				format_tWriteJSONField_1_Out.setNewLineAfterDeclaration(false);
				format_tWriteJSONField_1_Out.setTrimText(false);
				format_tWriteJSONField_1_Out.setEncoding("ISO-8859-15");
				int[] orders_tWriteJSONField_1_Out = new int[1];

				/**
				 * [tWriteJSONField_1_Out begin ] stop
				 */

				/**
				 * [tFixedFlowInput_1 begin ] start
				 */

				ok_Hash.put("tFixedFlowInput_1", false);
				start_Hash.put("tFixedFlowInput_1", System.currentTimeMillis());

				currentComponent = "tFixedFlowInput_1";

				int tos_count_tFixedFlowInput_1 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("tFixedFlowInput_1", "tFixedFlowInput_1", "tFixedFlowInput");
					talendJobLogProcess(globalMap);
				}

				for (int i_tFixedFlowInput_1 = 0; i_tFixedFlowInput_1 < 1; i_tFixedFlowInput_1++) {

					row1.password = context.password;

					row1.site_id = context.site_id;

					row1.user_id = context.user_id;

					/**
					 * [tFixedFlowInput_1 begin ] stop
					 */

					/**
					 * [tFixedFlowInput_1 main ] start
					 */

					currentComponent = "tFixedFlowInput_1";

					tos_count_tFixedFlowInput_1++;

					/**
					 * [tFixedFlowInput_1 main ] stop
					 */

					/**
					 * [tFixedFlowInput_1 process_data_begin ] start
					 */

					currentComponent = "tFixedFlowInput_1";

					/**
					 * [tFixedFlowInput_1 process_data_begin ] stop
					 */

					/**
					 * [tWriteJSONField_1_Out main ] start
					 */

					currentVirtualComponent = "tWriteJSONField_1";

					currentComponent = "tWriteJSONField_1_Out";

					if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

							, "row1", "tFixedFlowInput_1", "tFixedFlowInput_1", "tFixedFlowInput",
							"tWriteJSONField_1_Out", "tWriteJSONField_1_Out", "tWriteXMLFieldOut"

					)) {
						talendJobLogProcess(globalMap);
					}

					if (log.isTraceEnabled()) {
						log.trace("row1 - " + (row1 == null ? "" : row1.toLogString()));
					}

					if (txf_tWriteJSONField_1_Out.getLastException() != null) {
						currentComponent = txf_tWriteJSONField_1_Out.getCurrentComponent();
						throw txf_tWriteJSONField_1_Out.getLastException();
					}

					if (txf_tWriteJSONField_1_Out.getLastError() != null) {
						throw txf_tWriteJSONField_1_Out.getLastError();
					}
					nb_line_tWriteJSONField_1_Out++;
					log.debug("tWriteJSONField_1_Out - Processing the record " + nb_line_tWriteJSONField_1_Out + ".");

					class ToStringHelper_tWriteJSONField_1_Out {
						public String toString(final Object value) {
							return value != null ? value.toString() : null;
						}
					}
					final ToStringHelper_tWriteJSONField_1_Out helper_tWriteJSONField_1_Out = new ToStringHelper_tWriteJSONField_1_Out();

					valueMap_tWriteJSONField_1_Out.clear();
					arraysValueMap_tWriteJSONField_1_Out.clear();
					valueMap_tWriteJSONField_1_Out.put("password", helper_tWriteJSONField_1_Out
							.toString((row1.password != null ? row1.password.toString() : null)));
					arraysValueMap_tWriteJSONField_1_Out.put("password", helper_tWriteJSONField_1_Out
							.toString((row1.password != null ? row1.password.toString() : null)));
					valueMap_tWriteJSONField_1_Out.put("site_id", helper_tWriteJSONField_1_Out
							.toString((row1.site_id != null ? row1.site_id.toString() : null)));
					arraysValueMap_tWriteJSONField_1_Out.put("site_id", helper_tWriteJSONField_1_Out
							.toString((row1.site_id != null ? row1.site_id.toString() : null)));
					valueMap_tWriteJSONField_1_Out.put("user_id", helper_tWriteJSONField_1_Out
							.toString((row1.user_id != null ? row1.user_id.toString() : null)));
					arraysValueMap_tWriteJSONField_1_Out.put("user_id", helper_tWriteJSONField_1_Out
							.toString((row1.user_id != null ? row1.user_id.toString() : null)));
					String strTemp_tWriteJSONField_1_Out = "";
					if (strCompCache_tWriteJSONField_1_Out == null) {
						strCompCache_tWriteJSONField_1_Out = strTemp_tWriteJSONField_1_Out;

					} else {
						nestXMLTool_tWriteJSONField_1_Out
								.replaceDefaultNameSpace(doc_tWriteJSONField_1_Out.getRootElement());
						java.io.StringWriter strWriter_tWriteJSONField_1_Out = new java.io.StringWriter();
						org.dom4j.io.XMLWriter output_tWriteJSONField_1_Out = new org.dom4j.io.XMLWriter(
								strWriter_tWriteJSONField_1_Out, format_tWriteJSONField_1_Out);
						output_tWriteJSONField_1_Out.write(doc_tWriteJSONField_1_Out);
						output_tWriteJSONField_1_Out.close();

						row2Struct row_tWriteJSONField_1_Out = new row2Struct();

						row_tWriteJSONField_1_Out.string = strWriter_tWriteJSONField_1_Out.toString();
						listGroupby_tWriteJSONField_1_Out.add(row_tWriteJSONField_1_Out);

						doc_tWriteJSONField_1_Out.clearContent();
						needRoot_tWriteJSONField_1_Out = true;
						for (int i_tWriteJSONField_1_Out = 0; i_tWriteJSONField_1_Out < orders_tWriteJSONField_1_Out.length; i_tWriteJSONField_1_Out++) {
							orders_tWriteJSONField_1_Out[i_tWriteJSONField_1_Out] = 0;
						}

						if (groupbyList_tWriteJSONField_1_Out != null
								&& groupbyList_tWriteJSONField_1_Out.size() >= 0) {
							groupbyList_tWriteJSONField_1_Out.clear();
						}
						strCompCache_tWriteJSONField_1_Out = strTemp_tWriteJSONField_1_Out;
					}

					org.dom4j.Element subTreeRootParent_tWriteJSONField_1_Out = null;

					// build root xml tree
					if (needRoot_tWriteJSONField_1_Out) {
						needRoot_tWriteJSONField_1_Out = false;
						org.dom4j.Element root_tWriteJSONField_1_Out = doc_tWriteJSONField_1_Out.addElement("rootTag");
						subTreeRootParent_tWriteJSONField_1_Out = root_tWriteJSONField_1_Out;
						org.dom4j.Element root_0_tWriteJSONField_1_Out = root_tWriteJSONField_1_Out
								.addElement("site_id");
						if (valueMap_tWriteJSONField_1_Out.get("site_id") != null) {
							nestXMLTool_tWriteJSONField_1_Out.setText(root_0_tWriteJSONField_1_Out,
									valueMap_tWriteJSONField_1_Out.get("site_id"));
						}
						org.dom4j.Element root_1_tWriteJSONField_1_Out = root_tWriteJSONField_1_Out
								.addElement("password");
						if (valueMap_tWriteJSONField_1_Out.get("password") != null) {
							nestXMLTool_tWriteJSONField_1_Out.setText(root_1_tWriteJSONField_1_Out,
									valueMap_tWriteJSONField_1_Out.get("password"));
						}
						root4Group_tWriteJSONField_1_Out = subTreeRootParent_tWriteJSONField_1_Out;
					} else {
						subTreeRootParent_tWriteJSONField_1_Out = root4Group_tWriteJSONField_1_Out;
					}
					// build group xml tree
					// build loop xml tree
					org.dom4j.Element loop_tWriteJSONField_1_Out = org.dom4j.DocumentHelper.createElement("user_id");
					if (orders_tWriteJSONField_1_Out[0] == 0) {
						orders_tWriteJSONField_1_Out[0] = 1;
					}
					if (1 < orders_tWriteJSONField_1_Out.length) {
						orders_tWriteJSONField_1_Out[1] = 0;
					}
					subTreeRootParent_tWriteJSONField_1_Out.elements().add(orders_tWriteJSONField_1_Out[0]++,
							loop_tWriteJSONField_1_Out);
					if (valueMap_tWriteJSONField_1_Out.get("user_id") != null) {
						nestXMLTool_tWriteJSONField_1_Out.setText(loop_tWriteJSONField_1_Out,
								valueMap_tWriteJSONField_1_Out.get("user_id"));
					}

					tos_count_tWriteJSONField_1_Out++;

					/**
					 * [tWriteJSONField_1_Out main ] stop
					 */

					/**
					 * [tWriteJSONField_1_Out process_data_begin ] start
					 */

					currentVirtualComponent = "tWriteJSONField_1";

					currentComponent = "tWriteJSONField_1_Out";

					/**
					 * [tWriteJSONField_1_Out process_data_begin ] stop
					 */

					/**
					 * [tWriteJSONField_1_Out process_data_end ] start
					 */

					currentVirtualComponent = "tWriteJSONField_1";

					currentComponent = "tWriteJSONField_1_Out";

					/**
					 * [tWriteJSONField_1_Out process_data_end ] stop
					 */

					/**
					 * [tFixedFlowInput_1 process_data_end ] start
					 */

					currentComponent = "tFixedFlowInput_1";

					/**
					 * [tFixedFlowInput_1 process_data_end ] stop
					 */

					/**
					 * [tFixedFlowInput_1 end ] start
					 */

					currentComponent = "tFixedFlowInput_1";

				}
				globalMap.put("tFixedFlowInput_1_NB_LINE", 1);

				ok_Hash.put("tFixedFlowInput_1", true);
				end_Hash.put("tFixedFlowInput_1", System.currentTimeMillis());

				/**
				 * [tFixedFlowInput_1 end ] stop
				 */

				/**
				 * [tWriteJSONField_1_Out end ] start
				 */

				currentVirtualComponent = "tWriteJSONField_1";

				currentComponent = "tWriteJSONField_1_Out";

				if (nb_line_tWriteJSONField_1_Out > 0) {
					nestXMLTool_tWriteJSONField_1_Out
							.replaceDefaultNameSpace(doc_tWriteJSONField_1_Out.getRootElement());
					java.io.StringWriter strWriter_tWriteJSONField_1_Out = new java.io.StringWriter();
					org.dom4j.io.XMLWriter output_tWriteJSONField_1_Out = new org.dom4j.io.XMLWriter(
							strWriter_tWriteJSONField_1_Out, format_tWriteJSONField_1_Out);
					output_tWriteJSONField_1_Out.write(doc_tWriteJSONField_1_Out);
					output_tWriteJSONField_1_Out.close();
					row2Struct row_tWriteJSONField_1_Out = new row2Struct();

					row_tWriteJSONField_1_Out.string = strWriter_tWriteJSONField_1_Out.toString();
					listGroupby_tWriteJSONField_1_Out.add(row_tWriteJSONField_1_Out);

				}
				globalMap.put("tWriteJSONField_1_Out_NB_LINE", nb_line_tWriteJSONField_1_Out);
				globalMap.put("tWriteJSONField_1_In_FINISH" + (listGroupby_tWriteJSONField_1_Out == null ? ""
						: listGroupby_tWriteJSONField_1_Out.hashCode()), "true");

				txf_tWriteJSONField_1_Out.join();

				if (txf_tWriteJSONField_1_Out.getLastException() != null) {
					currentComponent = txf_tWriteJSONField_1_Out.getCurrentComponent();
					throw txf_tWriteJSONField_1_Out.getLastException();
				}

				if (txf_tWriteJSONField_1_Out.getLastError() != null) {
					throw txf_tWriteJSONField_1_Out.getLastError();
				}

				resourceMap.put("finish_tWriteJSONField_1_Out", true);
				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "row1", 2, 0,
						"tFixedFlowInput_1", "tFixedFlowInput_1", "tFixedFlowInput", "tWriteJSONField_1_Out",
						"tWriteJSONField_1_Out", "tWriteXMLFieldOut", "output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("tWriteJSONField_1_Out - " + ("Done."));

				ok_Hash.put("tWriteJSONField_1_Out", true);
				end_Hash.put("tWriteJSONField_1_Out", System.currentTimeMillis());

				if (execStat) {
					runStat.updateStatOnConnection("OnComponentOk", 0, "ok");
				}

				/**
				 * [tWriteJSONField_1_Out end ] stop
				 */

			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			te.setVirtualComponentName(currentVirtualComponent);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tFixedFlowInput_1 finally ] start
				 */

				currentComponent = "tFixedFlowInput_1";

				/**
				 * [tFixedFlowInput_1 finally ] stop
				 */

				/**
				 * [tWriteJSONField_1_Out finally ] start
				 */

				currentVirtualComponent = "tWriteJSONField_1";

				currentComponent = "tWriteJSONField_1_Out";

				java.util.Queue listGroupby_tWriteJSONField_1_Out = (java.util.Queue) globalMap
						.get("queue_tWriteJSONField_1_In");
				if (resourceMap.get("finish_tWriteJSONField_1_Out") == null) {
					globalMap.put("tWriteJSONField_1_In_FINISH_WITH_EXCEPTION"
							+ (listGroupby_tWriteJSONField_1_Out == null ? ""
									: listGroupby_tWriteJSONField_1_Out.hashCode()),
							"true");
				}

				if (listGroupby_tWriteJSONField_1_Out != null) {
					globalMap.put("tWriteJSONField_1_In_FINISH" + (listGroupby_tWriteJSONField_1_Out == null ? ""
							: listGroupby_tWriteJSONField_1_Out.hashCode()), "true");
				}
				// workaround for 37349 - in case of normal execution it will pass normally
				// in case it fails and handle by catch - it will wait for child thread finish
				Thread txf_tWriteJSONField_1_Out = (Thread) globalMap.get("wrtXMLFieldIn_tWriteJSONField_1_Out");
				if (txf_tWriteJSONField_1_Out != null) {
					txf_tWriteJSONField_1_Out.join();
				}

				/**
				 * [tWriteJSONField_1_Out finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tFixedFlowInput_1_SUBPROCESS_STATE", 1);
	}

	public static class row4Struct implements routines.system.IPersistableRow<row4Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_token = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_token = new byte[0];

		public String token;

		public String getToken() {
			return this.token;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_token.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_token.length == 0) {
						commonByteArray_APEX_OMNICANAL_token = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_token = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_token, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_token, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_token.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_token.length == 0) {
						commonByteArray_APEX_OMNICANAL_token = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_token = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_token, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_token, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_token) {

				try {

					int length = 0;

					this.token = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_token) {

				try {

					int length = 0;

					this.token = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.token, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// String

				writeString(this.token, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("token=" + token);
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (token == null) {
				sb.append("<null>");
			} else {
				sb.append(token);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row4Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row5Struct implements routines.system.IPersistableRow<row5Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_token = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_token = new byte[0];

		public Integer statusCode;

		public Integer getStatusCode() {
			return this.statusCode;
		}

		public routines.system.Document body;

		public routines.system.Document getBody() {
			return this.body;
		}

		public String string;

		public String getString() {
			return this.string;
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_token.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_token.length == 0) {
						commonByteArray_APEX_OMNICANAL_token = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_token = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_token, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_token, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_token.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_token.length == 0) {
						commonByteArray_APEX_OMNICANAL_token = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_token = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_token, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_token, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_token) {

				try {

					int length = 0;

					this.statusCode = readInteger(dis);

					this.body = (routines.system.Document) dis.readObject();

					this.string = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				} catch (ClassNotFoundException eCNFE) {
					throw new RuntimeException(eCNFE);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_token) {

				try {

					int length = 0;

					this.statusCode = readInteger(dis);

					this.body = (routines.system.Document) dis.readObject();

					this.string = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				} catch (ClassNotFoundException eCNFE) {
					throw new RuntimeException(eCNFE);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// Integer

				writeInteger(this.statusCode, dos);

				// Document

				dos.writeObject(this.body);

				// String

				writeString(this.string, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// Integer

				writeInteger(this.statusCode, dos);

				// Document

				dos.writeObject(this.body);

				// String

				writeString(this.string, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("statusCode=" + String.valueOf(statusCode));
			sb.append(",body=" + String.valueOf(body));
			sb.append(",string=" + string);
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (statusCode == null) {
				sb.append("<null>");
			} else {
				sb.append(statusCode);
			}

			sb.append("|");

			if (body == null) {
				sb.append("<null>");
			} else {
				sb.append(body);
			}

			sb.append("|");

			if (string == null) {
				sb.append("<null>");
			} else {
				sb.append(string);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row5Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row3Struct implements routines.system.IPersistableRow<row3Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_token = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_token = new byte[0];

		public Integer statusCode;

		public Integer getStatusCode() {
			return this.statusCode;
		}

		public routines.system.Document body;

		public routines.system.Document getBody() {
			return this.body;
		}

		public String string;

		public String getString() {
			return this.string;
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (intNum == null) {
				marshaller.writeByte(-1);
			} else {
				marshaller.writeByte(0);
				marshaller.writeInt(intNum);
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_token.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_token.length == 0) {
						commonByteArray_APEX_OMNICANAL_token = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_token = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_token, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_token, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_token.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_token.length == 0) {
						commonByteArray_APEX_OMNICANAL_token = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_token = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_token, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_token, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_token) {

				try {

					int length = 0;

					this.statusCode = readInteger(dis);

					this.body = (routines.system.Document) dis.readObject();

					this.string = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				} catch (ClassNotFoundException eCNFE) {
					throw new RuntimeException(eCNFE);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_token) {

				try {

					int length = 0;

					this.statusCode = readInteger(dis);

					this.body = (routines.system.Document) dis.readObject();

					this.string = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				} catch (ClassNotFoundException eCNFE) {
					throw new RuntimeException(eCNFE);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// Integer

				writeInteger(this.statusCode, dos);

				// Document

				dos.writeObject(this.body);

				// String

				writeString(this.string, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// Integer

				writeInteger(this.statusCode, dos);

				// Document

				dos.writeObject(this.body);

				// String

				writeString(this.string, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("statusCode=" + String.valueOf(statusCode));
			sb.append(",body=" + String.valueOf(body));
			sb.append(",string=" + string);
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (statusCode == null) {
				sb.append("<null>");
			} else {
				sb.append(statusCode);
			}

			sb.append("|");

			if (body == null) {
				sb.append("<null>");
			} else {
				sb.append(body);
			}

			sb.append("|");

			if (string == null) {
				sb.append("<null>");
			} else {
				sb.append(string);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row3Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row2Struct implements routines.system.IPersistableRow<row2Struct> {
		final static byte[] commonByteArrayLock_APEX_OMNICANAL_token = new byte[0];
		static byte[] commonByteArray_APEX_OMNICANAL_token = new byte[0];

		public String string;

		public String getString() {
			return this.string;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_token.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_token.length == 0) {
						commonByteArray_APEX_OMNICANAL_token = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_token = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_APEX_OMNICANAL_token, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_token, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException {
			String strReturn = null;
			int length = 0;
			length = unmarshaller.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_APEX_OMNICANAL_token.length) {
					if (length < 1024 && commonByteArray_APEX_OMNICANAL_token.length == 0) {
						commonByteArray_APEX_OMNICANAL_token = new byte[1024];
					} else {
						commonByteArray_APEX_OMNICANAL_token = new byte[2 * length];
					}
				}
				unmarshaller.readFully(commonByteArray_APEX_OMNICANAL_token, 0, length);
				strReturn = new String(commonByteArray_APEX_OMNICANAL_token, 0, length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException {
			if (str == null) {
				marshaller.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				marshaller.writeInt(byteArray.length);
				marshaller.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_token) {

				try {

					int length = 0;

					this.string = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void readData(org.jboss.marshalling.Unmarshaller dis) {

			synchronized (commonByteArrayLock_APEX_OMNICANAL_token) {

				try {

					int length = 0;

					this.string = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.string, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public void writeData(org.jboss.marshalling.Marshaller dos) {
			try {

				// String

				writeString(this.string, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("string=" + string);
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (string == null) {
				sb.append("<null>");
			} else {
				sb.append(string);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row2Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(), object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tWriteJSONField_1_InProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("tWriteJSONField_1_In_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;
		String currentVirtualComponent = null;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row2Struct row2 = new row2Struct();
				row3Struct row3 = new row3Struct();
				row3Struct row5 = row3;
				row4Struct row4 = new row4Struct();

				/**
				 * [tLogRow_1 begin ] start
				 */

				ok_Hash.put("tLogRow_1", false);
				start_Hash.put("tLogRow_1", System.currentTimeMillis());

				currentComponent = "tLogRow_1";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "row4");

				int tos_count_tLogRow_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tLogRow_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tLogRow_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tLogRow_1 = new StringBuilder();
							log4jParamters_tLogRow_1.append("Parameters:");
							log4jParamters_tLogRow_1.append("BASIC_MODE" + " = " + "false");
							log4jParamters_tLogRow_1.append(" | ");
							log4jParamters_tLogRow_1.append("TABLE_PRINT" + " = " + "true");
							log4jParamters_tLogRow_1.append(" | ");
							log4jParamters_tLogRow_1.append("VERTICAL" + " = " + "false");
							log4jParamters_tLogRow_1.append(" | ");
							log4jParamters_tLogRow_1.append("PRINT_CONTENT_WITH_LOG4J" + " = " + "true");
							log4jParamters_tLogRow_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tLogRow_1 - " + (log4jParamters_tLogRow_1));
						}
					}
					new BytesLimit65535_tLogRow_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tLogRow_1", "tLogRow_1", "tLogRow");
					talendJobLogProcess(globalMap);
				}

				///////////////////////

				class Util_tLogRow_1 {

					String[] des_top = { ".", ".", "-", "+" };

					String[] des_head = { "|=", "=|", "-", "+" };

					String[] des_bottom = { "'", "'", "-", "+" };

					String name = "";

					java.util.List<String[]> list = new java.util.ArrayList<String[]>();

					int[] colLengths = new int[1];

					public void addRow(String[] row) {

						for (int i = 0; i < 1; i++) {
							if (row[i] != null) {
								colLengths[i] = Math.max(colLengths[i], row[i].length());
							}
						}
						list.add(row);
					}

					public void setTableName(String name) {

						this.name = name;
					}

					public StringBuilder format() {

						StringBuilder sb = new StringBuilder();

						sb.append(print(des_top));

						int totals = 0;
						for (int i = 0; i < colLengths.length; i++) {
							totals = totals + colLengths[i];
						}

						// name
						sb.append("|");
						int k = 0;
						for (k = 0; k < (totals + 0 - name.length()) / 2; k++) {
							sb.append(' ');
						}
						sb.append(name);
						for (int i = 0; i < totals + 0 - name.length() - k; i++) {
							sb.append(' ');
						}
						sb.append("|\n");

						// head and rows
						sb.append(print(des_head));
						for (int i = 0; i < list.size(); i++) {

							String[] row = list.get(i);

							java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());

							StringBuilder sbformat = new StringBuilder();
							sbformat.append("|%1$-");
							sbformat.append(colLengths[0]);
							sbformat.append("s");

							sbformat.append("|\n");

							formatter.format(sbformat.toString(), (Object[]) row);

							sb.append(formatter.toString());
							if (i == 0)
								sb.append(print(des_head)); // print the head
						}

						// end
						sb.append(print(des_bottom));
						return sb;
					}

					private StringBuilder print(String[] fillChars) {
						StringBuilder sb = new StringBuilder();
						// first column
						sb.append(fillChars[0]);

						// last column
						for (int i = 0; i < colLengths[0] - fillChars[0].length() - fillChars[1].length() + 2; i++) {
							sb.append(fillChars[2]);
						}
						sb.append(fillChars[1]);
						sb.append("\n");
						return sb;
					}

					public boolean isTableEmpty() {
						if (list.size() > 1)
							return false;
						return true;
					}
				}
				Util_tLogRow_1 util_tLogRow_1 = new Util_tLogRow_1();
				util_tLogRow_1.setTableName("tLogRow_1");
				util_tLogRow_1.addRow(new String[] { "token", });
				StringBuilder strBuffer_tLogRow_1 = null;
				int nb_line_tLogRow_1 = 0;
///////////////////////    			

				/**
				 * [tLogRow_1 begin ] stop
				 */

				/**
				 * [tExtractJSONFields_1 begin ] start
				 */

				ok_Hash.put("tExtractJSONFields_1", false);
				start_Hash.put("tExtractJSONFields_1", System.currentTimeMillis());

				currentComponent = "tExtractJSONFields_1";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "row5");

				int tos_count_tExtractJSONFields_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tExtractJSONFields_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tExtractJSONFields_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tExtractJSONFields_1 = new StringBuilder();
							log4jParamters_tExtractJSONFields_1.append("Parameters:");
							log4jParamters_tExtractJSONFields_1.append("READ_BY" + " = " + "JSONPATH");
							log4jParamters_tExtractJSONFields_1.append(" | ");
							log4jParamters_tExtractJSONFields_1.append("JSON_PATH_VERSION" + " = " + "2_1_0");
							log4jParamters_tExtractJSONFields_1.append(" | ");
							log4jParamters_tExtractJSONFields_1.append("JSONFIELD" + " = " + "string");
							log4jParamters_tExtractJSONFields_1.append(" | ");
							log4jParamters_tExtractJSONFields_1.append("JSON_LOOP_QUERY" + " = " + "\"$\"");
							log4jParamters_tExtractJSONFields_1.append(" | ");
							log4jParamters_tExtractJSONFields_1.append("MAPPING_4_JSONPATH" + " = " + "[{QUERY="
									+ ("\"token\"") + ", SCHEMA_COLUMN=" + ("token") + "}]");
							log4jParamters_tExtractJSONFields_1.append(" | ");
							log4jParamters_tExtractJSONFields_1.append("DIE_ON_ERROR" + " = " + "false");
							log4jParamters_tExtractJSONFields_1.append(" | ");
							log4jParamters_tExtractJSONFields_1.append("USE_LOOP_AS_ROOT" + " = " + "true");
							log4jParamters_tExtractJSONFields_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tExtractJSONFields_1 - " + (log4jParamters_tExtractJSONFields_1));
						}
					}
					new BytesLimit65535_tExtractJSONFields_1().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tExtractJSONFields_1", "tExtractJSONFields_1", "tExtractJSONFields");
					talendJobLogProcess(globalMap);
				}

				int nb_line_tExtractJSONFields_1 = 0;
				String jsonStr_tExtractJSONFields_1 = "";

				class JsonPathCache_tExtractJSONFields_1 {
					final java.util.Map<String, com.jayway.jsonpath.JsonPath> jsonPathString2compiledJsonPath = new java.util.HashMap<String, com.jayway.jsonpath.JsonPath>();

					public com.jayway.jsonpath.JsonPath getCompiledJsonPath(String jsonPath) {
						if (jsonPathString2compiledJsonPath.containsKey(jsonPath)) {
							return jsonPathString2compiledJsonPath.get(jsonPath);
						} else {
							com.jayway.jsonpath.JsonPath compiledLoopPath = com.jayway.jsonpath.JsonPath
									.compile(jsonPath);
							jsonPathString2compiledJsonPath.put(jsonPath, compiledLoopPath);
							return compiledLoopPath;
						}
					}
				}

				JsonPathCache_tExtractJSONFields_1 jsonPathCache_tExtractJSONFields_1 = new JsonPathCache_tExtractJSONFields_1();

				/**
				 * [tExtractJSONFields_1 begin ] stop
				 */

				/**
				 * [tLogRow_2 begin ] start
				 */

				ok_Hash.put("tLogRow_2", false);
				start_Hash.put("tLogRow_2", System.currentTimeMillis());

				currentComponent = "tLogRow_2";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "row3");

				int tos_count_tLogRow_2 = 0;

				if (log.isDebugEnabled())
					log.debug("tLogRow_2 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tLogRow_2 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tLogRow_2 = new StringBuilder();
							log4jParamters_tLogRow_2.append("Parameters:");
							log4jParamters_tLogRow_2.append("BASIC_MODE" + " = " + "true");
							log4jParamters_tLogRow_2.append(" | ");
							log4jParamters_tLogRow_2.append("TABLE_PRINT" + " = " + "false");
							log4jParamters_tLogRow_2.append(" | ");
							log4jParamters_tLogRow_2.append("VERTICAL" + " = " + "false");
							log4jParamters_tLogRow_2.append(" | ");
							log4jParamters_tLogRow_2.append("FIELDSEPARATOR" + " = " + "\"|\"");
							log4jParamters_tLogRow_2.append(" | ");
							log4jParamters_tLogRow_2.append("PRINT_HEADER" + " = " + "false");
							log4jParamters_tLogRow_2.append(" | ");
							log4jParamters_tLogRow_2.append("PRINT_UNIQUE_NAME" + " = " + "false");
							log4jParamters_tLogRow_2.append(" | ");
							log4jParamters_tLogRow_2.append("PRINT_COLNAMES" + " = " + "false");
							log4jParamters_tLogRow_2.append(" | ");
							log4jParamters_tLogRow_2.append("USE_FIXED_LENGTH" + " = " + "false");
							log4jParamters_tLogRow_2.append(" | ");
							log4jParamters_tLogRow_2.append("PRINT_CONTENT_WITH_LOG4J" + " = " + "true");
							log4jParamters_tLogRow_2.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tLogRow_2 - " + (log4jParamters_tLogRow_2));
						}
					}
					new BytesLimit65535_tLogRow_2().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tLogRow_2", "tLogRow_2", "tLogRow");
					talendJobLogProcess(globalMap);
				}

				///////////////////////

				final String OUTPUT_FIELD_SEPARATOR_tLogRow_2 = "|";
				java.io.PrintStream consoleOut_tLogRow_2 = null;

				StringBuilder strBuffer_tLogRow_2 = null;
				int nb_line_tLogRow_2 = 0;
///////////////////////    			

				/**
				 * [tLogRow_2 begin ] stop
				 */

				/**
				 * [tRESTClient_2 begin ] start
				 */

				ok_Hash.put("tRESTClient_2", false);
				start_Hash.put("tRESTClient_2", System.currentTimeMillis());

				currentComponent = "tRESTClient_2";

				runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, 0, 0, "row2");

				int tos_count_tRESTClient_2 = 0;

				if (enableLogStash) {
					talendJobLog.addCM("tRESTClient_2", "tRESTClient_2", "tRESTClient");
					talendJobLogProcess(globalMap);
				}

				/**
				 * [tRESTClient_2 begin ] stop
				 */

				/**
				 * [tWriteJSONField_1_In begin ] start
				 */

				ok_Hash.put("tWriteJSONField_1_In", false);
				start_Hash.put("tWriteJSONField_1_In", System.currentTimeMillis());

				currentVirtualComponent = "tWriteJSONField_1";

				currentComponent = "tWriteJSONField_1_In";

				int tos_count_tWriteJSONField_1_In = 0;

				if (log.isDebugEnabled())
					log.debug("tWriteJSONField_1_In - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tWriteJSONField_1_In {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tWriteJSONField_1_In = new StringBuilder();
							log4jParamters_tWriteJSONField_1_In.append("Parameters:");
							log4jParamters_tWriteJSONField_1_In.append("JSONFIELD" + " = " + "string");
							log4jParamters_tWriteJSONField_1_In.append(" | ");
							log4jParamters_tWriteJSONField_1_In.append("DESTINATION" + " = " + "tWriteJSONField_1");
							log4jParamters_tWriteJSONField_1_In.append(" | ");
							log4jParamters_tWriteJSONField_1_In.append("REMOVE_ROOT" + " = " + "true");
							log4jParamters_tWriteJSONField_1_In.append(" | ");
							log4jParamters_tWriteJSONField_1_In.append("GROUPBYS" + " = " + "[]");
							log4jParamters_tWriteJSONField_1_In.append(" | ");
							log4jParamters_tWriteJSONField_1_In.append("QUOTE_ALL_VALUES" + " = " + "false");
							log4jParamters_tWriteJSONField_1_In.append(" | ");
							log4jParamters_tWriteJSONField_1_In.append("ALLOW_EMPTY_STRINGS" + " = " + "false");
							log4jParamters_tWriteJSONField_1_In.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tWriteJSONField_1_In - " + (log4jParamters_tWriteJSONField_1_In));
						}
					}
					new BytesLimit65535_tWriteJSONField_1_In().limitLog4jByte();
				}
				if (enableLogStash) {
					talendJobLog.addCM("tWriteJSONField_1_In", "tWriteJSONField_1_In", "tWriteJSONFieldIn");
					talendJobLogProcess(globalMap);
				}

				int nb_line_tWriteJSONField_1_In = 0;
				net.sf.json.xml.XMLSerializer xmlSerializer_tWriteJSONField_1_In = new net.sf.json.xml.XMLSerializer();
				xmlSerializer_tWriteJSONField_1_In.clearNamespaces();
				xmlSerializer_tWriteJSONField_1_In.setSkipNamespaces(true);
				xmlSerializer_tWriteJSONField_1_In.setForceTopLevelObject(false);
				xmlSerializer_tWriteJSONField_1_In.setUseEmptyStrings(false);

				java.util.Queue<row2Struct> queue_tWriteJSONField_1_In = (java.util.Queue<row2Struct>) globalMap
						.get("queue_tWriteJSONField_1_In");

				String readFinishMarkWithPipeId_tWriteJSONField_1_In = "tWriteJSONField_1_In_FINISH"
						+ (queue_tWriteJSONField_1_In == null ? "" : queue_tWriteJSONField_1_In.hashCode());
				String str_tWriteJSONField_1_In = null;

				while (!globalMap.containsKey(readFinishMarkWithPipeId_tWriteJSONField_1_In)
						|| !queue_tWriteJSONField_1_In.isEmpty()) {
					if (!queue_tWriteJSONField_1_In.isEmpty()) {

						/**
						 * [tWriteJSONField_1_In begin ] stop
						 */

						/**
						 * [tWriteJSONField_1_In main ] start
						 */

						currentVirtualComponent = "tWriteJSONField_1";

						currentComponent = "tWriteJSONField_1_In";

						row2Struct result_tWriteJSONField_1_In = queue_tWriteJSONField_1_In.poll();
						str_tWriteJSONField_1_In = result_tWriteJSONField_1_In.string;
						// Convert XML to JSON
						net.sf.json.JsonStandard jsonStandard_tWriteJSONField_1_In = net.sf.json.JsonStandard.LEGACY;
						xmlSerializer_tWriteJSONField_1_In.setJsonStandard(jsonStandard_tWriteJSONField_1_In);
						net.sf.json.JSON json_tWriteJSONField_1_In = xmlSerializer_tWriteJSONField_1_In
								.read(str_tWriteJSONField_1_In);
						row2.string = net.sf.json.util.JSONUtils.jsonToStandardizedString(json_tWriteJSONField_1_In,
								jsonStandard_tWriteJSONField_1_In);

						nb_line_tWriteJSONField_1_In++;

						tos_count_tWriteJSONField_1_In++;

						/**
						 * [tWriteJSONField_1_In main ] stop
						 */

						/**
						 * [tWriteJSONField_1_In process_data_begin ] start
						 */

						currentVirtualComponent = "tWriteJSONField_1";

						currentComponent = "tWriteJSONField_1_In";

						/**
						 * [tWriteJSONField_1_In process_data_begin ] stop
						 */

						/**
						 * [tRESTClient_2 main ] start
						 */

						currentComponent = "tRESTClient_2";

						if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

								, "row2", "tWriteJSONField_1_In", "tWriteJSONField_1_In", "tWriteJSONFieldIn",
								"tRESTClient_2", "tRESTClient_2", "tRESTClient"

						)) {
							talendJobLogProcess(globalMap);
						}

						if (log.isTraceEnabled()) {
							log.trace("row2 - " + (row2 == null ? "" : row2.toLogString()));
						}

						row3 = null;

// expected response body
						javax.ws.rs.core.Response responseDoc_tRESTClient_2 = null;

						try {
							// request body
							org.dom4j.Document requestDoc_tRESTClient_2 = null;
							String requestString_tRESTClient_2 = null;
							requestString_tRESTClient_2 = row2.string;

							Object requestBody_tRESTClient_2 = requestDoc_tRESTClient_2 != null
									? requestDoc_tRESTClient_2
									: requestString_tRESTClient_2;

							// resposne class name
							Class<?> responseClass_tRESTClient_2 = String.class;

							// create web client instance
							org.apache.cxf.jaxrs.client.JAXRSClientFactoryBean factoryBean_tRESTClient_2 = new org.apache.cxf.jaxrs.client.JAXRSClientFactoryBean();

							boolean inOSGi = routines.system.BundleUtils.inOSGi();

							final java.util.List<org.apache.cxf.feature.Feature> features_tRESTClient_2 = new java.util.ArrayList<org.apache.cxf.feature.Feature>();

							String url = context.url_onestock;
							// {baseUri}tRESTClient
							factoryBean_tRESTClient_2.setServiceName(new javax.xml.namespace.QName(url, "tRESTClient"));
							factoryBean_tRESTClient_2.setAddress(url);

							factoryBean_tRESTClient_2.setFeatures(features_tRESTClient_2);

							java.util.List<Object> providers_tRESTClient_2 = new java.util.ArrayList<Object>();
							providers_tRESTClient_2.add(new org.apache.cxf.jaxrs.provider.dom4j.DOM4JProvider() {
								// workaround for https://jira.talendforge.org/browse/TESB-7276
								public org.dom4j.Document readFrom(Class<org.dom4j.Document> cls,
										java.lang.reflect.Type type, java.lang.annotation.Annotation[] anns,
										javax.ws.rs.core.MediaType mt,
										javax.ws.rs.core.MultivaluedMap<String, String> headers, java.io.InputStream is)
										throws IOException, javax.ws.rs.WebApplicationException {
									String contentLength = headers.getFirst("Content-Length");
									if (!org.apache.cxf.common.util.StringUtils.isEmpty(contentLength)
											&& Integer.valueOf(contentLength) <= 0) {
										try {
											return org.dom4j.DocumentHelper.parseText("<root/>");
										} catch (org.dom4j.DocumentException e_tRESTClient_2) {
											e_tRESTClient_2.printStackTrace();
										}
										return null;
									}
									return super.readFrom(cls, type, anns, mt, headers, is);
								}
							});
							org.apache.cxf.jaxrs.provider.json.JSONProvider jsonProvider_tRESTClient_2 = new org.apache.cxf.jaxrs.provider.json.JSONProvider();
							jsonProvider_tRESTClient_2.setIgnoreNamespaces(true);
							jsonProvider_tRESTClient_2.setAttributesToElements(true);

							jsonProvider_tRESTClient_2.setSerializeAsArray(true);
							List<String> arrayKeys = new java.util.ArrayList<String>(
									java.util.Arrays.asList("".split(" ")));
							jsonProvider_tRESTClient_2.setArrayKeys(arrayKeys);

							jsonProvider_tRESTClient_2.setSupportUnwrapped(true);
							jsonProvider_tRESTClient_2.setWrapperName("root");

							jsonProvider_tRESTClient_2.setDropRootElement(false);
							jsonProvider_tRESTClient_2.setConvertTypesToStrings(false);
							providers_tRESTClient_2.add(jsonProvider_tRESTClient_2);
							factoryBean_tRESTClient_2.setProviders(providers_tRESTClient_2);
							factoryBean_tRESTClient_2.setTransportId("http://cxf.apache.org/transports/http");

							boolean use_auth_tRESTClient_2 = false;

							org.apache.cxf.jaxrs.client.WebClient webClient_tRESTClient_2 = factoryBean_tRESTClient_2
									.createWebClient();

							// set request path
							webClient_tRESTClient_2.path("");

							// set connection properties
							org.apache.cxf.jaxrs.client.ClientConfiguration clientConfig_tRESTClient_2 = org.apache.cxf.jaxrs.client.WebClient
									.getConfig(webClient_tRESTClient_2);
							org.apache.cxf.transport.http.auth.HttpAuthSupplier httpAuthSupplerHttpConduit = null;
							org.apache.cxf.transport.http.HTTPConduit conduit_tRESTClient_2 = clientConfig_tRESTClient_2
									.getHttpConduit();

							if (clientConfig_tRESTClient_2.getEndpoint() != null) {
								org.apache.cxf.service.model.EndpointInfo endpointInfo_tRESTClient_2 = clientConfig_tRESTClient_2
										.getEndpoint().getEndpointInfo();
								if (endpointInfo_tRESTClient_2 != null) {
									endpointInfo_tRESTClient_2.setProperty("enable.webclient.operation.reporting",
											true);
								}
							}

							if (!inOSGi) {
								conduit_tRESTClient_2.getClient().setReceiveTimeout((long) (60 * 1000L));
								conduit_tRESTClient_2.getClient().setConnectionTimeout((long) (30 * 1000L));
								boolean use_proxy_tRESTClient_2 = false;

							}

							// set Content-Type
							webClient_tRESTClient_2.type("application/json");

							// set Accept-Type
							webClient_tRESTClient_2.accept("application/json");

							// set optional query and header properties if any

							if (use_auth_tRESTClient_2 && "OAUTH2_BEARER".equals("BASIC")) {
								// set oAuth2 bearer token
								org.apache.cxf.rs.security.oauth2.client.BearerAuthSupplier authSupplier = new org.apache.cxf.rs.security.oauth2.client.BearerAuthSupplier();
								authSupplier.setAccessToken("");
								conduit_tRESTClient_2.setAuthSupplier(authSupplier);
							}

							// if FORM request then capture query parameters into Form, otherwise set them
							// as queries

							try {
								// start send request

								responseDoc_tRESTClient_2 = webClient_tRESTClient_2.post(requestBody_tRESTClient_2);

								int webClientResponseStatus_tRESTClient_2 = webClient_tRESTClient_2.getResponse()
										.getStatus();
								if (webClientResponseStatus_tRESTClient_2 >= 300) {
									throw new javax.ws.rs.WebApplicationException(
											webClient_tRESTClient_2.getResponse());
								}

								if (row3 == null) {
									row3 = new row3Struct();
								}

								row3.statusCode = webClientResponseStatus_tRESTClient_2;
								row3.string = "";

								Object responseObj_tRESTClient_2 = null;
								if (responseDoc_tRESTClient_2 != null && responseDoc_tRESTClient_2.hasEntity()) {
									responseObj_tRESTClient_2 = responseDoc_tRESTClient_2
											.readEntity(responseClass_tRESTClient_2);

									if (responseObj_tRESTClient_2 != null) {
										if (responseClass_tRESTClient_2 == String.class
												&& responseObj_tRESTClient_2 instanceof String) {
											row3.string = (String) responseObj_tRESTClient_2;
										} else {
											routines.system.Document responseTalendDoc_tRESTClient_2 = null;
											if (null != responseObj_tRESTClient_2) {
												responseTalendDoc_tRESTClient_2 = new routines.system.Document();
												if (responseObj_tRESTClient_2 instanceof org.dom4j.Document) {
													responseTalendDoc_tRESTClient_2.setDocument(
															(org.dom4j.Document) responseObj_tRESTClient_2);
												}
											}
											row3.body = responseTalendDoc_tRESTClient_2;
										}
									}
								}

								globalMap.put("tRESTClient_2_HEADERS",
										webClient_tRESTClient_2.getResponse().getHeaders());
								globalMap.put("tRESTClient_2_COOKIES",
										webClient_tRESTClient_2.getResponse().getCookies());

							} catch (javax.ws.rs.WebApplicationException ex_tRESTClient_2) {
								globalMap.put("tRESTClient_2_ERROR_MESSAGE", ex_tRESTClient_2.getMessage());

								throw ex_tRESTClient_2;

							}

						} catch (Exception e_tRESTClient_2) {
							globalMap.put("tRESTClient_2_ERROR_MESSAGE", e_tRESTClient_2.getMessage());

							throw new TalendException(e_tRESTClient_2, currentComponent, globalMap);

						}

						tos_count_tRESTClient_2++;

						/**
						 * [tRESTClient_2 main ] stop
						 */

						/**
						 * [tRESTClient_2 process_data_begin ] start
						 */

						currentComponent = "tRESTClient_2";

						/**
						 * [tRESTClient_2 process_data_begin ] stop
						 */
// Start of branch "row3"
						if (row3 != null) {

							/**
							 * [tLogRow_2 main ] start
							 */

							currentComponent = "tLogRow_2";

							if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

									, "row3", "tRESTClient_2", "tRESTClient_2", "tRESTClient", "tLogRow_2", "tLogRow_2",
									"tLogRow"

							)) {
								talendJobLogProcess(globalMap);
							}

							if (log.isTraceEnabled()) {
								log.trace("row3 - " + (row3 == null ? "" : row3.toLogString()));
							}

///////////////////////		

							strBuffer_tLogRow_2 = new StringBuilder();

							if (row3.statusCode != null) { //

								strBuffer_tLogRow_2.append(String.valueOf(row3.statusCode));

							} //

							strBuffer_tLogRow_2.append("|");

							if (row3.body != null) { //

								strBuffer_tLogRow_2.append(String.valueOf(row3.body));

							} //

							strBuffer_tLogRow_2.append("|");

							if (row3.string != null) { //

								strBuffer_tLogRow_2.append(String.valueOf(row3.string));

							} //

							if (globalMap.get("tLogRow_CONSOLE") != null) {
								consoleOut_tLogRow_2 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
							} else {
								consoleOut_tLogRow_2 = new java.io.PrintStream(
										new java.io.BufferedOutputStream(System.out));
								globalMap.put("tLogRow_CONSOLE", consoleOut_tLogRow_2);
							}
							log.info("tLogRow_2 - Content of row " + (nb_line_tLogRow_2 + 1) + ": "
									+ strBuffer_tLogRow_2.toString());
							consoleOut_tLogRow_2.println(strBuffer_tLogRow_2.toString());
							consoleOut_tLogRow_2.flush();
							nb_line_tLogRow_2++;
//////

//////                    

///////////////////////    			

							row5 = row3;

							tos_count_tLogRow_2++;

							/**
							 * [tLogRow_2 main ] stop
							 */

							/**
							 * [tLogRow_2 process_data_begin ] start
							 */

							currentComponent = "tLogRow_2";

							/**
							 * [tLogRow_2 process_data_begin ] stop
							 */

							/**
							 * [tExtractJSONFields_1 main ] start
							 */

							currentComponent = "tExtractJSONFields_1";

							if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

									, "row5", "tLogRow_2", "tLogRow_2", "tLogRow", "tExtractJSONFields_1",
									"tExtractJSONFields_1", "tExtractJSONFields"

							)) {
								talendJobLogProcess(globalMap);
							}

							if (log.isTraceEnabled()) {
								log.trace("row5 - " + (row5 == null ? "" : row5.toLogString()));
							}

							if (row5.string != null) {// C_01
								jsonStr_tExtractJSONFields_1 = row5.string.toString();

								row4 = null;

								String loopPath_tExtractJSONFields_1 = "$";
								java.util.List<Object> resultset_tExtractJSONFields_1 = new java.util.ArrayList<Object>();

								boolean isStructError_tExtractJSONFields_1 = true;
								com.jayway.jsonpath.ReadContext document_tExtractJSONFields_1 = null;
								try {
									document_tExtractJSONFields_1 = com.jayway.jsonpath.JsonPath
											.parse(jsonStr_tExtractJSONFields_1);
									com.jayway.jsonpath.JsonPath compiledLoopPath_tExtractJSONFields_1 = jsonPathCache_tExtractJSONFields_1
											.getCompiledJsonPath(loopPath_tExtractJSONFields_1);
									Object result_tExtractJSONFields_1 = document_tExtractJSONFields_1.read(
											compiledLoopPath_tExtractJSONFields_1, net.minidev.json.JSONObject.class);
									if (result_tExtractJSONFields_1 instanceof net.minidev.json.JSONArray) {
										resultset_tExtractJSONFields_1 = (net.minidev.json.JSONArray) result_tExtractJSONFields_1;
									} else {
										resultset_tExtractJSONFields_1.add(result_tExtractJSONFields_1);
									}

									isStructError_tExtractJSONFields_1 = false;
								} catch (java.lang.Exception ex_tExtractJSONFields_1) {
									globalMap.put("tExtractJSONFields_1_ERROR_MESSAGE",
											ex_tExtractJSONFields_1.getMessage());
									log.error("tExtractJSONFields_1 - " + ex_tExtractJSONFields_1.getMessage());
									System.err.println(ex_tExtractJSONFields_1.getMessage());
								}

								String jsonPath_tExtractJSONFields_1 = null;
								com.jayway.jsonpath.JsonPath compiledJsonPath_tExtractJSONFields_1 = null;

								Object value_tExtractJSONFields_1 = null;

								Object root_tExtractJSONFields_1 = null;
								for (int i_tExtractJSONFields_1 = 0; isStructError_tExtractJSONFields_1
										|| (i_tExtractJSONFields_1 < resultset_tExtractJSONFields_1
												.size()); i_tExtractJSONFields_1++) {
									if (!isStructError_tExtractJSONFields_1) {
										Object row_tExtractJSONFields_1 = resultset_tExtractJSONFields_1
												.get(i_tExtractJSONFields_1);
										row4 = null;
										row4 = new row4Struct();
										nb_line_tExtractJSONFields_1++;
										try {
											jsonPath_tExtractJSONFields_1 = "token";
											compiledJsonPath_tExtractJSONFields_1 = jsonPathCache_tExtractJSONFields_1
													.getCompiledJsonPath(jsonPath_tExtractJSONFields_1);

											try {

												value_tExtractJSONFields_1 = compiledJsonPath_tExtractJSONFields_1
														.read(row_tExtractJSONFields_1);

												row4.token = value_tExtractJSONFields_1 == null ?

														null

														: value_tExtractJSONFields_1.toString();
											} catch (com.jayway.jsonpath.PathNotFoundException e_tExtractJSONFields_1) {
												globalMap.put("tExtractJSONFields_1_ERROR_MESSAGE",
														e_tExtractJSONFields_1.getMessage());
												row4.token =

														null

												;
											}
										} catch (java.lang.Exception ex_tExtractJSONFields_1) {
											globalMap.put("tExtractJSONFields_1_ERROR_MESSAGE",
													ex_tExtractJSONFields_1.getMessage());
											log.error("tExtractJSONFields_1 - " + ex_tExtractJSONFields_1.getMessage());
											System.err.println(ex_tExtractJSONFields_1.getMessage());
											row4 = null;
										}

									}

									isStructError_tExtractJSONFields_1 = false;

									log.debug("tExtractJSONFields_1 - Extracting the record "
											+ nb_line_tExtractJSONFields_1 + ".");
//}

									tos_count_tExtractJSONFields_1++;

									/**
									 * [tExtractJSONFields_1 main ] stop
									 */

									/**
									 * [tExtractJSONFields_1 process_data_begin ] start
									 */

									currentComponent = "tExtractJSONFields_1";

									/**
									 * [tExtractJSONFields_1 process_data_begin ] stop
									 */
// Start of branch "row4"
									if (row4 != null) {

										/**
										 * [tLogRow_1 main ] start
										 */

										currentComponent = "tLogRow_1";

										if (runStat.update(execStat, enableLogStash, iterateId, 1, 1

												, "row4", "tExtractJSONFields_1", "tExtractJSONFields_1",
												"tExtractJSONFields", "tLogRow_1", "tLogRow_1", "tLogRow"

										)) {
											talendJobLogProcess(globalMap);
										}

										if (log.isTraceEnabled()) {
											log.trace("row4 - " + (row4 == null ? "" : row4.toLogString()));
										}

///////////////////////		

										String[] row_tLogRow_1 = new String[1];

										if (row4.token != null) { //
											row_tLogRow_1[0] = String.valueOf(row4.token);

										} //

										util_tLogRow_1.addRow(row_tLogRow_1);
										nb_line_tLogRow_1++;
										log.info("tLogRow_1 - Content of row " + nb_line_tLogRow_1 + ": "
												+ TalendString.unionString("|", row_tLogRow_1));
//////

//////                    

///////////////////////    			

										tos_count_tLogRow_1++;

										/**
										 * [tLogRow_1 main ] stop
										 */

										/**
										 * [tLogRow_1 process_data_begin ] start
										 */

										currentComponent = "tLogRow_1";

										/**
										 * [tLogRow_1 process_data_begin ] stop
										 */

										/**
										 * [tLogRow_1 process_data_end ] start
										 */

										currentComponent = "tLogRow_1";

										/**
										 * [tLogRow_1 process_data_end ] stop
										 */

									} // End of branch "row4"

									// end for
								}

							} // C_01

							/**
							 * [tExtractJSONFields_1 process_data_end ] start
							 */

							currentComponent = "tExtractJSONFields_1";

							/**
							 * [tExtractJSONFields_1 process_data_end ] stop
							 */

							/**
							 * [tLogRow_2 process_data_end ] start
							 */

							currentComponent = "tLogRow_2";

							/**
							 * [tLogRow_2 process_data_end ] stop
							 */

						} // End of branch "row3"

						/**
						 * [tRESTClient_2 process_data_end ] start
						 */

						currentComponent = "tRESTClient_2";

						/**
						 * [tRESTClient_2 process_data_end ] stop
						 */

						/**
						 * [tWriteJSONField_1_In process_data_end ] start
						 */

						currentVirtualComponent = "tWriteJSONField_1";

						currentComponent = "tWriteJSONField_1_In";

						/**
						 * [tWriteJSONField_1_In process_data_end ] stop
						 */

						/**
						 * [tWriteJSONField_1_In end ] start
						 */

						currentVirtualComponent = "tWriteJSONField_1";

						currentComponent = "tWriteJSONField_1_In";

					}
				}

				String readFinishWithExceptionMarkWithPipeId_tWriteJSONField_1_In = "tWriteJSONField_1_In_FINISH_WITH_EXCEPTION"
						+ (queue_tWriteJSONField_1_In == null ? "" : queue_tWriteJSONField_1_In.hashCode());
				if (globalMap.containsKey(readFinishWithExceptionMarkWithPipeId_tWriteJSONField_1_In)) {
					if (!(globalMap instanceof java.util.concurrent.ConcurrentHashMap)) {
						globalMap.put(readFinishWithExceptionMarkWithPipeId_tWriteJSONField_1_In, null);// syn
					}
					globalMap.remove(readFinishWithExceptionMarkWithPipeId_tWriteJSONField_1_In);
					return;
				}
				globalMap.remove("queue_tWriteJSONField_1_In");

				if (!(globalMap instanceof java.util.concurrent.ConcurrentHashMap)) {
					globalMap.put(readFinishMarkWithPipeId_tWriteJSONField_1_In, null);// syn
				}
				globalMap.remove(readFinishMarkWithPipeId_tWriteJSONField_1_In);

				globalMap.put("tWriteJSONField_1_NB_LINE", nb_line_tWriteJSONField_1_In);
				log.debug("tWriteJSONField_1_In - Processed records count: " + nb_line_tWriteJSONField_1_In + " .");

				if (log.isDebugEnabled())
					log.debug("tWriteJSONField_1_In - " + ("Done."));

				ok_Hash.put("tWriteJSONField_1_In", true);
				end_Hash.put("tWriteJSONField_1_In", System.currentTimeMillis());

				/**
				 * [tWriteJSONField_1_In end ] stop
				 */

				/**
				 * [tRESTClient_2 end ] start
				 */

				currentComponent = "tRESTClient_2";

				if (globalMap.get("tRESTClient_2_NB_LINE") == null) {
					globalMap.put("tRESTClient_2_NB_LINE", 1);
				}

// [tRESTCliend_end]
				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "row2", 2, 0,
						"tWriteJSONField_1_In", "tWriteJSONField_1_In", "tWriteJSONFieldIn", "tRESTClient_2",
						"tRESTClient_2", "tRESTClient", "output")) {
					talendJobLogProcess(globalMap);
				}

				ok_Hash.put("tRESTClient_2", true);
				end_Hash.put("tRESTClient_2", System.currentTimeMillis());

				/**
				 * [tRESTClient_2 end ] stop
				 */

				/**
				 * [tLogRow_2 end ] start
				 */

				currentComponent = "tLogRow_2";

//////
//////
				globalMap.put("tLogRow_2_NB_LINE", nb_line_tLogRow_2);
				if (log.isInfoEnabled())
					log.info("tLogRow_2 - " + ("Printed row count: ") + (nb_line_tLogRow_2) + ("."));

///////////////////////    			

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "row3", 2, 0,
						"tRESTClient_2", "tRESTClient_2", "tRESTClient", "tLogRow_2", "tLogRow_2", "tLogRow",
						"output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("tLogRow_2 - " + ("Done."));

				ok_Hash.put("tLogRow_2", true);
				end_Hash.put("tLogRow_2", System.currentTimeMillis());

				/**
				 * [tLogRow_2 end ] stop
				 */

				/**
				 * [tExtractJSONFields_1 end ] start
				 */

				currentComponent = "tExtractJSONFields_1";

				globalMap.put("tExtractJSONFields_1_NB_LINE", nb_line_tExtractJSONFields_1);
				log.debug("tExtractJSONFields_1 - Extracted records count: " + nb_line_tExtractJSONFields_1 + " .");

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "row5", 2, 0,
						"tLogRow_2", "tLogRow_2", "tLogRow", "tExtractJSONFields_1", "tExtractJSONFields_1",
						"tExtractJSONFields", "output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("tExtractJSONFields_1 - " + ("Done."));

				ok_Hash.put("tExtractJSONFields_1", true);
				end_Hash.put("tExtractJSONFields_1", System.currentTimeMillis());

				/**
				 * [tExtractJSONFields_1 end ] stop
				 */

				/**
				 * [tLogRow_1 end ] start
				 */

				currentComponent = "tLogRow_1";

//////

				java.io.PrintStream consoleOut_tLogRow_1 = null;
				if (globalMap.get("tLogRow_CONSOLE") != null) {
					consoleOut_tLogRow_1 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
				} else {
					consoleOut_tLogRow_1 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
					globalMap.put("tLogRow_CONSOLE", consoleOut_tLogRow_1);
				}

				consoleOut_tLogRow_1.println(util_tLogRow_1.format().toString());
				consoleOut_tLogRow_1.flush();
//////
				globalMap.put("tLogRow_1_NB_LINE", nb_line_tLogRow_1);
				if (log.isInfoEnabled())
					log.info("tLogRow_1 - " + ("Printed row count: ") + (nb_line_tLogRow_1) + ("."));

///////////////////////    			

				if (runStat.updateStatAndLog(execStat, enableLogStash, resourceMap, iterateId, "row4", 2, 0,
						"tExtractJSONFields_1", "tExtractJSONFields_1", "tExtractJSONFields", "tLogRow_1", "tLogRow_1",
						"tLogRow", "output")) {
					talendJobLogProcess(globalMap);
				}

				if (log.isDebugEnabled())
					log.debug("tLogRow_1 - " + ("Done."));

				ok_Hash.put("tLogRow_1", true);
				end_Hash.put("tLogRow_1", System.currentTimeMillis());

				/**
				 * [tLogRow_1 end ] stop
				 */

			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			te.setVirtualComponentName(currentVirtualComponent);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tWriteJSONField_1_In finally ] start
				 */

				currentVirtualComponent = "tWriteJSONField_1";

				currentComponent = "tWriteJSONField_1_In";

				/**
				 * [tWriteJSONField_1_In finally ] stop
				 */

				/**
				 * [tRESTClient_2 finally ] start
				 */

				currentComponent = "tRESTClient_2";

				/**
				 * [tRESTClient_2 finally ] stop
				 */

				/**
				 * [tLogRow_2 finally ] start
				 */

				currentComponent = "tLogRow_2";

				/**
				 * [tLogRow_2 finally ] stop
				 */

				/**
				 * [tExtractJSONFields_1 finally ] start
				 */

				currentComponent = "tExtractJSONFields_1";

				/**
				 * [tExtractJSONFields_1 finally ] stop
				 */

				/**
				 * [tLogRow_1 finally ] start
				 */

				currentComponent = "tLogRow_1";

				/**
				 * [tLogRow_1 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tWriteJSONField_1_In_SUBPROCESS_STATE", 1);
	}

	public void talendJobLogProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
		globalMap.put("talendJobLog_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [talendJobLog begin ] start
				 */

				ok_Hash.put("talendJobLog", false);
				start_Hash.put("talendJobLog", System.currentTimeMillis());

				currentComponent = "talendJobLog";

				int tos_count_talendJobLog = 0;

				for (JobStructureCatcherUtils.JobStructureCatcherMessage jcm : talendJobLog.getMessages()) {
					org.talend.job.audit.JobContextBuilder builder_talendJobLog = org.talend.job.audit.JobContextBuilder
							.create().jobName(jcm.job_name).jobId(jcm.job_id).jobVersion(jcm.job_version)
							.custom("process_id", jcm.pid).custom("thread_id", jcm.tid).custom("pid", pid)
							.custom("father_pid", fatherPid).custom("root_pid", rootPid);
					org.talend.logging.audit.Context log_context_talendJobLog = null;

					if (jcm.log_type == JobStructureCatcherUtils.LogType.PERFORMANCE) {
						long timeMS = jcm.end_time - jcm.start_time;
						String duration = String.valueOf(timeMS);

						log_context_talendJobLog = builder_talendJobLog.sourceId(jcm.sourceId)
								.sourceLabel(jcm.sourceLabel).sourceConnectorType(jcm.sourceComponentName)
								.targetId(jcm.targetId).targetLabel(jcm.targetLabel)
								.targetConnectorType(jcm.targetComponentName).connectionName(jcm.current_connector)
								.rows(jcm.row_count).duration(duration).build();
						auditLogger_talendJobLog.flowExecution(log_context_talendJobLog);
					} else if (jcm.log_type == JobStructureCatcherUtils.LogType.JOBSTART) {
						log_context_talendJobLog = builder_talendJobLog.timestamp(jcm.moment).build();
						auditLogger_talendJobLog.jobstart(log_context_talendJobLog);
					} else if (jcm.log_type == JobStructureCatcherUtils.LogType.JOBEND) {
						long timeMS = jcm.end_time - jcm.start_time;
						String duration = String.valueOf(timeMS);

						log_context_talendJobLog = builder_talendJobLog.timestamp(jcm.moment).duration(duration)
								.status(jcm.status).build();
						auditLogger_talendJobLog.jobstop(log_context_talendJobLog);
					} else if (jcm.log_type == JobStructureCatcherUtils.LogType.RUNCOMPONENT) {
						log_context_talendJobLog = builder_talendJobLog.timestamp(jcm.moment)
								.connectorType(jcm.component_name).connectorId(jcm.component_id)
								.connectorLabel(jcm.component_label).build();
						auditLogger_talendJobLog.runcomponent(log_context_talendJobLog);
					} else if (jcm.log_type == JobStructureCatcherUtils.LogType.FLOWINPUT) {// log current component
																							// input line
						long timeMS = jcm.end_time - jcm.start_time;
						String duration = String.valueOf(timeMS);

						log_context_talendJobLog = builder_talendJobLog.connectorType(jcm.component_name)
								.connectorId(jcm.component_id).connectorLabel(jcm.component_label)
								.connectionName(jcm.current_connector).connectionType(jcm.current_connector_type)
								.rows(jcm.total_row_number).duration(duration).build();
						auditLogger_talendJobLog.flowInput(log_context_talendJobLog);
					} else if (jcm.log_type == JobStructureCatcherUtils.LogType.FLOWOUTPUT) {// log current component
																								// output/reject line
						long timeMS = jcm.end_time - jcm.start_time;
						String duration = String.valueOf(timeMS);

						log_context_talendJobLog = builder_talendJobLog.connectorType(jcm.component_name)
								.connectorId(jcm.component_id).connectorLabel(jcm.component_label)
								.connectionName(jcm.current_connector).connectionType(jcm.current_connector_type)
								.rows(jcm.total_row_number).duration(duration).build();
						auditLogger_talendJobLog.flowOutput(log_context_talendJobLog);
					}

				}

				/**
				 * [talendJobLog begin ] stop
				 */

				/**
				 * [talendJobLog main ] start
				 */

				currentComponent = "talendJobLog";

				tos_count_talendJobLog++;

				/**
				 * [talendJobLog main ] stop
				 */

				/**
				 * [talendJobLog process_data_begin ] start
				 */

				currentComponent = "talendJobLog";

				/**
				 * [talendJobLog process_data_begin ] stop
				 */

				/**
				 * [talendJobLog process_data_end ] start
				 */

				currentComponent = "talendJobLog";

				/**
				 * [talendJobLog process_data_end ] stop
				 */

				/**
				 * [talendJobLog end ] start
				 */

				currentComponent = "talendJobLog";

				ok_Hash.put("talendJobLog", true);
				end_Hash.put("talendJobLog", System.currentTimeMillis());

				/**
				 * [talendJobLog end ] stop
				 */
			} // end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent, globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [talendJobLog finally ] start
				 */

				currentComponent = "talendJobLog";

				/**
				 * [talendJobLog finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("talendJobLog_SUBPROCESS_STATE", 1);
	}

	public String resuming_logs_dir_path = null;
	public String resuming_checkpoint_path = null;
	public String parent_part_launcher = null;
	private String resumeEntryMethodName = null;
	private boolean globalResumeTicket = false;

	public boolean watch = false;
	// portStats is null, it means don't execute the statistics
	public Integer portStats = null;
	public int portTraces = 4334;
	public String clientHost;
	public String defaultClientHost = "localhost";
	public String contextStr = "Default";
	public boolean isDefaultContext = true;
	public String pid = "0";
	public String rootPid = null;
	public String fatherPid = null;
	public String fatherNode = null;
	public long startTime = 0;
	public boolean isChildJob = false;
	public String log4jLevel = "";

	private boolean enableLogStash;

	private boolean execStat = true;

	private ThreadLocal<java.util.Map<String, String>> threadLocal = new ThreadLocal<java.util.Map<String, String>>() {
		protected java.util.Map<String, String> initialValue() {
			java.util.Map<String, String> threadRunResultMap = new java.util.HashMap<String, String>();
			threadRunResultMap.put("errorCode", null);
			threadRunResultMap.put("status", "");
			return threadRunResultMap;
		};
	};

	protected PropertiesWithType context_param = new PropertiesWithType();
	public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();

	public String status = "";

	public static void main(String[] args) {
		final token tokenClass = new token();

		int exitCode = tokenClass.runJobInTOS(args);
		if (exitCode == 0) {
			log.info("TalendJob: 'token' - Done.");
		}

		System.exit(exitCode);
	}

	public String[][] runJob(String[] args) {

		int exitCode = runJobInTOS(args);
		String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };

		return bufferValue;
	}

	public boolean hastBufferOutputComponent() {
		boolean hastBufferOutput = false;

		return hastBufferOutput;
	}

	public int runJobInTOS(String[] args) {
		// reset status
		status = "";

		String lastStr = "";
		for (String arg : args) {
			if (arg.equalsIgnoreCase("--context_param")) {
				lastStr = arg;
			} else if (lastStr.equals("")) {
				evalParam(arg);
			} else {
				evalParam(lastStr + " " + arg);
				lastStr = "";
			}
		}
		enableLogStash = "true".equalsIgnoreCase(System.getProperty("audit.enabled"));

		if (!"".equals(log4jLevel)) {

			if ("trace".equalsIgnoreCase(log4jLevel)) {
				org.apache.logging.log4j.core.config.Configurator.setLevel(log.getName(),
						org.apache.logging.log4j.Level.TRACE);
			} else if ("debug".equalsIgnoreCase(log4jLevel)) {
				org.apache.logging.log4j.core.config.Configurator.setLevel(log.getName(),
						org.apache.logging.log4j.Level.DEBUG);
			} else if ("info".equalsIgnoreCase(log4jLevel)) {
				org.apache.logging.log4j.core.config.Configurator.setLevel(log.getName(),
						org.apache.logging.log4j.Level.INFO);
			} else if ("warn".equalsIgnoreCase(log4jLevel)) {
				org.apache.logging.log4j.core.config.Configurator.setLevel(log.getName(),
						org.apache.logging.log4j.Level.WARN);
			} else if ("error".equalsIgnoreCase(log4jLevel)) {
				org.apache.logging.log4j.core.config.Configurator.setLevel(log.getName(),
						org.apache.logging.log4j.Level.ERROR);
			} else if ("fatal".equalsIgnoreCase(log4jLevel)) {
				org.apache.logging.log4j.core.config.Configurator.setLevel(log.getName(),
						org.apache.logging.log4j.Level.FATAL);
			} else if ("off".equalsIgnoreCase(log4jLevel)) {
				org.apache.logging.log4j.core.config.Configurator.setLevel(log.getName(),
						org.apache.logging.log4j.Level.OFF);
			}
			org.apache.logging.log4j.core.config.Configurator
					.setLevel(org.apache.logging.log4j.LogManager.getRootLogger().getName(), log.getLevel());

		}
		log.info("TalendJob: 'token' - Start.");

		if (enableLogStash) {
			java.util.Properties properties_talendJobLog = new java.util.Properties();
			properties_talendJobLog.setProperty("root.logger", "audit");
			properties_talendJobLog.setProperty("encoding", "UTF-8");
			properties_talendJobLog.setProperty("application.name", "Talend Studio");
			properties_talendJobLog.setProperty("service.name", "Talend Studio Job");
			properties_talendJobLog.setProperty("instance.name", "Talend Studio Job Instance");
			properties_talendJobLog.setProperty("propagate.appender.exceptions", "none");
			properties_talendJobLog.setProperty("log.appender", "file");
			properties_talendJobLog.setProperty("appender.file.path", "audit.json");
			properties_talendJobLog.setProperty("appender.file.maxsize", "52428800");
			properties_talendJobLog.setProperty("appender.file.maxbackup", "20");
			properties_talendJobLog.setProperty("host", "false");

			System.getProperties().stringPropertyNames().stream().filter(it -> it.startsWith("audit.logger."))
					.forEach(key -> properties_talendJobLog.setProperty(key.substring("audit.logger.".length()),
							System.getProperty(key)));

			org.apache.logging.log4j.core.config.Configurator
					.setLevel(properties_talendJobLog.getProperty("root.logger"), org.apache.logging.log4j.Level.DEBUG);

			auditLogger_talendJobLog = org.talend.job.audit.JobEventAuditLoggerFactory
					.createJobAuditLogger(properties_talendJobLog);
		}

		if (clientHost == null) {
			clientHost = defaultClientHost;
		}

		if (pid == null || "0".equals(pid)) {
			pid = TalendString.getAsciiRandomString(6);
		}

		if (rootPid == null) {
			rootPid = pid;
		}
		if (fatherPid == null) {
			fatherPid = pid;
		} else {
			isChildJob = true;
		}

		if (portStats != null) {
			// portStats = -1; //for testing
			if (portStats < 0 || portStats > 65535) {
				// issue:10869, the portStats is invalid, so this client socket can't open
				System.err.println("The statistics socket port " + portStats + " is invalid.");
				execStat = false;
			}
		} else {
			execStat = false;
		}
		boolean inOSGi = routines.system.BundleUtils.inOSGi();

		if (inOSGi) {
			java.util.Dictionary<String, Object> jobProperties = routines.system.BundleUtils.getJobProperties(jobName);

			if (jobProperties != null && jobProperties.get("context") != null) {
				contextStr = (String) jobProperties.get("context");
			}
		}

		try {
			// call job/subjob with an existing context, like: --context=production. if
			// without this parameter, there will use the default context instead.
			java.io.InputStream inContext = token.class.getClassLoader()
					.getResourceAsStream("apex_omnicanal/token_0_1/contexts/" + contextStr + ".properties");
			if (inContext == null) {
				inContext = token.class.getClassLoader()
						.getResourceAsStream("config/contexts/" + contextStr + ".properties");
			}
			if (inContext != null) {
				try {
					// defaultProps is in order to keep the original context value
					if (context != null && context.isEmpty()) {
						defaultProps.load(inContext);
						context = new ContextProperties(defaultProps);
					}
				} finally {
					inContext.close();
				}
			} else if (!isDefaultContext) {
				// print info and job continue to run, for case: context_param is not empty.
				System.err.println("Could not find the context " + contextStr);
			}

			if (!context_param.isEmpty()) {
				context.putAll(context_param);
				// set types for params from parentJobs
				for (Object key : context_param.keySet()) {
					String context_key = key.toString();
					String context_type = context_param.getContextType(context_key);
					context.setContextType(context_key, context_type);

				}
			}
			class ContextProcessing {
				private void processContext_0() {
					context.setContextType("authorization", "id_String");
					if (context.getStringValue("authorization") == null) {
						context.authorization = null;
					} else {
						context.authorization = (String) context.getProperty("authorization");
					}
					context.setContextType("password", "id_Password");
					if (context.getStringValue("password") == null) {
						context.password = null;
					} else {
						String pwd_password_value = context.getProperty("password");
						context.password = null;
						if (pwd_password_value != null) {
							if (context_param.containsKey("password")) {// no need to decrypt if it come from program
																		// argument or parent job runtime
								context.password = pwd_password_value;
							} else if (!pwd_password_value.isEmpty()) {
								try {
									context.password = routines.system.PasswordEncryptUtil
											.decryptPassword(pwd_password_value);
									context.put("password", context.password);
								} catch (java.lang.RuntimeException e) {
									// do nothing
								}
							}
						}
					}
					context.setContextType("password_akeneo", "id_String");
					if (context.getStringValue("password_akeneo") == null) {
						context.password_akeneo = null;
					} else {
						context.password_akeneo = (String) context.getProperty("password_akeneo");
					}
					context.setContextType("site_id", "id_String");
					if (context.getStringValue("site_id") == null) {
						context.site_id = null;
					} else {
						context.site_id = (String) context.getProperty("site_id");
					}
					context.setContextType("url_akeneo", "id_String");
					if (context.getStringValue("url_akeneo") == null) {
						context.url_akeneo = null;
					} else {
						context.url_akeneo = (String) context.getProperty("url_akeneo");
					}
					context.setContextType("url_onestock", "id_String");
					if (context.getStringValue("url_onestock") == null) {
						context.url_onestock = null;
					} else {
						context.url_onestock = (String) context.getProperty("url_onestock");
					}
					context.setContextType("user_id", "id_String");
					if (context.getStringValue("user_id") == null) {
						context.user_id = null;
					} else {
						context.user_id = (String) context.getProperty("user_id");
					}
					context.setContextType("username", "id_String");
					if (context.getStringValue("username") == null) {
						context.username = null;
					} else {
						context.username = (String) context.getProperty("username");
					}
				}

				public void processAllContext() {
					processContext_0();
				}
			}

			new ContextProcessing().processAllContext();
		} catch (java.io.IOException ie) {
			System.err.println("Could not load context " + contextStr);
			ie.printStackTrace();
		}

		// get context value from parent directly
		if (parentContextMap != null && !parentContextMap.isEmpty()) {
			if (parentContextMap.containsKey("authorization")) {
				context.authorization = (String) parentContextMap.get("authorization");
			}
			if (parentContextMap.containsKey("password")) {
				context.password = (java.lang.String) parentContextMap.get("password");
			}
			if (parentContextMap.containsKey("password_akeneo")) {
				context.password_akeneo = (String) parentContextMap.get("password_akeneo");
			}
			if (parentContextMap.containsKey("site_id")) {
				context.site_id = (String) parentContextMap.get("site_id");
			}
			if (parentContextMap.containsKey("url_akeneo")) {
				context.url_akeneo = (String) parentContextMap.get("url_akeneo");
			}
			if (parentContextMap.containsKey("url_onestock")) {
				context.url_onestock = (String) parentContextMap.get("url_onestock");
			}
			if (parentContextMap.containsKey("user_id")) {
				context.user_id = (String) parentContextMap.get("user_id");
			}
			if (parentContextMap.containsKey("username")) {
				context.username = (String) parentContextMap.get("username");
			}
		}

		// Resume: init the resumeUtil
		resumeEntryMethodName = ResumeUtil.getResumeEntryMethodName(resuming_checkpoint_path);
		resumeUtil = new ResumeUtil(resuming_logs_dir_path, isChildJob, rootPid);
		resumeUtil.initCommonInfo(pid, rootPid, fatherPid, projectName, jobName, contextStr, jobVersion);

		List<String> parametersToEncrypt = new java.util.ArrayList<String>();
		parametersToEncrypt.add("password");
		// Resume: jobStart
		resumeUtil.addLog("JOB_STARTED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "",
				"", "", "", "", resumeUtil.convertToJsonText(context, parametersToEncrypt));

		if (execStat) {
			try {
				runStat.openSocket(!isChildJob);
				runStat.setAllPID(rootPid, fatherPid, pid, jobName);
				runStat.startThreadStat(clientHost, portStats);
				runStat.updateStatOnJob(RunStat.JOBSTART, fatherNode);
			} catch (java.io.IOException ioException) {
				ioException.printStackTrace();
			}
		}

		java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap = new java.util.concurrent.ConcurrentHashMap<Object, Object>();
		globalMap.put("concurrentHashMap", concurrentHashMap);

		long startUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		long endUsedMemory = 0;
		long end = 0;

		startTime = System.currentTimeMillis();

		this.globalResumeTicket = true;// to run tPreJob

		if (enableLogStash) {
			talendJobLog.addJobStartMessage();
			try {
				talendJobLogProcess(globalMap);
			} catch (java.lang.Exception e) {
				e.printStackTrace();
			}
		}

		this.globalResumeTicket = false;// to run others jobs

		try {
			errorCode = null;
			tFixedFlowInput_1Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}
		} catch (TalendException e_tFixedFlowInput_1) {
			globalMap.put("tFixedFlowInput_1_SUBPROCESS_STATE", -1);

			e_tFixedFlowInput_1.printStackTrace();

		}

		this.globalResumeTicket = true;// to run tPostJob

		end = System.currentTimeMillis();

		if (watch) {
			System.out.println((end - startTime) + " milliseconds");
		}

		endUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		if (false) {
			System.out.println((endUsedMemory - startUsedMemory) + " bytes memory increase when running : token");
		}
		if (enableLogStash) {
			talendJobLog.addJobEndMessage(startTime, end, status);
			try {
				talendJobLogProcess(globalMap);
			} catch (java.lang.Exception e) {
				e.printStackTrace();
			}
		}

		if (execStat) {
			runStat.updateStatOnJob(RunStat.JOBEND, fatherNode);
			runStat.stopThreadStat();
		}
		int returnCode = 0;

		if (errorCode == null) {
			returnCode = status != null && status.equals("failure") ? 1 : 0;
		} else {
			returnCode = errorCode.intValue();
		}
		resumeUtil.addLog("JOB_ENDED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "",
				"" + returnCode, "", "", "");

		return returnCode;

	}

	// only for OSGi env
	public void destroy() {

	}

	private java.util.Map<String, Object> getSharedConnections4REST() {
		java.util.Map<String, Object> connections = new java.util.HashMap<String, Object>();

		return connections;
	}

	private void evalParam(String arg) {
		if (arg.startsWith("--resuming_logs_dir_path")) {
			resuming_logs_dir_path = arg.substring(25);
		} else if (arg.startsWith("--resuming_checkpoint_path")) {
			resuming_checkpoint_path = arg.substring(27);
		} else if (arg.startsWith("--parent_part_launcher")) {
			parent_part_launcher = arg.substring(23);
		} else if (arg.startsWith("--watch")) {
			watch = true;
		} else if (arg.startsWith("--stat_port=")) {
			String portStatsStr = arg.substring(12);
			if (portStatsStr != null && !portStatsStr.equals("null")) {
				portStats = Integer.parseInt(portStatsStr);
			}
		} else if (arg.startsWith("--trace_port=")) {
			portTraces = Integer.parseInt(arg.substring(13));
		} else if (arg.startsWith("--client_host=")) {
			clientHost = arg.substring(14);
		} else if (arg.startsWith("--context=")) {
			contextStr = arg.substring(10);
			isDefaultContext = false;
		} else if (arg.startsWith("--father_pid=")) {
			fatherPid = arg.substring(13);
		} else if (arg.startsWith("--root_pid=")) {
			rootPid = arg.substring(11);
		} else if (arg.startsWith("--father_node=")) {
			fatherNode = arg.substring(14);
		} else if (arg.startsWith("--pid=")) {
			pid = arg.substring(6);
		} else if (arg.startsWith("--context_type")) {
			String keyValue = arg.substring(15);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.setContextType(keyValue.substring(0, index),
							replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.setContextType(keyValue.substring(0, index), keyValue.substring(index + 1));
				}

			}

		} else if (arg.startsWith("--context_param")) {
			String keyValue = arg.substring(16);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.put(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.put(keyValue.substring(0, index), keyValue.substring(index + 1));
				}
			}
		} else if (arg.startsWith("--log4jLevel=")) {
			log4jLevel = arg.substring(13);
		} else if (arg.startsWith("--audit.enabled") && arg.contains("=")) {// for trunjob call
			final int equal = arg.indexOf('=');
			final String key = arg.substring("--".length(), equal);
			System.setProperty(key, arg.substring(equal + 1));
		}
	}

	private static final String NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY = "<TALEND_NULL>";

	private final String[][] escapeChars = { { "\\\\", "\\" }, { "\\n", "\n" }, { "\\'", "\'" }, { "\\r", "\r" },
			{ "\\f", "\f" }, { "\\b", "\b" }, { "\\t", "\t" } };

	private String replaceEscapeChars(String keyValue) {

		if (keyValue == null || ("").equals(keyValue.trim())) {
			return keyValue;
		}

		StringBuilder result = new StringBuilder();
		int currIndex = 0;
		while (currIndex < keyValue.length()) {
			int index = -1;
			// judege if the left string includes escape chars
			for (String[] strArray : escapeChars) {
				index = keyValue.indexOf(strArray[0], currIndex);
				if (index >= 0) {

					result.append(keyValue.substring(currIndex, index + strArray[0].length()).replace(strArray[0],
							strArray[1]));
					currIndex = index + strArray[0].length();
					break;
				}
			}
			// if the left string doesn't include escape chars, append the left into the
			// result
			if (index < 0) {
				result.append(keyValue.substring(currIndex));
				currIndex = currIndex + keyValue.length();
			}
		}

		return result.toString();
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public String getStatus() {
		return status;
	}

	ResumeUtil resumeUtil = null;
}
/************************************************************************************************
 * 134017 characters generated by Talend Data Integration on the 23 mai 2024 à
 * 14:59:45 CEST
 ************************************************************************************************/